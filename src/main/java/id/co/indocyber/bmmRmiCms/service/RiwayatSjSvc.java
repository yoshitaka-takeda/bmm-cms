package id.co.indocyber.bmmRmiCms.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import id.co.indocyber.bmmRmiCms.dto.MobGoodsReceiveAttachmentDto;
import id.co.indocyber.bmmRmiCms.dto.MobGoodsReceiveDto;
import id.co.indocyber.bmmRmiCms.dto.MobSjDto;
import id.co.indocyber.bmmRmiCms.dto.ReportSjDto;

public interface RiwayatSjSvc {

	public Map<String, Object> findAll(String search, 
			Date startDate, Date endDate, int pageNo, int perPage, String orderBy, String direction);
	public Map<String, Object> findByCustomer(String custCode, 
			String search, Date startDate, Date endDate, int pageNo, int perPage, String orderBy, String direction);
	public Map<String, Object> findByTransportir(String transportirCode, 
			String search, Date startDate, Date endDate, int pageNo, int perPage, String orderBy, String direction);
	
	public MobGoodsReceiveDto getReceiveBySj(Integer sjNo);
	public List<MobGoodsReceiveAttachmentDto> getListAttachmentBySj(Integer sjNo);
	public Boolean cekTransit(Integer sptNo);
<<<<<<< HEAD
	public List<ReportSjDto>getReportSj(List<String> listNoSj);
=======
	public List<ReportSjDto> getReportSj(List<String> listNoSj);
>>>>>>> branch 'master' of https://gitlab.com/ari.haryanto/rmi-spt-sj.git
	
}
