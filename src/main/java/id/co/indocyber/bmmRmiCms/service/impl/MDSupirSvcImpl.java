package id.co.indocyber.bmmRmiCms.service.impl;

import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dao.MAddressTypeDao;
import id.co.indocyber.bmmRmiCms.dao.MEducationTypeDao;
import id.co.indocyber.bmmRmiCms.dao.MGenderDao;
import id.co.indocyber.bmmRmiCms.dao.MMaritalStatusDao;
import id.co.indocyber.bmmRmiCms.dao.MReligionDao;
import id.co.indocyber.bmmRmiCms.dao.MobDriverDao;
import id.co.indocyber.bmmRmiCms.dao.MobMappingTransportirDriverDao;
import id.co.indocyber.bmmRmiCms.dto.MAddressTypeDto;
import id.co.indocyber.bmmRmiCms.dto.MEducationTypeDto;
import id.co.indocyber.bmmRmiCms.dto.MGenderDto;
import id.co.indocyber.bmmRmiCms.dto.MMaritalStatusDto;
import id.co.indocyber.bmmRmiCms.dto.MReligionDto;
import id.co.indocyber.bmmRmiCms.dto.MobDriverDto;
import id.co.indocyber.bmmRmiCms.dto.MobMappingTransportirDriverDto;
import id.co.indocyber.bmmRmiCms.entity.MAddressType;
import id.co.indocyber.bmmRmiCms.entity.MEducationType;
import id.co.indocyber.bmmRmiCms.entity.MGender;
import id.co.indocyber.bmmRmiCms.entity.MMaritalStatus;
import id.co.indocyber.bmmRmiCms.entity.MReligion;
import id.co.indocyber.bmmRmiCms.entity.MobDriver;
import id.co.indocyber.bmmRmiCms.entity.MobMappingTransportirDriver;
import id.co.indocyber.bmmRmiCms.service.MDSupirSvc;
import id.co.indocyber.bmmRmiCms.tools.StringUtil;
import ma.glasnost.orika.MapperFacade;

@Service("mDSupirSvc")
@Transactional
public class MDSupirSvcImpl extends BaseDaoImpl implements MDSupirSvc {
	
	@Autowired
	private MapperFacade mapperFacade;
	
	@Autowired
	private MobDriverDao mobDriverDao;
	
	@Autowired
	private MGenderDao mGenderDao;
	
	@Autowired
	private MAddressTypeDao mAddressTypeDao;
	
	@Autowired
	private MMaritalStatusDao mMaritalStatusDao;
	
	@Autowired
	private MReligionDao mReligionDao;
	
	@Autowired
	private MEducationTypeDao mEducationTypeDao;
	
	@Autowired
	private MobMappingTransportirDriverDao mobMappingTransportirDriverDao;
	
	@Override
	public Map<String, Object> findByArg(String search, Integer pageNo, int perPage, String colName, boolean sortAsc) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%"); 
		
		
		String sql = "select a.id, a.fullname, a.birthday, a.phone_num, a.address, "
				+ "a.is_active, c.transportir_name, a.modified_date, "
				+ "TIMESTAMPDIFF(YEAR, a.birthday, CURDATE()) as age  "
				+ "from mob_driver a "
				+ "left join mob_mapping_transportir_driver b on a.id = b.driver_id "
				+ "left join mob_transportir c on b.transportir_code = c.code "
				+ "where "
				+ "a.id like ('"+search+"') "
				+ "or upper(a.fullname) like upper('"+search+"') "
				+ "or a.phone_num like '"+search+"' "
				+ "or upper(a.address) like upper('"+search+"') "
				+ "or upper(c.transportir_name) like upper('"+search+"') "
				+ "		or upper(case "
				+ "				when a.is_active is true then 'Aktif' "
				+ "				when a.is_active is false then 'Non Aktif' "
				+ "				end) like upper('"+search+"') "
				+ "or TIMESTAMPDIFF(YEAR, a.birthday, CURDATE()) like ('"+search+"') ";
		
		String sort = "";
		if(sortAsc){
			sort = "asc";
		}else{
			sort = "desc";
		}
		int pageSize = 25;
		Pageable pageable = new PageRequest((pageNo-1)*pageSize, pageSize,
				new Sort(new Sort.Order(Direction.fromString(sort),colName)));
		
		Map<String, Object> mapList = getPaging(sql, pageable);
		

//		List<MobDriver> listObj = mobDriverDao.findAllDataByArg(sql, new PageRequest(pageNo-1, 10, generateSort(colName,sortAsc)));
		@SuppressWarnings("unchecked")
		List<Object[]> listObj = (List<Object[]>) mapList.get("contentData");
		List<MobDriverDto> listDto = new ArrayList<>();
		for(Object[] m : listObj){
			MobDriverDto dto = new MobDriverDto();
			dto.setId((int) m[0]);
			dto.setFullname((String)m[1]);
			dto.setBirthday((Date)m[2]);
			dto.setPhoneNum((String)m[3]);
			dto.setAddress((String)m[4]);
			dto.setIsActive((boolean)m[5]);
			dto.setNamaTransportir((String)m[6]);
			if(dto.getBirthday()!=null){
				dto.setAge(((BigInteger) m[8]).intValue());				
			}else{
				dto.setAge(0);
			}
			listDto.add(dto);
		}
			
		Integer totalSIze = (Integer) mapList.get("totalRecords");
		Map<String, Object> map = new HashMap<>();
		map.put("content", listDto);
		map.put("totalSize", totalSIze);
		return map;
	}

	@Override
	public Integer countAllBySearch(String search) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		return mobDriverDao.countAllData(search).intValue();
	}

	@Override
	public List<MAddressTypeDto> cbListAddress() {
		List<MAddressType> listObj = mAddressTypeDao.findAllActive();
		List<MAddressTypeDto> listDto = new ArrayList<>();
		for(MAddressType a : listObj){
			MAddressTypeDto dto = mapperFacade.map(a, MAddressTypeDto.class);
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public List<String> cbListProvince() {
		List<String> listProv = mobDriverDao.getProvince();
		return listProv;
	}

	@Override
	public List<MGenderDto> cbListGender() {
		List<MGender> listObj = mGenderDao.findAllActive();
		List<MGenderDto> listDto = new ArrayList<>();
		for(MGender g : listObj){
			MGenderDto dto = mapperFacade.map(g, MGenderDto.class);
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public List<MMaritalStatusDto> cbListMarital() {
		List<MMaritalStatus> listObj = mMaritalStatusDao.findAllActive();
		List<MMaritalStatusDto> listDto = new ArrayList<>();
		for(MMaritalStatus m : listObj){
			MMaritalStatusDto dto = mapperFacade.map(m, MMaritalStatusDto.class);
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public List<MReligionDto> cbListReligion() {
		List<MReligion> listObj = mReligionDao.findAllActive();
		List<MReligionDto> listDto = new ArrayList<>();
		for(MReligion r : listObj){
			MReligionDto dto = mapperFacade.map(r, MReligionDto.class);
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public int save(MobDriverDto dto, MobMappingTransportirDriverDto mappingDto) {
		try{
			
			Integer maxId;
			if(dto.getId() == null){
				maxId = mobDriverDao.getMaxId() == null?0:mobDriverDao.getMaxId().intValue();
				maxId=maxId+1;
			}else{
				maxId = dto.getId();
			}
			
			
			
			MobDriver m = mapperFacade.map(dto, MobDriver.class);
			m.setId(maxId);
			mobDriverDao.save(m);
			
			if(mappingDto != null){
				MobMappingTransportirDriver mapping = mapperFacade.map(mappingDto, MobMappingTransportirDriver.class);
				mapping.setCreated(dto.getCreatedBy());
				mapping.setCreatedDate(dto.getCreatedDate());
				mapping.setModified(dto.getModifiedBy());
				mapping.setModifiedDate(dto.getModifiedDate());
				mapping.setDriverId(maxId);
				mobMappingTransportirDriverDao.save(mapping);				
			}
		
			
			return 1;
		}catch(Exception e){
			e.printStackTrace();
			return 0;
		}
		
	}

	@Override
	public List<MEducationTypeDto> cbListEducation() {
		List<MEducationType> listObj = mEducationTypeDao.findAllActive();
		List<MEducationTypeDto> listDto = new ArrayList<>();
		for(MEducationType e : listObj){
			MEducationTypeDto dto = mapperFacade.map(e, MEducationTypeDto.class);
			listDto.add(dto);
		}
		return listDto;
	}
	
	@Override
	public List<MobDriverDto> findAll(Integer pageNo, String colName,
			boolean sortAsc) {
		List<MobDriver> entities = mobDriverDao.findAll(
			new PageRequest(pageNo - 1, 10, generateSort(colName, sortAsc))).getContent();
		List<MobDriverDto> listDto = new ArrayList<>();
				
		for(MobDriver m : entities){
			MobDriverDto dto = mapperFacade.map(m, MobDriverDto.class);
			if(dto.getBirthday() != null){
				dto.setAge(mobDriverDao.getAgeDriver(dto.getBirthday()).intValue());
			}else{
				dto.setAge(0);
			}
			
			//get transportir name
			List<Object[]> mapTransportir = mobMappingTransportirDriverDao.findDataByDriver(m.getId()); 
			if(mapTransportir.size()!=0){
				for(Object[] o : mapTransportir){
					dto.setNamaTransportir((String) o[1]);					
				}
			}
			
			listDto.add(dto);
		}
		return listDto;
	}



	@Override
	public Integer countAll() {
		return ((Long) mobDriverDao.count()).intValue();
	}

	@Override
	public Map<String,Object> findByTransportir(String transportirCode, String search, 
			Integer pageNo, Integer perPage ,String colName, boolean sortAsc) {
//		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
//		List<MobDriver> listObj = mobDriverDao.findByTransportir(transportirCode,search, new PageRequest(pageNo-1, perPage, generateSort(colName,sortAsc)));
//		List<MobDriverDto> listDto = new ArrayList<>();
//		for(MobDriver m : listObj){
//			MobDriverDto dto = mapperFacade.map(m, MobDriverDto.class);
//			if(dto.getBirthday() != null){
//				dto.setAge(mobDriverDao.getAgeDriver(dto.getBirthday()).intValue());
//			}else{
//				dto.setAge(0);
//			}
//			listDto.add(dto);
//		}
//		return listDto;
		
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%"); 
		
		
		String sql = "select a.id, a.fullname, a.birthday, a.phone_num, a.address, "
				+ "a.is_active, c.transportir_name, a.modified_date, "
				+ "TIMESTAMPDIFF(YEAR, a.birthday, CURDATE()) as age  "
				+ "from mob_driver a "
				+ "left join mob_mapping_transportir_driver b on a.id = b.driver_id "
				+ "left join mob_transportir c on b.transportir_code = c.code "
				+ "where "
				+ "a.id = b.driver_id "
				+ "and b.transportir_code = '"+transportirCode+"' "
				+ "and (a.id like ('"+search+"') "
				+ "or upper(a.fullname) like upper('"+search+"') "
				+ "or a.phone_num like '"+search+"' "
				+ "or upper(a.address) like upper('"+search+"') "
				+ "or upper(c.transportir_name) like upper('"+search+"') "
				+ "		or upper(case "
				+ "				when a.is_active is true then 'Aktif' "
				+ "				when a.is_active is false then 'Non Aktif' "
				+ "				end) like upper('"+search+"') "
				+ "or TIMESTAMPDIFF(YEAR, a.birthday, CURDATE()) like ('"+search+"')) ";
		
		String sort = "";
		if(sortAsc){
			sort = "asc";
		}else{
			sort = "desc";
		}
		int pageSize = 25;
		Pageable pageable = new PageRequest((pageNo-1)*pageSize, pageSize,
				new Sort(new Sort.Order(Direction.fromString(sort),colName)));
		
		Map<String, Object> mapList = getPaging(sql, pageable);
		

//		List<MobDriver> listObj = mobDriverDao.findAllDataByArg(sql, new PageRequest(pageNo-1, 10, generateSort(colName,sortAsc)));
		@SuppressWarnings("unchecked")
		List<Object[]> listObj = (List<Object[]>) mapList.get("contentData");
		List<MobDriverDto> listDto = new ArrayList<>();
		for(Object[] m : listObj){
			MobDriverDto dto = new MobDriverDto();
			dto.setId((int) m[0]);
			dto.setFullname((String)m[1]);
			dto.setBirthday((Date)m[2]);
			dto.setPhoneNum((String)m[3]);
			dto.setAddress((String)m[4]);
			dto.setIsActive((boolean)m[5]);
			dto.setNamaTransportir((String)m[6]);
			if(dto.getBirthday()!=null){
				dto.setAge(((BigInteger) m[8]).intValue());				
			}else{
				dto.setAge(0);
			}
			listDto.add(dto);
		}
			
		Integer totalSIze = (Integer) mapList.get("totalRecords");
		Map<String, Object> map = new HashMap<>();
		map.put("content", listDto);
		map.put("totalSize", totalSIze);
		return map;

	}

	@Override
	public Integer countByTransportir(String transportirCode, String search) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		return mobDriverDao.countByTransportir(transportirCode, search).intValue();
	}

	@Override
	public void saveMappingDriver(MobMappingTransportirDriverDto dto) {
		try{
			MobMappingTransportirDriver m = mapperFacade.map(dto, MobMappingTransportirDriver.class);
			mobMappingTransportirDriverDao.save(m);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public Sort generateSort(String columnName, boolean ascending) {
		return new Sort(ascending ? Sort.Direction.ASC : Sort.Direction.DESC,
				columnName);
	}

	@Override
	public MobDriverDto findOne(int id) {
		MobDriver driver = mobDriverDao.findOne(id);
		MobDriverDto driverDto = new MobDriverDto();
		driverDto = mapperFacade.map(driver, MobDriverDto.class);
		return driverDto;
	}

	@Override
	public List<MobDriverDto> findDriverByPhoneNum(String phoneNum) {
		List<MobDriver> list = mobDriverDao.findDriverByPhone(phoneNum);
		List<MobDriverDto> listDto = new ArrayList<>();
		for(MobDriver o : list){
			MobDriverDto dto = new MobDriverDto();
			dto = mapperFacade.map(o, MobDriverDto.class);
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public List<MobDriverDto> findDriverByEmail(String email) {
		List<MobDriver> list = mobDriverDao.findDriverByEmail(email);
		List<MobDriverDto> listDto = new ArrayList<>();
		for(MobDriver o : list){
			MobDriverDto dto = new MobDriverDto();
			dto = mapperFacade.map(o, MobDriverDto.class);
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public List<MobDriverDto> findDriverByNoKtp(String noKtp) {
		List<MobDriver> list = mobDriverDao.findDriverByNoKtp(noKtp);
		List<MobDriverDto> listDto = new ArrayList<>();
		for(MobDriver o : list){
			MobDriverDto dto = new MobDriverDto();
			dto = mapperFacade.map(o, MobDriverDto.class);
			listDto.add(dto);
		}
		return listDto;
	}

}
