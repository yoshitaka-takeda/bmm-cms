package id.co.indocyber.bmmRmiCms.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dao.MobSptDao;
import id.co.indocyber.bmmRmiCms.dto.MobSptDto;
import id.co.indocyber.bmmRmiCms.service.SptBaruSvc;
import id.co.indocyber.bmmRmiCms.tools.DateUtil;
import id.co.indocyber.bmmRmiCms.tools.StringUtil;
import ma.glasnost.orika.MapperFacade;

@Service("sptBaruSvc")
@Transactional
public class SptBaruSvcImpl extends BaseDaoImpl implements SptBaruSvc {
	
	@Autowired
	private MapperFacade mapperFacade;
	
	@Autowired
	private MobSptDao mobSptDao;
	
	@Override
	public List<MobSptDto> findAll(String search, 
			Date startDate, Date endDate, int pageNo, int perPage, String orderBy, String direction) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		if(startDate == null){
			startDate = DateUtil.getMinDate();
		}
		
		if(endDate == null){
			endDate = DateUtil.getMaxDate();
		}

		String tempStartDate = df.format(startDate);
		String tempEndDate = df.format(endDate);
		
		Pageable pageable = new PageRequest(pageNo - 1, perPage,
				new Sort(new Sort.Order(Direction.fromString(direction),orderBy)));
		List<Object[]> listObj = mobSptDao.findAllNew(search, tempStartDate, tempEndDate, pageable);
		List<MobSptDto> listDto = new ArrayList<>();
		for(Object[] o : listObj){
			MobSptDto dto = mapperFacade.map(o[0], MobSptDto.class);
			dto.setStatusText((String) o[1]);
			dto.setDriverName((String) o[2]);
			
			if(dto.getTakeAssignmentLatitude() != null && dto.getTakeAssignmentLongitude() != null){
				dto.setTakeAssignmentLatitudeLongitude(dto.getTakeAssignmentLatitude()+", "+dto.getTakeAssignmentLongitude());
			}else if(dto.getTakeAssignmentLatitude() == null && dto.getTakeAssignmentLongitude() != null){
				dto.setTakeAssignmentLatitudeLongitude(dto.getTakeAssignmentLongitude().toString());
			}else if(dto.getTakeAssignmentLatitude() != null && dto.getTakeAssignmentLongitude() == null){
				dto.setTakeAssignmentLatitudeLongitude(dto.getTakeAssignmentLatitude().toString());
			}else{
				dto.setTakeAssignmentLatitudeLongitude("");
			}
			
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public Integer countAll(String search, Date startDate, Date endDate) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		if(startDate == null){
			startDate = DateUtil.getMinDate();
		}
		
		if(endDate == null){
			endDate = DateUtil.getMaxDate();
		}

		String tempStartDate = df.format(startDate);
		String tempEndDate = df.format(endDate);
		return mobSptDao.contAllNew(search, tempStartDate, tempEndDate).intValue();
	}

	@Override
	public Map<String, Object> findByTransportir(String transportirCode, 
			String search, Date startDate, Date endDate, int pageNo, int perPage, String orderBy, String direction) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		if(startDate == null){
			startDate = DateUtil.getMinDate();
		}
		
		if(endDate == null){
			endDate = DateUtil.getMaxDate();
		}

		String tempStartDate = df.format(startDate);
		String tempEndDate = df.format(endDate);		
		
		Pageable pageable = new PageRequest(pageNo - 1, perPage,
				new Sort(new Sort.Order(Direction.fromString(direction),orderBy)));
		
		String sql = "select a.spt_no, a.spt_date, a.spt_expired_date, "
					+ "		a.sppb_no, a.sppb_key, a.cust_code, "
					+ "		a.cust_name, a.address_ship_to, a.transportir_code, "
					+ "		a.transportir_name, a.address_ship_from, a.item_code, "
					+ "		a.item_name, a.qty_sppb, a.vehicle_no, "
					+ "		a.vehicle_type, a.driver_id, a.phone, "
					+ "		a.qty, a.container_no, a.sj_no, "
					+ "		a.weight_netto, a.weight_bruto, a.ktp_picture, "
					+ "		a.take_assignment_latitude, a.take_assignment_longitude, a.take_assignment_date, "
					+ "		a.take_assignment_by, a.status, a.is_transit, "
					+ "		a.reference_code, a.ship_from, b.fullname, "
					+ "		case "
					+ "			when upper(a.status) = upper('o') then 'Open' "
					+ "			when upper(a.status) = upper('a') then 'Approved' "
					+ "			when upper(a.status) = upper('l') then 'Batal' "
					+ "			when upper(a.status) = upper('i') then 'In Weighbridge' "
					+ "			when upper(a.status) = upper('t') then 'Tapping' "
					+ "			when upper(a.status) = upper('m') then 'MBS' "
					+ "			when upper(a.status) = upper('c') then 'Closed' "
					+ "		end, "
					+ "		c.vehicle_type_name, "
					+ "		a.modified_date"
					+ "	from mob_spt a "
					+ "		join mob_driver b on a.driver_id = b.id "
					+ "		left join m_vehicle_type c on a.vehicle_type = c.id "
					+ "	where a.transportir_code = '"+transportirCode+"' "
					+ "		and upper(a.status) = upper('o') "
					+ " 	and (date_format(a.spt_date, '%Y-%m-%d') >= '"+tempStartDate+"' and date_format(a.spt_expired_date, '%Y-%m-%d') <= '"+tempEndDate+"') "								
					+ "		and (a.spt_no like '"+search+"' "
					+ "			or b.fullname like '"+search+"' "
					+ "			or a.sppb_no like '"+search+"' "
					+ "			or date_format(a.spt_date,'%d-%b-%Y') like '"+search+"' "
					+ "			or date_format(a.spt_expired_date,'%d-%b-%Y') like '"+search+"' "
					+ "			or upper(case "
					+ "				when upper(a.status) = upper('o') then 'Open' "
					+ "				when upper(a.status) = upper('a') then 'Approved' "
					+ "				when upper(a.status) = upper('l') then 'Batal' "
					+ "				when upper(a.status) = upper('i') then 'In Weighbridge' "
					+ "				when upper(a.status) = upper('t') then 'Tapping' "
					+ "				when upper(a.status) = upper('m') then 'MBS' "
					+ "				when upper(a.status) = upper('c') then 'Closed' "
					+ "			   end) like upper('"+search+"') "
					+ "			or upper(case "
					+ "				when a.is_transit = 1 then 'Ya' "
					+ "				when a.is_transit = 0 then 'Tidak'"
					+ "				end) like upper('"+search+"'))";
		
		Map<String, Object> mapList = getPaging(sql, pageable);
		@SuppressWarnings("unchecked")
		List<Object[]> listObj = (List<Object[]>) mapList.get("contentData");
		List<MobSptDto> listDto = new ArrayList<>();
		for(Object[] o : listObj){
			MobSptDto dto = new MobSptDto();
			dto.setSptNo((int) o[0]);
			dto.setSptDate((Date) o[1]);
			dto.setSptExpiredDate((Date) o[2]);
			dto.setSppbNo((int) o[3]);
			dto.setSppbKey((int) o[4]);
			dto.setCustCode((String) o[5]);
			dto.setCustName((String) o[6]);
			dto.setAddressShipTo((String) o[7]);
			dto.setTransportirCode((String) o[8]);
			dto.setTransportirName((String) o[9]);
			dto.setAddressShipFrom((String) o[10]);
			
			dto.setItemCode((String) o[11]);
			dto.setItemName((String) o[12]);
			dto.setQtySppb((BigDecimal) o[13]);
			dto.setVehicleNo((String) o[14]);
			dto.setVehicleType((Integer) o[15]);
			dto.setDriverId((int) o[16]);
			dto.setPhone((String) o[17]);
			dto.setQty((BigDecimal) o[18]);
			dto.setContainerNo((String) o[19]);
			dto.setSjNo((String) o[20]);
			dto.setWeightNetto((BigDecimal) o[21]);
			
			dto.setWeightBruto((BigDecimal) o[22]);
			dto.setKtpPicture((String) o[23]);
			dto.setTakeAssignmentLatitude((BigDecimal) o[24]);
			dto.setTakeAssignmentLongitude((BigDecimal) o[25]);
			dto.setTakeAssignmentDate((Date) o[26]);
			dto.setTakeAssignmentBy((String) o[27]);
//			byte status = (byte) o[28];
//			dto.setStatus((String) o[28]);			
			byte isTransit = (byte) o[29];
			if(isTransit == 1){
				dto.setIsTransit(true);
			}else{
				dto.setIsTransit(false);
			}
			dto.setReferenceCode((String) o[30]);
			dto.setShipFrom((String) o[31]);
			dto.setDriverName((String) o[32]);
			dto.setStatusText((String) o[33]);
			dto.setVehicleTypeText((String) o[34]);
			
			if(dto.getTakeAssignmentLatitude() != null && dto.getTakeAssignmentLongitude() != null){
				dto.setTakeAssignmentLatitudeLongitude(dto.getTakeAssignmentLatitude()+", "+dto.getTakeAssignmentLongitude());
			}else if(dto.getTakeAssignmentLatitude() == null && dto.getTakeAssignmentLongitude() != null){
				dto.setTakeAssignmentLatitudeLongitude(dto.getTakeAssignmentLongitude().toString());
			}else if(dto.getTakeAssignmentLatitude() != null && dto.getTakeAssignmentLongitude() == null){
				dto.setTakeAssignmentLatitudeLongitude(dto.getTakeAssignmentLatitude().toString());
			}else{
				dto.setTakeAssignmentLatitudeLongitude("");
			}			
			
			listDto.add(dto);
		}
	
		Integer totalSIze = (Integer) mapList.get("totalRecords");
		Map<String, Object> map = new HashMap<>();
		map.put("content", listDto);
		map.put("totalSize", totalSIze);
		return map;
	}

	@Override
	public void updateTransit(Boolean isTransit, Integer sptNo) {
		mobSptDao.updateTransit(isTransit, sptNo);		
	}

	@Override
	public Map<String, Object> findByCustomer(String custId, 
			String search, Date startDate, Date endDate, int pageNo, int perPage, String orderBy, String direction) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		if(startDate == null){
			startDate = DateUtil.getMinDate();
		}
		
		if(endDate == null){
			endDate = DateUtil.getMaxDate();
		}

		String tempStartDate = df.format(startDate);
		String tempEndDate = df.format(endDate);		

		Pageable pageable = new PageRequest(pageNo - 1, perPage,
				new Sort(new Sort.Order(Direction.fromString(direction),orderBy)));
		
		String sql = "select a.spt_no, a.spt_date, a.spt_expired_date, "
					+ "		a.sppb_no, a.sppb_key, a.cust_code, "
					+ "		a.cust_name, a.address_ship_to, a.transportir_code, "
					+ "		a.transportir_name, a.address_ship_from, a.item_code, "
					+ "		a.item_name, a.qty_sppb, a.vehicle_no, "
					+ "		a.vehicle_type, a.driver_id, a.phone, "
					+ "		a.qty, a.container_no, a.sj_no, "
					+ "		a.weight_netto, a.weight_bruto, a.ktp_picture, "
					+ "		a.take_assignment_latitude, a.take_assignment_longitude, a.take_assignment_date, "
					+ "		a.take_assignment_by, a.status, a.is_transit, "
					+ "		a.reference_code, a.ship_from, b.fullname, "
					+ "		case "
					+ "			when upper(a.status) = upper('o') then 'Open' "
					+ "			when upper(a.status) = upper('a') then 'Approved' "
					+ "			when upper(a.status) = upper('l') then 'Batal' "
					+ "			when upper(a.status) = upper('i') then 'In Weighbridge' "
					+ "			when upper(a.status) = upper('t') then 'Tapping' "
					+ "			when upper(a.status) = upper('m') then 'MBS' "
					+ "			when upper(a.status) = upper('c') then 'Closed' "
					+ "		end, "
					+ "		c.vehicle_type_name, "
					+ "		a.modified_date "
					+ "	from mob_spt a "
					+ "		join mob_driver b on a.driver_id = b.id "
					+ "		left join m_vehicle_type c on a.vehicle_type = c.id "
					+ "	where a.cust_code = '"+custId+"' "
					+ "		and upper(a.status) = upper('o') "
					+ " 	and (date_format(a.spt_date, '%Y-%m-%d') >= '"+tempStartDate+"' and date_format(a.spt_expired_date, '%Y-%m-%d') <= '"+tempEndDate+"') "													
					+ "		and (a.spt_no like '"+search+"' "
					+ "			or b.fullname like '"+search+"' "
					+ "			or a.sppb_no like '"+search+"' "
					+ "			or date_format(a.spt_date,'%d-%b-%Y') like '"+search+"' "
					+ "			or date_format(a.spt_expired_date,'%d-%b-%Y') like '"+search+"' "
					+ "			or upper(case "
					+ "				when upper(a.status) = upper('o') then 'Open' "
					+ "				when upper(a.status) = upper('a') then 'Approved' "
					+ "				when upper(a.status) = upper('l') then 'Batal' "
					+ "				when upper(a.status) = upper('i') then 'In Weighbridge' "
					+ "				when upper(a.status) = upper('t') then 'Tapping' "
					+ "				when upper(a.status) = upper('m') then 'MBS' "
					+ "				when upper(a.status) = upper('c') then 'Closed' "
					+ "			   end) like upper('"+search+"') "
					+ "			or upper(case "
					+ "				when a.is_transit = 1 then 'Ya' "
					+ "				when a.is_transit = 0 then 'Tidak'"
					+ "				end) like upper('"+search+"'))";
		
		Map<String, Object> mapList = getPaging(sql, pageable);
		@SuppressWarnings("unchecked")
		List<Object[]> listObj = (List<Object[]>) mapList.get("contentData");
		List<MobSptDto> listDto = new ArrayList<>();
		for(Object[] o : listObj){
			MobSptDto dto = new MobSptDto();
			dto.setSptNo((int) o[0]);
			dto.setSptDate((Date) o[1]);
			dto.setSptExpiredDate((Date) o[2]);
			dto.setSppbNo((int) o[3]);
			dto.setSppbKey((int) o[4]);
			dto.setCustCode((String) o[5]);
			dto.setCustName((String) o[6]);
			dto.setAddressShipTo((String) o[7]);
			dto.setTransportirCode((String) o[8]);
			dto.setTransportirName((String) o[9]);
			dto.setAddressShipFrom((String) o[10]);
			
			dto.setItemCode((String) o[11]);
			dto.setItemName((String) o[12]);
			dto.setQtySppb((BigDecimal) o[13]);
			dto.setVehicleNo((String) o[14]);
			dto.setVehicleType((Integer) o[15]);
			dto.setDriverId((int) o[16]);
			dto.setPhone((String) o[17]);
			dto.setQty((BigDecimal) o[18]);
			dto.setContainerNo((String) o[19]);
			dto.setSjNo((String) o[20]);
			dto.setWeightNetto((BigDecimal) o[21]);
			
			dto.setWeightBruto((BigDecimal) o[22]);
			dto.setKtpPicture((String) o[23]);
			dto.setTakeAssignmentLatitude((BigDecimal) o[24]);
			dto.setTakeAssignmentLongitude((BigDecimal) o[25]);
			dto.setTakeAssignmentDate((Date) o[26]);
			dto.setTakeAssignmentBy((String) o[27]);
//			byte status = (byte) o[28];
//			dto.setStatus((String) o[28]);			
			byte isTransit = (byte) o[29];
			if(isTransit == 1){
				dto.setIsTransit(true);
			}else{
				dto.setIsTransit(false);
			}
			dto.setReferenceCode((String) o[30]);
			dto.setShipFrom((String) o[31]);
			dto.setDriverName((String) o[32]);
			dto.setStatusText((String) o[33]);
			dto.setVehicleTypeText((String) o[34]);
			
			if(dto.getTakeAssignmentLatitude() != null && dto.getTakeAssignmentLongitude() != null){
				dto.setTakeAssignmentLatitudeLongitude(dto.getTakeAssignmentLatitude()+", "+dto.getTakeAssignmentLongitude());
			}else if(dto.getTakeAssignmentLatitude() == null && dto.getTakeAssignmentLongitude() != null){
				dto.setTakeAssignmentLatitudeLongitude(dto.getTakeAssignmentLongitude().toString());
			}else if(dto.getTakeAssignmentLatitude() != null && dto.getTakeAssignmentLongitude() == null){
				dto.setTakeAssignmentLatitudeLongitude(dto.getTakeAssignmentLatitude().toString());
			}else{
				dto.setTakeAssignmentLatitudeLongitude("");
			}			
			
			listDto.add(dto);
		}
	
		Integer totalSIze = (Integer) mapList.get("totalRecords");
		Map<String, Object> map = new HashMap<>();
		map.put("content", listDto);
		map.put("totalSize", totalSIze);
		return map;
	}

}
