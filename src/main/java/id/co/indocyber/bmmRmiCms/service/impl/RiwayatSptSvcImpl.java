package id.co.indocyber.bmmRmiCms.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dao.MobSptDao;
import id.co.indocyber.bmmRmiCms.dto.MobSptDto;
import id.co.indocyber.bmmRmiCms.service.RiwayatSptSvc;
import id.co.indocyber.bmmRmiCms.tools.DateUtil;
import id.co.indocyber.bmmRmiCms.tools.StringUtil;
import ma.glasnost.orika.MapperFacade;

@Service("riwayatSptSvc")
@Transactional
public class RiwayatSptSvcImpl implements RiwayatSptSvc {

	@Autowired
	private MapperFacade mapperFacade;
	
	@Autowired
	private MobSptDao mobSptDao;
	
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");	
	
	@Override
	public List<MobSptDto> findAll(String search, 
		Date startDate, Date endDate, int pageNo, int perPage, String orderBy, String direction) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		if(startDate == null){
			startDate = DateUtil.getMinDate();
		}
		
		if(endDate == null){
			endDate = DateUtil.getMaxDate();
		}

		String tempStartDate = df.format(startDate);
		String tempEndDate = df.format(endDate);
		
		Pageable pageable = new PageRequest(pageNo - 1, perPage,
				new Sort(new Sort.Order(Direction.fromString(direction),orderBy)));
		List<Object[]> listObj = mobSptDao.findAllHistory(search, tempStartDate, tempEndDate, pageable);
		List<MobSptDto> listDto = new ArrayList<>();
		for(Object[] o : listObj){
			MobSptDto dto = mapperFacade.map(o[0], MobSptDto.class);
			dto.setStatusText((String) o[1]);
			dto.setDriverName((String) o[2]);
			listDto.add(dto);
		}		
		return listDto;
	}

	@Override
	public Integer countAll(String search, Date startDate, Date endDate) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		if(startDate == null){
			startDate = DateUtil.getMinDate();
		}
		
		if(endDate == null){
			endDate = DateUtil.getMaxDate();
		}

		String tempStartDate = df.format(startDate);
		String tempEndDate = df.format(endDate);
		return mobSptDao.countAllHistory(search, tempStartDate, tempEndDate).intValue();
	}

	@Override
	public Map<String, Object> findByTransportir(String transportirCode, String search, 
			Date startDate, Date endDate, int pageNo, int perPage, String orderBy, String direction) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		if(startDate == null){
			startDate = DateUtil.getMinDate();
		}
		
		if(endDate == null){
			endDate = DateUtil.getMaxDate();
		}

		String tempStartDate = df.format(startDate);
		String tempEndDate = df.format(endDate);
		Pageable pageable = new PageRequest(pageNo - 1, perPage,
				new Sort(new Sort.Order(Direction.fromString(direction),orderBy)));
		List<Object[]> listObj = mobSptDao.findAllHistoryByTransportir(transportirCode, search, tempStartDate, tempEndDate ,pageable);
		List<MobSptDto> listDto = new ArrayList<>();
		for(Object[] o : listObj){
			MobSptDto dto = mapperFacade.map(o[0], MobSptDto.class);
			if(dto.getStatus() != null){
				if(dto.getStatus().equalsIgnoreCase("o")){
					dto.setStatusText("Open");
				}else if(dto.getStatus().equalsIgnoreCase("a")){
					dto.setStatusText("Approved");
				}else if(dto.getStatus().equalsIgnoreCase("l")){
					dto.setStatusText("Batal");
				}else if(dto.getStatus().equalsIgnoreCase("i")){
					dto.setStatusText("In Weighbridge");
				}else if(dto.getStatus().equalsIgnoreCase("t")){
					dto.setStatusText("Tapping");
				}else if(dto.getStatus().equalsIgnoreCase("m")){
					dto.setStatusText("MBS");
				}else if(dto.getStatus().equalsIgnoreCase("c")){
					dto.setStatusText("Closed");
				}else if(dto.getStatus().equalsIgnoreCase("i")){
					dto.setStatusText("Selesai supir 1");
				}
			}
			//dto.setStatusText((String) o[1]);
			dto.setDriverName((String) o[2]);
			listDto.add(dto);
		}		
		
		Integer totalSize = mobSptDao.countAllHistoryByTransportir(transportirCode, search, tempStartDate, tempEndDate).intValue();
		Map<String, Object> map = new HashMap<>();
		map.put("content", listDto);
		map.put("totalSize", totalSize);
		return map;
	}

	@Override
	public Map<String, Object> findByCustomer(String customerCode, 
			String search, Date startDate, Date endDate, int pageNo, int perPage, String orderBy, String direction) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		if(startDate == null){
			startDate = DateUtil.getMinDate();
		}
		
		if(endDate == null){
			endDate = DateUtil.getMaxDate();
		}

		String tempStartDate = df.format(startDate);
		String tempEndDate = df.format(endDate);

		Pageable pageable = new PageRequest(pageNo - 1, perPage,
				new Sort(new Sort.Order(Direction.fromString(direction),orderBy)));
		List<Object[]> listObj = mobSptDao.findAllHistoryByCustomer(customerCode, search, tempStartDate, tempEndDate ,pageable);
		List<MobSptDto> listDto = new ArrayList<>();
		for(Object[] o : listObj){
			MobSptDto dto = mapperFacade.map(o[0], MobSptDto.class);
			dto.setStatusText((String) o[1]);
			dto.setDriverName((String) o[2]);
			listDto.add(dto);
		}		
		
		Integer totalSize = mobSptDao.countAllHistoryByCustomer(customerCode, search).intValue();
		Map<String, Object> map = new HashMap<>();
		map.put("content", listDto);
		map.put("totalSize", totalSize);
		return map;
	}
	
}
