package id.co.indocyber.bmmRmiCms.service;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;

import java.util.List;

public interface MMenuRoleSvc {
	public List<MMenuRoleDto> getAccessPrivs(Integer roleId, String module);

	public void save(List<MMenuRoleDto> rolePrivs);
}
