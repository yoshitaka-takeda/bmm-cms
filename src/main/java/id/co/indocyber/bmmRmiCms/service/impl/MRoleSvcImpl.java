package id.co.indocyber.bmmRmiCms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dao.MRoleDao;
import id.co.indocyber.bmmRmiCms.entity.MRole;
import id.co.indocyber.bmmRmiCms.service.MRoleSvc;

@Service("mRoleSvc")
@Transactional
public class MRoleSvcImpl extends SortGenerator implements MRoleSvc{
	@Autowired
	MRoleDao mRoleDao;
	
	@Override
	public List<MRole> findAll() {
		return mRoleDao.findAll();
	}
	
	@Override
	public List<MRole> findAll(Integer pageNo, String colName, boolean sortAsc) {
		return new ArrayList<>(mRoleDao.findAll(
				new PageRequest(pageNo-1, 10, generateSort(colName, sortAsc))).getContent());
	}
	
	@Override
	public MRole findOne(Integer idRole){
		if(idRole==null) return null;
		return mRoleDao.findOne(idRole);
	}
	
	@Override
	public boolean existsName(Integer id, String roleName){
		MRole find = findOne(id);
		if(find!=null && find.getRoleName()!=null){
			if(find.getRoleName().trim().equals(roleName)){
				return false;
			}
			else return mRoleDao.findByName(roleName).size()!=0;
		}
		else{
			return mRoleDao.findByName(roleName).size()!=0;
		}
	}

	@Override
	public Integer countAll() {
		return ((Long)mRoleDao.count()).intValue();
	}

	@Override
	public void disable(Integer id) {
		MRole role = findOne(id);
		role.setIsActive(false);
		mRoleDao.save(role);
	}
	
	@Override
	public List<MRole> findByArg(String arg, Integer pageNo, String colName, boolean sortAsc){
		return mRoleDao.findByArg(arg, new PageRequest(pageNo-1, 10, generateSort(colName, sortAsc)));
	}
	
	@Override
	public Integer countByArg(String arg){
		return mRoleDao.countByArg(arg).intValue();
	}

	@Override
	public MRole save(MRole editRole) {
		editRole.setIsActive(editRole.getIsActive()==null?false:editRole.getIsActive());
		editRole.setIsAdmin(editRole.getIsAdmin()==null?false:editRole.getIsAdmin());
		return mRoleDao.save(editRole);
	}
}
