package id.co.indocyber.bmmRmiCms.service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.zkoss.image.AImage;

import id.co.indocyber.bmmRmiCms.dao.MGlobalImageDao;
import id.co.indocyber.bmmRmiCms.dao.MobGoodsReceiveAttachmentDao;
import id.co.indocyber.bmmRmiCms.dao.MobGoodsReceiveDao;
import id.co.indocyber.bmmRmiCms.dao.MobSjDao;
import id.co.indocyber.bmmRmiCms.dao.MobSptDao;
import id.co.indocyber.bmmRmiCms.dto.MobGoodsReceiveAttachmentDto;
import id.co.indocyber.bmmRmiCms.dto.MobGoodsReceiveDto;
import id.co.indocyber.bmmRmiCms.dto.MobSjDto;
import id.co.indocyber.bmmRmiCms.dto.ReportSjDto;
import id.co.indocyber.bmmRmiCms.entity.MGlobalImage;
import id.co.indocyber.bmmRmiCms.entity.MobGoodsReceive;
import id.co.indocyber.bmmRmiCms.entity.MobGoodsReceiveAttachment;
import id.co.indocyber.bmmRmiCms.service.RiwayatSjSvc;
import id.co.indocyber.bmmRmiCms.tools.DateUtil;
import id.co.indocyber.bmmRmiCms.tools.StringUtil;
import ma.glasnost.orika.MapperFacade;

@Service("riwayatSjSvc")
@Transactional
public class RiwayatSjSvcImpl extends BaseDaoImpl implements RiwayatSjSvc {
	
	@Autowired
	private MapperFacade mapperFacade;
	
	@Autowired
	private MobSjDao mobSjDao;
	
	@Autowired
	private MobSptDao mobSptDao; 
	
	@Autowired
	private MGlobalImageDao mGlobalImageDao;
	
	@Autowired
	private MobGoodsReceiveDao mobGoodsReceiveDao;
	
	@Autowired
	private MobGoodsReceiveAttachmentDao mobGoodsReceiveAttachmentDao;
	
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");	
	
	@Override
	public Map<String, Object> findAll(String search, 
			Date startDate, Date endDate, int pageNo, int perPage, String orderBy, String direction) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		if(startDate == null){
			startDate = DateUtil.getMinDate();
		}
		
		if(endDate == null){
			endDate = DateUtil.getMaxDate();
		}

		String tempStartDate = df.format(startDate);
		String tempEndDate = df.format(endDate);		
		
		Pageable pageable = new PageRequest((pageNo-1)*perPage, perPage,
				new Sort(new Sort.Order(Direction.fromString(direction),orderBy)));
		String sql = "select a.sj_no, a.sj_date, a.spt_no, "
				+ "		a.spm_no, a.segel1, a.segel2,"
				+ "		a.segel3, a.segel4, a.segel5,"
				+ "		a.segel6, a.segel7, a.segel8, "
				+ "		a.cust_code, a.cust_name, a.address_ship_to, "
				+ "		a.transportir_code, a.transportir_name, a.address_ship_from, "
				+ "		a.item_code, a.item_name, a.qty_sppb, "
				+ "		a.vehicle_no, a.vehicle_type, a.driver_id, "
				+ "		a.phone, a.qty, a.container_no, "
				+ "		a.departing_from_latitude, a.departing_from_longitude, a.departing_from_date, "
				+ "		a.departing_from_by, a.first_destination_latitude, a.first_destination_longitude, "
				+ "		a.first_destination_date, a.first_destination_by, a.transit_latitude, "
				+ "		a.transit_longitude, a.transit_date, a.transit_by, "
				+ "		a.destination_latitude, a.destination_longitude, a.destination_date, "
				+ "		a.destination_by, a.pin, a.status, "
				+ "		a.created, a.created_date, a.modified, "
				+ "		a.modified_date, a.destination_from_by, a.destination_from_date, "
				+ "		a.transportir_id, b.fullname, "
				+ "		case "
				+ "			when upper(a.status) = upper('a') then 'Approved' "
				+ "			when upper(a.status) = upper('l') then 'Batal' "
				+ "			when upper(a.status) = upper('i') then 'In Wighbridge' "
				+ "			when upper(a.status) = upper('t') then 'Tapping' "
				+ "			when upper(a.status) = upper('c') then 'Close' "
				+ "		end as stat, "
				+ "		c.is_transit "					
				+ "	from mob_sj a "
				+ "		left join mob_driver b on a.driver_id = b.id "
				+ "		join mob_spt c on a.spt_no = c.spt_no "
				+ "		join mob_goods_receive d on a.sj_no = d.sj_no "
				+ " where upper(a.status) != upper('o') "
				+ "		and (date_format(a.sj_date, '%Y-%m-%d') between '"+tempStartDate+"' and '"+tempEndDate+"') "
				+ "		and upper(a.status) = upper('c')"
				+ "		and (a.spt_no like '"+search+"' "
				+ "			or a.sj_no like '"+search+"' "
				+ "			or a.sj_date like '"+search+"' "
				+ "			or upper(a.cust_name) like upper('"+search+"')"
				+ "			or upper(a.transportir_name) like upper('"+search+"')"
				+ "			or date_format(a.sj_date,'%d-%b-%Y') like '"+search+"' "
				+ "			or upper(b.fullname) like upper('"+search+"') "
				+ "			or a.phone like '"+search+"')";
		Map<String, Object> mapList = getPaging(sql, pageable);
		@SuppressWarnings("unchecked")
		List<Object[]> listObj = (List<Object[]>) mapList.get("contentData");
		List<MobSjDto> listDto = new ArrayList<>();
		for(Object[] o : listObj){
			MobSjDto dto = new MobSjDto();
			dto.setSjNo((Integer) o[0]);
			dto.setSjDate((Date) o[1]);
			dto.setSptNo((Integer) o[2]);
			dto.setSpmNo((String) o[3]);
			dto.setSegel1((String) o[4]);
			dto.setSegel2((String) o[5]);
			dto.setSegel3((String) o[6]);
			dto.setSegel4((String) o[7]);
			dto.setSegel5((String) o[8]);
			dto.setSegel6((String) o[9]);
			dto.setSegel7((String) o[10]);
			dto.setSegel8((String) o[11]);
			dto.setCustCode((String) o[12]);
			dto.setCustName((String) o[13]);
			dto.setAddressShipTo((String) o[14]);
			dto.setTransportirCode((String) o[15]);
			dto.setTransportirName((String) o[16]);
			dto.setAddressShipFrom((String) o[17]);
			dto.setItemCode((String) o[18]);
			dto.setItemName((String) o[19]);
			dto.setQtySppb((BigDecimal) o[20]);
			dto.setVehicleNo((String) o[21]);
			dto.setVehicleType((Integer) o[22]);
			dto.setDriverId((Integer) o[23]);
			dto.setPhone((String) o[24]);
			dto.setQty((BigDecimal) o[25]);
			dto.setContainerNo((String) o[26]);
			dto.setDepartingFromLatitude((BigDecimal) o[27]);
			dto.setDepartingFromLongitude((BigDecimal) o[28]);
			dto.setDepartingFromDate((Date) o[29]);
			dto.setDepartingFromBy((String) o[30]);
			dto.setFirstDestinationLatitude((BigDecimal) o[31]);
			dto.setFirstDestinationLongitude((BigDecimal) o[32]);
			dto.setFirstDestinationDate((Date) o[33]);
			dto.setFirstDestinationBy((String) o[34]);
			dto.setTransitLatitude((BigDecimal) o[35]);
			dto.setTransitLongitude((BigDecimal) o[36]);
			
			dto.setTransitDate((Date) o[37]);
			dto.setTransitBy((String) o[38]);
			dto.setDestinationLatitude((BigDecimal) o[39]);
			dto.setDestinationLongitude((BigDecimal) o[40]);
			dto.setDestinationDate((Date) o[41]);
			dto.setDestinationBy((String) o[42]);
	
			dto.setPin((String) o[43]);
	//		dto.setStatus((String) o[44]);
			dto.setCreated((String) o[45]);
			dto.setCreatedDate((Date) o[46]);
			dto.setModified((String) o[47]);
			dto.setModifiedDate((Date) o[48]);
			dto.setDestinationFromBy((String) o[49]);
			dto.setDestinationFromDate((Date) o[50]);
	
			dto.setTransportirId((Integer) o[51]);
			dto.setDriverName((String) o[52]);
			dto.setStatusText((String) o[53]);
			
			byte isTransit = (byte) o[54];
			if(isTransit == 1){
				dto.setIsTransit(true);
			}else{
				dto.setIsTransit(false);
			}
			
			if(dto.getDepartingFromLatitude() != null && dto.getDepartingFromLongitude() != null){
				dto.setDepartingFromLatitudeLongitude(dto.getDepartingFromLatitude() + ", "+ dto.getDepartingFromLongitude());
			}else if(dto.getDepartingFromLatitude() == null && dto.getDepartingFromLongitude() != null){
				dto.setDepartingFromLatitudeLongitude(dto.getDepartingFromLongitude().toString());
			}else if(dto.getDepartingFromLatitude() != null && dto.getDepartingFromLongitude() == null){
				dto.setDepartingFromLatitudeLongitude(dto.getDepartingFromLatitude().toString());
			}else{
				dto.setDepartingFromLatitudeLongitude("");
			}
			
			if(dto.getFirstDestinationLatitude() != null && dto.getFirstDestinationLongitude() != null){
				dto.setFirstDestinationLatitudeLongitude(dto.getFirstDestinationLatitude()+", "+dto.getFirstDestinationLongitude());
			}else if(dto.getFirstDestinationLatitude() == null && dto.getFirstDestinationLongitude() != null){
				dto.setFirstDestinationLatitudeLongitude(dto.getFirstDestinationLongitude().toString());
			}else if(dto.getFirstDestinationLatitude() != null && dto.getFirstDestinationLongitude() == null){
				dto.setFirstDestinationLatitudeLongitude(dto.getFirstDestinationLatitude().toString());
			}else{
				dto.setFirstDestinationLatitudeLongitude("");
			}
			
			if(dto.getTransitLatitude() != null && dto.getTransitLongitude() != null){
				dto.setTransitLatitudeLongitude(dto.getTransitLatitude() +", "+dto.getTransitLongitude());
			}else if(dto.getTransitLatitude() == null && dto.getTransitLongitude() != null){
				dto.setTransitLatitudeLongitude(dto.getTransitLongitude().toString());
			}else if(dto.getTransitLatitude() != null && dto.getTransitLongitude() == null){
				dto.setTransitLatitudeLongitude(dto.getTransitLatitude().toString());
			}else{
				dto.setTransitLatitudeLongitude("");
			}
	
			if(dto.getDestinationLatitude() != null && dto.getDestinationLongitude() != null){
				dto.setDestinationLatitudeLongitude(dto.getDestinationLatitude() +", "+dto.getDestinationLongitude());
			}else if(dto.getDestinationLatitude() != null && dto.getDestinationLongitude() == null){
				dto.setDestinationLatitudeLongitude(dto.getDestinationLatitude().toString());
			}else if(dto.getDestinationLatitude() == null && dto.getDestinationLongitude() != null){
				dto.setDestinationLatitudeLongitude(dto.getDestinationLongitude().toString());
			}else{
				dto.setDestinationLatitudeLongitude("");
			}
			
			listDto.add(dto);
		}
	
		Map<String, Object> map = new HashMap<>();
		map.put("content", listDto);
		map.put("totalSize", mapList.get("totalRecords"));		
		return map;
	}

	@Override
	public MobGoodsReceiveDto getReceiveBySj(Integer sjNo) {
		List<Object[]> listObj = mobGoodsReceiveDao.findBySj(sjNo);
		MobGoodsReceiveDto dto = new MobGoodsReceiveDto();
		for(Object[] o : listObj){
			dto.setDriverName((String) o[0]);
			dto.setVehicleNo((String) o[1]);
			dto.setQtyReceive((BigDecimal) o[2]);
			dto.setNameOfRecipient((String) o[3]);
			dto.setPhone((String) o[4]);
			dto.setVehicleTypeText((String) o[5]);
			dto.setNotes((String) o[6]);
			dto.setSignature((String) o[7]);
			if(dto.getSignature() != null){
				try {
					URL url = new URL(dto.getSignature());
					try {
						dto.setImgSignature(new AImage(url));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				dto.setDefSignature(mGlobalImageDao.findOne("IMG001").getParmImgValue());
				try {
					dto.setImgSignature(new AImage("img signature", dto.getDefSignature()));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return dto;
	}

	@Override
	public List<MobGoodsReceiveAttachmentDto> getListAttachmentBySj(Integer sjNo) {
		try{
			List<MobGoodsReceiveAttachment> listObj = mobGoodsReceiveAttachmentDao.getListBySj(sjNo);
			List<MobGoodsReceiveAttachmentDto> listDto = new ArrayList<>();
			for(MobGoodsReceiveAttachment m : listObj){
				MobGoodsReceiveAttachmentDto dto = mapperFacade.map(m, MobGoodsReceiveAttachmentDto.class);
				URL url = new URL(m.getDocumentPath());
				dto.setAttachment(new AImage(url));
				listDto.add(dto);
			}
			return listDto;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		
	}

	@Override
	public Map<String, Object> findByCustomer(String custCode, 
			String search, Date startDate, Date endDate, int pageNo, int perPage, String orderBy,
			String direction) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		if(startDate == null){
			startDate = DateUtil.getMinDate();
		}
		
		if(endDate == null){
			endDate = DateUtil.getMaxDate();
		}

		String tempStartDate = df.format(startDate);
		String tempEndDate = df.format(endDate);		

		Pageable pageable = new PageRequest((pageNo-1)*perPage, perPage,
				new Sort(new Sort.Order(Direction.fromString(direction),orderBy)));
		String sql = "select a.sj_no, a.sj_date, a.spt_no,"
				+ "		a.spm_no, a.segel1, a.segel2,"
				+ "		a.segel3, a.segel4, a.segel5,"
				+ "		a.segel6, a.segel7, a.segel8, "
				+ "		a.cust_code, a.cust_name, a.address_ship_to, "
				+ "		a.transportir_code, a.transportir_name, a.address_ship_from, "
				+ "		a.item_code, a.item_name, a.qty_sppb, "
				+ "		a.vehicle_no, a.vehicle_type, a.driver_id, "
				+ "		a.phone, a.qty, a.container_no, "
				+ "		a.departing_from_latitude, a.departing_from_longitude, a.departing_from_date, "
				+ "		a.departing_from_by, a.first_destination_latitude, a.first_destination_longitude, "
				+ "		a.first_destination_date, a.first_destination_by, a.transit_latitude, "
				+ "		a.transit_longitude, a.transit_date, a.transit_by, "
				+ "		a.destination_latitude, a.destination_longitude, a.destination_date, "
				+ "		a.destination_by, a.pin, a.status, "
				+ "		a.created, a.created_date, a.modified, "
				+ "		a.modified_date, a.destination_from_by, a.destination_from_date, "
				+ "		a.transportir_id, b.fullname, "
				+ "		case "
				+ "			when upper(a.status) = upper('a') then 'Approved' "
				+ "			when upper(a.status) = upper('l') then 'Batal' "
				+ "			when upper(a.status) = upper('i') then 'In Wighbridge' "
				+ "			when upper(a.status) = upper('t') then 'Tapping' "
				+ "			when upper(a.status) = upper('c') then 'Close' "
				+ "		end as stat, "
				+ "		c.is_transit "				
				+ "	from mob_sj a "
				+ "		left join mob_driver b on a.driver_id = b.id "
				+ "		join mob_spt c on a.spt_no = c.spt_no "
				+ "		join mob_goods_receive d on a.sj_no = d.sj_no "
				+ " where upper(a.status) != upper('o') "
				+ "		and (date_format(a.sj_date, '%Y-%m-%d') between '"+tempStartDate+"' and '"+tempEndDate+"') "				
				+ "		and a.cust_code = '"+custCode+"' "
				+ "		and (a.spt_no like '"+search+"' "
				+ "			or a.sj_no like '"+search+"' "
				+ "			or a.sj_date like '"+search+"' "
				+ "			or upper(a.cust_name) like upper('"+search+"')"
				+ "			or upper(a.transportir_name) like upper('"+search+"')"
				+ "			or upper(case "
				+ "				when upper(a.status) = upper('a') then 'Approved' "
				+ "				when upper(a.status) = upper('l') then 'Batal' "
				+ "				when upper(a.status) = upper('i') then 'In Wighbridge' "
				+ "				when upper(a.status) = upper('t') then 'Tapping' "
				+ "				when upper(a.status) = upper('c') then 'Close' "
				+ "			end) like upper('"+search+"') "
				+ "			or date_format(a.sj_date,'%d-%b-%Y') like '"+search+"' "
				+ "			or upper(b.fullname) like upper('"+search+"') "
				+ "			or a.phone like '"+search+"')";
		Map<String, Object> mapList = getPaging(sql, pageable);
		@SuppressWarnings("unchecked")
		List<Object[]> listObj = (List<Object[]>) mapList.get("contentData");
		List<MobSjDto> listDto = new ArrayList<>();
		for(Object[] o : listObj){
			MobSjDto dto = new MobSjDto();
			dto.setSjNo((Integer) o[0]);
			dto.setSjDate((Date) o[1]);
			dto.setSptNo((Integer) o[2]);
			dto.setSpmNo((String) o[3]);
			dto.setSegel1((String) o[4]);
			dto.setSegel2((String) o[5]);
			dto.setSegel3((String) o[6]);
			dto.setSegel4((String) o[7]);
			dto.setSegel5((String) o[8]);
			dto.setSegel6((String) o[9]);
			dto.setSegel7((String) o[10]);
			dto.setSegel8((String) o[11]);
			dto.setCustCode((String) o[12]);
			dto.setCustName((String) o[13]);
			dto.setAddressShipTo((String) o[14]);
			dto.setTransportirCode((String) o[15]);
			dto.setTransportirName((String) o[16]);
			dto.setAddressShipFrom((String) o[17]);
			dto.setItemCode((String) o[18]);
			dto.setItemName((String) o[19]);
			dto.setQtySppb((BigDecimal) o[20]);
			dto.setVehicleNo((String) o[21]);
			dto.setVehicleType((Integer) o[22]);
			dto.setDriverId((Integer) o[23]);
			dto.setPhone((String) o[24]);
			dto.setQty((BigDecimal) o[25]);
			dto.setContainerNo((String) o[26]);
			dto.setDepartingFromLatitude((BigDecimal) o[27]);
			dto.setDepartingFromLongitude((BigDecimal) o[28]);
			dto.setDepartingFromDate((Date) o[29]);
			dto.setDepartingFromBy((String) o[30]);
			dto.setFirstDestinationLatitude((BigDecimal) o[31]);
			dto.setFirstDestinationLongitude((BigDecimal) o[32]);
			dto.setFirstDestinationDate((Date) o[33]);
			dto.setFirstDestinationBy((String) o[34]);
			dto.setTransitLatitude((BigDecimal) o[35]);
			dto.setTransitLongitude((BigDecimal) o[36]);
			
			dto.setTransitDate((Date) o[37]);
			dto.setTransitBy((String) o[38]);
			dto.setDestinationLatitude((BigDecimal) o[39]);
			dto.setDestinationLongitude((BigDecimal) o[40]);
			dto.setDestinationDate((Date) o[41]);
			dto.setDestinationBy((String) o[42]);
	
			dto.setPin((String) o[43]);
	//		dto.setStatus((String) o[44]);
			dto.setCreated((String) o[45]);
			dto.setCreatedDate((Date) o[46]);
			dto.setModified((String) o[47]);
			dto.setModifiedDate((Date) o[48]);
			dto.setDestinationFromBy((String) o[49]);
			dto.setDestinationFromDate((Date) o[50]);
	
			dto.setTransportirId((Integer) o[51]);
			dto.setDriverName((String) o[52]);
			dto.setStatusText((String) o[53]);
	
			byte isTransit = (byte) o[54];
			if(isTransit == 1){
				dto.setIsTransit(true);
			}else{
				dto.setIsTransit(false);
			}
			
			if(dto.getDepartingFromLatitude() != null && dto.getDepartingFromLongitude() != null){
				dto.setDepartingFromLatitudeLongitude(dto.getDepartingFromLatitude() + ", "+ dto.getDepartingFromLongitude());
			}else if(dto.getDepartingFromLatitude() == null && dto.getDepartingFromLongitude() != null){
				dto.setDepartingFromLatitudeLongitude(dto.getDepartingFromLongitude().toString());
			}else if(dto.getDepartingFromLatitude() != null && dto.getDepartingFromLongitude() == null){
				dto.setDepartingFromLatitudeLongitude(dto.getDepartingFromLatitude().toString());
			}else{
				dto.setDepartingFromLatitudeLongitude("");
			}
			
			if(dto.getFirstDestinationLatitude() != null && dto.getFirstDestinationLongitude() != null){
				dto.setFirstDestinationLatitudeLongitude(dto.getFirstDestinationLatitude()+", "+dto.getFirstDestinationLongitude());
			}else if(dto.getFirstDestinationLatitude() == null && dto.getFirstDestinationLongitude() != null){
				dto.setFirstDestinationLatitudeLongitude(dto.getFirstDestinationLongitude().toString());
			}else if(dto.getFirstDestinationLatitude() != null && dto.getFirstDestinationLongitude() == null){
				dto.setFirstDestinationLatitudeLongitude(dto.getFirstDestinationLatitude().toString());
			}else{
				dto.setFirstDestinationLatitudeLongitude("");
			}
			
			if(dto.getTransitLatitude() != null && dto.getTransitLongitude() != null){
				dto.setTransitLatitudeLongitude(dto.getTransitLatitude() +", "+dto.getTransitLongitude());
			}else if(dto.getTransitLatitude() == null && dto.getTransitLongitude() != null){
				dto.setTransitLatitudeLongitude(dto.getTransitLongitude().toString());
			}else if(dto.getTransitLatitude() != null && dto.getTransitLongitude() == null){
				dto.setTransitLatitudeLongitude(dto.getTransitLatitude().toString());
			}else{
				dto.setTransitLatitudeLongitude("");
			}
	
			if(dto.getDestinationLatitude() != null && dto.getDestinationLongitude() != null){
				dto.setDestinationLatitudeLongitude(dto.getDestinationLatitude() +", "+dto.getDestinationLongitude());
			}else if(dto.getDestinationLatitude() != null && dto.getDestinationLongitude() == null){
				dto.setDestinationLatitudeLongitude(dto.getDestinationLatitude().toString());
			}else if(dto.getDestinationLatitude() == null && dto.getDestinationLongitude() != null){
				dto.setDestinationLatitudeLongitude(dto.getDestinationLongitude().toString());
			}else{
				dto.setDestinationLatitudeLongitude("");
			}
			
			listDto.add(dto);
		}
	
		Map<String, Object> map = new HashMap<>();
		map.put("content", listDto);
		map.put("totalSize", mapList.get("totalRecords"));		
		return map;
	}

	@Override
	public Map<String, Object> findByTransportir(String transportirCode, String search, 
			Date startDate, Date endDate, int pageNo, int perPage, String orderBy, String direction) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		if(startDate == null){
			startDate = DateUtil.getMinDate();
		}
		
		if(endDate == null){
			endDate = DateUtil.getMaxDate();
		}

		String tempStartDate = df.format(startDate);
		String tempEndDate = df.format(endDate);		

		Pageable pageable = new PageRequest((pageNo-1)*perPage, perPage,
				new Sort(new Sort.Order(Direction.fromString(direction),orderBy)));
		String sql = "select a.sj_no, a.sj_date, a.spt_no,"
				+ "		a.spm_no, a.segel1, a.segel2,"
				+ "		a.segel3, a.segel4, a.segel5,"
				+ "		a.segel6, a.segel7, a.segel8, "
				+ "		a.cust_code, a.cust_name, a.address_ship_to, "
				+ "		a.transportir_code, a.transportir_name, a.address_ship_from, "
				+ "		a.item_code, a.item_name, a.qty_sppb, "
				+ "		a.vehicle_no, a.vehicle_type, a.driver_id, "
				+ "		a.phone, a.qty, a.container_no, "
				+ "		a.departing_from_latitude, a.departing_from_longitude, a.departing_from_date, "
				+ "		a.departing_from_by, a.first_destination_latitude, a.first_destination_longitude, "
				+ "		a.first_destination_date, a.first_destination_by, a.transit_latitude, "
				+ "		a.transit_longitude, a.transit_date, a.transit_by, "
				+ "		a.destination_latitude, a.destination_longitude, a.destination_date, "
				+ "		a.destination_by, a.pin, a.status, "
				+ "		a.created, a.created_date, a.modified, "
				+ "		a.modified_date, a.destination_from_by, a.destination_from_date, "
				+ "		a.transportir_id, b.fullname, "
				+ "		case "
				+ "			when upper(a.status) = upper('a') then 'Approved' "
				+ "			when upper(a.status) = upper('l') then 'Batal' "
				+ "			when upper(a.status) = upper('i') then 'In Wighbridge' "
				+ "			when upper(a.status) = upper('t') then 'Tapping' "
				+ "			when upper(a.status) = upper('c') then 'Close' "
				+ "		end as stat, "					
				+ "		c.is_transit "
				+ "	from mob_sj a "
				+ "		left join mob_driver b on a.driver_id = b.id "
				+ "		join mob_spt c on a.spt_no = c.spt_no "
				+ "		join mob_goods_receive d on a.sj_no = d.sj_no "
				+ " where upper(a.status) != upper('o') "
				+ "		and a.departing_from_latitude is not null "
				+ " 	and a.departing_from_longitude is not null "
				+ "		and (date_format(a.sj_date, '%Y-%m-%d') between '"+tempStartDate+"' and '"+tempEndDate+"') "								
				+ "		and a.transportir_code = '"+transportirCode+"' "
				+ "		and (a.spt_no like '"+search+"' "
				+ "			or a.sj_no like '"+search+"' "
				+ "			or a.sj_date like '"+search+"' "
				+ "			or upper(a.cust_name) like upper('"+search+"')"
				+ "			or upper(a.transportir_name) like upper('"+search+"')"
				+ "			or upper(case "
				+ "				when upper(a.status) = upper('a') then 'Approved' "
				+ "				when upper(a.status) = upper('l') then 'Batal' "
				+ "				when upper(a.status) = upper('i') then 'In Wighbridge' "
				+ "				when upper(a.status) = upper('t') then 'Tapping' "
				+ "				when upper(a.status) = upper('c') then 'Close' "
				+ "			end) like upper('"+search+"') "
				+ "			or date_format(a.sj_date,'%d-%b-%Y') like '"+search+"' "
				+ "			or upper(b.fullname) like upper('"+search+"') "
				+ "			or a.phone like '"+search+"')";		
		Map<String, Object> mapList = getPaging(sql, pageable);
		@SuppressWarnings("unchecked")
		List<Object[]> listObj = (List<Object[]>) mapList.get("contentData");
		List<MobSjDto> listDto = new ArrayList<>();
		for(Object[] o : listObj){
			MobSjDto dto = new MobSjDto();
			dto.setSjNo((Integer) o[0]);
			dto.setSjDate((Date) o[1]);
			dto.setSptNo((Integer) o[2]);
			dto.setSpmNo((String) o[3]);
			dto.setSegel1((String) o[4]);
			dto.setSegel2((String) o[5]);
			dto.setSegel3((String) o[6]);
			dto.setSegel4((String) o[7]);
			dto.setSegel5((String) o[8]);
			dto.setSegel6((String) o[9]);
			dto.setSegel7((String) o[10]);
			dto.setSegel8((String) o[11]);
			dto.setCustCode((String) o[12]);
			dto.setCustName((String) o[13]);
			dto.setAddressShipTo((String) o[14]);
			dto.setTransportirCode((String) o[15]);
			dto.setTransportirName((String) o[16]);
			dto.setAddressShipFrom((String) o[17]);
			dto.setItemCode((String) o[18]);
			dto.setItemName((String) o[19]);
			dto.setQtySppb((BigDecimal) o[20]);
			dto.setVehicleNo((String) o[21]);
			dto.setVehicleType((Integer) o[22]);
			dto.setDriverId((Integer) o[23]);
			dto.setPhone((String) o[24]);
			dto.setQty((BigDecimal) o[25]);
			dto.setContainerNo((String) o[26]);
			dto.setDepartingFromLatitude((BigDecimal) o[27]);
			dto.setDepartingFromLongitude((BigDecimal) o[28]);
			dto.setDepartingFromDate((Date) o[29]);
			dto.setDepartingFromBy((String) o[30]);
			dto.setFirstDestinationLatitude((BigDecimal) o[31]);
			dto.setFirstDestinationLongitude((BigDecimal) o[32]);
			dto.setFirstDestinationDate((Date) o[33]);
			dto.setFirstDestinationBy((String) o[34]);
			dto.setTransitLatitude((BigDecimal) o[35]);
			dto.setTransitLongitude((BigDecimal) o[36]);
			
			dto.setTransitDate((Date) o[37]);
			dto.setTransitBy((String) o[38]);
			dto.setDestinationLatitude((BigDecimal) o[39]);
			dto.setDestinationLongitude((BigDecimal) o[40]);
			dto.setDestinationDate((Date) o[41]);
			dto.setDestinationBy((String) o[42]);
	
			dto.setPin((String) o[43]);
	//		dto.setStatus((String) o[44]);
			dto.setCreated((String) o[45]);
			dto.setCreatedDate((Date) o[46]);
			dto.setModified((String) o[47]);
			dto.setModifiedDate((Date) o[48]);
			dto.setDestinationFromBy((String) o[49]);
			dto.setDestinationFromDate((Date) o[50]);
	
			dto.setTransportirId((Integer) o[51]);
			dto.setDriverName((String) o[52]);
			dto.setStatusText((String) o[53]);
			
			byte isTransit = (byte) o[54];
			if(isTransit == 1){
				dto.setIsTransit(true);
			}else{
				dto.setIsTransit(false);
			}
			
			if(dto.getDepartingFromLatitude() != null && dto.getDepartingFromLongitude() != null){
				dto.setDepartingFromLatitudeLongitude(dto.getDepartingFromLatitude() + ", "+ dto.getDepartingFromLongitude());
			}else if(dto.getDepartingFromLatitude() == null && dto.getDepartingFromLongitude() != null){
				dto.setDepartingFromLatitudeLongitude(dto.getDepartingFromLongitude().toString());
			}else if(dto.getDepartingFromLatitude() != null && dto.getDepartingFromLongitude() == null){
				dto.setDepartingFromLatitudeLongitude(dto.getDepartingFromLatitude().toString());
			}else{
				dto.setDepartingFromLatitudeLongitude("");
			}
			
			if(dto.getFirstDestinationLatitude() != null && dto.getFirstDestinationLongitude() != null){
				dto.setFirstDestinationLatitudeLongitude(dto.getFirstDestinationLatitude()+", "+dto.getFirstDestinationLongitude());
			}else if(dto.getFirstDestinationLatitude() == null && dto.getFirstDestinationLongitude() != null){
				dto.setFirstDestinationLatitudeLongitude(dto.getFirstDestinationLongitude().toString());
			}else if(dto.getFirstDestinationLatitude() != null && dto.getFirstDestinationLongitude() == null){
				dto.setFirstDestinationLatitudeLongitude(dto.getFirstDestinationLatitude().toString());
			}else{
				dto.setFirstDestinationLatitudeLongitude("");
			}
			
			if(dto.getTransitLatitude() != null && dto.getTransitLongitude() != null){
				dto.setTransitLatitudeLongitude(dto.getTransitLatitude() +", "+dto.getTransitLongitude());
			}else if(dto.getTransitLatitude() == null && dto.getTransitLongitude() != null){
				dto.setTransitLatitudeLongitude(dto.getTransitLongitude().toString());
			}else if(dto.getTransitLatitude() != null && dto.getTransitLongitude() == null){
				dto.setTransitLatitudeLongitude(dto.getTransitLatitude().toString());
			}else{
				dto.setTransitLatitudeLongitude("");
			}
	
			if(dto.getDestinationLatitude() != null && dto.getDestinationLongitude() != null){
				dto.setDestinationLatitudeLongitude(dto.getDestinationLatitude() +", "+dto.getDestinationLongitude());
			}else if(dto.getDestinationLatitude() != null && dto.getDestinationLongitude() == null){
				dto.setDestinationLatitudeLongitude(dto.getDestinationLatitude().toString());
			}else if(dto.getDestinationLatitude() == null && dto.getDestinationLongitude() != null){
				dto.setDestinationLatitudeLongitude(dto.getDestinationLongitude().toString());
			}else{
				dto.setDestinationLatitudeLongitude("");
			}			
			
			listDto.add(dto);
		}
	
		Map<String, Object> map = new HashMap<>();
		map.put("content", listDto);
		map.put("totalSize", mapList.get("totalRecords"));		
		return map;
	}

	@Override
	public Boolean cekTransit(Integer sptNo) {
		Boolean result;
		if(mobSptDao.findTransitBySpt(sptNo) != null){
			result = mobSptDao.findTransitBySpt(sptNo);
		}else{
			result = false;
		}
		return result;
	}

	@Override
	public List<ReportSjDto> getReportSj(List<String> listNoSj) {
		List<Object[]> listObjectSj = mobSjDao.getReportSjDto(listNoSj);
		List<ReportSjDto> listreportSj = new ArrayList<>();
		for(Object[] o: listObjectSj ){
			ReportSjDto dto = new ReportSjDto();
			dto.setSjNo(((Integer) o[0]).toString());
			dto.setSjDate((Date) o[1]);
			dto.setCustName((String) o[2]);
			dto.setAddressShipTo((String) o[3]);
			dto.setItemName((String) o[4]);
			dto.setQty(((BigDecimal)o[5]).doubleValue());
			dto.setQtyReceive(((BigDecimal) o[6]).doubleValue());
			dto.setNameOfRecipient((String)o[7]);
			dto.setNotes((String)o[8]);
			dto.setNoSppb((Integer)o[9]);
			dto.setNoSpt((Integer)o[10]);
			dto.setTglSpt((Date)o[11]);
			dto.setNoSpm((String) o[12]);
			dto.setDriverName((String)o[13]);
			dto.setNoPhone((String)o[14]);
			dto.setNoVehicle((String)o[15]);
			dto.setNoContainer((String)o[16]);
			dto.setTakeAssigmentDate((Date)o[17]);
			dto.setTakeAssigmentLatLong((String)o[18]);
			dto.setDepartingFromDate((Date)o[19]);
			dto.setDepartingFromLatLong((String)o[20]);
			dto.setFirstDestinationDate((Date)o[21]);
			dto.setFirstDestinationLatLong((String)o[22]);
			dto.setTransitDate((Date)o[23]);
			dto.setTransitLatLong((String)o[24]);
			dto.setDestinationDate((Date)o[25]);
			dto.setDestinationLatLong((String)o[26]);
			dto.setStatus((String)o[27]);
			dto.setTransportirName((String)o[28]);
			listreportSj.add(dto);
		}
		
		return listreportSj;
	}

}
