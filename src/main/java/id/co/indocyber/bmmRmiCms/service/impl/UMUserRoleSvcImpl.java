package id.co.indocyber.bmmRmiCms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dao.MRoleDao;
import id.co.indocyber.bmmRmiCms.dao.MUserDao;
import id.co.indocyber.bmmRmiCms.dao.MUserRoleDao;
import id.co.indocyber.bmmRmiCms.dto.MRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MUserRoleDto;
import id.co.indocyber.bmmRmiCms.entity.MRole;
import id.co.indocyber.bmmRmiCms.entity.MUser;
import id.co.indocyber.bmmRmiCms.entity.MUserRole;
import id.co.indocyber.bmmRmiCms.service.UMUserRoleSvc;
import id.co.indocyber.bmmRmiCms.tools.StringUtil;
import ma.glasnost.orika.MapperFacade;

@Service("uMUserRoleSvc")
@Transactional
public class UMUserRoleSvcImpl extends SortGenerator implements UMUserRoleSvc {
	
	@Autowired
	private MUserRoleDao mUserRoleDao;
	
	
	@Autowired
	private MRoleDao mRoleDao;
	
	@Autowired
	private MUserDao mUserDao;
	
	@Autowired
	private MapperFacade mapperFacade;
	
	@Override
	public List<MUserRoleDto> findAll(String search, Integer pageNo, String colName, boolean sortAsc) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		List<Object[]> listObj = mUserRoleDao.findAllData(search,
				new PageRequest(pageNo-1, 10, generateSort(colName,sortAsc)));
		List<MUserRoleDto> listDto = new ArrayList<>();
		for(Object[] o : listObj){
			MUserRoleDto dto = mapperFacade.map(o[0], MUserRoleDto.class);
			dto.setRoleName((String) o[1]);
			listDto.add(dto);			
		}
		return listDto;
	}

	@Override
	public Integer countAll(String search) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		return ((Long)mUserRoleDao.countAllData(search)).intValue();
	}

	@Override
	public int save(MUserRoleDto dto) {
		try{
			MUserRole role = mapperFacade.map(dto, MUserRole.class);
			mUserRoleDao.save(role);
			return 1;
		}catch(Exception e){
			e.printStackTrace();
			return 0;
		}		
	}

	@Override
	public List<MRoleDto> cbListRole() {
		String search =null;
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		List<MRole> listObj = mRoleDao.findAllDataActive(search);
		List<MRoleDto> listDto = new ArrayList<>();
		for(MRole r : listObj){
			MRoleDto dto = new MRoleDto();
			dto = mapperFacade.map(r, MRoleDto.class);
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public List<MUserDto> cbListUser(String search) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		List<MUser> listObj = mUserDao.findAllDataAvailable(search);
		List<MUserDto> listDto = new ArrayList<>();
		for(MUser u :  listObj){
			MUserDto dto = mapperFacade.map(u, MUserDto.class);
			listDto.add(dto);
		}
		return listDto;
	}

}
