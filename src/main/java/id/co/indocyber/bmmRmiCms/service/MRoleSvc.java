package id.co.indocyber.bmmRmiCms.service;

import id.co.indocyber.bmmRmiCms.entity.MRole;

import java.util.List;

public interface MRoleSvc {
	public List<MRole> findAll();
	public List<MRole> findAll(Integer pageNo, String colName, boolean sortAsc);
	public Integer countAll();
	
	public MRole findOne(Integer idRole);
	
	public boolean existsName(Integer id, String roleName);
	public void disable(Integer id);
	
	public List<MRole> findByArg(String arg, Integer pageNo, String colName, boolean sortAsc);
	public Integer countByArg(String arg);
	
	public MRole save(MRole editRole);
}
