package id.co.indocyber.bmmRmiCms.service;

import java.util.List;

import id.co.indocyber.bmmRmiCms.dto.MRoleDto;

public interface UMRoleSvc {
	
	public List<MRoleDto> findAll(String search, Integer pageNo, Integer perPage ,String colName,boolean sortAsc);	
	public Integer countAll(String search);
	public int save(MRoleDto dto);
	public boolean cekRole (String namaRole);

	
}
