package id.co.indocyber.bmmRmiCms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dao.MUserRoleDao;
import id.co.indocyber.bmmRmiCms.dto.MUserRoleDto;
import id.co.indocyber.bmmRmiCms.entity.MUserRole;
import id.co.indocyber.bmmRmiCms.service.MUserRoleSvc;
import ma.glasnost.orika.MapperFacade;

@Service("mUserRoleSvc")
@Transactional
public class MUserRoleSvcImpl implements MUserRoleSvc{
	@Autowired
	MUserRoleDao mUserRoleDao;

	private MapperFacade mapperFacade;
	
	@Override
	public MUserRole findUserRole(String email) {
		return mUserRoleDao.findUserRole(email);
	}

	@Override
	public List<MUserRole> findAll() {
		return mUserRoleDao.findAll();
	}

	@Override
	public MUserRole cmsAccess(String email) {
		return mUserRoleDao.getCmsAccess(email);
	}

	@Override
	public void save(MUserRole uR) {
		mUserRoleDao.save(uR);
	}

	@Override
	public Boolean isRoleAdministrator(String email) {
		Long cekData = mUserRoleDao.countDataByEmail(email);
		if(cekData > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Boolean isRoleCustomer(String email) {
		Long cekData = mUserRoleDao.countDataCustomer(email);
		if(cekData > 0){
			return true;
		}else{
			return false;
		}
		
	}
	
}
