package id.co.indocyber.bmmRmiCms.service;

import java.util.List;

import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.entity.MUser;

public interface MUserSvc {
	public boolean login(String usernameOrEmail, String password);
	public boolean existence(String usernameOrEmail);
	public MUser findByUsernameOrEmail(String usernameOrEmail);
	
	public List<MUser> findAll();
	
	public List<MUserDto> findAll(Integer pageNo, String colName, boolean sortAsc);
	public Integer countAll();
	
	public void save(MUserDto editUser);
	public boolean existsMobile(String username, String phoneNum);
	public boolean existsEmail(String username, String email);
	public void disable(String username);
	
	public List<MUserDto> findByArg(String arg, Integer pageNo, String string,
			boolean sortAsc);
	public Integer countByArg(String arg);
	
	public void updateEmployeePhone(Integer id, String phoneNum);
	void updateDriverPhone(Integer id, String phoneNum);
}
