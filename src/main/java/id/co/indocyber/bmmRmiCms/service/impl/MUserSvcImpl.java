package id.co.indocyber.bmmRmiCms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dao.MUserDao;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.entity.MUser;
import id.co.indocyber.bmmRmiCms.service.MUserSvc;

@Service("mUserSvc")
@Transactional
public class MUserSvcImpl extends SortGenerator implements MUserSvc{
	@Autowired
	MUserDao mUserDao;
	
	public String hash(String username, String pass){
		String toHash = username+"berkah"+pass+"manisindo";
		return DigestUtils.sha1Hex(toHash);
	}
	
	@Override
	public boolean login(String usernameOrEmail, String password) {
		MUser user = findByUsernameOrEmail(usernameOrEmail);
		
		if(user == null) {
			return false;
		}
		else {
			System.err.println("Hash di login = "+hash(user.getUsername(),password));
			if(hash(user.getUsername(),password).equals(user.getPassword()))
				return true;
			return false;
			
		}
	}
	
	@Override
	public MUser findByUsernameOrEmail(String usernameOrEmail){
		return mUserDao.findByUsernameOrEmail(usernameOrEmail);
	}

	@Override
	public List<MUser> findAll() {
		return mUserDao.findAll();
	}

	@Override
	public boolean existence(String usernameOrEmail) {
		return findByUsernameOrEmail(usernameOrEmail)!=null;
	}

	@Override
	public List<MUserDto> findAll(Integer pageNo, String colName,boolean sortAsc) {
//		List<Object[]> entities = mUserDao.getAll(
//			new PageRequest(pageNo-1, 10, generateSort(colName,sortAsc)));
//		List<MUserDto> res = new ArrayList<>();
//		for (Object[] x: entities){
//			MUserDto y = new MUserDto((MUser)x[0]);
//			y.setRoleName((String)x[1]);
//			res.add(y);
//		}
		return null;
	}

	@Override
	public Integer countAll() {
		return ((Long)mUserDao.count()).intValue();
	}

	@Override
	public void save(MUserDto editUser) {
		MUser user = new MUser();
		user.setCreatedBy(editUser.getCreatedBy());
		user.setCreatedDate(editUser.getCreatedDate());
		user.setEmail(editUser.getEmail());
		user.setFullname(editUser.getFullname());
		user.setImei(editUser.getImei());
		user.setIsActive(editUser.getIsActive());
		user.setModifiedBy(editUser.getModifiedBy());
		user.setModifiedDate(editUser.getModifiedDate());
		user.setNipeg(editUser.getNipeg());
//		if(editUser.isRehash() == true){
//			user.setPassword(hash(editUser.getUsername(),editUser.getPassword()));
//		}else{
//			user.setPassword(editUser.getPassword());
//		}
		user.setPassword(editUser.getPassword());	
		user.setPersonId(editUser.getPersonId());
		user.setPhoneNum(editUser.getPhoneNum());
		user.setPhotoText(editUser.getPhotoText());
		user.setPswdDefault(editUser.getPswdDefault());
		user.setReferenceCode(editUser.getReferenceCode());
		user.setUsername(editUser.getUsername());
		mUserDao.save(user);
	}

	@Override
	public boolean existsMobile(String username, String phoneNum) {
		return false;
	}

	@Override
	public boolean existsEmail(String username, String email) {
		return false;
	}

	@Override
	public void disable(String username) {
		MUser user = findByUsernameOrEmail(username);
		user.setIsActive(false);
		mUserDao.save(user);
	}

	@Override
	public List<MUserDto> findByArg(String arg, Integer pageNo, String colName,
			boolean sortAsc) {
//		List<Object[]> entities = mUserDao.findByArg(arg,
//				new PageRequest(pageNo-1, 10, generateSort(colName,sortAsc)));
//		List<MUserDto> res = new ArrayList<>();
//		for (Object[] x: entities){
//			MUserDto y = new MUserDto((MUser)x[0]);
//			y.setRoleName((String)x[1]);
//			res.add(y);
//		}
		return null;
	}

	@Override
	public Integer countByArg(String arg) {
		return mUserDao.countByArg(arg).intValue();
	}

	@Override
	public void updateEmployeePhone(Integer id, String phoneNum) {
		MUser user = mUserDao.findEmployee(id);
		if(user!=null){
			user.setPhoneNum(phoneNum);
			mUserDao.save(user);
		}
	}
	
	@Override
	public void updateDriverPhone(Integer id, String phoneNum) {
		MUser user = mUserDao.findDriver(id);
		if(user!=null){
			user.setPhoneNum(phoneNum);
			mUserDao.save(user);
		}
	}
}
