package id.co.indocyber.bmmRmiCms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dao.MRoleDao;
import id.co.indocyber.bmmRmiCms.dto.MRoleDto;
import id.co.indocyber.bmmRmiCms.entity.MRole;
import id.co.indocyber.bmmRmiCms.service.UMRoleSvc;
import id.co.indocyber.bmmRmiCms.tools.StringUtil;
import ma.glasnost.orika.MapperFacade;

@Service("uMRoleSvc")
@Transactional
public class UmRoleSvcImpl extends SortGenerator implements UMRoleSvc {
	
	@Autowired
	private MRoleDao mRoleDao;
	
	@Autowired
	private MapperFacade mapperFacade; 
	
	@Override
	public List<MRoleDto> findAll(String search, Integer pageNo, Integer perPage ,String colName, boolean sortAsc) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		List<MRole> listObj = mRoleDao.findAllData(search, 
				new PageRequest(pageNo-1, perPage, generateSort(colName,sortAsc)));
		List<MRoleDto> listDto = new ArrayList<>();
		for(MRole r : listObj){
			MRoleDto dto = mapperFacade.map(r, MRoleDto.class);
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public Integer countAll(String search) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		return ((Long)mRoleDao.countAllData(search)).intValue();
	}

	@Override
	public int save(MRoleDto dto) {
		try{
			MRole role = mapperFacade.map(dto, MRole.class);
			mRoleDao.save(role);
			return 1;
		}catch(Exception e){
			e.printStackTrace();
			return 0;
		}
		
	}

	@Override
	public boolean cekRole(String namaRole) {
		try{
			namaRole = StringUtil.surroundString(StringUtil.nevl(namaRole, "%"), "%");			
			MRole role = mRoleDao.findOneByRoleName(namaRole);
			if(role.getId()==null){
				return false;
			}else{
				return true;				
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

}
