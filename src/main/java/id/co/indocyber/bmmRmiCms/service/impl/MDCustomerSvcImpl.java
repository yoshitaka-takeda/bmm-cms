package id.co.indocyber.bmmRmiCms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dao.MobCustomerDao;
import id.co.indocyber.bmmRmiCms.dto.MobCustomerDto;
import id.co.indocyber.bmmRmiCms.entity.MobCustomer;
import id.co.indocyber.bmmRmiCms.service.MDCustomerSvc;
import id.co.indocyber.bmmRmiCms.tools.StringUtil;
import ma.glasnost.orika.MapperFacade;

@Service("mDCustomerSvc")
@Transactional
public class MDCustomerSvcImpl extends SortGenerator implements MDCustomerSvc{
	
	@Autowired
	private MapperFacade mapperFacade;
	
	@Autowired
	private MobCustomerDao mobCustomerDao;
	
	@Override
	public List<MobCustomerDto> findAll(String search, Integer pageNo, String colName, boolean sortAsc) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		List<MobCustomerDto> listDto = new ArrayList<>();
		for(MobCustomer e : 
			mobCustomerDao.findAllData(
					search, new PageRequest(pageNo-1, 25, generateSort(colName,sortAsc)))){
			MobCustomerDto dto = mapperFacade.map(e, MobCustomerDto.class);
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public Integer countAll(String search) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		return mobCustomerDao.countAllData(search).intValue();
	}

	@Override
	public int save(MobCustomerDto dto) {
		try{
			MobCustomer m = mapperFacade.map(dto, MobCustomer.class);
			mobCustomerDao.save(m);
			return 1;
		}catch(Exception e){
			e.printStackTrace();
			return 0;			
		}
		
	}

	@Override
	public Integer countAll() {
		return ((Long) mobCustomerDao.count()).intValue();
	}
	
	@Override
	public List<MobCustomerDto> findAll(Integer pageNo, String colName,
			boolean sortAsc) {
		List<MobCustomer> entities = mobCustomerDao.findAll(
			new PageRequest(pageNo - 1, 10, generateSort(colName, sortAsc))).getContent();
		List<MobCustomerDto> listDto = new ArrayList<>();
		listDto = mapperFacade.mapAsList(entities, MobCustomerDto.class);
		return listDto;
	}

}
