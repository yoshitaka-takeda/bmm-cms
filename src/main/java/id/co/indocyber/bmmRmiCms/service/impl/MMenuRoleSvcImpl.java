package id.co.indocyber.bmmRmiCms.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import id.co.indocyber.bmmRmiCms.dao.MMenuDao;
import id.co.indocyber.bmmRmiCms.dao.MMenuRoleDao;
import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.entity.MMenu;
import id.co.indocyber.bmmRmiCms.entity.MMenuRole;
import id.co.indocyber.bmmRmiCms.service.MMenuRoleSvc;

@Service("mMenuRoleSvc")
@Transactional
public class MMenuRoleSvcImpl implements MMenuRoleSvc{
	@Autowired
	MMenuRoleDao mMenuRoleDao;
	@Autowired
	MMenuDao mMenuDao;

	@Override
	public List<MMenuRoleDto> getAccessPrivs(Integer roleId, String module) {
		List<MMenuRoleDto> res = new ArrayList<MMenuRoleDto>();
		if (roleId != null) {
			for (Object[] privs : mMenuRoleDao.getAccessPrivs(roleId,module)) {
				res.add(new MMenuRoleDto((MMenuRole) privs[0], (MMenu) privs[1]));
			}
			for (MMenu x : mMenuDao.loadMenu(module)) {
				boolean found = false;
				for (MMenuRoleDto y : res) {
					if (x.getMenuCode().equals(y.getMenuCode())) {
						found = true;
						break;
					}
				}
				System.err.println("foundsss = "+found);
				if (!found) {
					MMenuRole newMenu = new MMenuRole();
					newMenu.setRoleId(roleId);
					newMenu.setMenuCode(x.getMenuCode());
					newMenu.setIsActive(false);
					newMenu.setIsAdd(false);
					newMenu.setIsDelete(false);
					newMenu.setIsDownload(false);
					newMenu.setIsEdit(false);
					newMenu.setIsExport(false);
					newMenu.setIsImport(false);
					newMenu.setIsPrint(false);
					newMenu.setIsView(false);
					res.add(new MMenuRoleDto(newMenu, x));
				}
			}
		} else {
			for (MMenu x : mMenuDao.loadMenu(module)) {
				MMenuRole newMenu = new MMenuRole();
				newMenu.setRoleId(roleId);
				newMenu.setMenuCode(x.getMenuCode());
				newMenu.setIsActive(false);
				newMenu.setIsAdd(false);
				newMenu.setIsDelete(false);
				newMenu.setIsDownload(false);
				newMenu.setIsEdit(false);
				newMenu.setIsExport(false);
				newMenu.setIsImport(false);
				newMenu.setIsPrint(false);
				newMenu.setIsView(false);
				res.add(new MMenuRoleDto(newMenu, x));
			}
		}
		return res;
	}
	
	@Override
	public void save(List<MMenuRoleDto> rolePrivs) {
		// TODO Auto-generated method stub
		List<MMenuRole> entities = new ArrayList<>();
		for (MMenuRoleDto x : rolePrivs) {
			// SKIP YANG DIKOSONGIN ACCESSNYA (Khusus create)
			if(!x.calculateAccess() && x.getId() ==  null) continue;
			
			MMenuRole y = new MMenuRole();
			y.setCreatedDate(new Timestamp(x.getCreatedDate()!=null?
				x.getCreatedDate().getTime():new java.util.Date().getTime()));
			y.setCreated(x.getCreated()==null?x.getModified():x.getCreated());
			y.setId(x.getId());
			y.setIsActive(true);
			y.setIsAdd(x.getIsAdd());
			y.setIsDelete(x.getIsDelete());
			y.setIsDownload(x.getIsDownload());
			y.setIsEdit(x.getIsEdit());
			y.setIsExport(x.getIsExport());
			y.setIsImport(x.getIsImport());
			y.setIsPrint(x.getIsPrint());
			if(StringUtils.isEmpty(x.getParentMenuCode()) && x.getModule().equals("Web")){
				y.setIsView(false);
			}
			else{
				y.setIsView(x.calculateAccess());
			}
			y.setMenuCode(x.getMenuCode());
			y.setRoleId(x.getRoleId());
			y.setModified(x.getModified());
			y.setModifiedDate(new Timestamp(x.getModifiedDate()!=null?
				x.getModifiedDate().getTime():new java.util.Date().getTime()));
			
			if(!StringUtils.isEmpty(x.getParentMenuCode())){
				for(MMenuRole z: entities){
					if(z.getMenuCode().equals(x.getParentMenuCode())){
						z.setIsView(z.getIsView()||y.getIsView());
						break;
					}
					else continue;
				}
			}
			
			entities.add(y);
		}
		mMenuRoleDao.save(entities);
	}
}
