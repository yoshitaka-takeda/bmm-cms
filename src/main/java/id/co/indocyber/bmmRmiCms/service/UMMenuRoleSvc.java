package id.co.indocyber.bmmRmiCms.service;

import java.util.List;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;

public interface UMMenuRoleSvc {
	
	public List<MMenuRoleDto> findMenuRoleByParam(Integer roleId);
	public List<MMenuRoleDto> findAllMenuRole();
		
	public List<MMenuRoleDto> findAllUserRole(String search, Integer pageNo, Integer perPage ,String colName, boolean sortAsc);	
	public Integer countAll(String search);
	
	public List<MMenuRoleDto> findMenuRoleByMail(String email, Boolean status);
	public List<MMenuRoleDto> findMenuRoleByRoleId(int roleId, Boolean status);
	
	public List<MRoleDto> cbListRole(String search);
	public List<MUserDto> findUserByRole(Integer roleId);
	
	public void deleteByRole(Integer id);
	
	public void save(MMenuRoleDto dto);
	public int updateStatus(List<MMenuRoleDto> list);
	

}
