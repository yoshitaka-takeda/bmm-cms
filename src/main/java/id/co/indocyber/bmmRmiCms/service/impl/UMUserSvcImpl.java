package id.co.indocyber.bmmRmiCms.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dao.MRoleDao;
import id.co.indocyber.bmmRmiCms.dao.MUserDao;
import id.co.indocyber.bmmRmiCms.dao.MUserRoleDao;
import id.co.indocyber.bmmRmiCms.dao.MobCustomerDao;
import id.co.indocyber.bmmRmiCms.dao.MobDriverDao;
import id.co.indocyber.bmmRmiCms.dao.MobEmployeeDao;
import id.co.indocyber.bmmRmiCms.dao.MobTransportirDao;
import id.co.indocyber.bmmRmiCms.dto.MRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MUserRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MobCustomerDto;
import id.co.indocyber.bmmRmiCms.dto.MobDriverDto;
import id.co.indocyber.bmmRmiCms.dto.MobEmployeeDto;
import id.co.indocyber.bmmRmiCms.dto.MobMappingTransportirDriverDto;
import id.co.indocyber.bmmRmiCms.dto.MobTransportirDto;
import id.co.indocyber.bmmRmiCms.entity.MRole;
import id.co.indocyber.bmmRmiCms.entity.MUser;
import id.co.indocyber.bmmRmiCms.entity.MUserRole;
import id.co.indocyber.bmmRmiCms.entity.MobCustomer;
import id.co.indocyber.bmmRmiCms.entity.MobDriver;
import id.co.indocyber.bmmRmiCms.entity.MobEmployee;
import id.co.indocyber.bmmRmiCms.entity.MobTransportir;
import id.co.indocyber.bmmRmiCms.service.UMUserSvc;
import id.co.indocyber.bmmRmiCms.tools.StringUtil;
import ma.glasnost.orika.MapperFacade;

@Service("uMUserSvc")
@Transactional
public class UMUserSvcImpl extends BaseDaoImpl implements UMUserSvc {
	
	@Autowired
	private MUserDao mUserDao;
	
	@Autowired
	private MRoleDao mRoleDao;
	
	@Autowired
	private MUserRoleDao mUserRoleDao;
	
	@Autowired
	private MobDriverDao mobDriverDao;
	
	@Autowired
	private MobCustomerDao mobCustomerDao;
	
	@Autowired
	private MobEmployeeDao mobEmployeeDao;
	
	@Autowired
	private MobTransportirDao mobTransportirDao;
	
	@Autowired
	private MapperFacade mapperFacade;
	
	public String hash(String username, String pass){
		String toHash = username+"berkah"+pass+"manisindo";
		return DigestUtils.sha1Hex(toHash);
	}
	
	@Override
	public Map<String, Object> findAll(String search, Integer pageNo, Integer perPage ,String colName, boolean sortAsc) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		
		String sort = "";
		if(sortAsc){
			sort = "asc";
		}else{
			sort = "desc";
		}
		
		String sql = "select a.email, a.username, "
				+ "		a.fullname, a.photo_text, "
				+ "		a.password, a.nipeg, "
				+ "		a.person_id, a.phone_num, "
				+ "		a.IMEI, a.token, "
				+ "		a.pswd_default, a.reference_code, "
				+ "		a.is_active, a.created_by, "
				+ "		a.created_date, a.modified_by, "
				+ "		a.modified_date, b.role_id, "
				+ "		b.module_apps, c.role_name "
				+ "	from m_user a "
				+ "		join m_user_role b on a.email = b.email "
				+ "		join m_role c on b.role_id = c.id "
				+ "	where upper(a.username) like upper('"+search+"') "
				+ "		or upper(a.email) like upper('"+search+"') "
				+ "		or upper(a.fullname) like upper('"+search+"') "
				+ "		or upper(c.role_name) like upper('"+search+"') "
				+ "		or a.imei like '"+search+"' "
				+ "		or upper(case "
				+ "			when a.is_active = 1 then 'Aktif' "
				+ "			when a.is_active = 0 then 'Non Aktif' "
				+ "		end) like upper('"+search+"')";		
		Pageable pageable = new PageRequest((pageNo-1)*perPage, perPage,
				new Sort(new Sort.Order(Direction.fromString(sort),colName)));
		Map<String, Object> mapList = getPaging(sql, pageable);
		@SuppressWarnings("unchecked")
		List<Object[]> listObj = (List<Object[]>) mapList.get("contentData");
		List<MUserDto> listDto = new ArrayList<>();
		for(Object[] o : listObj){
//			MUserDto dto = mapperFacade.map(o[0], MUserDto.class);
			MUserDto dto = new MUserDto();
			dto.setEmail((String) o[0]);
			dto.setUsername((String) o[1]);
			dto.setFullname((String) o[2]);
			dto.setPhotoText((String) o[3]);
			dto.setPassword((String) o[4]);
			dto.setNipeg((String) o[5]);
			dto.setPersonId((Integer) o[6]);
			dto.setPhoneNum((String) o[7]);
			dto.setImei((String) o[8]);
			
			dto.setPswdDefault((String) o[10]);
			dto.setReferenceCode((String) o[11]);
			dto.setIsActive((Boolean) o[12]);
//			byte isActive = (byte) o[12];
//			if(isActive == 1){
//				dto.setIsActive(true);
//			}else{
//				dto.setIsActive(false);
//			}
			dto.setCreatedBy((String) o[13]);
			dto.setCreatedDate((Timestamp) o[14]);
			dto.setModifiedBy((String) o[15]);
			dto.setModifiedDate((Timestamp) o[16]);
			
			dto.setRoleId((Integer) o[17]);
			dto.setModuleApps((String) o[18]);
			dto.setRoleName((String) o[19]);
			listDto.add(dto);			
		}
		Integer totalSIze = (Integer) mapList.get("totalRecords");
		Map<String, Object> map = new HashMap<>();
		map.put("content", listDto);
		map.put("totalSize", totalSIze);
		return map;
	}

	@Override
	public void save(MUserDto dto) {
		try{
			
			MUser mUser = mapperFacade.map(dto, MUser.class);			
			if(dto.isRehash() == true){
				mUser.setPassword(hash(dto.getUsername().trim(),dto.getPassword().trim()));				
			}else{
				mUser.setPassword(dto.getPassword());
			}
			
			if(dto.isAddMode() == true && dto.getModuleApps().equalsIgnoreCase("Mobile") && dto.isRehash() == true){
				mUser.setPassword(hash(dto.getUsername(),"bmm2019"));
				mUser.setPswdDefault(hash(dto.getUsername(),"bmm2019"));				
			}else{
				mUser.setPswdDefault(hash(dto.getUsername(),"bmm2019"));
			}
			
			System.err.println("Svc : Pass UI  = "+dto.getPassword());
			System.err.println("Svc : Pass SVC = "+mUser.getPassword());
			mUserDao.save(mUser);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public Integer countAll(String search) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		return ((Long)mUserDao.countByArg(search)).intValue();
	}

	@Override
	public boolean existsEmail(String username, String email) {
		return false;
	}

	@Override
	public boolean existsMobile(String username, String phoneNum) {
		return false;
	}

	@Override
	public List<MobEmployeeDto> getListEmployee(String search) {
		try{
			search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
			List<MobEmployee> listObj = mobEmployeeDao.findAllDataActive(search);
			List<MobEmployeeDto> listtDto = new ArrayList<>();
			for(MobEmployee m : listObj){
				MobEmployeeDto dto = mapperFacade.map(m, MobEmployeeDto.class);
				listtDto.add(dto);
			}
			return listtDto;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<MRoleDto> getListRole(String search) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		List<MRole> listObj = mRoleDao.findAllDataActive(search);
		List<MRoleDto> listDto = new ArrayList<>();
		for(MRole m : listObj){
			MRoleDto dto = mapperFacade.map(m, MRoleDto.class);
			listDto.add(dto);
		}
		return listDto;
	}


	@Override
	public Integer cekDataByMail(String email) {
		return mUserDao.countDataByMail(email);
	}


	@Override
	public Integer cekDataByUsername(String username) {
		return mUserDao.countDataByUsername(username);
	}


	@Override
	public Map<String, Object> findByTransporterId(String transporterId, String search, Integer pageNo, String colName,
			boolean sortAsc) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		String sort = "";
		if(sortAsc){
			sort = "asc";
		}else{
			sort = "desc";
		}
		
		int pageSize = 25;
		
		Pageable pageable = new PageRequest((pageNo-1)*pageSize, pageSize,
				new Sort(new Sort.Order(Direction.fromString(sort),colName)));
		
		String sql = "select DISTINCT a.email, a.username, a.fullname, "
					+ "		a.password, a.person_id, a.imei, "
					+ "		a.reference_code, a.is_active, a.created_by,  "
					+ "		a.created_date, a.modified_by, a.modified_date, "
					+ "		c.role_id, c.module_apps, d.role_name "
					+ " from m_user a "
					+ " 	join m_user_role c on a.email = c.email "
					+ " 	join m_role d on c.role_id = d.id "
					+ "	where "
					+ "		a.reference_code = '"+transporterId+"' "
					+ "		and (upper(a.username) like upper('"+search+"') "
					+ "			or upper(a.email) like upper('"+search+"') "
					+ "			or upper(a.fullname) like upper('"+search+"') "
					+ "			or upper(d.role_name) like upper('"+search+"') "
					+ "			or a.imei like '"+search+"' "
					+ "			or upper(case "
					+ "						when a.is_active = 1 then 'Aktif' "
					+ "						when a.is_active = 0 then 'Non Aktif' "
					+ "					end) like upper('"+search+"')) "
					+ "  UNION "
					+ "select DISTINCT a.email, a.username, a.fullname, "
					+ "		a.password, a.person_id, a.imei, "
					+ "		a.reference_code, a.is_active, a.created_by,  "
					+ "		a.created_date, a.modified_by, a.modified_date, "
					+ "		c.role_id, c.module_apps, d.role_name "
					+ "from "
					+ "m_user a "
					+ "join "
					+ "mob_mapping_transportir_driver b on "
					+ "a.reference_code=b.driver_id "
					+ "join "
					+ "m_user_role c  "
					+ "on a.email = c.email "
					+ "join "
					+ "m_role d  "
					+ "on c.role_id = d.id "
					+ " where "
					+ "b.transportir_code = '"+transporterId+"' "
					+ "and c.module_apps = 'Mobile' "
					+ "and (upper(a.username) like upper('"+search+"') "
					+ "			or upper(a.email) like upper('"+search+"') "
					+ "			or upper(a.fullname) like upper('"+search+"') "
					+ "			or upper(d.role_name) like upper('"+search+"') "
					+ "			or a.imei like '"+search+"' "
					+ "			or upper(case "
					+ "						when a.is_active = 1 then 'Aktif' "
					+ "						when a.is_active = 0 then 'Non Aktif' "
					+ "					end) like upper('"+search+"')) ";
		Map<String, Object> mapList = getPaging(sql, pageable);
		@SuppressWarnings("unchecked")
		List<Object[]> listObj = (List<Object[]>) mapList.get("contentData");
		List<MUserDto> listDto = new ArrayList<>();
		for(Object[] m : listObj){
			MUserDto dto = new MUserDto();
			dto.setEmail((String) m[0]);
			dto.setUsername((String) m[1]);
			dto.setFullname((String) m[2]);
			dto.setPassword((String) m[3]);
			dto.setPersonId((Integer) m[4]);
			dto.setImei((String) m[5]);
			dto.setReferenceCode((String) m[6]);
			dto.setIsActive((Byte) m[7]==0?false:true);
			dto.setCreatedBy((String) m[8]);
			dto.setCreatedDate((Timestamp) m[9]);
			dto.setModifiedBy((String) m[10]);
			dto.setModifiedDate((Timestamp) m[11]);
			dto.setRoleId((Integer) m[12]);
			dto.setModuleApps((String) m[13]);
			dto.setRoleName((String) m[14]);
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("content", listDto);
		map.put("totalSize", mapList.get("totalRecords"));		
		return map;
	}


	@Override
	public Integer countByTransporterId(String transporterId, String search) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");		
		return mUserDao.countUserByTransporterId(transporterId, search).intValue();
	}


	@Override
	public void saveUserRole(MUserRoleDto dto) {
		MUserRole r = mapperFacade.map(dto, MUserRole.class);
		mUserRoleDao.save(r);		
	}


	@Override
	public List<String> getListModul() {
		List<String> listModule = new ArrayList<>();		
		listModule.add("Mobile");
		listModule.add("Web");
		return listModule;
	}


	@Override
	public List<MobDriverDto> getAllListDriver(String search) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		List<MobDriver> listObj = mobDriverDao.findAvailableUserDriver(search);
		List<MobDriverDto> listDto = new ArrayList<>();
		for(MobDriver d : listObj){
			MobDriverDto dto = mapperFacade.map(d, MobDriverDto.class);
			listDto.add(dto);
		}
		return listDto;
	}


	@Override
	public List<MobDriverDto> getListDriverByTransportir(String tranportirCode, String search) {
		try{
			search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
			List<MobDriver> listObj = mobDriverDao.findAvailableUserDriverByTransportir(tranportirCode, search);
			List<MobDriverDto> listDto = new ArrayList<>();
			for(MobDriver d : listObj){
				MobDriverDto dto = mapperFacade.map(d, MobDriverDto.class);
				listDto.add(dto);
			}
			return listDto;			
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}


	@Override
	public List<MobTransportirDto> getListTransportir(String search) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		List<MobTransportir> listObj = mobTransportirDao.findAllActive(search);
		List<MobTransportirDto> listDto = new ArrayList<>();
		for(MobTransportir t : listObj){
			MobTransportirDto dto =mapperFacade.map(t, MobTransportirDto.class);
			listDto.add(dto);
		}
		return listDto;
	}


	@Override
	public MobTransportirDto findOneTransportir(String code) {
		MobTransportir t = mobTransportirDao.findOne(code);
		MobTransportirDto dto = mapperFacade.map(t, MobTransportirDto.class);
		return dto;
	}


	@Override
	public List<MobCustomerDto> getListCustomer(String search) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		List<MobCustomer> listObj = mobCustomerDao.findAllData(search);
		List<MobCustomerDto> listDto = new ArrayList<>();
		for(MobCustomer c : listObj){
			MobCustomerDto dto = mapperFacade.map(c, MobCustomerDto.class);
			listDto.add(dto);
		}
		return listDto;
	}


}
