package id.co.indocyber.bmmRmiCms.service.impl;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dao.MobDriverDao;
import id.co.indocyber.bmmRmiCms.dao.MobMappingTransportirDriverDao;
import id.co.indocyber.bmmRmiCms.dao.MobTransportirDao;
import id.co.indocyber.bmmRmiCms.dto.MobDriverDto;
import id.co.indocyber.bmmRmiCms.dto.MobMappingTransportirDriverDto;
import id.co.indocyber.bmmRmiCms.dto.MobTransportirDto;
import id.co.indocyber.bmmRmiCms.entity.MobDriver;
import id.co.indocyber.bmmRmiCms.entity.MobMappingTransportirDriver;
import id.co.indocyber.bmmRmiCms.entity.MobMappingTransportirDriverPK;
import id.co.indocyber.bmmRmiCms.entity.MobTransportir;
import id.co.indocyber.bmmRmiCms.service.MappingTransporterSupirSvc;
import id.co.indocyber.bmmRmiCms.tools.StringUtil;
import ma.glasnost.orika.MapperFacade;


@Service("mappingTransporterSupirSvc")
@Transactional
public class MappingTransporterSupirSvcImpl extends BaseDaoImpl implements MappingTransporterSupirSvc {
	
	@Autowired
	private MapperFacade mapperFacade;
	
	@Autowired
	private MobDriverDao mobDriverDao;
	
	@Autowired
	private MobTransportirDao mobTransportirDao;
	
	@Autowired
	private MobMappingTransportirDriverDao mobMappingTransportirDriverDao;
	
	@Override
	public List<MobMappingTransportirDriverDto> findAll(String search, int pageNo, int perPage,
			String orderBy, String direction) {
		
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		Pageable pageable = new PageRequest(pageNo - 1, perPage,
				new Sort(new Sort.Order(Direction.fromString(direction),
						orderBy)));
		List<Object[]> listObj = mobMappingTransportirDriverDao.findAllData(search, pageable);
		List<MobMappingTransportirDriverDto> listDto = new ArrayList<>();
		for(Object[] o : listObj){
			MobMappingTransportirDriverDto dto = mapperFacade.map(o[0], MobMappingTransportirDriverDto.class);
			dto.setTransportirName((String) o[1]);
			dto.setDriverName((String) o[2]);
			dto.setDriverPhone((String) o[3]);
			dto.setDriverGender((String) o[4]);
			dto.setDriverBirthday((Date) o[5]);
			if(dto.getDriverBirthday() != null){
				dto.setDriverAge(mobDriverDao.getAgeDriver(dto.getDriverBirthday()).intValue());
			}else{
				dto.setDriverAge(0);
			}
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public Integer countAll(String search) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		return mobMappingTransportirDriverDao.countAllData(search).intValue();
	}

	@Override
	public List<MobDriverDto> getListDriver(String search) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		List<MobDriver> listObj = mobDriverDao.findAvailableDriver(search);
		List<MobDriverDto> listDto = new ArrayList<>();
		for(MobDriver d : listObj){
			MobDriverDto dto = mapperFacade.map(d, MobDriverDto.class);
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public int save(MobMappingTransportirDriverDto dto) {
		try{
			MobMappingTransportirDriver d = mapperFacade.map(dto, MobMappingTransportirDriver.class);
			mobMappingTransportirDriverDao.save(d);
			return 1;
		}catch(Exception e){
			e.printStackTrace();
			return 0;
		}		
	}

	@Override
	public List<MobMappingTransportirDriverDto> findByTransportir(String id, String search, int pageNo, int perPage,
			String orderBy, String direction) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		Pageable pageable = new PageRequest(pageNo - 1, perPage,
				new Sort(new Sort.Order(Direction.fromString(direction),
						orderBy)));
		List<Object[]> listObj = mobMappingTransportirDriverDao.findDataByTransportir(id, search, pageable);
		List<MobMappingTransportirDriverDto> listDto = new ArrayList<>();
		for(Object[] o : listObj){
			MobMappingTransportirDriverDto dto = mapperFacade.map(o[0], MobMappingTransportirDriverDto.class);
			dto.setTransportirName((String) o[1]);
			dto.setDriverName((String) o[2]);
			dto.setDriverPhone((String) o[3]);
			dto.setDriverGender((String) o[4]);
			dto.setDriverBirthday((Date) o[5]);
			if(dto.getDriverBirthday() != null){
				dto.setDriverAge(mobDriverDao.getAgeDriver(dto.getDriverBirthday()).intValue());
			}else{
				dto.setDriverAge(0);
			}
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public Integer countByTransportir(String id, String search) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		return mobMappingTransportirDriverDao.countDataByTransportir(id, search).intValue();
	}

	@Override
	public List<MobTransportirDto> getListTransporter(String search) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		List<MobTransportir> listObj = mobTransportirDao.findAllActive(search);
		List<MobTransportirDto> listDto = new ArrayList<>();
		for(MobTransportir t : listObj){
			MobTransportirDto dto = mapperFacade.map(t, MobTransportirDto.class);
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public void saveAll(List<MobMappingTransportirDriverDto> list, String created) {
		try{
			for(MobMappingTransportirDriverDto dto : list){
				MobMappingTransportirDriver m = mapperFacade.map(dto, MobMappingTransportirDriver.class);
				m.setIsActive(true);
				m.setCreated(created);
				m.setCreatedDate(new Timestamp(System.currentTimeMillis()));
				m.setModified(created);
				m.setModifiedDate(new Timestamp(System.currentTimeMillis()));
				mobMappingTransportirDriverDao.save(m);
			}					
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public List<MobDriverDto> getListDriverByTransporter(String transporterCode) {
		List<MobDriver> listObj = mobDriverDao.findByTransporter(transporterCode);
		List<MobDriverDto> listDto = new ArrayList<>();
		for(MobDriver m : listObj){
			MobDriverDto dto = mapperFacade.map(m, MobDriverDto.class);
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public int delete(MobMappingTransportirDriverDto dto) {
		try{
			MobMappingTransportirDriverPK pk = new MobMappingTransportirDriverPK();
			pk.setDriverId(dto.getDriverId());
			pk.setTransportirCode(dto.getTransportirCode());
			mobMappingTransportirDriverDao.delete(pk);			
			return 1;
		}catch(Exception e){
			e.printStackTrace();
			return 0;			
		}
	}
	
	@Override
	public Map<String, Object> findByArg(String search, Integer pageNo, int perPage, String colName, boolean sortAsc) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%"); 
				
		String sql = "select a.id, a.fullname, a.birthday, "
				+ "		case "
				+ "			when upper(a.gender) = upper('1') then 'Laki-laki' "
				+ "			when upper(a.gender) = upper('2') then 'Perempuan' "
				+ "		end as gender, "
				+ "b.is_active, c.transportir_name, "
				+ "TIMESTAMPDIFF(YEAR, a.birthday, CURDATE()) as age, b.transportir_code, "
				+ "b.created, b.created_date, b.modified, b.modified_date "
				+ "from mob_driver a "
				+ " join mob_mapping_transportir_driver b on a.id = b.driver_id "
				+ " join mob_transportir c on b.transportir_code = c.code "
				+ "where "
				+ "a.id like ('"+search+"') "
				+ "or upper(a.fullname) like upper('"+search+"') "
				+ "or upper(c.transportir_name) like upper('"+search+"') "
				+ "			or upper(case "
				+ "				when upper(a.gender) = upper('1') then 'Laki-laki' "
				+ "				when upper(a.gender) = upper('2') then 'Perempuan' "
				+ "			end) like upper('"+search+"') "
				+ "		or upper(case "
				+ "				when b.is_active is true then 'Aktif' "
				+ "				when b.is_active is false then 'Non Aktif' "
				+ "				end) like upper('"+search+"') "
				+ "or TIMESTAMPDIFF(YEAR, a.birthday, CURDATE()) like ('"+search+"') ";
		
		String sort = "";
		if(sortAsc){
			sort = "asc";
		}else{
			sort = "desc";
		}
		int pageSize = 25;
		Pageable pageable = new PageRequest((pageNo-1)*pageSize, pageSize,
				new Sort(new Sort.Order(Direction.fromString(sort),colName)));
		
		Map<String, Object> mapList = getPaging(sql, pageable);
		
//		List<MobDriver> listObj = mobDriverDao.findAllDataByArg(sql, new PageRequest(pageNo-1, 10, generateSort(colName,sortAsc)));
		@SuppressWarnings("unchecked")
		List<Object[]> listObj = (List<Object[]>) mapList.get("contentData");
		List<MobMappingTransportirDriverDto> listDto = new ArrayList<>();
		for(Object[] m : listObj){
			MobMappingTransportirDriverDto dto = new MobMappingTransportirDriverDto();
			dto.setDriverId((int)m[0]);
			dto.setDriverName((String) m[1]);
			dto.setDriverBirthday((Date) m[2]);
			dto.setDriverGender((String) m[3]);
			dto.setIsActive((boolean)m[4]);
			dto.setTransportirName((String)m[5]);
			if(dto.getDriverBirthday()!=null){
				dto.setDriverAge(((BigInteger) m[6]).intValue());				
			}else{
				dto.setDriverAge(0);
			}
			dto.setTransportirCode((String)m[7]);
			dto.setCreated((String)m[8]);
			dto.setCreatedDate((Timestamp)m[9]);
			dto.setModified((String)m[10]);
			dto.setModifiedDate((Timestamp)m[11]);
			listDto.add(dto);
		}
			
		Integer totalSIze = (Integer) mapList.get("totalRecords");
		Map<String, Object> map = new HashMap<>();
		map.put("content", listDto);
		map.put("totalSize", totalSIze);
		return map;
	}

	@Override
	public MobMappingTransportirDriverDto findOne(int driverId,
			String transportirCode) {
		
		MobMappingTransportirDriver o = mobMappingTransportirDriverDao.findOne(driverId, transportirCode);
			MobMappingTransportirDriverDto dto = new MobMappingTransportirDriverDto();
			dto = mapperFacade.map(o, MobMappingTransportirDriverDto.class);
		
		return dto;
	}



}
