package id.co.indocyber.bmmRmiCms.service;

import id.co.indocyber.bmmRmiCms.dto.MGlobalImageDto;

public interface MGlobalImageSvc {
	
	public MGlobalImageDto findOne(String parmImgCode);
	
}
