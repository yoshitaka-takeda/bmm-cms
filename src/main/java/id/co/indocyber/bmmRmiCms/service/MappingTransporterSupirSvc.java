package id.co.indocyber.bmmRmiCms.service;

import java.util.List;
import java.util.Map;

import id.co.indocyber.bmmRmiCms.dto.MobDriverDto;
import id.co.indocyber.bmmRmiCms.dto.MobMappingTransportirDriverDto;
import id.co.indocyber.bmmRmiCms.dto.MobTransportirDto;

public interface MappingTransporterSupirSvc {
	
	public List<MobMappingTransportirDriverDto> findAll(String search, 
			int pageNo, int perPage, String orderBy, String direction);
	public Integer countAll(String search);

	public Map<String, Object> findByArg(String search, Integer pageNo, int perPage, String colName, boolean sortAsc);

	public List<MobMappingTransportirDriverDto> findByTransportir(String id,
			String search, int pageNo, int perPage, String orderBy, String direction);
	public Integer countByTransportir(String id, String search);
	
	public List<MobDriverDto> getListDriver(String search);
	public List<MobTransportirDto> getListTransporter(String search);
	
	public List<MobDriverDto> getListDriverByTransporter(String transporterCode);
	
	public int save(MobMappingTransportirDriverDto dto);
	public void saveAll(List<MobMappingTransportirDriverDto> list, String created);
	
	public int delete(MobMappingTransportirDriverDto dto);
	
	public MobMappingTransportirDriverDto findOne(int driverId, String transportirCode);
	
	
}
