package id.co.indocyber.bmmRmiCms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dao.MGlobalImageDao;
import id.co.indocyber.bmmRmiCms.dto.MGlobalImageDto;
import id.co.indocyber.bmmRmiCms.entity.MGlobalImage;
import id.co.indocyber.bmmRmiCms.service.MGlobalImageSvc;
import ma.glasnost.orika.MapperFacade;

@Service("mGlobalImageSvc")
@Transactional
public class MGlobalImageSvcImpl implements MGlobalImageSvc {
	
	@Autowired
	private MapperFacade mapperFacade;
	
	@Autowired
	private MGlobalImageDao mGlobalImageDao;
	
	@Override
	public MGlobalImageDto findOne(String parmImgCode) {
		MGlobalImage m = mGlobalImageDao.findOne(parmImgCode);
		MGlobalImageDto dto = mapperFacade.map(m, MGlobalImageDto.class);
		return dto;
	}

}
