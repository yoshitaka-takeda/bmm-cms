package id.co.indocyber.bmmRmiCms.service;

import java.util.List;
import java.util.Map;

import id.co.indocyber.bmmRmiCms.dto.MAddressTypeDto;
import id.co.indocyber.bmmRmiCms.dto.MEducationTypeDto;
import id.co.indocyber.bmmRmiCms.dto.MGenderDto;
import id.co.indocyber.bmmRmiCms.dto.MMaritalStatusDto;
import id.co.indocyber.bmmRmiCms.dto.MReligionDto;
import id.co.indocyber.bmmRmiCms.dto.MobDriverDto;
import id.co.indocyber.bmmRmiCms.dto.MobMappingTransportirDriverDto;

public interface MDSupirSvc {
	
	public Map<String, Object> findByArg(String search, Integer pageNo, int perPage, String colName, boolean sortAsc);
	public MobDriverDto findOne(int id);
	public List<MobDriverDto> findAll(Integer pageNo, String colName, boolean sortAsc);
	public Integer countAllBySearch(String search);
	public Integer countAll();
	
	public Map<String, Object> findByTransportir(String transportirCode, 
			String search, Integer pageNo, Integer perPage ,String colName, boolean sortAsc);
	public Integer countByTransportir(String transportirCode, String search);
	public List<MobDriverDto> findDriverByPhoneNum(String phoneNum);
	public List<MobDriverDto> findDriverByEmail(String email);
	public List<MobDriverDto> findDriverByNoKtp(String noKtp);
	
	public List<MAddressTypeDto> cbListAddress();
	public List<String> cbListProvince();
	public List<MGenderDto> cbListGender();
	public List<MMaritalStatusDto> cbListMarital();
	public List<MReligionDto> cbListReligion();
	public List<MEducationTypeDto> cbListEducation();
	
	public int save(MobDriverDto dto, MobMappingTransportirDriverDto mappingDto);
	public void saveMappingDriver(MobMappingTransportirDriverDto dto);
	
}
