package id.co.indocyber.bmmRmiCms.service;

import java.util.List;

import id.co.indocyber.bmmRmiCms.dto.MVehicleTypeDto;
import id.co.indocyber.bmmRmiCms.dto.MobVehicleDto;

public interface MDKendaraanSvc {
	
	public List<MobVehicleDto> findAll(String search, Integer pageNo, Integer perPage ,String orderBy,boolean direction);
	public List<MobVehicleDto> findAll(Integer pageNo, String orderBy,boolean direction);
	public List<MVehicleTypeDto> listVehicletype();
	public Integer countAll(String search);
	public Integer countAll();
	public int save(MobVehicleDto dto);
	public boolean cekId(String vehicleNo);
	
}
