package id.co.indocyber.bmmRmiCms.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import id.co.indocyber.bmmRmiCms.dto.MobSptDto;

public interface SptBaruSvc {
	
	public List<MobSptDto> findAll(String search, 
			Date startDate, Date endDate, int pageNo, int perPage, String orderBy, String direction);
	public Integer countAll(String search, Date startDate, Date endDate);
	
	public Map<String, Object> findByTransportir(String transportirCode, 
			String search, Date startDate, Date endDate, int pageNo, int perPage, String orderBy, String direction);
	public Map<String, Object> findByCustomer(String custId, 
			String search, Date startDate, Date endDate, int pageNo, int perPage, String orderBy, String direction);
	
	public void updateTransit(Boolean isTransit, Integer sptNo);
	

}
