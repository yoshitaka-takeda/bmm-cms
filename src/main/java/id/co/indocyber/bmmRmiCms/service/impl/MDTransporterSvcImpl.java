package id.co.indocyber.bmmRmiCms.service.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.controller.BaseVmd;
import id.co.indocyber.bmmRmiCms.dao.MobTransportirDao;
import id.co.indocyber.bmmRmiCms.dto.MobDriverDto;
import id.co.indocyber.bmmRmiCms.dto.MobTransportirDto;
import id.co.indocyber.bmmRmiCms.entity.MobDriver;
import id.co.indocyber.bmmRmiCms.entity.MobTransportir;
import id.co.indocyber.bmmRmiCms.service.MDTransporterSvc;
import id.co.indocyber.bmmRmiCms.tools.StringUtil;
import ma.glasnost.orika.MapperFacade;

@Service("mDTransporterSvc")
@Transactional
public class MDTransporterSvcImpl extends BaseDaoImpl implements MDTransporterSvc{
	
	@Autowired
	private MapperFacade mapperFacade;
	
	@Autowired
	private MobTransportirDao mobTransportirDao;
	
	@Override
	public List<MobTransportirDto> findAll(String search, Integer pageNo, Integer perPage ,String colName, boolean sortAsc) {
//		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
//		List<MobTransportir> listObj = mobTransportirDao.findAllData(search, new PageRequest(pageNo-1, perPage, generateSort(colName,sortAsc)));
//		List<MobTransportirDto> listDto = new ArrayList<>();
//		for(MobTransportir o : listObj){
//			MobTransportirDto dto = mapperFacade.map(o, MobTransportirDto.class);
//			listDto.add(dto);
//		}
//		return listDto;
		return null;
	}

	@Override
	public Integer countAll(String search) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		return mobTransportirDao.countAllData(search).intValue();
	}

	@Override
	public int save(MobTransportirDto dto) {
		try{
			MobTransportir m = mapperFacade.map(dto, MobTransportir.class);
			mobTransportirDao.save(m);
			return 1;
		}catch(Exception e){
			e.printStackTrace();
			return 0;			
		}
		
	}

	@Override
	public Integer countAll() {
		return ((Long) mobTransportirDao.count()).intValue();
	}
	
	@Override
	public List<MobTransportirDto> findAll(Integer pageNo, String colName,
			boolean sortAsc) {
//		List<MobTransportir> entities = mobTransportirDao.findAll(
//			new PageRequest(pageNo - 1, 10, generateSort(colName, sortAsc))).getContent();
//		List<MobTransportirDto> listDto = new ArrayList<>();
//		listDto = mapperFacade.mapAsList(entities, MobTransportirDto.class);
		return null;
	}
	
	@Override
	public Map<String, Object> findByArg(String search, Integer pageNo, int perPage, String colName, boolean sortAsc) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%"); 
				
		String sql = "select a.code, a.transportir_name, a.area_text, a.transportir_type, a.vehicle_type, "
				+ "a.price, a.reference_code, a.is_active, a.processed_by, a.processed_date, a.id, a.area_code, a.vendor_name, "
				+ "b.vehicle_type_name "
				+ "from mob_transportir a "
				+ "left join m_vehicle_type b on a.vehicle_type = b.id "
				+ "where "
				+ "a.code like ('"+search+"') "
				+ "or upper(a.transportir_name) like upper('"+search+"') "
				+ "or upper(a.transportir_type) like upper('"+search+"') "
				+ "		or upper(case "
				+ "				when a.is_active is true then 'Aktif' "
				+ "				when a.is_active is false then 'Non Aktif' "
				+ "				end) like upper('"+search+"') "
				+ "or upper(a.reference_code) like upper('"+search+"') ";
		
		String sort = "";
		if(sortAsc){
			sort = "asc";
		}else{
			sort = "desc";
		}
		int pageSize = 25;
		Pageable pageable = new PageRequest((pageNo-1)*pageSize, pageSize,
				new Sort(new Sort.Order(Direction.fromString(sort),colName)));
		
		Map<String, Object> mapList = getPaging(sql, pageable);
		

//		List<MobDriver> listObj = mobDriverDao.findAllDataByArg(sql, new PageRequest(pageNo-1, 10, generateSort(colName,sortAsc)));
		@SuppressWarnings("unchecked")
		List<Object[]> listObj = (List<Object[]>) mapList.get("contentData");
		List<MobTransportirDto> listDto = new ArrayList<>();
		for(Object[] m : listObj){
			MobTransportirDto dto = new MobTransportirDto();
			dto.setCode((String)m[0]);
			dto.setTransportirName((String)m[1]);
			dto.setTransportirType((String)m[3]);
			dto.setVehicleType((String)m[4]);
			dto.setPrice((BigDecimal)m[5]);
			dto.setReferenceCode((String)m[6]);
			dto.setIsActive((boolean)m[7]);
			dto.setProcessedBy((String)m[8]);
			dto.setProcessedDate((Timestamp)m[9]);
			dto.setAreaCode((String)m[11]);
			dto.setVendorName((String)m[12]);
			dto.setVehicleTypeText((String)m[13]);
			listDto.add(dto);
		}
			
		Integer totalSIze = (Integer) mapList.get("totalRecords");
		Map<String, Object> map = new HashMap<>();
		map.put("content", listDto);
		map.put("totalSize", totalSIze);
		return map;
	}


}
