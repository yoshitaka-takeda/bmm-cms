package id.co.indocyber.bmmRmiCms.service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.zkoss.image.AImage;

import com.google.gson.Gson;

import id.co.indocyber.bmmRmiCms.dao.MGlobalImageDao;
import id.co.indocyber.bmmRmiCms.dao.MVehicleTypeDao;
import id.co.indocyber.bmmRmiCms.dao.MobDriverDao;
import id.co.indocyber.bmmRmiCms.dao.MobGoodsReceiveAttachmentDao;
import id.co.indocyber.bmmRmiCms.dao.MobGoodsReceiveDao;
import id.co.indocyber.bmmRmiCms.dao.MobMappingTransportirDriverDao;
import id.co.indocyber.bmmRmiCms.dao.MobSjDao;
import id.co.indocyber.bmmRmiCms.dao.MobSptDao;
import id.co.indocyber.bmmRmiCms.dto.MVehicleTypeDto;
import id.co.indocyber.bmmRmiCms.dto.MobDriverDto;
import id.co.indocyber.bmmRmiCms.dto.MobGoodsReceiveAttachmentDto;
import id.co.indocyber.bmmRmiCms.dto.MobGoodsReceiveDto;
import id.co.indocyber.bmmRmiCms.dto.MobSjDto;
import id.co.indocyber.bmmRmiCms.entity.MVehicleType;
import id.co.indocyber.bmmRmiCms.entity.MobDriver;
import id.co.indocyber.bmmRmiCms.entity.MobGoodsReceive;
import id.co.indocyber.bmmRmiCms.entity.MobGoodsReceiveAttachment;
import id.co.indocyber.bmmRmiCms.service.SjBaruSvc;
import id.co.indocyber.bmmRmiCms.tools.DateUtil;
import id.co.indocyber.bmmRmiCms.tools.StringUtil;
import ma.glasnost.orika.MapperFacade;

@Service("sjBaruSvc")
@Transactional
public class SjBaruSvcImpl extends BaseDaoImpl implements SjBaruSvc{
	
	@Autowired
	private MapperFacade mapperFacade;
	
	@Autowired
	private MobSjDao mobSjDao;
	
	@Autowired
	private MobSptDao mobSptDao;
		
	@Autowired
	private MobDriverDao mobDriverDao;

	@Autowired
	private MGlobalImageDao mGlobalImageDao;	
	
	@Autowired
	private MVehicleTypeDao mVehicleTypeDao;
	
	@Autowired
	private MobGoodsReceiveDao mobGoodsReceiveDao;
	
	@Autowired
	private MobGoodsReceiveAttachmentDao mobGoodsReceiveAttachmentDao;
	
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");		
	
	@Override
	public Map<String, Object> findAll(String search, 
			Date startDate, Date endDate, int pageNo, int perPage, String orderBy, String direction) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		if(startDate == null){
			startDate = DateUtil.getMinDate();
		}
		
		if(endDate == null){
			endDate = DateUtil.getMaxDate();
		}

		String tempStartDate = df.format(startDate);
		String tempEndDate = df.format(endDate);		
		
		Pageable pageable = new PageRequest((pageNo-1)*10, perPage,
				new Sort(new Sort.Order(Direction.fromString(direction),orderBy)));
		String sql = "select a.sj_no, a.sj_date, a.spt_no,"
				+ "		a.spm_no, a.segel1, a.segel2,"
				+ "		a.segel3, a.segel4, a.segel5,"
				+ "		a.segel6, a.segel7, a.segel8, "
				+ "		a.cust_code, a.cust_name, a.address_ship_to, "
				+ "		a.transportir_code, a.transportir_name, a.address_ship_from, "
				+ "		a.item_code, a.item_name, a.qty_sppb, "
				+ "		a.vehicle_no, a.vehicle_type, a.driver_id, "
				+ "		a.phone, a.qty, a.container_no, "
				+ "		a.departing_from_latitude, a.departing_from_longitude, a.departing_from_date, "
				+ "		a.departing_from_by, a.first_destination_latitude, a.first_destination_longitude, "
				+ "		a.first_destination_date, a.first_destination_by, a.transit_latitude, "
				+ "		a.transit_longitude, a.transit_date, a.transit_by, "
				+ "		a.destination_latitude, a.destination_longitude, a.destination_date, "
				+ "		a.destination_by, a.pin, a.status, "
				+ "		a.created, a.created_date, a.modified, "
				+ "		a.modified_date, a.destination_from_by, a.destination_from_date, "
				+ "		a.transportir_id, b.fullname, "
				+ "		case "
				+ "			when upper(a.status) = upper('o') then 'Open' "
				+ "			when upper(a.status) = upper('a') then 'Approved' "
				+ "			when upper(a.status) = upper('l') then 'Batal' "
				+ "			when upper(a.status) = upper('i') then 'In Wighbridge' "
				+ "			when upper(a.status) = upper('t') then 'Tapping' "
				+ "			when upper(a.status) = upper('c') then 'Selesai' "
				+ "			when upper(a.status) = upper('r') then 'Selesai supir 1' "
				+ "		end, "
				+ "		c.vehicle_type_name, d.is_transit as transitSpt "					
				+ "	from mob_sj a "
				+ "		left join mob_driver b on a.driver_id = b.id "
				+ "		left join m_vehicle_type c on a.vehicle_type = c.id "
				+ "		join mob_spt d on a.spt_no = d.spt_no "
				+ " where upper(a.status) in (upper('r'), upper('o')) "
				+ "		and (date_format(a.sj_date, '%Y-%m-%d') between '"+tempStartDate+"' and '"+tempEndDate+"') "
				+ "		and	(a.spt_no like '"+search+"' "
				+ "			or a.sj_no like '"+search+"' "
				+ "			or a.sj_date like '"+search+"' "
				+ "			or upper(a.cust_name) like upper('"+search+"')"
				+ "			or upper(a.transportir_name) like upper('"+search+"')"
				+ "			or upper(case "
				+ "				when upper(a.status) = upper('o') then 'Open' "
				+ "				when upper(a.status) = upper('a') then 'Approved' "
				+ "				when upper(a.status) = upper('l') then 'Batal' "
				+ "				when upper(a.status) = upper('i') then 'In Wighbridge' "
				+ "				when upper(a.status) = upper('t') then 'Tapping' "
				+ "				when upper(a.status) = upper('c') then 'Selesai' "
				+ "				when upper(a.status) = upper('r') then 'Selesai supir 1' "
				+ "			end) like upper('"+search+"')"
				+ "			or date_format(a.sj_date,'%d-%b-%Y') like '"+search+"' "
				+ "			or upper(b.fullname) like upper('"+search+"') "
				+ "			or a.phone like '"+search+"')";
	Map<String, Object> mapList = getPaging(sql, pageable);
	@SuppressWarnings("unchecked")
	List<Object[]> listObj = (List<Object[]>) mapList.get("contentData");
	List<MobSjDto> listDto = new ArrayList<>();
	for(Object[] o : listObj){
		MobSjDto dto = new MobSjDto();
		dto.setSjNo((Integer) o[0]);
		dto.setSjDate((Date) o[1]);
		dto.setSptNo((Integer) o[2]);
		dto.setSpmNo((String) o[3]);
		dto.setSegel1((String) o[4]);
		dto.setSegel2((String) o[5]);
		dto.setSegel3((String) o[6]);
		dto.setSegel4((String) o[7]);
		dto.setSegel5((String) o[8]);
		dto.setSegel6((String) o[9]);
		dto.setSegel7((String) o[10]);
		dto.setSegel8((String) o[11]);
		dto.setCustCode((String) o[12]);
		dto.setCustName((String) o[13]);
		dto.setAddressShipTo((String) o[14]);
		dto.setTransportirCode((String) o[15]);
		dto.setTransportirName((String) o[16]);
		dto.setAddressShipFrom((String) o[17]);
		dto.setItemCode((String) o[18]);
		dto.setItemName((String) o[19]);
		dto.setQtySppb((BigDecimal) o[20]);
		dto.setVehicleNo((String) o[21]);
		dto.setVehicleType((Integer) o[22]);
		dto.setDriverId((Integer) o[23]);
		dto.setPhone((String) o[24]);
		dto.setQty((BigDecimal) o[25]);
		dto.setContainerNo((String) o[26]);
		dto.setDepartingFromLatitude((BigDecimal) o[27]);
		dto.setDepartingFromLongitude((BigDecimal) o[28]);
		dto.setDepartingFromDate((Date) o[29]);
		dto.setDepartingFromBy((String) o[30]);
		dto.setFirstDestinationLatitude((BigDecimal) o[31]);
		dto.setFirstDestinationLongitude((BigDecimal) o[32]);
		dto.setFirstDestinationDate((Date) o[33]);
		dto.setFirstDestinationBy((String) o[34]);
		dto.setTransitLatitude((BigDecimal) o[35]);
		dto.setTransitLongitude((BigDecimal) o[36]);
		
		dto.setTransitDate((Date) o[37]);
		dto.setTransitBy((String) o[38]);
		dto.setDestinationLatitude((BigDecimal) o[39]);
		dto.setDestinationLongitude((BigDecimal) o[40]);
		dto.setDestinationDate((Date) o[41]);
		dto.setDestinationBy((String) o[42]);

		dto.setPin((String) o[43]);
//		dto.setStatus((String) o[44]);
		dto.setCreated((String) o[45]);
		dto.setCreatedDate((Date) o[46]);
		dto.setModified((String) o[47]);
		dto.setModifiedDate((Date) o[48]);
		dto.setDestinationFromBy((String) o[49]);
		dto.setDestinationFromDate((Date) o[50]);
		
		dto.setTransportirId((Integer) o[51]);
		dto.setDriverName((String) o[52]);
		dto.setStatusText((String) o[53]);
		dto.setVehicleTypeText((String) o[54]);
		
		byte byteValue = (byte) o[55];
		dto.setFlagVisibleKonfirmasi(false);
		dto.setFlagVisiblePenugasan(false);
		
		if(byteValue == 1){
			dto.setIsTransitSpt(true);
			List<Object[]> tempList = mobGoodsReceiveDao.getDataBySjNo(dto.getSjNo());
			if(tempList.size() > 0){
				dto.setFlagVisibleKonfirmasi(true);
				dto.setFlagVisiblePenugasan(false);
			}else{
				dto.setFlagVisibleKonfirmasi(false);
				dto.setFlagVisiblePenugasan(true);				
			}
		}else{
			dto.setIsTransitSpt(false);
			dto.setFlagVisibleKonfirmasi(true);
			dto.setFlagVisiblePenugasan(false);
		}
<<<<<<< HEAD
=======
		
>>>>>>> branch 'master' of https://gitlab.com/ari.haryanto/rmi-spt-sj.git
		if(dto.getDepartingFromLatitude() != null && dto.getDepartingFromLongitude() != null){
			dto.setDepartingFromLatitudeLongitude(dto.getDepartingFromLatitude() + ", "+ dto.getDepartingFromLongitude());
		}else if(dto.getDepartingFromLatitude() == null && dto.getDepartingFromLongitude() != null){
			dto.setDepartingFromLatitudeLongitude(dto.getDepartingFromLongitude().toString());
		}else if(dto.getDepartingFromLatitude() != null && dto.getDepartingFromLongitude() == null){
			dto.setDepartingFromLatitudeLongitude(dto.getDepartingFromLatitude().toString());
		}else{
			dto.setDepartingFromLatitudeLongitude("");
		}
				
		listDto.add(dto);
	}
	
//	Integer totalSize = mobSjDao.countAllDataNewTransportir(transportirCode, search).intValue();
	Map<String, Object> map = new HashMap<>();
	map.put("content", listDto);
	map.put("totalSize", mapList.get("totalRecords"));		
	return map;
	}

	@Override
	public Map<String, Object> findDataNewByTransportir(String transportirCode, 
			String search, Date startDate, Date endDate, int pageNo, int perPage,
			String orderBy, String direction) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		if(startDate == null){
			startDate = DateUtil.getMinDate();
		}
		
		if(endDate == null){
			endDate = DateUtil.getMaxDate();
		}

		String tempStartDate = df.format(startDate);
		String tempEndDate = df.format(endDate);		
		
		Pageable pageable = new PageRequest((pageNo-1)*10, perPage,
				new Sort(new Sort.Order(Direction.fromString(direction),orderBy)));
		String sql = "select a.sj_no, a.sj_date, a.spt_no,"
					+ "		a.spm_no, a.segel1, a.segel2,"
					+ "		a.segel3, a.segel4, a.segel5,"
					+ "		a.segel6, a.segel7, a.segel8, "
					+ "		a.cust_code, a.cust_name, a.address_ship_to, "
					+ "		a.transportir_code, a.transportir_name, a.address_ship_from, "
					+ "		a.item_code, a.item_name, a.qty_sppb, "
					+ "		a.vehicle_no, a.vehicle_type, a.driver_id, "
					+ "		a.phone, a.qty, a.container_no, "
					+ "		a.departing_from_latitude, a.departing_from_longitude, a.departing_from_date, "
					+ "		a.departing_from_by, a.first_destination_latitude, a.first_destination_longitude, "
					+ "		a.first_destination_date, a.first_destination_by, a.transit_latitude, "
					+ "		a.transit_longitude, a.transit_date, a.transit_by, "
					+ "		a.destination_latitude, a.destination_longitude, a.destination_date, "
					+ "		a.destination_by, a.pin, a.status, "
					+ "		a.created, a.created_date, a.modified, "
					+ "		a.modified_date, a.destination_from_by, a.destination_from_date, "
					+ "		a.transportir_id, b.fullname, "
					+ "		case "
					+ "			when upper(a.status) = upper('o') then 'Open' "
					+ "			when upper(a.status) = upper('a') then 'Approved' "
					+ "			when upper(a.status) = upper('l') then 'Batal' "
					+ "			when upper(a.status) = upper('i') then 'In Wighbridge' "
					+ "			when upper(a.status) = upper('t') then 'Tapping' "
					+ "			when upper(a.status) = upper('c') then 'Selesai' "
					+ "			when upper(a.status) = upper('r') then 'Selesai supir 1' "
					+ "		end, "
					+ "		c.vehicle_type_name, d.is_transit as transitSpt "					
					+ "	from mob_sj a "
					+ "		left join mob_driver b on a.driver_id = b.id "
					+ "		left join m_vehicle_type c on a.vehicle_type = c.id "
					+ "		join mob_spt d on a.spt_no = d.spt_no "
					+ " where upper(a.status) in (upper('r'), upper('o')) "
					+ "		and a.transportir_code = '"+transportirCode+"' "
					+ "		and (date_format(a.sj_date, '%Y-%m-%d') between '"+tempStartDate+"' and '"+tempEndDate+"') "
					+ "		and (a.spt_no like '"+search+"' "
					+ "			or a.sj_no like '"+search+"' "
					+ "			or a.sj_date like '"+search+"' "
					+ "			or upper(a.cust_name) like upper('"+search+"')"
					+ "			or upper(a.transportir_name) like upper('"+search+"')"
					+ "			or upper(case "
					+ "				when upper(a.status) = upper('o') then 'Open' "
					+ "				when upper(a.status) = upper('a') then 'Approved' "
					+ "				when upper(a.status) = upper('l') then 'Batal' "
					+ "				when upper(a.status) = upper('i') then 'In Wighbridge' "
					+ "				when upper(a.status) = upper('t') then 'Tapping' "
					+ "				when upper(a.status) = upper('c') then 'Selesai' "
					+ "				when upper(a.status) = upper('r') then 'Selesai supir 1' "
					+ "			end) like upper('"+search+"') "
					+ "			or date_format(a.sj_date,'%d-%b-%Y') like '"+search+"' "					
					+ "			or upper(b.fullname) like upper('"+search+"') "
					+ "			or a.phone like '"+search+"')";
		Map<String, Object> mapList = getPaging(sql, pageable);
		@SuppressWarnings("unchecked")
		List<Object[]> listObj = (List<Object[]>) mapList.get("contentData");
		List<MobSjDto> listDto = new ArrayList<>();
		for(Object[] o : listObj){
			MobSjDto dto = new MobSjDto();
			dto.setSjNo((Integer) o[0]);
			dto.setSjDate((Date) o[1]);
			dto.setSptNo((Integer) o[2]);
			dto.setSpmNo((String) o[3]);
			dto.setSegel1((String) o[4]);
			dto.setSegel2((String) o[5]);
			dto.setSegel3((String) o[6]);
			dto.setSegel4((String) o[7]);
			dto.setSegel5((String) o[8]);
			dto.setSegel6((String) o[9]);
			dto.setSegel7((String) o[10]);
			dto.setSegel8((String) o[11]);
			dto.setCustCode((String) o[12]);
			dto.setCustName((String) o[13]);
			dto.setAddressShipTo((String) o[14]);
			dto.setTransportirCode((String) o[15]);
			dto.setTransportirName((String) o[16]);
			dto.setAddressShipFrom((String) o[17]);
			dto.setItemCode((String) o[18]);
			dto.setItemName((String) o[19]);
			dto.setQtySppb((BigDecimal) o[20]);
			dto.setVehicleNo((String) o[21]);
			dto.setVehicleType((Integer) o[22]);
			dto.setDriverId((Integer) o[23]);
			dto.setPhone((String) o[24]);
			dto.setQty((BigDecimal) o[25]);
			dto.setContainerNo((String) o[26]);
			dto.setDepartingFromLatitude((BigDecimal) o[27]);
			dto.setDepartingFromLongitude((BigDecimal) o[28]);
			dto.setDepartingFromDate((Date) o[29]);
			dto.setDepartingFromBy((String) o[30]);
			dto.setFirstDestinationLatitude((BigDecimal) o[31]);
			dto.setFirstDestinationLongitude((BigDecimal) o[32]);
			dto.setFirstDestinationDate((Date) o[33]);
			dto.setFirstDestinationBy((String) o[34]);
			dto.setTransitLatitude((BigDecimal) o[35]);
			dto.setTransitLongitude((BigDecimal) o[36]);
			
			dto.setTransitDate((Date) o[37]);
			dto.setTransitBy((String) o[38]);
			dto.setDestinationLatitude((BigDecimal) o[39]);
			dto.setDestinationLongitude((BigDecimal) o[40]);
			dto.setDestinationDate((Date) o[41]);
			dto.setDestinationBy((String) o[42]);

			dto.setPin((String) o[43]);
//			dto.setStatus((String) o[44]);
			dto.setCreated((String) o[45]);
			dto.setCreatedDate((Date) o[46]);
			dto.setModified((String) o[47]);
			dto.setModifiedDate((Date) o[48]);
			dto.setDestinationFromBy((String) o[49]);
			dto.setDestinationFromDate((Date) o[50]);

			dto.setTransportirId((Integer) o[51]);
			dto.setDriverName((String) o[52]);
			dto.setStatusText((String) o[53]);
			dto.setVehicleTypeText((String) o[54]);
			
			byte byteValue = (byte) o[55];
			
			if(byteValue == 1){
				dto.setIsTransitSpt(true);
				List<Object[]> tempList = mobGoodsReceiveDao.getDataBySjNo(dto.getSjNo());
				if(tempList.size() > 0){
					dto.setFlagVisibleKonfirmasi(true);
					dto.setFlagVisiblePenugasan(false);
				}else{
					dto.setFlagVisibleKonfirmasi(false);
					dto.setFlagVisiblePenugasan(true);				
					
				}
			}else{
				dto.setIsTransitSpt(false);
				dto.setFlagVisibleKonfirmasi(true);
				dto.setFlagVisiblePenugasan(false);
			}
			if(dto.getDepartingFromLatitude() != null && dto.getDepartingFromLongitude() != null){
				dto.setDepartingFromLatitudeLongitude(dto.getDepartingFromLatitude() + ", "+ dto.getDepartingFromLongitude());
			}else if(dto.getDepartingFromLatitude() == null && dto.getDepartingFromLongitude() != null){
				dto.setDepartingFromLatitudeLongitude(dto.getDepartingFromLongitude().toString());
			}else if(dto.getDepartingFromLatitude() != null && dto.getDepartingFromLongitude() == null){
				dto.setDepartingFromLatitudeLongitude(dto.getDepartingFromLatitude().toString());
			}else{
				dto.setDepartingFromLatitudeLongitude("");
			}
			
			if(dto.getDepartingFromLatitude() != null && dto.getDepartingFromLongitude() != null){
				dto.setDepartingFromLatitudeLongitude(dto.getDepartingFromLatitude() + ", "+ dto.getDepartingFromLongitude());
			}else if(dto.getDepartingFromLatitude() == null && dto.getDepartingFromLongitude() != null){
				dto.setDepartingFromLatitudeLongitude(dto.getDepartingFromLongitude().toString());
			}else if(dto.getDepartingFromLatitude() != null && dto.getDepartingFromLongitude() == null){
				dto.setDepartingFromLatitudeLongitude(dto.getDepartingFromLatitude().toString());
			}else{
				dto.setDepartingFromLatitudeLongitude("");
			}
			
			listDto.add(dto);
		}
		
//		Integer totalSize = mobSjDao.countAllDataNewTransportir(transportirCode, search).intValue();
		Map<String, Object> map = new HashMap<>();
		map.put("content", listDto);
		map.put("totalSize", mapList.get("totalRecords"));		
		return map;
	}

	@Override
	public Map<String, Object> findDataNewByCustomer(String custCode, 
			String search, Date startDate, Date endDate, int pageNo, int perPage, String orderBy, String direction) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		if(startDate == null){
			startDate = DateUtil.getMinDate();
		}
		
		if(endDate == null){
			endDate = DateUtil.getMaxDate();
		}

		String tempStartDate = df.format(startDate);
		String tempEndDate = df.format(endDate);		
		
		Pageable pageable = new PageRequest((pageNo-1)*10, perPage,
				new Sort(new Sort.Order(Direction.fromString(direction),orderBy)));
		String sql = "select a.sj_no, a.sj_date, a.spt_no,"
				+ "		a.spm_no, a.segel1, a.segel2,"
				+ "		a.segel3, a.segel4, a.segel5,"
				+ "		a.segel6, a.segel7, a.segel8, "
				+ "		a.cust_code, a.cust_name, a.address_ship_to, "
				+ "		a.transportir_code, a.transportir_name, a.address_ship_from, "
				+ "		a.item_code, a.item_name, a.qty_sppb, "
				+ "		a.vehicle_no, a.vehicle_type, a.driver_id, "
				+ "		a.phone, a.qty, a.container_no, "
				+ "		a.departing_from_latitude, a.departing_from_longitude, a.departing_from_date, "
				+ "		a.departing_from_by, a.first_destination_latitude, a.first_destination_longitude, "
				+ "		a.first_destination_date, a.first_destination_by, a.transit_latitude, "
				+ "		a.transit_longitude, a.transit_date, a.transit_by, "
				+ "		a.destination_latitude, a.destination_longitude, a.destination_date, "
				+ "		a.destination_by, a.pin, a.status, "
				+ "		a.created, a.created_date, a.modified, "
				+ "		a.modified_date, a.destination_from_by, a.destination_from_date, "
				+ "		a.transportir_id, b.fullname, "
				+ "		case "
				+ "			when upper(a.status) = upper('o') then 'Open' "
				+ "			when upper(a.status) = upper('a') then 'Approved' "
				+ "			when upper(a.status) = upper('l') then 'Batal' "
				+ "			when upper(a.status) = upper('i') then 'In Wighbridge' "
				+ "			when upper(a.status) = upper('t') then 'Tapping' "
				+ "			when upper(a.status) = upper('c') then 'Selesai' "
				+ "			when upper(a.status) = upper('r') then 'Selesai supir 1' "
				+ "		end "					
				+ "	from mob_sj a "
				+ "		left join mob_driver b on a.driver_id = b.id "
				+ " where upper(a.status) in (upper('r'), upper('o')) "
				+ "		and (date_format(a.sj_date, '%Y-%m-%d') between '"+tempStartDate+"' and '"+tempEndDate+"') "
				+ "		and a.cust_code = '"+custCode+"' "
				+ "		and (a.spt_no like '"+search+"' "
				+ "			or a.sj_no like '"+search+"' "
				+ "			or a.sj_date like '"+search+"' "
				+ "			or upper(a.cust_name) like upper('"+search+"')"
				+ "			or upper(a.transportir_name) like upper('"+search+"')"
				+ "			or upper(case "
				+ "				when upper(a.status) = upper('o') then 'Open' "
				+ "				when upper(a.status) = upper('a') then 'Approved' "
				+ "				when upper(a.status) = upper('l') then 'Batal' "
				+ "				when upper(a.status) = upper('i') then 'In Wighbridge' "
				+ "				when upper(a.status) = upper('t') then 'Tapping' "
				+ "				when upper(a.status) = upper('c') then 'Selesai' "
				+ "				when upper(a.status) = upper('r') then 'Selesai supir 1' "
				+ "			end) like upper('"+search+"') "
				+ "			or date_format(a.sj_date,'%d-%b-%Y') like '"+search+"' "
				+ "			or upper(b.fullname) like upper('"+search+"') "
				+ "			or a.phone like '"+search+"')";
		Map<String, Object> mapList = getPaging(sql, pageable);
		@SuppressWarnings("unchecked")
		List<Object[]> listObj = (List<Object[]>) mapList.get("contentData");
		List<MobSjDto> listDto = new ArrayList<>();
		for(Object[] o : listObj){
			MobSjDto dto = new MobSjDto();
			dto.setSjNo((Integer) o[0]);
			dto.setSjDate((Date) o[1]);
			dto.setSptNo((Integer) o[2]);
			dto.setSpmNo((String) o[3]);
			dto.setSegel1((String) o[4]);
			dto.setSegel2((String) o[5]);
			dto.setSegel3((String) o[6]);
			dto.setSegel4((String) o[7]);
			dto.setSegel5((String) o[8]);
			dto.setSegel6((String) o[9]);
			dto.setSegel7((String) o[10]);
			dto.setSegel8((String) o[11]);
			dto.setCustCode((String) o[12]);
			dto.setCustName((String) o[13]);
			dto.setAddressShipTo((String) o[14]);
			dto.setTransportirCode((String) o[15]);
			dto.setTransportirName((String) o[16]);
			dto.setAddressShipFrom((String) o[17]);
			dto.setItemCode((String) o[18]);
			dto.setItemName((String) o[19]);
			dto.setQtySppb((BigDecimal) o[20]);
			dto.setVehicleNo((String) o[21]);
			dto.setVehicleType((Integer) o[22]);
			dto.setDriverId((Integer) o[23]);
			dto.setPhone((String) o[24]);
			dto.setQty((BigDecimal) o[25]);
			dto.setContainerNo((String) o[26]);
			dto.setDepartingFromLatitude((BigDecimal) o[27]);
			dto.setDepartingFromLongitude((BigDecimal) o[28]);
			dto.setDepartingFromDate((Date) o[29]);
			dto.setDepartingFromBy((String) o[30]);
			dto.setFirstDestinationLatitude((BigDecimal) o[31]);
			dto.setFirstDestinationLongitude((BigDecimal) o[32]);
			dto.setFirstDestinationDate((Date) o[33]);
			dto.setFirstDestinationBy((String) o[34]);
			dto.setTransitLatitude((BigDecimal) o[35]);
			dto.setTransitLongitude((BigDecimal) o[36]);
			
			dto.setTransitDate((Date) o[37]);
			dto.setTransitBy((String) o[38]);
			dto.setDestinationLatitude((BigDecimal) o[39]);
			dto.setDestinationLongitude((BigDecimal) o[40]);
			dto.setDestinationDate((Date) o[41]);
			dto.setDestinationBy((String) o[42]);
	
			dto.setPin((String) o[43]);
	//		dto.setStatus((String) o[44]);
			dto.setCreated((String) o[45]);
			dto.setCreatedDate((Date) o[46]);
			dto.setModified((String) o[47]);
			dto.setModifiedDate((Date) o[48]);
			dto.setDestinationFromBy((String) o[49]);
			dto.setDestinationFromDate((Date) o[50]);
	
			dto.setTransportirId((Integer) o[51]);
			dto.setDriverName((String) o[52]);
			dto.setStatusText((String) o[53]);
			
			if(dto.getDepartingFromLatitude() != null && dto.getDepartingFromLongitude() != null){
				dto.setDepartingFromLatitudeLongitude(dto.getDepartingFromLatitude() + ", "+ dto.getDepartingFromLongitude());
			}else if(dto.getDepartingFromLatitude() == null && dto.getDepartingFromLongitude() != null){
				dto.setDepartingFromLatitudeLongitude(dto.getDepartingFromLongitude().toString());
			}else if(dto.getDepartingFromLatitude() != null && dto.getDepartingFromLongitude() == null){
				dto.setDepartingFromLatitudeLongitude(dto.getDepartingFromLatitude().toString());
			}else{
				dto.setDepartingFromLatitudeLongitude("");
			}
<<<<<<< HEAD
=======
			
>>>>>>> branch 'master' of https://gitlab.com/ari.haryanto/rmi-spt-sj.git
			listDto.add(dto);
		}
	
		Map<String, Object> map = new HashMap<>();
		map.put("content", listDto);
		map.put("totalSize", mapList.get("totalRecords"));		
		return map;
	}

	@Override
	public List<MobDriverDto> getListDriverByTransporte(String transportirCode, String search) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		List<MobDriver> listObj = mobDriverDao.findByTransportirNoPaging(transportirCode, search);
		List<MobDriverDto> listDto = new ArrayList<>();
		for(MobDriver d : listObj){
			MobDriverDto dto = mapperFacade.map(d, MobDriverDto.class);
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public List<MVehicleTypeDto> getListVehicle() {
		List<MVehicleType> listObj = mVehicleTypeDao.findAllActive();
		List<MVehicleTypeDto> listDto = new ArrayList<>();
		for(MVehicleType t : listObj){
			MVehicleTypeDto dto = mapperFacade.map(t, MVehicleTypeDto.class);
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public void updateDriver(Integer driverId, String phone, String vehicleNo, Integer vehicleType, Integer sjNo, String username) {
		
		MobGoodsReceive m = new MobGoodsReceive();
		m.setSjNo(sjNo);
		m.setVehicleNo(vehicleNo);
		m.setVehicleType(vehicleType);
		m.setDriverId(driverId);
		m.setCreated(username);
		m.setCreatedDate(new Date());
		m.setModified(username);
		m.setModifiedDate(new Date());
		mobGoodsReceiveDao.save(m);
		
	}

	@Override
	public void saveAttactment(List<MobGoodsReceiveAttachmentDto> listDto) {
		for(MobGoodsReceiveAttachmentDto dto : listDto){
			MobGoodsReceiveAttachment m = mapperFacade.map(dto, MobGoodsReceiveAttachment.class);
			mobGoodsReceiveAttachmentDao.save(m);
		}
	}

	@Override
	public MobGoodsReceiveDto getReceive(Integer sjNo) {
		List<Object[]> listObj = mobGoodsReceiveDao.findBySj(sjNo);
		MobGoodsReceiveDto dto = new MobGoodsReceiveDto();
		for(Object[] o :  listObj){
			dto.setDriverName((String) o[0]);
			dto.setVehicleNo((String) o[1]);
			dto.setQtyReceive((BigDecimal) o[2]);
			dto.setNameOfRecipient((String) o[3]);
			dto.setPhone((String) o[4]);
			dto.setVehicleTypeText((String) o[5]);
			dto.setNotes((String) o[6]);
			dto.setSignature((String) o[7]);
			try {
				if(dto.getSignature() != null){
					URL url = new URL(dto.getSignature());
					dto.setImgSignature(new AImage(url));
				}else{
					dto.setDefSignature(mGlobalImageDao.findOne("IMG001").getParmImgValue());
					dto.setImgSignature(new AImage("img signature", dto.getDefSignature()));
				}
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return dto;
	}

	@Override
	public List<MobGoodsReceiveAttachmentDto> getListReceiveAttachment(Integer sjNo) {
		try{
			List<MobGoodsReceiveAttachment> listObj = mobGoodsReceiveAttachmentDao.getListBySj(sjNo);
			List<MobGoodsReceiveAttachmentDto> listDto = new ArrayList<>();
			for(MobGoodsReceiveAttachment m : listObj){
				MobGoodsReceiveAttachmentDto dto = mapperFacade.map(m, MobGoodsReceiveAttachmentDto.class);
				URL url = new URL(dto.getDocumentPath());
				dto.setAttachment(new AImage(url));
				listDto.add(dto);
			}
			return listDto;			
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void saveReceive(MobGoodsReceiveDto dto) {
		MobGoodsReceive r = mapperFacade.map(dto, MobGoodsReceive.class);
		mobGoodsReceiveDao.save(r);		
	}

	@Override
	public Integer countDataAttchmentBySj(Integer sjNo) {
		return mobGoodsReceiveAttachmentDao.countDataBySj(sjNo).intValue();
	}

	@Override
	public void updateStatusSj(Integer sjNo, String status, Date destinationDate) {
		try{
			mobSjDao.updateStatus(status, sjNo, destinationDate);
		}catch(Exception e){
			e.printStackTrace();
		}				
	}

	@Override
	public void updateReceive(BigDecimal qtyReceive, String nameOfRecipient, String notes, 
			String signature, String username, Date modifiedDate, Integer sjNo) {
		try{
			mobGoodsReceiveDao.updateMobReceiveBySj(qtyReceive, notes, nameOfRecipient, signature, username, modifiedDate, sjNo);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
<<<<<<< HEAD
	public void setApiSj(Integer sjNo, Double jmlTerima, String note,
			String pathSignature, String namaPenerima) {
		// TODO Auto-generated method stub
		HashMap<String, Object> result = new HashMap<>();
		HttpResponse response;
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet("http://api.berkahmm.com:8097/zirconite/tokenservices/getresttoken");
		JSONObject jobj = new JSONObject();
		try {
			response = httpClient.execute(request);

			HttpEntity entity = response.getEntity();
			String responseString;
			responseString = EntityUtils.toString(entity);
			jobj = new JSONObject(responseString);
			@SuppressWarnings("unchecked")
			HashMap<String, Object> yourHashMap = new Gson().fromJson(
					jobj.get("data").toString(), HashMap.class);
			String a = yourHashMap.get("token").toString();
			
			HttpPost post = new HttpPost("http://api.berkahmm.com:8097/zirconite/sptservices/mobsj/update");

			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			urlParameters.add(new BasicNameValuePair("token", a));
			urlParameters.add(new BasicNameValuePair("sj_no", sjNo.toString()));
			urlParameters.add(new BasicNameValuePair("jml_terima", jmlTerima.toString()));
			urlParameters.add(new BasicNameValuePair("note", note));
			urlParameters.add(new BasicNameValuePair("signature_path", pathSignature));
			urlParameters.add(new BasicNameValuePair("receiver", namaPenerima));
			

			post.setEntity(new UrlEncodedFormEntity(urlParameters));
							
			response = httpClient.execute(post);
			HttpEntity entity2 = response.getEntity();
			String responseString2 = EntityUtils.toString(entity2);
			
			jobj = new JSONObject(responseString2);
			@SuppressWarnings("unchecked")
			HashMap<String, Object> yourHashMapstatus = new Gson().fromJson(
					jobj.get("data").toString(), HashMap.class);
			String sts = yourHashMapstatus.get("data").toString();
			System.out.println("Status update Sj : "+sts);
		}catch(Exception e){
			e.printStackTrace();
		}
		
=======
	public void setApiSj(Integer sjNo, Double jmlTerima,String note, String pathSignature, String namaPenerima){
		HashMap<String, Object> result = new HashMap<>();
		HttpResponse response;
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet("http://api.rejosomanisindo.com:6026/tokenservices/getresttoken");
		JSONObject jobj = new JSONObject();
		try {
			response = httpClient.execute(request);

			HttpEntity entity = response.getEntity();
			String responseString;
			responseString = EntityUtils.toString(entity);
			jobj = new JSONObject(responseString);
			@SuppressWarnings("unchecked")
			HashMap<String, Object> yourHashMap = new Gson().fromJson(
					jobj.get("data").toString(), HashMap.class);
			String a = yourHashMap.get("token").toString();
			
			HttpPost post = new HttpPost("http://api.rejosomanisindo.com:6026/sptservices/mobsj/update");

			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			urlParameters.add(new BasicNameValuePair("token", a));
			urlParameters.add(new BasicNameValuePair("sj_no", sjNo.toString()));
			urlParameters.add(new BasicNameValuePair("jml_terima", jmlTerima.toString()));
			urlParameters.add(new BasicNameValuePair("note", note));
			urlParameters.add(new BasicNameValuePair("signature_path", pathSignature));
			urlParameters.add(new BasicNameValuePair("receiver", namaPenerima));
			

			post.setEntity(new UrlEncodedFormEntity(urlParameters));
							
			response = httpClient.execute(post);
			HttpEntity entity2 = response.getEntity();
			String responseString2 = EntityUtils.toString(entity2);
			
			jobj = new JSONObject(responseString2);
			@SuppressWarnings("unchecked")
			HashMap<String, Object> yourHashMapstatus = new Gson().fromJson(
					jobj.get("data").toString(), HashMap.class);
			String sts = yourHashMapstatus.get("data").toString();
			System.out.println("Status update Sj : "+sts);
		}catch(Exception e){
			e.printStackTrace();
		}
>>>>>>> branch 'master' of https://gitlab.com/ari.haryanto/rmi-spt-sj.git
	}
	
}
