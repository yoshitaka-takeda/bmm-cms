package id.co.indocyber.bmmRmiCms.service;

import java.util.List;
import java.util.Map;

import id.co.indocyber.bmmRmiCms.dto.MRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MUserRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MobCustomerDto;
import id.co.indocyber.bmmRmiCms.dto.MobDriverDto;
import id.co.indocyber.bmmRmiCms.dto.MobEmployeeDto;
import id.co.indocyber.bmmRmiCms.dto.MobMappingTransportirDriverDto;
import id.co.indocyber.bmmRmiCms.dto.MobTransportirDto;

public interface UMUserSvc {
	
	public Map<String, Object> findAll(String search, Integer pageNo, Integer perPage, String colName, boolean sortAsc);	
	public Integer countAll(String search);
	public void save(MUserDto dto);
	public void saveUserRole(MUserRoleDto dto);
	
	public List<MobEmployeeDto> getListEmployee(String search);
	public List<MRoleDto> getListRole(String search);
	
	public boolean existsEmail(String username, String email);
	public boolean existsMobile(String username, String phoneNum);
	
	public Integer cekDataByMail(String email);
	public Integer cekDataByUsername(String username);
	
	public Map<String, Object> findByTransporterId(String transporterId, String search, Integer pageNo, String colName,boolean sortAsc);
	public Integer countByTransporterId(String transporterId, String search);

	public List<String> getListModul();
	
	public List<MobDriverDto> getAllListDriver(String search);
	public List<MobDriverDto> getListDriverByTransportir(String tranportirCode, String search);
	
	public List<MobTransportirDto> getListTransportir(String search);	
	public MobTransportirDto findOneTransportir(String code);
	
	public List<MobCustomerDto> getListCustomer(String search);
}
