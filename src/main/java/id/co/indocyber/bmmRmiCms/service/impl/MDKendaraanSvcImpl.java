package id.co.indocyber.bmmRmiCms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dao.MVehicleTypeDao;
import id.co.indocyber.bmmRmiCms.dao.MobVehicleDao;
import id.co.indocyber.bmmRmiCms.dto.MVehicleTypeDto;
import id.co.indocyber.bmmRmiCms.dto.MobTransportirDto;
import id.co.indocyber.bmmRmiCms.dto.MobVehicleDto;
import id.co.indocyber.bmmRmiCms.entity.MVehicleType;
import id.co.indocyber.bmmRmiCms.entity.MobTransportir;
import id.co.indocyber.bmmRmiCms.entity.MobVehicle;
import id.co.indocyber.bmmRmiCms.service.MDKendaraanSvc;
import id.co.indocyber.bmmRmiCms.tools.StringUtil;
import ma.glasnost.orika.MapperFacade;

@Service("mDKendaraanSvc")
@Transactional
public class MDKendaraanSvcImpl extends SortGenerator implements MDKendaraanSvc {
	
	@Autowired
	private MapperFacade mapperFacade; 
	
	@Autowired
	private MobVehicleDao mobVehicleDao;
	
	@Autowired
	private MVehicleTypeDao mVehicleTypeDao; 
	
	@Override
	public List<MobVehicleDto> findAll(String search, Integer pageNo, Integer perPage ,String orderBy,boolean direction) {
		
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
//		Pageable pageable = new PageRequest((pageNo - 1) * pageNo, 10,
//				new Sort(new Sort.Order(Direction.fromString(direction),
//						orderBy)));
		
		List<Object[]> listObj = mobVehicleDao.findAllData(search, 
				new PageRequest(pageNo - 1, perPage, generateSort(orderBy, direction)));
		List<MobVehicleDto> listDto = new ArrayList<>();
		for(Object[] o : listObj){
			MobVehicleDto dto = mapperFacade.map(o[0], MobVehicleDto.class);
			dto.setVehicleTypeText((String) o[1]);
			if(dto.getCapacity() != null){
				dto.setCapacityText(
					(dto.getCapacity().stripTrailingZeros().scale()<=0 ? 
						((Integer)dto.getCapacity().intValue()).toString() : 
							((Double)dto.getCapacity().doubleValue()).toString()) + " Kg");
			}else{
				dto.setCapacityText("0 Kg");
			}
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public Integer countAll(String search) {		
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		return mobVehicleDao.contAllData(search).intValue();
	}

	@Override
	public int save(MobVehicleDto dto) {
		try{
			MobVehicle v = mapperFacade.map(dto, MobVehicle.class);
			mobVehicleDao.save(v);
			return 1;
		}catch(Exception e){
			e.printStackTrace();
			return 0;
		}		
	}

	@Override
	public List<MVehicleTypeDto> listVehicletype() {
		List<MVehicleType> listObj = mVehicleTypeDao.findAllActive();
		List<MVehicleTypeDto> listDto = new ArrayList<>();
		for(MVehicleType v : listObj){
			MVehicleTypeDto dto = mapperFacade.map(v, MVehicleTypeDto.class);
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public boolean cekId(String vehicleNo) {
		return mobVehicleDao.findOne(vehicleNo)!=null;
	}

	@Override
	public List<MobVehicleDto> findAll(Integer pageNo, String orderBy,
			boolean direction) {
		List<MobVehicle> entities = mobVehicleDao.findAll(
				new PageRequest(pageNo - 1, 10, generateSort(orderBy, direction))).getContent();
		List<MobVehicleDto> listDto = new ArrayList<>();
		listDto = mapperFacade.mapAsList(entities, MobVehicleDto.class);
		return listDto;
	}

	@Override
	public Integer countAll() {
		return ((Long) mobVehicleDao.count()).intValue();
	}

}
