package id.co.indocyber.bmmRmiCms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dao.MMenuDao;
import id.co.indocyber.bmmRmiCms.dao.MMenuRoleDao;
import id.co.indocyber.bmmRmiCms.dao.MRoleDao;
import id.co.indocyber.bmmRmiCms.dao.MUserDao;
import id.co.indocyber.bmmRmiCms.dto.MMenuDto;
import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MUserRoleDto;
import id.co.indocyber.bmmRmiCms.entity.MMenu;
import id.co.indocyber.bmmRmiCms.entity.MMenuRole;
import id.co.indocyber.bmmRmiCms.entity.MRole;
import id.co.indocyber.bmmRmiCms.entity.MUser;
import id.co.indocyber.bmmRmiCms.service.UMMenuRoleSvc;
import id.co.indocyber.bmmRmiCms.tools.StringUtil;
import ma.glasnost.orika.MapperFacade;

@Service("uMMenuRoleSvc")
@Transactional
public class UMMenuRoleSvcImpl extends SortGenerator implements UMMenuRoleSvc {

	@Autowired
	private MRoleDao mRoleDao;
	
	@Autowired
	private MMenuDao mMenuDao;
	
	@Autowired
	private MUserDao mUserDao;
		
	@Autowired
	private MMenuRoleDao mMenuRoleDao;
	
	@Autowired
	private MapperFacade mapperFacade;
	
	@Override
	public List<MMenuRoleDto> findAllUserRole(String search, Integer pageNo, Integer perPage ,String colName, boolean sortAsc) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		Pageable pageable = new PageRequest(pageNo - 1, perPage,
				new Sort(new Sort.Order(Direction.fromString("desc"),
						colName)));
		List<Object[]> listObj = mMenuRoleDao.findAllData(search, pageable);
		List<MMenuRoleDto> listDto = new ArrayList<>();
		for(Object[] o : listObj){
			MMenuRoleDto dto = new MMenuRoleDto();
			dto.setRoleId((Integer) o[0]);
			dto.setRoleName((String) o[1]);
			dto.setRoleDescription((String) o[2]);
			dto.setIsActive((Boolean) o[3]);
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public List<MRoleDto> cbListRole(String search) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		List<MRole> listObj = mRoleDao.findAllDataActiveWsearch(search);
		List<MRoleDto> listDto = new ArrayList<>();
		for(MRole r : listObj){
			MRoleDto dto = mapperFacade.map(r, MRoleDto.class);
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public void save(MMenuRoleDto dto) {
		try{
			MMenuRole o = mapperFacade.map(dto, MMenuRole.class);
			mMenuRoleDao.save(o);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public Integer countAll(String search) {
		search = StringUtil.surroundString(StringUtil.nevl(search, "%"), "%");
		return mMenuRoleDao.countAllData(search).intValue();
	}

	@Override
	public List<MMenuRoleDto> findMenuRoleByParam(Integer roleId) {
		List<Object[]> listObj = mMenuRoleDao.findAllByRoleId(roleId);
		List<MMenuRoleDto> listTempDto = new ArrayList<>();
		for(Object[] r : listObj){
			MMenuRoleDto dto = mapperFacade.map(r[2], MMenuRoleDto.class);			
			dto.setMenuName((String) r[1]);
			listTempDto.add(dto);
		}
		
		List<MMenuRoleDto> listDto = new ArrayList<>();
		if(listDto.size() == 0){
			List<MMenu> listObj2 = mMenuRoleDao.findAllMenuActive();
			for(MMenu m : listObj2){
				MMenuRoleDto dto = new MMenuRoleDto();
				dto = mapperFacade.map(m, MMenuRoleDto.class);
				if(m.getParentMenuCode() != null){
					String menuParent = mMenuDao.findOne(m.getParentMenuCode()).getMenuDisplayName();
					dto.setMenuName(menuParent+" -> "+m.getMenuDisplayName());
				}else{
					dto.setMenuName(m.getMenuDisplayName());
				}
				
				for(MMenuRoleDto d : listTempDto){
					if(dto.getMenuCode().equals(d.getMenuCode())){
						dto.setIsView(d.getIsView());
						dto.setIsAdd(d.getIsAdd());
						dto.setIsEdit(d.getIsEdit());
						dto.setIsDelete(d.getIsDelete());
						dto.setIsExport(d.getIsExport());
						dto.setIsImport(d.getIsImport());
						dto.setIsActive(d.getIsActive());
					}
				}
				
				listDto.add(dto);
			}
		}
		
		return listDto;
	}

	@Override
	public List<MMenuRoleDto> findAllMenuRole() {
		
		List<MMenu> listObj = mMenuRoleDao.findAllMenuActive();
				
		List<MMenuRoleDto> listDto = new ArrayList<>();
		for(MMenu m : listObj){
			MMenuRoleDto dto = mapperFacade.map(m, MMenuRoleDto.class);
			if(m.getParentMenuCode() != null){
				String menuParent = mMenuDao.findOne(m.getParentMenuCode()).getMenuDisplayName();
				dto.setMenuName(menuParent+" -> "+m.getMenuDisplayName());
			}else{
				dto.setMenuName(m.getMenuDisplayName());
			}
			
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public int updateStatus(List<MMenuRoleDto> list) {
		try{
			for(MMenuRoleDto dto : list){
				mMenuRoleDao.updateData(dto.getIsActive(), dto.getRoleId());
			}			
			return 1;
		}catch(Exception e){
			e.printStackTrace();
			return 0;
		}		
	}

	@Override
	public List<MMenuRoleDto> findMenuRoleByMail(String email, Boolean status) {
		List<MMenuRole> listObj = mMenuRoleDao.findDataByMail(email);
		List<MMenuRoleDto> listDto = new ArrayList<>();
		for(MMenuRole r : listObj){
			MMenuRoleDto dto = mapperFacade.map(r, MMenuRoleDto.class);
			dto.setIsActive(status);
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public List<MMenuRoleDto> findMenuRoleByRoleId(int roleId, Boolean status) {
		List<MMenuRole> listObj = mMenuRoleDao.findDataByRoleId(roleId);
		List<MMenuRoleDto> listDto = new ArrayList<>();
		for(MMenuRole r : listObj){
			MMenuRoleDto dto = mapperFacade.map(r, MMenuRoleDto.class);
			dto.setIsActive(status);
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public List<MUserDto> findUserByRole(Integer roleId) {
		List<MUser> listObj = mUserDao.findAllByRole(roleId);
		List<MUserDto> listDto = new ArrayList<>();
		for(MUser u : listObj){
			MUserDto dto = mapperFacade.map(u, MUserDto.class);
			listDto.add(dto);
		}
		return listDto;
	}

	@Override
	public void deleteByRole(Integer id) {
		try{
			mMenuRoleDao.deleteByRoleId(id);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}

}
