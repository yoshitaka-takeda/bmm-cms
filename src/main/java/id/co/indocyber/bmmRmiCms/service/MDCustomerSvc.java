package id.co.indocyber.bmmRmiCms.service;

import java.util.List;

import id.co.indocyber.bmmRmiCms.dto.MobCustomerDto;

public interface MDCustomerSvc {
	
	public List<MobCustomerDto> findAll(String search, Integer pageNo, String colName, boolean sortAsc);
	public Integer countAll(String search);
	
	public List<MobCustomerDto> findAll(Integer pageNo, String colName, boolean sortAsc);
	public Integer countAll();

	
	public int save(MobCustomerDto dto);
	
}
