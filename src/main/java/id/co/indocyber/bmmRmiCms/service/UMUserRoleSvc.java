package id.co.indocyber.bmmRmiCms.service;

import java.util.List;

import id.co.indocyber.bmmRmiCms.dto.MRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MUserRoleDto;

public interface UMUserRoleSvc {

	public List<MUserRoleDto> findAll(String search, Integer pageNo, String colName,boolean sortAsc);	
	public Integer countAll(String search);
	public int save(MUserRoleDto dto);
	
	public List<MRoleDto> cbListRole();
	public List<MUserDto> cbListUser(String search);

	

}
