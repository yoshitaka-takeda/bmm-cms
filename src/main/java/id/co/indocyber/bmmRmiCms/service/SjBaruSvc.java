package id.co.indocyber.bmmRmiCms.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import id.co.indocyber.bmmRmiCms.dto.MVehicleTypeDto;
import id.co.indocyber.bmmRmiCms.dto.MobDriverDto;
import id.co.indocyber.bmmRmiCms.dto.MobGoodsReceiveAttachmentDto;
import id.co.indocyber.bmmRmiCms.dto.MobGoodsReceiveDto;

public interface SjBaruSvc {

	public Map<String, Object> findAll(String search, 
			Date startDate, Date endDate, int pageNo, int perPage, String orderBy, String direction);
	
	public Map<String, Object> findDataNewByTransportir(String transportirCode,
			String search, Date startDate, Date endDate, int pageNo, int perPage, String orderBy, String direction);
	
	public Map<String, Object> findDataNewByCustomer(String custCode,
			String search, Date startDate, Date endDate ,int pageNo, int perPage, String orderBy, String direction);
	
	public List<MobDriverDto> getListDriverByTransporte(String transportirCode, String search);
	public List<MVehicleTypeDto> getListVehicle();
	
	public void updateDriver(Integer driverId, String phone, 
			String vehicleNo, Integer vehicleType, Integer sjNo, String username);
	public void updateStatusSj(Integer sjNo, String status, Date destinationDate);
	public void updateReceive(BigDecimal qtyReceive, String nameOfRecipient, 
			String notes, String signature, String username, Date modifiedDate, Integer sjNo);
	public void saveReceive(MobGoodsReceiveDto dto);
	public void saveAttactment(List<MobGoodsReceiveAttachmentDto> listDto);
	
	public MobGoodsReceiveDto getReceive(Integer sjNo);
	public List<MobGoodsReceiveAttachmentDto> getListReceiveAttachment(Integer sjNo);
	
	public Integer countDataAttchmentBySj(Integer sjNo);
	
	public void setApiSj(Integer sjNo, Double jmlTerima,String note, String pathSignature, String namaPenerima);
	
}
