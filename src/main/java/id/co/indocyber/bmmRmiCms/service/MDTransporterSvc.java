package id.co.indocyber.bmmRmiCms.service;

import java.util.List;
import java.util.Map;

import id.co.indocyber.bmmRmiCms.dto.MobTransportirDto;

public interface MDTransporterSvc {
	
	public List<MobTransportirDto> findAll(String search, Integer pageNo, Integer perPage ,String colName, boolean sortAsc);
	public Integer countAll(String search);
	
	public List<MobTransportirDto> findAll(Integer pageNo, String colName, boolean sortAsc);
	public Integer countAll();

	public Map<String, Object> findByArg(String search, Integer pageNo, int perPage, String colName, boolean sortAsc);
	
	public int save(MobTransportirDto dto);
	
}
