package id.co.indocyber.bmmRmiCms.report;

import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;

public class ReportConfig {

	public static final String SOURCE_QUERY_DATA = "WEB-INF/widgets/report/report_bmm_sptj.jasper";
	
	private String source;
	private Map<String, Object> parameters;
	private JRDataSource dataSource;
	private ReportType type;
	
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public Map<String, Object> getParameters() {
		return parameters;
	}
	public void setParameters(Map<String, Object> parameters) {
		this.parameters = parameters;
	}
	public JRDataSource getDataSource() {
		return dataSource;
	}
	public void setDataSource(JRDataSource dataSource) {
		this.dataSource = dataSource;
	}
	public ReportType getType() {
		return type;
	}
	public void setType(ReportType type) {
		this.type = type;
	} 
}
