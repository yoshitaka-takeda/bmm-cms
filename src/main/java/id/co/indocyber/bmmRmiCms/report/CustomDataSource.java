package id.co.indocyber.bmmRmiCms.report;

import java.util.List;

import id.co.indocyber.bmmRmiCms.dto.MobSjDto;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class CustomDataSource implements JRDataSource{

	
	private List<MobSjDto> listObj;
	private int index = -1;

	public CustomDataSource(List<MobSjDto> listObj) {
		super();
		this.listObj = listObj;
	}
	@Override
	public Object getFieldValue(JRField field) throws JRException {
		// TODO Auto-generated method stub
		String fieldName = field.getName();
		MobSjDto obj = listObj.get(index);
		
		if("noSj".equals(fieldName)){
			return obj.getSjNo();
		}else if("tglSj".equals(fieldName)){
			return obj.getSjDate();
		}else if("namaCustomer".equals(fieldName)){
			return obj.getCustName();
		}else if("alamat".equals(fieldName)){
			return obj.getAddressShipTo();
		}else if("namaBarang".equals(fieldName)){
			return obj.getItemName();
		}
		
		return "";
	}

	@Override
	public boolean next() throws JRException {
		return ++index < listObj.size();
	}

}
