package id.co.indocyber.bmmRmiCms.report;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;

import id.co.indocyber.bmmRmiCms.dto.ReportSjDto;
import id.co.indocyber.bmmRmiCms.entity.MobSj;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReport {

	private XSSFWorkbook workbook = new XSSFWorkbook();

	private XSSFCellStyle generateTitleStyle() {
		XSSFCellStyle cs = workbook.createCellStyle();
		cs.setWrapText(true);
		cs.setAlignment(CellStyle.ALIGN_CENTER);
		cs.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		Font bold = workbook.createFont();
		bold.setBoldweight(Font.BOLDWEIGHT_BOLD);
		cs.setFont(bold);
		return cs;
	}

	private XSSFCellStyle generateTableHeaderStyle() {
		XSSFCellStyle cs = generateTitleStyle();
		cs.setBorderBottom(CellStyle.BORDER_THIN);
		cs.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		cs.setBorderTop(CellStyle.BORDER_THIN);
		cs.setTopBorderColor(IndexedColors.BLACK.getIndex());
		cs.setBorderLeft(CellStyle.BORDER_THIN);
		cs.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		cs.setBorderRight(CellStyle.BORDER_THIN);
		cs.setRightBorderColor(IndexedColors.BLACK.getIndex());
		return cs;
	}

	private XSSFCellStyle generateTableContentStyle() {
		XSSFCellStyle cs = workbook.createCellStyle();
		cs.setBorderBottom(CellStyle.BORDER_THIN);
		cs.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		cs.setBorderTop(CellStyle.BORDER_THIN);
		cs.setTopBorderColor(IndexedColors.BLACK.getIndex());
		cs.setBorderLeft(CellStyle.BORDER_THIN);
		cs.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		cs.setBorderRight(CellStyle.BORDER_THIN);
		cs.setRightBorderColor(IndexedColors.BLACK.getIndex());
		return cs;
	}

	private XSSFCellStyle generateTableContentStyleCenter() {
		XSSFCellStyle cs = workbook.createCellStyle();
		cs.setBorderBottom(CellStyle.BORDER_THIN);
		cs.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		cs.setBorderTop(CellStyle.BORDER_THIN);
		cs.setTopBorderColor(IndexedColors.BLACK.getIndex());
		cs.setBorderLeft(CellStyle.BORDER_THIN);
		cs.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		cs.setBorderRight(CellStyle.BORDER_THIN);
		cs.setRightBorderColor(IndexedColors.BLACK.getIndex());
		cs.setAlignment(CellStyle.ALIGN_CENTER);
		cs.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		return cs;
	}

	public byte[] generateReport(List<ReportSjDto> daftarSj,
			String namaTransporter, Boolean isAdmin) {

		String[] KeteranganAngkutan = new String[] { "Nama Angkutan" };
		String[] Headers = new String[] { "NO", "NO SJ", "TGL SJ",
				"NAMA CUSTOMER", "ALAMAT PENGIRIMAN", "NAMA BARANG", "QTY SJ",
				"QTY DITERIMA", "NAMA PENERIMA", "CATATAN PENERIMA", "NO SPPB",
				"NO SPT", "TGL SPT", "NO SPM","NAMA TRANSPORTIR", "SUPIR", "NO TELP",
				"NO KENDARAAN", "NO KONTAINER" };

		String[] HeadersWaktuTempuhSupir = new String[] { "TGL TERIMA TUGAS",
				"LAT & LONG", "BERANGKAT DARI BMM", "LAT & LONG",
				"SAMPAI TUJUAN TRANSIT", "LAT & LONG", "TRANSIT", "LAT & LONG",
				"SAMPAI TUJUAN", "LAT & LONG" };

		String[] fieldNames = new String[] { "sjNo", "sjDate", "custName",
				"addressShipTo", "itemName", "qty", "qtyReceive",
				"nameOfRecipient", "notes", "noSppb", "noSpt", "tglSpt",
				"noSpm","transportirName", "driverName", "noPhone", "noVehicle", "noContainer",
				"takeAssigmentDate", "takeAssigmentLatLong",
				"departingFromDate", "departingFromLatLong",
				"firstDestinationDate", "firstDestinationLatLong",
				"transitDate", "transitLatLong", "destinationDate",
				"destinationLatLong", "status" };

		XSSFSheet sheet = workbook.createSheet("Sheet 1");

		CellStyle titleStyle = generateTitleStyle();
		CellStyle headerStyle = generateTableHeaderStyle();
		CellStyle contentStyle = generateTableContentStyle();
		CellStyle contentCenterStyle = generateTableContentStyleCenter();

		int rowNum = 0;

		// Row 1 (for Title)
		Row row = sheet.createRow(0);
		Cell titleCell = row.createCell(0);
		titleCell.setCellValue("RIWAYAT SURAT JALAN");
		titleCell.setCellStyle(titleStyle);
		sheet.addMergedRegion(new CellRangeAddress(0, // first row (0-based)
				0, // last row (0-based)
				0, // first column (0-based)
				29 // last column (0-based)
		));

		if (!isAdmin) {
			// Row 3 (Nama transportir)
			row = sheet.createRow(2);
			Cell pgCell = row.createCell(0);
			pgCell.setCellValue(KeteranganAngkutan[0]);
			sheet.addMergedRegion(new CellRangeAddress(2, // first row
															// (0-based)
					2, // last row (0-based)
					0, // first column (0-based)
					1 // last column (0-based)
			));

			pgCell = row.createCell(2);
			pgCell.setCellValue(namaTransporter);

		}
			sheet.createRow(7);
			sheet.createRow(8);
		

		// Header
		Cell critCell;
		for (int i = 0; i < Headers.length; i++) {
			row = sheet.getRow(7);
			critCell = row.createCell(i);
			critCell.setCellValue(Headers[i]);
			critCell.setCellStyle(headerStyle);
			row = sheet.getRow(8);
			critCell = row.createCell(i);
			critCell.setCellStyle(headerStyle);
			sheet.addMergedRegion(new CellRangeAddress(7, // first row (0-based)
					8, // last row (0-based)
					i, // first column (0-based)
					i // last column (0-based)
			));
		}

		// Waktu Tempuh Supir
		row = sheet.getRow(7);
		critCell = row.createCell(19);
		critCell.setCellStyle(headerStyle);
		critCell.setCellValue("Waktu Tempuh Supir");
		sheet.addMergedRegion(new CellRangeAddress(7, // first row (0-based)
				7, // last row (0-based)
				19, // first column (0-based)
				28 // last column (0-based)
		));

		for (int i = 0; i < HeadersWaktuTempuhSupir.length; i++) {
			row = sheet.getRow(7);
			critCell = row.createCell(19 + i);
			critCell.setCellValue("Waktu Tempuh Supir");
			critCell.setCellStyle(headerStyle);

			row = sheet.getRow(8);
			critCell = row.createCell(19 + i);
			critCell.setCellValue(HeadersWaktuTempuhSupir[i]);
			critCell.setCellStyle(headerStyle);
		}

		row = sheet.getRow(7);
		critCell = row.createCell(29);
		critCell.setCellValue("STATUS");
		critCell.setCellStyle(headerStyle);
		row = sheet.getRow(8);
		critCell = row.createCell(29);
		critCell.setCellStyle(headerStyle);
		sheet.addMergedRegion(new CellRangeAddress(7, // first row (0-based)
				8, // last row (0-based)
				29, // first column (0-based)
				29 // last column (0-based)
		));

		// Isi tabel

		Row content;
		int nomer = 1;

		Cell contentCell;
		for (ReportSjDto x : daftarSj) {
			content = sheet.createRow(8 + nomer);
			contentCell = content.createCell(0);
			contentCell.setCellValue(nomer++);
			contentCell.setCellStyle(contentStyle);
			for (int i = 0; i < fieldNames.length; i++) {
				contentCell = content.createCell(i + 1);
				try {
					Field field = x.getClass().getDeclaredField(fieldNames[i]);
					field.setAccessible(true);
					try {
						contentCell.setCellValue(field.get(x).toString());
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						break;
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						break;
					} catch (NullPointerException e) {
						contentCell.setCellValue("");
					} finally {
						contentCell.setCellStyle(contentStyle);

					}
				} catch (NoSuchFieldException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					break;
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					break;
				}
			}
		}

		for (int i = 0; i < 30; i++) {
			sheet.autoSizeColumn(i, true);
		}

		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			workbook.write(baos);
			return baos.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}
}
