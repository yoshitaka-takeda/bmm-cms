package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.indocyber.bmmRmiCms.entity.MobCustomer;

public interface MobCustomerDao extends JpaRepository<MobCustomer, String> {
	@Query("select a from MobCustomer a "
			+ " where a.isActive is true "
			+ "	and (upper(a.custCode) like upper(:search) "
			+ "		or upper(a.custName) like upper(:search) "
			+ "		or upper(a.address) like upper(:search))")	
	public abstract List<MobCustomer> findAllData(@Param("search")String search);
	
	@Query("select a from MobCustomer a "
			+ " where (upper(a.custCode) like upper(:search) "
			+ "		or upper(a.custName) like upper(:search) "
			+ "		or upper(a.address) like upper(:search) "
			+ "		or upper(case "
			+ "				when a.isActive is true then 'Aktif' "
			+ "				when a.isActive is false then 'Non Aktif' "
			+ "				end) like upper(:search) "
			+ "		or case "
			+ "			when a.phoneNum is null then '-' "
			+ "			else a.phoneNum "
			+ "			end like :search) "
			+ " order by a.isActive desc")	
	public abstract List<MobCustomer> findAllData(@Param("search") String search, Pageable pageable);

	@Query("select count(a) from MobCustomer a "
			+ " where a.isActive is true "
			+ "	and (upper(a.custCode) like upper(:search) "
			+ "		or upper(a.custName) like upper(:search) "
			+ "		or upper(a.address) like upper(:search))")
	public abstract Long countAllData(@Param("search") String search);
}
