package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MMenu;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MMenuDao extends JpaRepository<MMenu, String>{
	@Query("select menu from MMenu menu where menu.appType=:module order by menu.menuCode")
	public List<MMenu> loadMenu(@Param("module")String module);
	
}
