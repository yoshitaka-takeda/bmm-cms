package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.indocyber.bmmRmiCms.entity.MobMappingTransportirDriver;
import id.co.indocyber.bmmRmiCms.entity.MobMappingTransportirDriverPK;

public interface MobMappingTransportirDriverDao extends JpaRepository<MobMappingTransportirDriver, MobMappingTransportirDriverPK> {
	
	@Query("select a, b.transportirName, c.fullname, c.phoneNum, d.longText, c.birthday "
			+ "	from MobMappingTransportirDriver a, MobTransportir b, MobDriver c, MGender d "
			+ "	where a.transportirCode = b.code "
			+ "	and a.driverId = c.id "
			+ "	and d.code = c.gender "
			+ "	and (upper(c.fullname) like upper(:search) or "
			+ " upper(b.transportirName) like upper(:search)) "
			+ " order by a.isActive desc ")
	public abstract List<Object[]> findAllData(@Param("search") String search, Pageable page);
	
	@Query("select count(a) "
			+ "	from MobMappingTransportirDriver a, MobTransportir b, MobDriver c, MGender d "
			+ "	where a.transportirCode = b.code "
			+ "	and a.driverId = c.id "
			+ "	and d.code = c.gender "
			+ "	and (upper(c.fullname) like upper(:search) or "
			+ "upper(b.transportirName) like upper(:search))")
	public abstract Long countAllData(@Param("search") String search);

	@Query("select a, b.transportirName, c.fullname, c.phoneNum, d.longText, c.birthday "
			+ "	from MobMappingTransportirDriver a, MobTransportir b, MobDriver c, MGender d "
			+ "	where a.transportirCode = b.code "
			+ "	and a.driverId = c.id "
			+ "	and d.code = c.gender "
			+ "	and a.transportirCode = :id "
			+ "	and upper(c.fullname) like upper(:search)")
	public abstract List<Object[]> findDataByTransportir(
			@Param("id") String id, @Param("search") String search, Pageable page);
	
	@Query("select count(a) "
			+ "	from MobMappingTransportirDriver a, MobTransportir b, MobDriver c, MGender d "
			+ "	where a.transportirCode = b.code "
			+ "	and a.driverId = c.id "
			+ "	and d.code = c.gender "
			+ "	and a.transportirCode = :id "
			+ "	and upper(c.fullname) like upper(:search)")
	public abstract Long countDataByTransportir(@Param("id") String id, @Param("search") String search);


	@Query("select a, b.transportirName, c.fullname, c.phoneNum, d.longText, c.birthday "
			+ "	from MobMappingTransportirDriver a, MobTransportir b, MobDriver c, MGender d "
			+ "	where a.transportirCode = b.code "
			+ "	and a.driverId = c.id "
			+ "	and d.code = c.gender "
			+ "	and a.driverId = :id ")
	public abstract List<Object[]> findDataByDriver(
			@Param("id") Integer id);

	@Query("select a "
			+ "	from MobMappingTransportirDriver a "
			+ "	where a.transportirCode = :transportirCode "
			+ "	and a.driverId = :id ")
	public MobMappingTransportirDriver findOne(
			@Param("id") Integer driverId, @Param("transportirCode") String transportirCode);

}
