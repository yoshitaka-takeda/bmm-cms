package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MobEmployee;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobEmployeeDao extends JpaRepository<MobEmployee, Integer>{
	@Query("select emp from MobEmployee emp where emp.manager in (:id)")
	List<MobEmployee> findManagedEmployees(@Param("id")List<Integer> id);
	
	@Query("select count(emp) from MobEmployee emp where emp.manager in (:id)")
	public int countManagedEmployees(@Param("id")List<Integer> id);
	
	@Query("select emp from MobEmployee emp where emp.manager in (:id)")
	List<MobEmployee> findManagedEmployees2(@Param("id")List<Integer> id);
	
	@Query("select count(emp) from MobEmployee emp where emp.manager in (:id)")
	public int countManagedEmployees2(@Param("id")List<Integer> id);
	
	@Query("select emp from MobEmployee emp where lower(emp.fullname) = lower(:name)")
	List<MobEmployee> findByName(@Param("name")String fullname);

	@Query("select emp from MobEmployee emp where lower(emp.email) = lower(:email)")
	List<MobEmployee> findByEmail(@Param("email")String email);
	
	@Query("select emp from MobEmployee emp where emp.fullname like concat('%',:arg,'%') or "
			+ "emp.phoneNum like concat('%',:arg,'%') or emp.address like concat('%',:arg,'%')")
	List<MobEmployee> findByArg(@Param("arg")String arg, Pageable pageRequest);
	
	@Query("select count(emp) from MobEmployee emp where emp.fullname like concat('%',:arg,'%') or "
			+ "emp.phoneNum like concat('%',:arg,'%') or emp.address like concat('%',:arg,'%')")
	Long countByArg(@Param("arg")String arg);

	@Query("select emp from MobEmployee emp where emp.id not in ("
			+ "select user.personId from MUser user, MUserRole role where "
			+ "user.email = role.email and role.roleId not in (2,3))")
	List<MobEmployee> findAllNoUser();
	
	@Query("select a from MobEmployee a where a.isActive is true "
			+ "	and (upper(a.email) like upper(:search)"
			+ "		or upper(a.fullname) like upper(:search) "
			+ "		or a.nipeg like :search "
			+ "		or a.referenceCode like :search) "
			+ "	and a.email not in (select b.email from MUser b)")
	public abstract List<MobEmployee> findAllDataActive(@Param("search")String search);

}
