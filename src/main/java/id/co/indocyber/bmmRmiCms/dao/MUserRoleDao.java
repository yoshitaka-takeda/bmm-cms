package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MUserRole;
import id.co.indocyber.bmmRmiCms.entity.MUserRolePK;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MUserRoleDao extends JpaRepository<MUserRole, MUserRolePK>{
	@Query("select ur from MUserRole ur where ur.email = :email")
	public MUserRole findUserRole(@Param("email")String email);

	@Query("select ur from MUserRole ur where ur.email = :email and ur.moduleApps = 'Web'")
	public MUserRole getCmsAccess(@Param("email")String email);
	
	@Query("select a, b.roleName from MUserRole a, MRole b "
			+ "	where a.roleId = b.id "
			+ "	and (upper(a.email) like upper(:search) "
			+ "		or upper(b.roleName) like upper(:search) "
			+ "		or upper(a.moduleApps) like upper(:search)) ")
	public List<Object[]> findAllData(@Param("search") String search, Pageable page);
	
	@Query("select count(a) from MUserRole a, MRole b "
			+ "	where a.roleId = b.id "
			+ "	and (upper(a.email) like upper(:search) "
			+ "		or upper(b.roleName) like upper(:search) "
			+ "		or upper(a.moduleApps) like upper(:search)) ")	
	public Long countAllData(@Param("search") String search);
	
	@Query("select count(a) from MUserRole a "
			+ "	where a.email = :email "
			+ "	and a.roleId in (1,11) ")
	public Long countDataByEmail(@Param("email") String email);
	
	@Query("select count(a) from MUserRole a "
			+ "	where a.email = :email "
			+ "	and a.roleId = 12 ")	
	public Long countDataCustomer(@Param("email") String email);
}
