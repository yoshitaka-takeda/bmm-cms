package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MobVehicle;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobVehicleDao extends JpaRepository<MobVehicle, String>{
	
	@Query("select a, b.vehicleTypeName from MobVehicle a, MVehicleType b "
			+ "where a.vehicleType = b.id "
			+ "	and (upper(a.vehicleNo) like upper(:search) "
			+ "		or upper(b.vehicleTypeName) like upper(:search) "
			+ "		or a.capacity like :search "
			+ "		or a.productionYear like :search "
			+ "		or upper(case "
			+ "					when a.isActive is true then 'Aktif'"
			+ "					when a.isActive is false then 'Non Aktif'"
			+ "				end ) like upper(:search))")
	public abstract List<Object[]> findAllData(@Param("search") String search, Pageable page);
	
	@Query("select count(a) from MobVehicle a, MVehicleType b "
			+ "where a.vehicleType = b.id "
			+ "	and (upper(a.vehicleNo) like upper(:search) "
			+ "		or upper(b.vehicleTypeName) like upper(:search) "
			+ "		or a.capacity like :search "
			+ "		or a.productionYear like :search) "
			+ "		or upper(case "
			+ "					when a.isActive is true then 'Aktif'"
			+ "					when a.isActive is false then 'Non Aktif'"
			+ "				end ) like upper(:search))")
	public abstract Long contAllData(@Param("search") String search);
	
}
