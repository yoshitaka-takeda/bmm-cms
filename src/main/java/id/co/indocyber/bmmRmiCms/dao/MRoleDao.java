package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MRole;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MRoleDao extends JpaRepository<MRole, Integer>{
	@Query("select role from MRole role where lower(role.roleName) = lower(:roleName)")
	List<MRole> findByName(@Param("roleName")String roleName);

	@Query("select role from MRole role where role.roleName like concat('%',:arg,'%') or "
			+ "role.description like concat('%',:arg,'%')")
	List<MRole> findByArg(@Param("arg")String arg, Pageable pageable);
	
	@Query("select count(role) from MRole role where role.roleName like concat('%',:arg,'%') or "
			+ "role.description like concat('%',:arg,'%')")
	Long countByArg(@Param("arg")String arg);
	
	@Query("select a from MRole a "
			+ "	where "
			+ "		upper(a.roleName) like upper(:search) "
			+ "		or upper(a.description) like upper(:search) "
			+ "		or upper(case "
			+ "					when a.isActive is true then 'Aktif' "
			+ "					when a.isActive is false then 'Non Aktif' "
			+ "				end) like upper(:search)")
	public abstract List<MRole> findAllData(@Param("search") String search, Pageable page);
	
	
	@Query("select count(a) from MRole a where a.roleName like :search or a.description like :search")
	public abstract Long countAllData(@Param("search") String search);
	
	@Query("select a from MRole a "
			+ "	where a.isActive is true "
			+ "	and (upper(a.description) like upper(:search) "
			+ "		or upper(a.roleName) like upper(:search))")
	public abstract List<MRole> findAllDataActive(
			@Param("search") String search);
	
	@Query("select a from MRole a "
			+ "where a.isActive is true "
			+ "and a.roleName like :search")
	public abstract List<MRole> findAllDataActiveWsearch(@Param("search") String search);
	
	@Query("select a from MRole a where lower(a.roleName) like lower(:namaRole)")
	public abstract MRole findOneByRoleName(@Param("namaRole")String namaRole);
}
