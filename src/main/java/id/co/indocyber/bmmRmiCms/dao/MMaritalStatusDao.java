package id.co.indocyber.bmmRmiCms.dao;

import id.co.indocyber.bmmRmiCms.entity.MMaritalStatus;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface MMaritalStatusDao extends JpaRepository<MMaritalStatus, String>{
	
	@Query("select a from MMaritalStatus a where a.isActive is true")
	public List<MMaritalStatus> findAllActive();

}
