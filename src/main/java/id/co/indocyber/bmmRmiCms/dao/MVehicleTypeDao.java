package id.co.indocyber.bmmRmiCms.dao;

import id.co.indocyber.bmmRmiCms.entity.MVehicleType;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface MVehicleTypeDao extends JpaRepository<MVehicleType, Integer>{
	
	@Query("select a from MVehicleType a where a.isActive is true")
	public abstract List<MVehicleType> findAllActive();
	
}
