package id.co.indocyber.bmmRmiCms.dao;

import id.co.indocyber.bmmRmiCms.entity.MEducationType;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface MEducationTypeDao extends JpaRepository<MEducationType, String>{
	
	@Query("select a from MEducationType a where a.isActive is true")
	public abstract List<MEducationType> findAllActive();

}
