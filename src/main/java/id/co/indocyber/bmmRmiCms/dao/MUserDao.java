package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MUser;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MUserDao extends JpaRepository<MUser, String>{
	
	@Query("select a, b.roleId, b.moduleApps, c.roleName from MUser a, MUserRole b, MRole c "
			+ "	where a.email = b.email "
			+ "	and c.id = b.roleId "
			+ "	and (upper(a.username) like upper(:arg) "
			+ "	or upper(a.email) like upper(:arg) "
			+ "	or upper(a.imei) like upper(:arg) "
			+ "	or upper(a.fullname) like upper(:arg) "
			+ " or upper(c.roleName) like upper(:arg)) "
			+ " order by a.isActive desc ")
	public List<Object[]> findByArg(@Param("arg")String arg, Pageable pageable);
	
	@Query("select count(a) from MUser a, MUserRole b, MRole c "
			+ "	where a.email = b.email "
			+ "	and c.id = b.roleId "
			+ "	and (a.username like :arg "
			+ "	or a.email like :arg "
			+ "	or a.imei like :arg "
			+ "	or a.fullname like :arg)")
	public Long countByArg(@Param("arg")String arg);
	
	@Query("select user from MUser user where user.email = :user "
			+ "or user.username = :user or user.phoneNum = :user ")
	public MUser findByUsernameOrEmail(@Param("user")String usernameOrEmail);
	
	@Query("select user from MUser user, MUserRole ur where user.email = ur.email and "
			+ "ur.roleId not in(2,3) and user.personId = :id")
	public MUser findEmployee(@Param("id")Integer id);
	
	@Query("select user from MUser user, MUserRole ur where user.email = ur.email and "
			+ "ur.roleId = 3 and user.personId = :id")
	public MUser findDriver(@Param("id")Integer id);

	@Query("select a from MUser a where a.isActive is true and a.email like :search")
	public abstract List<MUser> findAllDataActive(@Param("search") String search);
	
	@Query("select a from MUser a "
			+ "	where a.isActive is true "
			+ "	and a.email not in (select b.email from MUserRole b where b.isActive is true) "
			+ "	and a.email like :search")
	public abstract List<MUser> findAllDataAvailable(@Param("search") String search);
	
	@Query("select a from MUser a, MUserRole b "
			+ "	where a.email = b.email "
			+ "	and b.roleId = :roleId")
	public abstract List<MUser> findAllByRole(@Param("roleId") Integer roleId);
	
	@Query("select count(a) from MUser a "
			+ "	where upper(a.email) = upper(:email)")	
	public abstract Integer countDataByMail(
			@Param("email")String email);
	
	@Query("select count(a) from MUser a "
			+ "	where upper(a.username) = upper(:username)")	
	public abstract Integer countDataByUsername(@Param("username")String username);

	@Query("select a, c.roleId, c.moduleApps, d.roleName from MUser a, MobMappingTransportirDriver b, MUserRole c, MRole d  "
			+ "	where a.referenceCode = b.transportirCode "
			+ "	and a.email = c.email "
			+ "	and d.id = c.roleId "
			+ "	and a.referenceCode = :code "
			+ "	and (upper(a.username) like upper(:search) "
			+ "		or upper(a.email) like upper(:search) "
			+ "		or upper(a.fullname) like upper(:search))")		
	public abstract List<Object[]> findUserByTransporterId(@Param("code")String code, 
			@Param("search") String search, Pageable page);

	@Query("select count(a) from MUser a, MobMappingTransportirDriver b, MUserRole c, MRole d  "
			+ "	where a.referenceCode = b.transportirCode "
			+ "	and a.email = c.email "
			+ "	and d.id = c.roleId "
			+ "	and a.referenceCode = :code "
			+ "	and (upper(a.username) like upper(:search) "
			+ "		or upper(a.email) like upper(:search) "
			+ "		or upper(a.fullname) like upper(:search))")		
	public abstract Long countUserByTransporterId(@Param("code")String code, 
			@Param("search") String search);

}
