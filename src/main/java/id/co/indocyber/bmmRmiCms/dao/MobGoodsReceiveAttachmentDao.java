package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.indocyber.bmmRmiCms.entity.MobGoodsReceiveAttachment;

public interface MobGoodsReceiveAttachmentDao extends JpaRepository<MobGoodsReceiveAttachment, Integer> {
	
	@Query("select a from MobGoodsReceiveAttachment a "
			+ "	where a.sjNo = :sjNo")
	public List<MobGoodsReceiveAttachment> getListBySj(@Param("sjNo") Integer sjNo);
	
	@Query("select count(a) from MobGoodsReceiveAttachment a "
			+ "	where a.sjNo = :sjNo")
	public Long countDataBySj(@Param("sjNo") Integer sjNo);

}
