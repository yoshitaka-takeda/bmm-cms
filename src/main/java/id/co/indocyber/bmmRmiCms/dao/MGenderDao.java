package id.co.indocyber.bmmRmiCms.dao;

import id.co.indocyber.bmmRmiCms.entity.MGender;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface MGenderDao extends JpaRepository<MGender, String>{
	
	@Query("select a from MGender a where a.isActive is true")
	public List<MGender> findAllActive();
	
}
