package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.indocyber.bmmRmiCms.entity.MobTransportir;

public interface MobTransportirDao extends JpaRepository<MobTransportir, String> {
	
	@Query("select a from MobTransportir a "
			+ "	where "
			+ "	(upper(a.transportirName) like upper(:search) "
			+ "	or upper(a.transportirType) like  upper(:search)"
			+ "	or a.referenceCode like upper(:search)"
			+ " or upper(a.code) like upper(:search))")
	public abstract List<MobTransportir> findAllData(
			@Param("search") String search, Pageable page);

	@Query("select count(a) from MobTransportir a"
			+ "	where "
			+ "	(upper(a.transportirName) like upper(:search) "
			+ "	or upper(a.transportirType) like  upper(:search)"
			+ "	or a.referenceCode like upper(:search)"
			+ " or upper(a.code) like upper(:search))")
	public abstract Long countAllData(@Param("search") String search);
	
	@Query("select a from MobTransportir a "
			+ "	where a.isActive is true "
			+ "	and (upper(a.transportirName) like upper(:search) "
			+ "		or upper(a.transportirType) like upper(:search))")
	public abstract List<MobTransportir> findAllActive(@Param("search") String search);
}
