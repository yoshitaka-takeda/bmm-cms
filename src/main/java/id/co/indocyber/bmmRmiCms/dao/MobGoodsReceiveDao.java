package id.co.indocyber.bmmRmiCms.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.indocyber.bmmRmiCms.entity.MobGoodsReceive;

public interface MobGoodsReceiveDao extends JpaRepository<MobGoodsReceive, Integer> {
	
	@Query("select count(a) from MobGoodsReceive a "
			+ "	where a.sjNo = :sjNo")	
	public abstract Long countBySj(@Param("sjNo") Integer sjNo);
	
	@Query(value = "select b.fullname, d.vehicle_no, " 
			+ " 	a.qty_receive, a.name_of_recipient, "
			+ " 	d.phone, c.vehicle_type_name, a.notes, a.signature "
			+ " from mob_goods_receive a "
			+ "		left join mob_driver b on a.driver_id = b.id "
			+ "		left join m_vehicle_type c on a.vehicle_type = c.id "
			+ "		join mob_sj d on a.sj_no = d.sj_no "
			+ "	where a.sj_no = :sjNo", nativeQuery = true)	
	public abstract List<Object[]> findBySj(@Param("sjNo") Integer sjNo);
	
	@Query("select a.sjNo, a.driverId from MobGoodsReceive a "
			+ "	where a.sjNo = :sjNo "
			+ "group by a.sjNo, a.driverId ")
	public abstract List<Object[]> getDataBySjNo(@Param("sjNo") Integer sjNo);
	
	@Modifying
	@Query("update MobGoodsReceive a "
			+ "	set a.qtyReceive = :qtyReceive, "
			+ "		a.notes = :notes, "
			+ "		a.nameOfRecipient = :nameOfRecipient, "
			+ "		a.signature = :signature, "
			+ "		a.modified = :username, "
			+ "		a.modifiedDate = :modifiedDate "
			+ "	where a.sjNo = :sjNo")
	public abstract void updateMobReceiveBySj(
			@Param("qtyReceive") BigDecimal qtyReceive,
			@Param("notes") String notes,
			@Param("nameOfRecipient") String nameOfRecipient,
			@Param("signature") String signature,
			@Param("username") String modified,
			@Param("modifiedDate") Date modifiedDate,
			@Param("sjNo") Integer sjNo);

}
