package id.co.indocyber.bmmRmiCms.dao;

import java.util.Date;
import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MobDriver;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobDriverDao extends JpaRepository<MobDriver, Integer>{
	
	@Query("select a from MobDriver a "
			+ "	where (a.fullname) like upper(:search) "
			+ "	or a.phoneNum like :search "
			+ "	or upper(a.address) like upper(:search) ")
	public abstract List<MobDriver> findAllDataByArg(@Param("search")String search, Pageable page);

	@Query("select count(a) from MobDriver a "
			+ "	where (a.fullname) like upper(:search) "
			+ "	or a.phoneNum like :search "
			+ "	or upper(a.address) like upper(:search) ")	
	public abstract Integer countAllData(@Param("search")String search);
	
	@Query("select province from MobDriver group by province")
	public abstract List<String> getProvince();
	
	@Query(value="select floor(DATEDIFF(SYSDATE(),DATE_FORMAT(:birthday,'%Y-%m-%d'))/365) as usia from mob_driver a", nativeQuery = true)
	public Long getAgeDriver(@Param("birthday") Date birthday);
	
	@Query("select a from MobDriver a "
			+ "	where a.id not in (select b.driverId from MobMappingTransportirDriver b) "
			+ "and (upper(a.fullname) like upper(:search) or a.phoneNum like :search) "
			+ "order by a.modifiedDate desc")
	public List<MobDriver> findAvailableDriver(@Param("search") String search);
	
	@Query("select a from MobDriver a "
			+ "	where a.email not in (select b.email from MUser b) "
			+ "	and (upper(a.fullname) like upper(:search) "
			+ "		or upper(a.email) like upper(:search) "
			+ "		or upper(a.referenceCode) like upper(:search))")
	public List<MobDriver> findAvailableUserDriver(@Param("search") String search);
	
	@Query("select a from MobDriver a, MobMappingTransportirDriver c "
			+ "	where c.driverId = a.id "
			+ "	and (a.phoneNum not in (select b.username from MUser b) or a.email is null) "
			+ "	and c.transportirCode = :transportirCode "
			+ "	and (upper(a.fullname) like upper(:search) "
			+ "		or upper(a.email) like upper(:search) "
			+ "		or upper(a.referenceCode) like upper(:search))")
	public List<MobDriver> findAvailableUserDriverByTransportir(
			@Param("transportirCode") String transportirCode, @Param("search") String search);
	
	@Query("select a from MobDriver a, MobMappingTransportirDriver b "
			+ "	where a.id = b.driverId "
			+ "	and b.transportirCode = :transporterCode")
	public List<MobDriver> findByTransporter(@Param("transporterCode") String transporterCode);
	
	@Query("select a from MobDriver a, MobMappingTransportirDriver b "
			+ "	where a.id = b.driverId "
			+ "	and b.transportirCode = :transportirCode "
			+ "	and (upper(a.fullname) like upper(:search) "
			+ "		or a.phoneNum like :search "
			+ "		or upper(a.address) like upper(:search)) "
			+ "		order by a.isActive desc ")
	public List<MobDriver> findByTransportir(@Param("transportirCode") String transportirCode,
			@Param("search") String search, Pageable page);

	@Query("select count(a) from MobDriver a, MobMappingTransportirDriver b "
			+ "	where a.id = b.driverId "
			+ "	and b.transportirCode = :transportirCode "
			+ "	and (upper(a.fullname) like upper(:search) "
			+ "		or a.phoneNum like :search "
			+ "		or upper(a.address) like upper(:search))")	
	public Long countByTransportir(@Param("transportirCode") String transportirCode,
			@Param("search") String search);
	
	@Query("select max(a.id) from MobDriver a")
	public Long getMaxId();
	
	@Query("select a from MobDriver a, MobMappingTransportirDriver b "
			+ "	where a.id = b.driverId "
			+ "	and b.transportirCode = :transportirCode "
			+ "	and (upper(a.fullname) like upper(:search) "
			+ "		or a.phoneNum like :search "
			+ "		or upper(a.address) like upper(:search))")
	public List<MobDriver> findByTransportirNoPaging(
			@Param("transportirCode") String transportirCode,
			@Param("search") String search);
	
	@Query("select a from MobDriver a "
			+ "	where a.phoneNum like :phoneNum ")
	public List<MobDriver> findDriverByPhone(@Param("phoneNum") String phoneNum);

	@Query("select a from MobDriver a "
			+ "	where a.email like :email ")
	public List<MobDriver> findDriverByEmail(@Param("email") String email);

	@Query("select a from MobDriver a "
			+ "	where a.noKtp like :noKtp ")
	public List<MobDriver> findDriverByNoKtp(@Param("noKtp") String noKtp);

}
