package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MMenu;
import id.co.indocyber.bmmRmiCms.entity.MMenuRole;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MMenuRoleDao extends JpaRepository<MMenuRole, Integer>{
	@Query("select mr, menu "
			+ "from MMenuRole mr, MMenu menu where mr.menuCode = menu.menuCode and menu.isActive is true "
			+ "and mr.roleId = :roleId and menu.appType = :module "
			+ "and mr.isView = 1 order by mr.menuCode")
	public List<Object[]> getAccessPrivs(@Param("roleId")Integer roleId, @Param("module")String module);
	
	@Query("select mr, menu "
			+ "from MMenuRole mr, MMenu menu where mr.menuCode = menu.menuCode and menu.isActive is true "
			+ "and mr.roleId = :roleId and menu.appType = :module "
			+ "order by mr.menuCode")
	public List<Object[]> getAccessPrivsForMaster(@Param("roleId")Integer roleId, @Param("module")String module);
	
	@Query("select a.roleId, d.roleName, d.description, a.isActive "
			+ "	from MMenuRole a, MUserRole b, MRole d "
			+ " where a.roleId = b.roleId "
			+ "	and a.roleId = d.id "
			+ "	and (upper(d.roleName) like upper(:search) "
			+ "		or upper(d.description) like upper(:search) "
			+ "		or upper(case "
			+ "					when a.isActive is true then 'Aktif' "
			+ "					when a.isActive is false then 'Tidak Aktif' "
			+ "				end) like upper(:search))"
			+ "	group by a.roleId, d.roleName, d.description, a.isActive "
			+ " order by a.isActive desc ")
	public List<Object[]> findAllData(@Param("search") String search, Pageable page);
	
//	@Query("select count(a) from MUserRole a, MRole b "
//			+ " where a.roleId = b.id "
//			+ "	and b.roleName like :search")
	@Query(value="select count(*) as countTemp from (select a.role_id "
			+ "	from m_menu_role a, m_user_role b, m_role d "
			+ " where a.role_id = b.role_id "
			+ "	and a.role_id = d.id "
			+ "	and (upper(d.role_name) like upper(:search) "
			+ "		or upper(d.description) like upper(:search) "
			+ "		or upper(case "
			+ "					when a.is_active = 1 then 'Aktif' "
			+ "					when a.is_active = 0 then 'Non Aktif' "
			+ "				end) like upper(:search))"
			+ " group by a.role_id) as temp",nativeQuery=true)
	public Long countAllData (@Param("search") String search);
	
//	@Query("select a.menuCode, a.menuDisplayName, b "
//			+ "	from MMenu a, MMenuRole b "
//			+ "	where a.menuCode = b.menuCode "
//			+ "	and a.isActive is true "
//			+ "	and b.roleId = :roleId "
//			+ "	order by a.menuCode")
//	public List<Object[]> findAllMenuByEmailAndRoleId(@Param("roleId") Integer roleId);
	

	@Query("select a.menuCode, a.menuDisplayName, b "
			+ "	from MMenu a, MMenuRole b "
			+ "	where a.menuCode = b.menuCode "
			+ "	and a.isActive is true "
			+ "	and b.roleId = :roleId "
			+ "	order by a.menuCode")	
	public List<Object[]> findAllByRoleId(@Param("roleId") Integer roleId);
	
	@Query("select a "
			+ "	from MMenu a "
			+ "	where a.isActive is true "
			+ "	order by a.menuCode asc")
	public List<MMenu> findAllMenuActive();
	
	@Modifying
	@Query(value="update m_menu_role a 	"
			+ "	set a.is_active = :status "
			+ "	where a.role_id = :id", nativeQuery = true)
	public void updateData(
			@Param("status") Boolean status, @Param("id")Integer id);
	
	@Query("select a from MMenuRole a, MUserRole b "
			+ "	where a.roleId = b.roleId "
			+ "	and b.email = :email")
	public List<MMenuRole> findDataByMail(@Param("email") String email);

	@Query("select a from MMenuRole a "
			+ "	where a.roleId = :roleId")
	public List<MMenuRole> findDataByRoleId(@Param("roleId") int roleId);

	@Modifying
	@Query(value="delete from m_menu_role where role_id = :roleId", nativeQuery = true)	
	public void deleteByRoleId(@Param("roleId") Integer id);
}
