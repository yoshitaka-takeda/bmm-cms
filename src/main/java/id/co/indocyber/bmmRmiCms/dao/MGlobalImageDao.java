package id.co.indocyber.bmmRmiCms.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.indocyber.bmmRmiCms.entity.MGlobalImage;

public interface MGlobalImageDao extends JpaRepository<MGlobalImage, String> {

}
