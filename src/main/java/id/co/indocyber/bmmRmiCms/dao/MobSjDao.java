package id.co.indocyber.bmmRmiCms.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.indocyber.bmmRmiCms.entity.MobSj;

public interface MobSjDao extends JpaRepository<MobSj, Integer>{
	
	@Query("select a, "
			+ "	case "
			+ "		when upper(a.status) = upper('o') then 'Open' "
			+ "		when upper(a.status) = upper('a') then 'Approved' "
			+ "		when upper(a.status) = upper('l') then 'Batal' "
			+ "		when upper(a.status) = upper('i') then 'In Wighbridge' "
			+ "		when upper(a.status) = upper('t') then 'Tapping' "
			+ "	end "
			+ "	from MobSj a "
			+ " where upper(a.status) = upper('o') "
			+ "		and (upper(a.custName) like upper(:search)"
			+ "			or upper(a.transportirName) like upper(:search))")
	public abstract List<Object[]> findAllDataNew(@Param("search") String search, Pageable page);

	@Query("select count(a) "
			+ "	from MobSj a "
			+ " where upper(a.status) = upper('o') "
			+ "		and (upper(a.custName) like upper(:search)"
			+ "			or upper(a.transportirName) like upper(:search))")	
	public abstract Long countAllNew(@Param("search") String search);
	
	@Query("select a, "
			+ "	case "
			+ "		when upper(a.status) = upper('o') then 'Open' "
			+ "		when upper(a.status) = upper('a') then 'Approved' "
			+ "		when upper(a.status) = upper('l') then 'Batal' "
			+ "		when upper(a.status) = upper('i') then 'In Wighbridge' "
			+ "		when upper(a.status) = upper('t') then 'Tapping' "
			+ "	end "
			+ "	from MobSj a "
			+ " where upper(a.status) <> upper('o') "
			+ "		and (upper(a.custName) like upper(:search)"
			+ "			or upper(a.transportirName) like upper(:search))")
	public abstract List<Object[]> findAllDataHistory(@Param("search") String search, Pageable page);

	@Query("select count(a) "
			+ "	from MobSj a "
			+ " where upper(a.status) <> upper('o') "
			+ "		and (upper(a.custName) like upper(:search)"
			+ "			or upper(a.transportirName) like upper(:search))")	
	public abstract Long countAllHistory(@Param("search") String search);
	
	@Query("select a, "
			+ "	case "
			+ "		when upper(a.status) = upper('o') then 'Open' "
			+ "		when upper(a.status) = upper('a') then 'Approved' "
			+ "		when upper(a.status) = upper('l') then 'Batal' "
			+ "		when upper(a.status) = upper('i') then 'In Wighbridge' "
			+ "		when upper(a.status) = upper('t') then 'Tapping' "
			+ "	end "
			+ "	from MobSj a "
			+ " where upper(a.status) = upper('o') "
			+ "		and a.transportirCode = :transportirCode "
			+ "		and (upper(a.custName) like upper(:search)"
			+ "			or upper(a.transportirName) like upper(:search))")
	public abstract List<Object[]> findAllDataNewByTransportir(@Param("transportirCode ") String transportirCode, 
			@Param("search") String search, Pageable page);
	
	@Query("select count(a) "
			+ "	from MobSj a "
			+ " where upper(a.status) = upper('o') "
			+ "		and a.transportirCode = :transportirCode "
			+ "		and (upper(a.custName) like upper(:search)"
			+ "			or upper(a.transportirName) like upper(:search))")
	public abstract Long countAllDataNewTransportir(
			@Param("transportirCode ") String transportirCode, @Param("search") String search);
	
	
	@Query("select a, "
			+ "	case "
			+ "		when upper(a.status) = upper('o') then 'Open' "
			+ "		when upper(a.status) = upper('a') then 'Approved' "
			+ "		when upper(a.status) = upper('l') then 'Batal' "
			+ "		when upper(a.status) = upper('i') then 'In Wighbridge' "
			+ "		when upper(a.status) = upper('t') then 'Tapping' "
			+ "	end "
			+ "	from MobSj a "
			+ " where upper(a.status) = upper('o') "
			+ "		and a.custCode = :custCode "
			+ "		and (upper(a.custName) like upper(:search)"
			+ "			or upper(a.transportirName) like upper(:search))")
	public abstract List<Object[]> findAllDataNewByCustomer(@Param("custCode ") String custCode, 
			@Param("search") String search, Pageable page);

	@Query("select count(a) "
			+ "	from MobSj a "
			+ " where upper(a.status) = upper('o') "
			+ "		and a.custCode = :custCode "
			+ "		and (upper(a.custName) like upper(:search)"
			+ "			or upper(a.transportirName) like upper(:search))")
	public abstract Long countAllDataNewByCustomer(
			@Param("custCode ") String custCode, @Param("search") String search);

	
	@Modifying
	@Query(value= "update mob_sj "
				+ "	set driver_id = :driverId,"
				+ "		phone = :phone, "
				+ "		vehicle_no = :vehicleNo, "
				+ "		vehicle_type = :vehicleType"
				+ " where sj_no = :sjNo", nativeQuery = true)
	public void updateDriver(
			@Param("driverId") Integer driverId,
			@Param("phone") String phone,
			@Param("vehicleNo") String vehicleNo,
			@Param("vehicleType") Integer vehicleType,
			@Param("sjNo") Integer sjNo);
	
	@Modifying
	@Query("update MobSj a "
			+ "	set a.status = :status, "
			+ "	a.destinationDate = :destinationDate "
			+ "	where a.sjNo = :sjNo")	
	public void updateStatus(
			@Param("status") String status, 
			@Param("sjNo") Integer sjNo,
			@Param("destinationDate") Date destinationDate);
	
	@Query(nativeQuery=true,value="select a.sj_no, a.sj_date, a.cust_name, a.address_ship_to, a.item_name, a.qty, "
			+ "c.qty_receive,c.name_of_recipient,c.notes, b.sppb_no, b.spt_no, b.spt_date, a.spm_no, "
			+ "d.fullname,d.phone_num, a.vehicle_no, a.container_no, b.take_assignment_date, "
			+ "concat(b.take_assignment_latitude,',', b.take_assignment_longitude) , a.departing_from_date, "
			+ "concat(a.departing_from_latitude,',', a.departing_from_longitude) , a.first_destination_date, "
			+ "concat(a.first_destination_latitude,',',a.first_destination_longitude), a.transit_date, "
			+ "CONCAT(a.transit_latitude,',',a.transit_longitude), a.destination_date, "
			+ "concat(a.destination_latitude,',',a.destination_longitude), "
			+ "case "
			+ "			when upper(a.status) = upper('a') then 'Approved' "
			+ "			when upper(a.status) = upper('l') then 'Batal' "
			+ "			when upper(a.status) = upper('i') then 'In Wighbridge' "
			+ "			when upper(a.status) = upper('t') then 'Tapping' "
			+ "			when upper(a.status) = upper('c') then 'Close' "
			+ "		end as stat, a.transportir_name "
			+ "from mob_sj a join mob_spt b "
			+ "on a.spt_no = b.spt_no "
			+ "join mob_goods_receive c "
			+ "on a.sj_no=c.sj_no "
			+ "join mob_driver d "
			+ "on c.driver_id = d.id "
			+ "where a.sj_no in ( :listSjNo ) and upper(a.status) = 'c' ")
	public List<Object[]> getReportSjDto( @Param("listSjNo") List<String> listSjNo);
	
}
