package id.co.indocyber.bmmRmiCms.dao;

import id.co.indocyber.bmmRmiCms.entity.MSugarcaneType;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MSugarcaneTypeDao extends JpaRepository<MSugarcaneType, String>{

}
