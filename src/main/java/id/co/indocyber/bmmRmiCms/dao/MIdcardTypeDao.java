package id.co.indocyber.bmmRmiCms.dao;

import id.co.indocyber.bmmRmiCms.entity.MIdcardType;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MIdcardTypeDao extends JpaRepository<MIdcardType, String>{

}
