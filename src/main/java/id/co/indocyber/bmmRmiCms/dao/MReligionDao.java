package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.co.indocyber.bmmRmiCms.entity.MReligion;

public interface MReligionDao extends JpaRepository<MReligion, String>{
	
	@Query("select a from MReligion a where a.isActive is true")
	public abstract List<MReligion> findAllActive();
	
}
