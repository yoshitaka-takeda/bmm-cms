package id.co.indocyber.bmmRmiCms.dao;

import id.co.indocyber.bmmRmiCms.entity.MAddressType;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface MAddressTypeDao extends JpaRepository<MAddressType, String>{
	
	@Query("select a from MAddressType a where a.isActive is true")
	public List<MAddressType> findAllActive();
	
}
