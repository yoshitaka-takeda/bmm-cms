package id.co.indocyber.bmmRmiCms.test;

import id.co.indocyber.bmmRmiCms.dao.MAddressTypeDao;

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.customsearch.Customsearch;
import com.google.api.services.customsearch.CustomsearchRequestInitializer;
import com.google.api.services.customsearch.model.Result;
import com.google.api.services.customsearch.model.Search;
import com.google.api.services.webmasters.Webmasters;
import com.google.api.services.webmasters.model.SitesListResponse;
import com.google.api.services.webmasters.model.WmxSite;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;

public class Test {

	public String hash(String username, String pass){
		String toHash = username+"rejoso"+pass+"manisindo";
		return DigestUtils.sha1Hex(toHash);
	}	
	
//	private static String CLIENT_ID = "YOUR_CLIENT_ID";
//	  private static String CLIENT_SECRET = "YOUR_CLIENT_SECRET";
//
//	  private static String REDIRECT_URI = "urn:ietf:wg:oauth:2.0:oob";
//
//	  private static String OAUTH_SCOPE = "https://www.googleapis.com/auth/webmasters.readonly";	
	
	public static void main(String args[]) throws IOException, GeneralSecurityException {
//		ConfigurableApplicationContext ctx = 
//				new ClassPathXmlApplicationContext("/META-INF/spring/app-config.xml");
//		MAddressTypeDao dao = ctx.getBean(MAddressTypeDao.class);
//		System.out.println(dao.findAll());
//		ctx.close();
//		String port = ( Executions.getCurrent().getServerPort()== 80 ) ? "" : (":"+Executions.getCurrent().getServerPort());
//		String url = Executions.getCurrent().getScheme()+"://"+Executions.getCurrent().getServerName()+port+Executions.getCurrent().getContextPath();
//		System.out.println("URL = "+url);

//	    HttpTransport httpTransport = new NetHttpTransport();
//	    JsonFactory jsonFactory = new JacksonFactory();
//
//	    GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
//	        httpTransport, jsonFactory, CLIENT_ID, CLIENT_SECRET, Arrays.asList(OAUTH_SCOPE))
//	        .setAccessType("online")
//	        .setApprovalPrompt("auto").build();
//
//	    String url = flow.newAuthorizationUrl().setRedirectUri(REDIRECT_URI).build();
//	    System.out.println("Please open the following URL in your browser then type the authorization code:");
//	    System.out.println("  " + url);
//	    System.out.println("Enter authorization code:");
//	    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//	    String code = br.readLine();
//
//	    GoogleTokenResponse response = flow.newTokenRequest(code).setRedirectUri(REDIRECT_URI).execute();
//	    GoogleCredential credential = new GoogleCredential().setFromTokenResponse(response);
//
//	    // Create a new authorized API client
//	    Webmasters service = new Webmasters.Builder(httpTransport, jsonFactory, credential)
//	        .setApplicationName("WebmastersCommandLine")
//	        .build();
//
//	    List<String> verifiedSites = new ArrayList<String>();
//	    Webmasters.Sites.List request = service.sites().list();
//
//	    // Get all sites that are verified
//	    try {
//	      SitesListResponse siteList = request.execute();
//	      for (WmxSite currentSite : siteList.getSiteEntry()) {
//	        String permissionLevel = currentSite.getPermissionLevel();
//	        if (permissionLevel.equals("siteOwner")) {
//	          verifiedSites.add(currentSite.getSiteUrl());
//	        }
//	      }
//	    } catch (IOException e) {
//	      System.out.println("An error occurred: " + e);
//	    }
//
//	    // Print all verified sites
//	    for (String currentSite : verifiedSites) {
//	      System.out.println(currentSite);
//	    }		

		
//	    String google = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=";
//	    String search = "stackoverflow";
//	    String charset = "UTF-8";
//
//	    URL url = new URL(google + URLEncoder.encode(search, charset));
//	    Reader reader = new InputStreamReader(url.openStream(), charset);
//	    GoogleResults results = new Gson().fromJson(reader, GoogleResults.class);
//
//	    // Show title and URL of 1st result.
//	    System.err.println("res = "+results.getResponseData().toString());
//	    System.out.println(results.getResponseData().getResults().get(0).getTitle());
//	    System.out.println(results.getResponseData().getResults().get(0).getUrl());		

//		String searchQuery = "test"; //The query to search
//	    String cx = "002845322276752338984:vxqzfa86nqc"; //Your search engine
//
//	    //Instance Customsearch
//	    Customsearch cs = new Customsearch.Builder(GoogleNetHttpTransport.newTrustedTransport(), JacksonFactory.getDefaultInstance(), null) 
//	                   .setApplicationName("MyApplication") 
//	                   .setGoogleClientRequestInitializer(new CustomsearchRequestInitializer("your api key")) 
//	                   .build();
//
//	    //Set search parameter
//	    Customsearch.Cse.List list = cs.cse().list(searchQuery).setCx(cx); 
//
//	    //Execute search
//	    Search result = list.execute();
//	    if (result.getItems()!=null){
//	        for (Result ri : result.getItems()) {
//	            //Get title, link, body etc. from search
//	            System.out.println("Hasil" + ri.getTitle() + ", " + ri.getLink());
//	        }
//	    }		

//		Map<String, String> parameter = new HashMap<>();
//		parameter.put("q", "Coffee");
//		parameter.put("location", "Portland");
//		GoogleSearchResults serp = new GoogleSearchResults(parameter);
//
//		JsonObject data = serp.getJson();
//		JsonArray results = (JsonArray) data.get("organic_results");
//		JsonObject first_result = results.get(0).getAsJsonObject();
//		System.out.println("first coffee: " + first_result.get("title").getAsString());		
	    
	    Test t = new Test();
	    String hash = t.hash("sendy123", "123456");
	                          
	    System.err.println("Hasil 1 = d78360b6fc0682c3705e0ad52f792acc78cbb49d");
	    System.err.println("Hasil 2 = "+hash);
	    
	    
	}



}
