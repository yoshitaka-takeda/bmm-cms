package id.co.indocyber.bmmRmiCms.tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Months;
import org.joda.time.Period;
import org.joda.time.Years;

/**
 * 
 * @author Leo Sendra
 */
public class DateUtil {
	public static final String DATE_FORMAT = "dd/MM/yyyy";
	private static final String DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm";
	private static Locale locale = new Locale("id", "ID");

	public static SimpleDateFormat getFormatter(String format) {
		return new SimpleDateFormat(format);
	}

	public static SimpleDateFormat getFormatterLocale(String format) {
		return new SimpleDateFormat(format, locale);
	}

	public static DateTime convertToJodaFormat(Date jdkDate) {
		return new DateTime(jdkDate);
	}

	public static Date convertToJdkFormat(DateTime jodaDate) {
		return jodaDate.toDate();
	}

	public static Date getMinDate() {
		return stringToDate("1/1/1900");
	}

	public static Date getMaxDate() {
		return stringToDate("31/12/4712");
	}

	public static Date stringToDateLocaleId(String strDate, String dateFormat) {
		try {
			return getFormatterLocale(dateFormat).parse(strDate);
		} catch (ParseException ex) {
			return null;
		}
	}

	public static Date stringToDateStd(String strDate, String dateFormat) {
		try {
			return getFormatter(dateFormat).parse(strDate);
		} catch (ParseException ex) {
			return null;
		}
	}

	public static Date stringToDate(String strDate) {
		try {
			return getFormatter(DATE_FORMAT).parse(strDate);
		} catch (ParseException ex) {
			return null;
		}
	}

	public static Date stringToDateTime(String strDate) {
		try {
			return getFormatter(DATE_TIME_FORMAT).parse(strDate);
		} catch (ParseException ex) {
			return null;
		}
	}

	public static String stringToDateToString(String strDate, String dateFormat) {
		try {
			Date date = getFormatter(dateFormat).parse(strDate);
			return getFormatter(DATE_FORMAT).format(date);
		} catch (ParseException ex) {
			return null;
		}
	}

	public static String dateTimeToString(Date date) {
		return getFormatter(DATE_TIME_FORMAT).format(date);
	}

	public static String dateToString(Date date) {
		return getFormatter(DATE_FORMAT).format(date);
	}

	public static String dateToString(Date date, String dateFormat) {
		return getFormatter(dateFormat).format(date);
	}

	public static String dateToStringUsingLocale(Date date, String dateFormat) {
		return getFormatterLocale(dateFormat).format(date);
	}

	public static String objectToDateFormatString(Object object,
			String dateFormat) {
		Date date = new Date();
		if (object instanceof Date) {
			date = (Date) object;
		}
		return getFormatter(dateFormat).format(date);
	}

	public static Date addHour(Date date, int number) {
		return convertToJdkFormat(convertToJodaFormat(date).plusHours(number));
	}

	public static Date addDate(Date date, int number) {
		return convertToJdkFormat(convertToJodaFormat(date).plusDays(number));
	}

	public static Date addMonth(Date date, int number) {
		return convertToJdkFormat(convertToJodaFormat(date).plusMonths(number));
	}

	public static Date addYear(Date date, int number) {
		return convertToJdkFormat(convertToJodaFormat(date).plusYears(number));
	}

	public static Date getSpecificDate(int year, int month, int date, int hour,
			int min, int sec) {
		return convertToJdkFormat(new DateTime(year, month, date, hour, min,
				sec));
	}

	/**
	 * Truncate date (remove time format from date)
	 * 
	 * @param date
	 * @return
	 */
	public static Date trunc(Date date) {
		return convertToJdkFormat(new DateTime(getYear(date), getMonth(date),
				getDate(date), 0, 0, 0));
	}

	public static int getYear(Date date) {
		return convertToJodaFormat(date).getYear();
	}

	public static int getMonth(Date date) {
		return convertToJodaFormat(date).getMonthOfYear();
	}

	public static int getDate(Date date) {
		return convertToJodaFormat(date).getDayOfMonth();
	}

	public static int getDateMaximum(Date date) {
		return toCalendar(date).getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	public static int getDaysBetween(Date startDate, Date endDate) {
		Days days = Days.daysBetween(convertToJodaFormat(startDate),
				convertToJodaFormat(endDate));
		return days.getDays();
	}

	public static int getMonthsBetween(Date startDate, Date endDate) {
		Months months = Months.monthsBetween(convertToJodaFormat(startDate),
				convertToJodaFormat(endDate));
		return months.getMonths();
	}

	public static int getYearsBetween(Date startDate, Date endDate) {
		Years years = Years.yearsBetween(convertToJodaFormat(startDate),
				convertToJodaFormat(endDate));
		return years.getYears();
	}

	public static int getHoursBetween(Date startDate, Date endDate) {
		Hours hours = Hours.hoursBetween(convertToJodaFormat(startDate),
				convertToJodaFormat(endDate));
		return hours.getHours();
	}

	public static int getMinutesBetween(Date startDate, Date endDate) {
		Minutes min = Minutes.minutesBetween(convertToJodaFormat(startDate),
				convertToJodaFormat(endDate));
		return min.getMinutes();
	}

	public static Period getPeriodBetween(Date startDate, Date endDate) {
		return new Period(convertToJodaFormat(startDate),
				convertToJodaFormat(endDate));
	}

	public static int getDaysFromPeriod(Period p) {
		return p.getDays();
	}

	public static int getMonthsFromPeriod(Period p) {
		return p.getMonths();
	}

	public static int getYearsFromPeriod(Period p) {
		return p.getYears();
	}

	public static boolean isDateCheck(String strDate, String dateFormat) {
		try {
			getFormatter(dateFormat).parse(strDate);

			return true;
		} catch (ParseException ex) {
			return false;
		} catch (NullPointerException e) {
			return false;
		}
	}

	public static boolean isDateCheck(Date strDate, String dateFormat) {
		try {
			String strDates = dateTimeToString(strDate);
			getFormatter(dateFormat).parse(strDates);

			return true;
		} catch (ParseException ex) {
			return false;
		} catch (NullPointerException e) {
			return false;
		}
	}

	public static Date updateDay(Date oldDate, int dateValue) {
		int year = getYear(oldDate);
		int month = getMonth(oldDate);
		return convertToJdkFormat(new DateTime(year, month, dateValue, 0, 0));
	}

	/***
	 * buat calendar
	 * 
	 * @param date
	 * @return
	 */
	public static Calendar toCalendar(Date date) {
		if (date == null) {
			throw new RuntimeException("Tanggal tidak boleh null");
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c;
	}
	
	public static String getLastDay(String month, String year) {
		GregorianCalendar calendar = new GregorianCalendar();
		int monthInt = Integer.parseInt(month);
		int yearInt = Integer.parseInt(year);
		monthInt = monthInt - 1;
		calendar.set(yearInt, monthInt, 1);
		int dayInt = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
		return Integer.toString(dayInt);
	}
	
	public static String getLastDayOfMonth(Integer month, String dateFormat) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, month);
		
		int max = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		cal.set(Calendar.DAY_OF_MONTH, max);
		return dateToString(cal.getTime(), dateFormat);
	}

//	public static void main(String[] args) {
//		DateTime start = new DateTime(2004, 12, 25, 20, 30, 0, 0);
//		System.out.println(DateUtil.addHour(convertToJdkFormat(start), 8));
//		DateTime end = new DateTime(2006, 1, 2, 0, 0, 0, 0);
//		String tanggal = "30.12.2012 20:55";
//		System.out.println(stringToDateTime(tanggal));
//		System.out.println(dateTimeToString(convertToJdkFormat(start)));
//		System.out.println(dateTimeToString(getMaxDate()));
//		// period of 1 year and 7 days
//		Period period = new Period(start, end);
//		System.out.println(period.getYears() + "--" + period.getMonths() + "--"
//				+ period.getDays());
//		System.out.println(getDaysBetween(
//				getSpecificDate(2014, 3, 1, 10, 0, 0),
//				getSpecificDate(2014, 4, 10, 10, 0, 0)));
//		System.out.println("keluar : " + updateDay(new Date(), 8));
//		System.out.println("TANGGAL : " + dateToStringUsingLocale(stringToDateStd("01-JUL-2017", "dd-MMM-yyyy"), "MMMM yyyy"));
//		System.out.println("Tanggal Akhir Bulan : " + getLastDay("07", "2017"));
//		System.out.println("Kalendar : " + getLastDayOfMonth(-2,"yyyyMMdd"));
//	}
	
	//add by KSN
	public static Date minusDate(Date date, int number) {
		return convertToJdkFormat(convertToJodaFormat(date).minusDays(number));
	}
	
	@SuppressWarnings("deprecation")
	public static Integer subtract(Date startDate, Date endDate)
	{
		return (int) Math.abs(endDate.getDate()-startDate.getDate());
	}
}
