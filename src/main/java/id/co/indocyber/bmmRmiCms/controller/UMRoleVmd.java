package id.co.indocyber.bmmRmiCms.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.util.StringUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.entity.MRole;
import id.co.indocyber.bmmRmiCms.service.MRoleSvc;
import id.co.indocyber.bmmRmiCms.service.UMRoleSvc;
import id.co.indocyber.bmmRmiCms.tools.StringConstants;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class UMRoleVmd extends BaseVmd {
	
	@WireVariable
	private UMRoleSvc uMRoleSvc;
	
	private MUserDto user;
	
	private List<MRoleDto> listRoleDto = new ArrayList<>();
	private MRoleDto editRoleDto, selectedRole;
	
	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;

	private boolean freezeId;
	private int editIndex = -1;
	private Boolean editMode;
	private String pageTitle;

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private boolean superAdmin;
	private Integer recordCount = 1;
	
	private String sortBy = "";
	private boolean sortAsc = false;
	
	
	@Init
	@NotifyChange({"editMode","user","listRoleDto","recordCount","maxPage"})
	public void load(){
		editMode = Boolean.valueOf((Boolean) Sessions.getCurrent().getAttribute("editMode"));
		user = (MUserDto) Sessions.getCurrent().getAttribute("user");
		
		listRoleDto = uMRoleSvc.findAll(searchData, pageNo, perPage ,"isActive desc, modifiedDate", false);
		recordCount = uMRoleSvc.countAll(searchData);
		maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
		
		for (MMenuRoleDto x : user.getRolePrivs()) {
			if ("UM-2".equals(x.getMenuCode())) {
				btnAdd = !x.getIsAdd();
				btnDel = !x.getIsDelete();
				btnDl = !x.getIsDownload();
				btnEdit = !x.getIsEdit();
				break;
			}
		}

	}
	
	@Command("switchPage")
	@NotifyChange({ "pageNo", "listRoleDto", "resetUser" })
	public void switchPage(@BindingParam("action") String action) {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}
			listRoleDto = uMRoleSvc.findAll(searchData, pageNo, perPage ,"isActive desc, modifiedDate", false);		}
	}
	
	@Command("src")
	@NotifyChange({"listRoleDto", "pageNo", "maxPage", "recordCount"})
	public void search() {
		search = true;
		pageNo = 1;
		listRoleDto = uMRoleSvc.findAll(searchData, pageNo, perPage ,"isActive desc, modifiedDate", false);		
		recordCount = uMRoleSvc.countAll(searchData);
		maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
	}
	
	@Command("reload")
	@NotifyChange({"recordCount","listRoleDto","maxPage","searchData"})
	public void reload(){
		searchData = "";
		recordCount = uMRoleSvc.countAll(searchData);
		listRoleDto = uMRoleSvc.findAll(searchData, pageNo, perPage ,"isActive desc, modifiedDate", false);		
		maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
	}

	
	@Command
	public void back(){
		editMode = false;
		BindUtils.postNotifyChange(null, null, this, "editMode");
	}
	
	@Command
	@NotifyChange({"selectedRole","flagIconWarning"})
	public void ubahStatusAktif(@BindingParam("dto") MRoleDto dto){
		showConfirm("E002");
		setFlagIconWarning(true);
		selectedRole = new MRoleDto();
		selectedRole = dto;
		selectedRole.setIsActive(false);
		selectedRole.setModifiedBy(user.getFullname());
		selectedRole.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@Command
	@NotifyChange({"selectedRole","flagIconWarning"})
	public void ubahStatusNonAktif(@BindingParam("dto") MRoleDto dto){
		showConfirm("E003");
		setFlagIconWarning(true);
		selectedRole = new MRoleDto();
		selectedRole = dto;
		selectedRole.setIsActive(true);
		selectedRole.setModifiedBy(user.getFullname());
		selectedRole.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@Command
	@NotifyChange({"recordCount","listRoleDto","maxPage"})
	public void yesConfirm(){
		int result = uMRoleSvc.save(selectedRole);
		if(result == 1){
			flagConfirmMsg = false;
			showSuccessMsgBox("E000");
		}else{
			showErrorMsgBox("E001");
		}
		
		//reload list
		recordCount = uMRoleSvc.countAll(searchData);
		listRoleDto = uMRoleSvc.findAll(searchData, pageNo, perPage ,"isActive desc, modifiedDate", false);
		maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
		BindUtils.postNotifyChange(null, null, this, "flagConfirmMsg");
	}

	@Command
	public void add(){
		editRoleDto = new MRoleDto();
		editMode = true;
		BindUtils.postNotifyChange(null, null, this, "editRoleDto");
		BindUtils.postNotifyChange(null, null, this, "editMode");
	}
	
	@Command
	public void edit(@BindingParam("dto") MRoleDto dto){
		editRoleDto = dto;
		editMode = true;
		BindUtils.postNotifyChange(null, null, this, "editRoleDto");
		BindUtils.postNotifyChange(null, null, this, "editMode");
	}
	
	@Command
	@NotifyChange({"editMode","recordCount","listRoleDto","maxPage"})
	public void save(@BindingParam("btn")Component btn) {
		boolean cekRoleName = uMRoleSvc.cekRole(editRoleDto.getRoleName().trim()==null?"":editRoleDto.getRoleName().trim());
		if(cekRoleName){
			showErrorMsgBox("E101");
		}else{
			try{
				if(editRoleDto.getIsActive() == null){
					editRoleDto.setIsActive(true);
				}
				if(editRoleDto.getIsAdmin() == null){
					editRoleDto.setIsAdmin(true);
				}		
				editRoleDto.setCreatedBy(user.getFullname());
				editRoleDto.setCreatedDate(new Timestamp(System.currentTimeMillis()));
				editRoleDto.setModifiedBy(user.getFullname());
				editRoleDto.setModifiedDate(new Timestamp(System.currentTimeMillis()));
				
				String fullErrMsg = StringConstants.MSG_FORM_TEMPLATE;
				int error = 0;
				if (StringUtils.isEmpty(editRoleDto.getRoleName())) {
					error++;
					fullErrMsg += String.format(
							StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Nama Grup");
				}
				if (StringUtils.isEmpty(editRoleDto.getDescription())) {
					error++;
					fullErrMsg += String.format(
							StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Deskripsi");
				}

				if (error > 0) {
					Messagebox.show(fullErrMsg, "Message", 0, null);
				}else{
					int result = uMRoleSvc.save(editRoleDto);
					if(result == 1){
						showSuccessMsgBox("E000");
					}else{
						showErrorMsgBox("E001");
					}
					editMode = false;
					
					//reload
					recordCount = uMRoleSvc.countAll(searchData);
					listRoleDto = uMRoleSvc.findAll(searchData, pageNo, perPage ,"isActive desc, modifiedDate", false);
					maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
					
					((Window)btn.getParent().getParent()).setVisible(false);
				}
			}catch(Exception e){
				e.printStackTrace();
				showErrorMsgBox("E001");
			}
		}
	}
	
	@Command
	public void close(){
		editMode = false;
		BindUtils.postNotifyChange(null, null, this, "editMode");		
	}
	
	@Command("sort")
	@NotifyChange({"listRoleDto"})
	public void sort(@BindingParam("col") String column) {
		if (activeCheck()) {
			if (sortBy.equals(column)) {
				sortAsc = !sortAsc;
			} else {
				sortBy = column;
				sortAsc = true;
			}
			listRoleDto = uMRoleSvc.findAll(searchData, pageNo, perPage ,"isActive desc, modifiedDate", false);
		}
	}

	
	public List<MRoleDto> getListRoleDto() {
		return listRoleDto;
	}
	public void setListRoleDto(List<MRoleDto> listRoleDto) {
		this.listRoleDto = listRoleDto;
	}
	public MRoleDto getEditRoleDto() {
		return editRoleDto;
	}
	public void setEditRoleDto(MRoleDto editRoleDto) {
		this.editRoleDto = editRoleDto;
	}
	public boolean isBtnAdd() {
		return btnAdd;
	}
	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}
	public boolean isBtnDel() {
		return btnDel;
	}
	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}
	public boolean isBtnEdit() {
		return btnEdit;
	}
	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}
	public boolean isBtnDl() {
		return btnDl;
	}
	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}
	public boolean isFreezeId() {
		return freezeId;
	}
	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}
	public int getEditIndex() {
		return editIndex;
	}
	public void setEditIndex(int editIndex) {
		this.editIndex = editIndex;
	}
	public Boolean getEditMode() {
		return editMode;
	}
	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}
	public String getPageTitle() {
		return pageTitle;
	}
	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}
	public Integer getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public Integer getMaxPage() {
		return maxPage;
	}
	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}
	public boolean isSearch() {
		return search;
	}
	public void setSearch(boolean search) {
		this.search = search;
	}
	public boolean isSuperAdmin() {
		return superAdmin;
	}
	public void setSuperAdmin(boolean superAdmin) {
		this.superAdmin = superAdmin;
	}
	public Integer getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public boolean isSortAsc() {
		return sortAsc;
	}
	public void setSortAsc(boolean sortAsc) {
		this.sortAsc = sortAsc;
	}
	public MUserDto getUser() {
		return user;
	}
	public void setUser(MUserDto user) {
		this.user = user;
	}
	public MRoleDto getSelectedRole() {
		return selectedRole;
	}

	public void setSelectedRole(MRoleDto selectedRole) {
		this.selectedRole = selectedRole;
	}
	
	

	
}
