package id.co.indocyber.bmmRmiCms.controller;

import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.entity.MUserRole;
import id.co.indocyber.bmmRmiCms.service.MUserRoleSvc;
import id.co.indocyber.bmmRmiCms.service.MUserSvc;

import java.util.Date;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Include;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public abstract class BaseVmd {
	
	@WireVariable
	private MUserRoleSvc mUserRoleSvc;
	@WireVariable
	private MUserSvc mUserSvc;
	
	private MUserDto user1;
	
	public int perPage = 25;
	
	public Date filterStartDate;
	public Date filterEndtDate;	
	
	//Variable untuk keperluan customMsgBox
	public String msg;
	public String searchData;
	public boolean flagIconWarning = false;
	public boolean flagIconCek = false;
	public boolean flagIconError = false;
	public boolean flagMsgFree =false;
	public boolean flagConfirmMsg = false;
	
	public void navigate(@BindingParam("uri") String locationUri) {
		//TODO: auto logout if account / role disabled (for navigation via buttons)
		//TODO: recheck account roles (?)
		Include include = (Include) Executions.getCurrent().getDesktop()
				.getPage("index").getFellow("mainInclude");
		Sessions.getCurrent().setAttribute("lastPage",locationUri);
		include.setSrc("/WEB-INF/pages" + locationUri);
	}

	// FOR EACH BUTTON
	public boolean activeCheck(){
		if (Sessions.getCurrent().getAttribute("user") != null) {
			user1 = (MUserDto) Sessions.getCurrent().getAttribute("user");
			MUserRole x = mUserRoleSvc.cmsAccess(user1.getEmail());
			if(!mUserSvc.findByUsernameOrEmail(user1.getUsername()).getIsActive() ||
					!(x==null? false : x.getIsActive())){
				Messagebox.show("Akun anda telah dinonaktifkan. Anda akan kembali ke halaman login.", 
						"Perhatian", new Button[] { Button.OK},
						Messagebox.ERROR, Button.OK,
						new EventListener<Messagebox.ClickEvent>() {
							@Override
							public void onEvent(ClickEvent event)
									throws Exception {
								// TODO Auto-generated method stub
								if (Messagebox.ON_OK.equals(event.getName())) {
									Sessions.getCurrent().removeAttribute("user");
									Sessions.getCurrent().removeAttribute("lastPage");
									Executions.getCurrent().sendRedirect("/login.zul");
								}
							}}
				);
				return false;
			}
			else{
				return true;
			}
		}
		else{
			return false;
		}
	}
	
	@Command
	@NotifyChange({ "flagIconCek", "flagIconWarning", "flagIconError", "msg","flagMsgFree" })
	public void showWarningMsgBox(String msg1) {
		msg = Labels.getLabel(msg1);
		flagIconWarning = true;
		flagIconCek = false;
		flagIconError = false;
		flagMsgFree = true;
		BindUtils.postNotifyChange(null, null, this, "flagIconCek");
		BindUtils.postNotifyChange(null, null, this, "flagIconError");
		BindUtils.postNotifyChange(null, null, this, "flagIconWarning");
		BindUtils.postNotifyChange(null, null, this, "msg");
		BindUtils.postNotifyChange(null, null, this, "flagMsgFree");
	}

	@Command
	@NotifyChange({ "flagIconCek", "flagIconWarning", "flagIconError", "msg", "flagMsgFree" })
	public void showSuccessMsgBox(String msg1) {
		msg = Labels.getLabel(msg1);
		flagIconWarning = false;
		flagIconCek = true;
		flagIconError = false;
		flagMsgFree = true;
		BindUtils.postNotifyChange(null, null, this, "flagIconCek");
		BindUtils.postNotifyChange(null, null, this, "flagIconError");
		BindUtils.postNotifyChange(null, null, this, "flagIconWarning");
		BindUtils.postNotifyChange(null, null, this, "msg");
		BindUtils.postNotifyChange(null, null, this, "flagMsgFree");
		
	}
	
	@Command
	@NotifyChange({ "flagIconCek", "flagIconWarning", "flagIconError", "msg", "flagMsgFree" })
	public void showErrorMsgBox(String msg1) {
		msg = Labels.getLabel(msg1);
		flagIconWarning = false;
		flagIconCek = false;
		flagIconError = true;
		flagMsgFree = true;
		BindUtils.postNotifyChange(null, null, this, "flagIconCek");
		BindUtils.postNotifyChange(null, null, this, "flagIconError");
		BindUtils.postNotifyChange(null, null, this, "flagIconWarning");
		BindUtils.postNotifyChange(null, null, this, "msg");
		BindUtils.postNotifyChange(null, null, this, "flagMsgFree");
		
	}

	
	@Command
	@NotifyChange({"msg", "flagConfirmMsg" })
	public void showConfirm(String msg1){
		msg = Labels.getLabel(msg1);
		flagConfirmMsg = true;
		BindUtils.postNotifyChange(null, null, this, "msg");
		BindUtils.postNotifyChange(null, null, this, "flagConfirmMsg");
	}
	
	@Command
	public void yesConfirm(){
		
	}
	
	@Command
	@NotifyChange({ "flagIconCek", "flagIconWarning", "flagIconError","flagMsgFree","flagConfirmMsg" })
	public void OK(){
		flagIconWarning = false;
		flagIconCek = false;
		flagIconError = false;
		flagMsgFree = false;
		flagConfirmMsg = false;
		BindUtils.postNotifyChange(null, null, this, "flagConfirmMsg");
		BindUtils.postNotifyChange(null, null, this, "flagIconCek");
		BindUtils.postNotifyChange(null, null, this, "flagIconError");
		BindUtils.postNotifyChange(null, null, this, "flagIconWarning");
		BindUtils.postNotifyChange(null, null, this, "flagMsgFree");

	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public boolean isFlagIconWarning() {
		return flagIconWarning;
	}

	public void setFlagIconWarning(boolean flagIconWarning) {
		this.flagIconWarning = flagIconWarning;
	}

	public boolean isFlagIconCek() {
		return flagIconCek;
	}

	public void setFlagIconCek(boolean flagIconCek) {
		this.flagIconCek = flagIconCek;
	}

	public boolean isFlagIconError() {
		return flagIconError;
	}

	public void setFlagIconError(boolean flagIconError) {
		this.flagIconError = flagIconError;
	}

	public boolean isFlagMsgFree() {
		return flagMsgFree;
	}

	public void setFlagMsgFree(boolean flagMsgFree) {
		this.flagMsgFree = flagMsgFree;
	}

	public boolean isFlagConfirmMsg() {
		return flagConfirmMsg;
	}

	public void setFlagConfirmMsg(boolean flagConfirmMsg) {
		this.flagConfirmMsg = flagConfirmMsg;
	}

	public String getSearchData() {
		return searchData;
	}

	public void setSearchData(String searchData) {
		this.searchData = searchData;
	}

	public int getPerPage() {
		return perPage;
	}

	public void setPerPage(int perPage) {
		this.perPage = perPage;
	}

	public Date getFilterStartDate() {
		return filterStartDate;
	}

	public void setFilterStartDate(Date filterStartDate) {
		this.filterStartDate = filterStartDate;
	}

	public Date getFilterEndtDate() {
		return filterEndtDate;
	}

	public void setFilterEndtDate(Date filterEndtDate) {
		this.filterEndtDate = filterEndtDate;
	}


	
}
