package id.co.indocyber.bmmRmiCms.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.entity.MRole;
import id.co.indocyber.bmmRmiCms.entity.MUser;
import id.co.indocyber.bmmRmiCms.entity.MUserRole;
import id.co.indocyber.bmmRmiCms.entity.UserMenuAccordion;
import id.co.indocyber.bmmRmiCms.service.MMenuRoleSvc;
import id.co.indocyber.bmmRmiCms.service.MRoleSvc;
import id.co.indocyber.bmmRmiCms.service.MUserRoleSvc;
import id.co.indocyber.bmmRmiCms.service.MUserSvc;
import id.co.indocyber.bmmRmiCms.tools.StringConstants;

import org.springframework.util.StringUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class LoginVmd {
	@WireVariable
	MUserSvc mUserSvc;
	
	@WireVariable
	MUserRoleSvc mUserRoleSvc;
	
	@WireVariable
	MRoleSvc mRoleSvc;

	@WireVariable
	MMenuRoleSvc mMenuRoleSvc;

	private String username;
	private String password;

	MUserDto user;
	List<MMenuRoleDto> rolePrivs;
	List<UserMenuAccordion> sidebarMenu;

	int maxWrong;
	int adminRole;
	Date today;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Init
	public void load() {
		
	}

	@Command("login")
	@NotifyChange({ "username", "password" })
	public void login() {
		if (!StringUtils.isEmpty(username) && !StringUtils.isEmpty(password)) {
			boolean existence;
			boolean notExpired;
			try {
				existence = mUserSvc.existence(username);
			} catch (Exception e) {
				existence = false;
			}
			if (existence) {
				user = new MUserDto(mUserSvc.findByUsernameOrEmail(username));
				if (user.getIsActive()) {
					if (mUserSvc.login(username, password)) {
						MUserRole role = mUserRoleSvc.cmsAccess(user.getEmail());
						if (role!=null){
							MRole r2 = mRoleSvc.findOne(role.getRoleId());
							if (role.getIsActive()&&( r2==null ? false : r2.getIsActive() )) {
								rolePrivs = mMenuRoleSvc.getAccessPrivs(role.getRoleId(), "Web");
								sidebarMenu = new ArrayList<UserMenuAccordion>();
								for (MMenuRoleDto x : rolePrivs) {
									sidebarMenu.add(new UserMenuAccordion(x.getMenuName().toUpperCase()
										, x.getMenuLink()));
								}
								user.setMenuList(sidebarMenu);
								user.setRolePrivs(rolePrivs);
								user.setRoleName(r2.getRoleName());
								user.setAdministrator(mUserRoleSvc.isRoleAdministrator(user.getEmail()));
								user.setCustomer(mUserRoleSvc.isRoleCustomer(user.getEmail()));
								Sessions.getCurrent().setAttribute("user",user);
								Executions.sendRedirect("/index.zul");
							} else {
								Clients.alert(StringConstants.MSG_ROLE_DISABLED, "Warning", "");
							}
						}
						else{
							Clients.alert(StringConstants.MSG_ROLE_UNAUTHORIZED, "Warning", "");
						}
					} else {
						Clients.alert(StringConstants.MSG_INVALID_PASSWORD, "Warning", "");
						
						/*
						int triesLeft = maxWrong - user.getCountInvalidPasswd() - 1;
						if (triesLeft > 0) {
							//Clients.alert(tbSysMessageSvc.getMessage("20003"),"Warning", "");
							
						} else {
							Clients.alert(tbSysMessageSvc.getMessage("20004"),"Warning", "");
						}
						*/
					}
				} else {
					Clients.alert(StringConstants.MSG_USER_DISABLED,"Warning", "");
				}
			} else {
				Clients.alert(StringConstants.MSG_USER_NOT_FOUND, "Warning", "");
			}
		} else {
			Clients.alert(StringConstants.MSG_LOGIN_FILL_ALL, "Warning", "");
		}
	}

	@Command("forgetPass")
	public void forgetPass() {
		Sessions.getCurrent().setAttribute("recover", new MUser());
		Executions.sendRedirect("/forgetPassword/forget1.zul");
	}
}
