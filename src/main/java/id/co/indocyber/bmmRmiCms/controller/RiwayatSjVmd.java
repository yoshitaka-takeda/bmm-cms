package id.co.indocyber.bmmRmiCms.controller;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.image.AImage;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Filedownload;
<<<<<<< HEAD
import org.zkoss.zul.ListModelList;
=======
>>>>>>> branch 'master' of https://gitlab.com/ari.haryanto/rmi-spt-sj.git
import org.zkoss.zul.Messagebox;

import id.co.indocyber.bmmRmiCms.dao.MobSjDao;
import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MobGoodsReceiveAttachmentDto;
import id.co.indocyber.bmmRmiCms.dto.MobGoodsReceiveDto;
import id.co.indocyber.bmmRmiCms.dto.MobSjDto;
<<<<<<< HEAD
import id.co.indocyber.bmmRmiCms.dto.ReportSjDto;
import id.co.indocyber.bmmRmiCms.entity.MUser;
import id.co.indocyber.bmmRmiCms.report.CustomDataSource;
import id.co.indocyber.bmmRmiCms.report.ExcelReport;
import id.co.indocyber.bmmRmiCms.report.ReportConfig;
import id.co.indocyber.bmmRmiCms.report.ReportType;
=======
import id.co.indocyber.bmmRmiCms.report.ExcelReport;
>>>>>>> branch 'master' of https://gitlab.com/ari.haryanto/rmi-spt-sj.git
import id.co.indocyber.bmmRmiCms.service.MGlobalImageSvc;
import id.co.indocyber.bmmRmiCms.service.RiwayatSjSvc;
import id.co.indocyber.bmmRmiCms.tools.StringConstants;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class RiwayatSjVmd extends BaseVmd {
	
	@WireVariable
	private RiwayatSjSvc riwayatSjSvc;
	
	@WireVariable
	private MGlobalImageSvc mGlobalImageSvc;
	
	private MUserDto user;
	
	private static final String urlGoogleMaps = "https://www.google.com/maps/place/";
	
	private List<MobSjDto> listIndex = new ArrayList<>();
	private MobSjDto selectedDto;
	
	private MobGoodsReceiveDto receiveDto;
	private List<MobGoodsReceiveAttachmentDto> listAttchment = new ArrayList<>();
	
	private boolean freezeId;
	
	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;	

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private boolean superAdmin;
	private Integer recordCount = 1;
	
	private boolean viewMode = false;
	private boolean konfirmasiMode = false;
	
	private boolean attachment1, attachment2, attachment3, attachment4, attachment5;
	private AImage img1, img2, img3, img4, img5, imgSignature;
	
	ReportType reportType = null;
	private ReportConfig reportConfig = null;

	private ListModelList<ReportType> reportTypesModel = new ListModelList<ReportType>(
			Arrays.asList(new ReportType("PDF", "pdf"), 
					new ReportType("Excel", "xls")));
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		
	@SuppressWarnings("unchecked")
	@Init
	@NotifyChange({"btnAdd","btnDel","btnDl","btnEdit","user",
		"listIndex","recordCount","maxPage","imgSignature"})
	public void load() throws ParseException{
		try{
			user = (MUserDto) Sessions.getCurrent().getAttribute("user");
			
			if(user.isAdministrator()){
				Map<String, Object> map = riwayatSjSvc.findAll(searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
				listIndex = (List<MobSjDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
			}else{
				if(user.isCustomer()){
					Map<String, Object> map = riwayatSjSvc.findByCustomer(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
					listIndex = (List<MobSjDto>) map.get("content");
					recordCount = (Integer) map.get("totalSize");
					maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
				}else{
					Map<String, Object> map = riwayatSjSvc.findByTransportir(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
					listIndex = (List<MobSjDto>) map.get("content");
					recordCount = (Integer) map.get("totalSize");
					maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
				}
			}
			
			for (MMenuRoleDto x : user.getRolePrivs()) {
				if ("SSJ-2".equals(x.getMenuCode())) {
					btnAdd = !x.getIsAdd();
					btnDel = !x.getIsDelete();
					btnDl = !x.getIsDownload();
					btnEdit = !x.getIsEdit();
					break;
				}
			}
			
			imgSignature = new AImage("img signature", mGlobalImageSvc.findOne("IMG001").getParmImgValue());
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Command("switchPage")
	@NotifyChange({ "pageNo", "listIndex"})
	public void switchPage(@BindingParam("action") String action) throws ParseException {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}

			if(user.isAdministrator()){
				Map<String, Object> map = riwayatSjSvc.findAll(searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
				listIndex = (List<MobSjDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
			}else{
				if(user.isCustomer()){
					Map<String, Object> map = riwayatSjSvc.findByCustomer(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
					listIndex = (List<MobSjDto>) map.get("content");
					recordCount = (Integer) map.get("totalSize");
					maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
				}else{
					Map<String, Object> map = riwayatSjSvc.findByTransportir(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
					listIndex = (List<MobSjDto>) map.get("content");
					recordCount = (Integer) map.get("totalSize");
					maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
				}
			}
		}	
	}
	
	@SuppressWarnings("unchecked")
	@Command("src")
	@NotifyChange({"listIndex", "pageNo", "maxPage", "recordCount","filterStartDate","filterEndtDate"})
	public void search() {
		search = true;
		pageNo = 1;
		
		if(filterStartDate == null && filterEndtDate != null){
			filterStartDate = new Date();
		}
		if(filterEndtDate == null && filterStartDate != null){
			filterEndtDate = new Date();
		}
		
		if(user.isAdministrator()){
			Map<String, Object> map = riwayatSjSvc.findAll(searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
			listIndex = (List<MobSjDto>) map.get("content");
			recordCount = (Integer) map.get("totalSize");
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
		}else{
			if(user.isCustomer()){
				Map<String, Object> map = riwayatSjSvc.findByCustomer(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
				listIndex = (List<MobSjDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
			}else{
				Map<String, Object> map = riwayatSjSvc.findByTransportir(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
				listIndex = (List<MobSjDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Command("reload")
	@NotifyChange({"recordCount","listIndex","maxPage","searchData","filterStartDate","filterEndtDate"})
	public void reload() throws ParseException{
		searchData = null;
		filterStartDate = null;
		filterEndtDate = null;
		
		if(user.isAdministrator()){
			Map<String, Object> map = riwayatSjSvc.findAll(searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
			listIndex = (List<MobSjDto>) map.get("content");
			recordCount = (Integer) map.get("totalSize");
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
		}else{
			if(user.isCustomer()){
				Map<String, Object> map = riwayatSjSvc.findByCustomer(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
				listIndex = (List<MobSjDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
			}else{
				Map<String, Object> map = riwayatSjSvc.findByTransportir(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
				listIndex = (List<MobSjDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
			}
		}
	}
	
	@Command("getReport")
	@NotifyChange("reportConfig")
	public void showReport() {
		
<<<<<<< HEAD
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("namaTransportir", user.getTransportirName());
		
		reportConfig = new ReportConfig();
		reportConfig.setSource(ReportConfig.SOURCE_QUERY_DATA);
		reportConfig.setType(new ReportType("Excel", "xls"));
		reportConfig.setParameters(parameters);
		reportConfig.setDataSource(new CustomDataSource(listIndex));
	
	}
	
	@Command("export")
	public void exportReport(){
		List<String> listNoSj = new ArrayList<>();
		for (MobSjDto x : listIndex) {
			listNoSj.add(x.getSjNo().toString());
		}
		
		if(!listIndex.isEmpty()){
			ExcelReport exp = new ExcelReport();
			Filedownload.save(exp.generateReport(riwayatSjSvc.getReportSj(listNoSj), user.getFullname(),user.isAdministrator()),null,
					"Riwayat Surat Jalan "+user.getFullname().replaceAll("/", "").trim()+".xlsx");
			
		}else{
			Messagebox.show(StringConstants.MSG_ERR_LIST_EMPTY_REPORT, "Message", 0, null);
		}
=======
	@Command("export")
	public void exportReport(){
		List<String> listNoSj = new ArrayList<>();
		for (MobSjDto x : listIndex) {
			listNoSj.add(x.getSjNo().toString());
		}
		if(!listIndex.isEmpty()){
			ExcelReport exp = new ExcelReport();
			Filedownload.save(exp.generateReport(riwayatSjSvc.getReportSj(listNoSj), user.getFullname(),user.isAdministrator()),null,
					"Riwayat Surat Jalan "+user.getFullname().replaceAll("/", "").trim()+".xlsx");
		
		}else{
			Messagebox.show(StringConstants.MSG_ERR_LIST_EMPTY_REPORT, "Message", 0, null);
		}
		
		
		
>>>>>>> branch 'master' of https://gitlab.com/ari.haryanto/rmi-spt-sj.git
		
		
	}
	
	@Command
	public void penugasanSupir(@BindingParam("item") MobSjDto dto) {
		selectedDto = dto;
		//cek transit
		if(riwayatSjSvc.cekTransit(dto.getSptNo())){
			selectedDto.setIsTransit(true);
		}else{
			selectedDto.setIsTransit(false);
		}
		
		konfirmasiMode = false;
		viewMode = true;
		BindUtils.postNotifyChange(null, null, this, "konfirmasiMode");
		BindUtils.postNotifyChange(null, null, this, "viewMode");
		BindUtils.postNotifyChange(null, null, this, "selectedDto");
	}
	
	@Command
	public void view(@BindingParam("item") MobSjDto dto){
		selectedDto = dto;
		konfirmasiMode = false;
		viewMode = true;
		BindUtils.postNotifyChange(null, null, this, "konfirmasiMode");
		BindUtils.postNotifyChange(null, null, this, "viewMode");
		BindUtils.postNotifyChange(null, null, this, "selectedDto");				
	}
	
	@Command
	public void konfirmasiBarang(@BindingParam("item") MobSjDto dto){
		selectedDto = dto;
		konfirmasiMode = true;
		viewMode = false;
		
		receiveDto = riwayatSjSvc.getReceiveBySj(dto.getSjNo());
		if(receiveDto != null){
			imgSignature = receiveDto.getImgSignature();
		}
		listAttchment = riwayatSjSvc.getListAttachmentBySj(dto.getSjNo());
		
		BindUtils.postNotifyChange(null, null, this, "viewMode");		
		BindUtils.postNotifyChange(null, null, this, "selectedDto");
		BindUtils.postNotifyChange(null, null, this, "receiveDto");
		BindUtils.postNotifyChange(null, null, this, "listAttchment");
		BindUtils.postNotifyChange(null, null, this, "konfirmasiMode");
	
		BindUtils.postNotifyChange(null, null, this, "img1");
		BindUtils.postNotifyChange(null, null, this, "attachment1");
		BindUtils.postNotifyChange(null, null, this, "img2");
		BindUtils.postNotifyChange(null, null, this, "attachment2");
		BindUtils.postNotifyChange(null, null, this, "imgSignature");

	}
	
	
	@Command
	public void close(){
		konfirmasiMode = false;
		viewMode = false;
		BindUtils.postNotifyChange(null, null, this, "viewMode");
		BindUtils.postNotifyChange(null, null, this, "konfirmasiMode");
	}
	
	@Command
	public void cekLokasiBerangkat(){
		//String koordinat = selectedDto.getDepartingFromLatitude() + ", " +selectedDto.getDepartingFromLongitude();
		//Executions.getCurrent().sendRedirect("http://www.google.com", "_blank");
		//System.err.println("final koordinat = "+urlGoogleMaps+koordinat);
		Executions.getCurrent().sendRedirect(urlGoogleMaps+selectedDto.getDepartingFromLatitudeLongitude(), "_blank");
	}

	@Command
	public void cekLokasiSampai(){
		//String koordinat = selectedDto.getDepartingFromLatitude() + ", " +selectedDto.getDepartingFromLongitude();
		//Executions.getCurrent().sendRedirect("http://www.google.com", "_blank");
		//System.err.println("final koordinat = "+urlGoogleMaps+koordinat);
		Executions.getCurrent().sendRedirect(urlGoogleMaps+selectedDto.getDestinationLatitudeLongitude(), "_blank");
	}
	
	@Command
	public void cekLokasiTransit(){
		//String koordinat = selectedDto.getDepartingFromLatitude() + ", " +selectedDto.getDepartingFromLongitude();
		//Executions.getCurrent().sendRedirect("http://www.google.com", "_blank");
		//System.err.println("final koordinat = "+urlGoogleMaps+koordinat);
		Executions.getCurrent().sendRedirect(urlGoogleMaps+selectedDto.getTransitLatitudeLongitude(), "_blank");
	}
	
	@Command
	public void cekLokasiSampaiTransit(){
		//String koordinat = selectedDto.getDepartingFromLatitude() + ", " +selectedDto.getDepartingFromLongitude();
		//Executions.getCurrent().sendRedirect("http://www.google.com", "_blank");
		//System.err.println("final koordinat = "+urlGoogleMaps+koordinat);
		Executions.getCurrent().sendRedirect(urlGoogleMaps+selectedDto.getFirstDestinationLatitudeLongitude(), "_blank");
	}

	public MUserDto getUser() {
		return user;
	}

	public void setUser(MUserDto user) {
		this.user = user;
	}

	public List<MobSjDto> getListIndex() {
		return listIndex;
	}

	public void setListIndex(List<MobSjDto> listIndex) {
		this.listIndex = listIndex;
	}

	public MobSjDto getSelectedDto() {
		return selectedDto;
	}

	public void setSelectedDto(MobSjDto selectedDto) {
		this.selectedDto = selectedDto;
	}

	public boolean isFreezeId() {
		return freezeId;
	}

	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}

	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnDel() {
		return btnDel;
	}

	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isBtnDl() {
		return btnDl;
	}

	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

	public boolean isSuperAdmin() {
		return superAdmin;
	}

	public void setSuperAdmin(boolean superAdmin) {
		this.superAdmin = superAdmin;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public boolean isKonfirmasiMode() {
		return konfirmasiMode;
	}

	public void setKonfirmasiMode(boolean konfirmasiMode) {
		this.konfirmasiMode = konfirmasiMode;
	}

	public boolean isViewMode() {
		return viewMode;
	}

	public void setViewMode(boolean viewMode) {
		this.viewMode = viewMode;
	}

	public MobGoodsReceiveDto getReceiveDto() {
		return receiveDto;
	}

	public void setReceiveDto(MobGoodsReceiveDto receiveDto) {
		this.receiveDto = receiveDto;
	}

	public List<MobGoodsReceiveAttachmentDto> getListAttchment() {
		return listAttchment;
	}

	public void setListAttchment(List<MobGoodsReceiveAttachmentDto> listAttchment) {
		this.listAttchment = listAttchment;
	}

	public boolean isAttachment1() {
		return attachment1;
	}

	public void setAttachment1(boolean attachment1) {
		this.attachment1 = attachment1;
	}

	public boolean isAttachment2() {
		return attachment2;
	}

	public void setAttachment2(boolean attachment2) {
		this.attachment2 = attachment2;
	}

	public boolean isAttachment3() {
		return attachment3;
	}

	public void setAttachment3(boolean attachment3) {
		this.attachment3 = attachment3;
	}

	public boolean isAttachment4() {
		return attachment4;
	}

	public void setAttachment4(boolean attachment4) {
		this.attachment4 = attachment4;
	}

	public boolean isAttachment5() {
		return attachment5;
	}

	public void setAttachment5(boolean attachment5) {
		this.attachment5 = attachment5;
	}

	public AImage getImg1() {
		return img1;
	}

	public void setImg1(AImage img1) {
		this.img1 = img1;
	}

	public AImage getImg2() {
		return img2;
	}

	public void setImg2(AImage img2) {
		this.img2 = img2;
	}

	public AImage getImg3() {
		return img3;
	}

	public void setImg3(AImage img3) {
		this.img3 = img3;
	}

	public AImage getImg4() {
		return img4;
	}

	public void setImg4(AImage img4) {
		this.img4 = img4;
	}

	public AImage getImg5() {
		return img5;
	}

	public void setImg5(AImage img5) {
		this.img5 = img5;
	}

	public AImage getImgSignature() {
		return imgSignature;
	}

	public void setImgSignature(AImage imgSignature) {
		this.imgSignature = imgSignature;
	}



	public ListModelList<ReportType> getReportTypesModel() {
		return reportTypesModel;
	}



	public void setReportTypesModel(ListModelList<ReportType> reportTypesModel) {
		this.reportTypesModel = reportTypesModel;
	}



	public ReportType getReportType() {
		return reportType;
	}



	public void setReportType(ReportType reportType) {
		this.reportType = reportType;
	}



	public ReportConfig getReportConfig() {
		return reportConfig;
	}



	public void setReportConfig(ReportConfig reportConfig) {
		this.reportConfig = reportConfig;
	}

	

}
