package id.co.indocyber.bmmRmiCms.controller;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.UserMenu;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zul.Include;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

public class IndexVmd extends SelectorComposer<Component>implements Serializable {
	private static final long serialVersionUID = 7010841483291616467L;

	private static final Logger logger = LoggerFactory
			.getLogger(IndexVmd.class);

	private String err;
	private String lastPage;
	private Map<String,String> linkMap = new HashMap<>();
	
	public String getErr() {
		return err;
	}

	public void setErr(String err) {
		this.err = err;
	}
	
	public String getLastPage() {
		return lastPage;
	}

	public void setLastPage(String lastPage) {
		this.lastPage = lastPage;
	}

	@AfterCompose
	public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		
		//TODO :PRODUCTION MATTER: (DEVELOPMENT ONLY)
		if (System.getProperty("debug") != null){
			BindUtils.postGlobalCommand(null, null, "openDebugMenu", null);
		}
		err = Executions.getCurrent().getParameter("err");
		
		MUserDto user = (MUserDto) Sessions.getCurrent().getAttribute("user");
		//ur = mUserRoleSvc.cmsAccess(user.getEmail());
		List<MMenuRoleDto> listMenuRoles = user.getRolePrivs();

		for (MMenuRoleDto priv : listMenuRoles) {
			if (priv.getIsView()) {
				linkMap.put(priv.getMenuCode(),priv.getMenuLink());
			}
		}
	}
	
	@Command("bookmarkNav")
	public void openMenu(@BindingParam("bookmark") final String bookmark) {
		//TODO: auto logout if account / role disabled (for navigation via sidebar)
		if(Sessions.getCurrent().getAttribute("editMode")==null)
			Sessions.getCurrent().setAttribute("editMode", false);
		
		if((Boolean)Sessions.getCurrent().getAttribute("editMode")){
			Messagebox.show("Data tidak akan disimpan. Apakah Anda yakin?", "Perhatian",
					new Button[] { Button.YES, Button.NO },
					Messagebox.QUESTION, Button.NO,
					new EventListener<Messagebox.ClickEvent>() {
						@Override
						public void onEvent(ClickEvent event) throws Exception {
							if (Messagebox.ON_YES.equals(event.getName())) {
								Include include = (Include) Executions.getCurrent().getDesktop()
										.getPage("index").getFellow("mainInclude");
								Sessions.getCurrent().setAttribute("lastPage",linkMap.getOrDefault(bookmark,"/ref.zul"));
								include.setSrc("/WEB-INF/pages" + linkMap.getOrDefault(bookmark,"/ref.zul"));
							}
						}
					}
				);
		}
		else{
			Include include = (Include) Executions.getCurrent().getDesktop()
					.getPage("index").getFellow("mainInclude");
			Sessions.getCurrent().setAttribute("lastPage",linkMap.getOrDefault(bookmark,"/ref.zul"));
			include.setSrc("/WEB-INF/pages" + linkMap.getOrDefault(bookmark,"/ref.zul"));
		}
	}
}