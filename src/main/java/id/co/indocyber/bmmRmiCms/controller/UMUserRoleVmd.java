package id.co.indocyber.bmmRmiCms.controller;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MUserRoleDto;
import id.co.indocyber.bmmRmiCms.service.UMUserRoleSvc;
import id.co.indocyber.bmmRmiCms.tools.StringConstants;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.util.StringUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class UMUserRoleVmd extends BaseVmd {

	@WireVariable
	private UMUserRoleSvc uMUserRoleSvc;
	
	private String urlIndex = "/userManagement/user_role/index.zul";
	
	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;

	private MUserDto user;
	private List<MUserRoleDto> userRoleList = new ArrayList<>();

	private MUserRoleDto editRole, selectedUserRole;
		
	private boolean freezeId;
	private String pageTitle;

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private Integer recordCount = 1;

	private String sortBy = "";
	private boolean sortAsc = false;
	
	private Boolean editMode = false;
	private Boolean viewMode = false;
	private Boolean detailWindow = false;
	
	private List<String> moduleList;
	private List<MRoleDto> listRole = new ArrayList<>();	
	private List<MUserDto> listUser = new ArrayList<>();
	
	private MRoleDto selectedRole;
	
	@Init
	@NotifyChange({ "btnAdd", "btnDel", "btnEdit", "btnDl","moduleList"})
	public void load() {
		editMode = Boolean.valueOf((Boolean) Sessions.getCurrent().getAttribute("editMode"));
		user = (MUserDto) Sessions.getCurrent().getAttribute("user");
				
		userRoleList = uMUserRoleSvc.findAll(searchData,pageNo, "id", sortAsc);
		recordCount = uMUserRoleSvc.countAll(searchData);
		maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
		
		//load List
		moduleList = Arrays.asList(new String[]{"Mobile","Web"});
		
		for (MMenuRoleDto x : user.getRolePrivs()) {
			if ("UM-2".equals(x.getMenuCode())) {
				btnAdd = !x.getIsAdd();
				btnDel = !x.getIsDelete();
				btnDl = !x.getIsDownload();
				btnEdit = !x.getIsEdit();
				break;
			}
		}
	}

	@Command
	@NotifyChange("selectedUserRole")
	public void ubahStatusAktif(@BindingParam("dto") MUserRoleDto dto){
		showConfirm("E002");
		selectedUserRole = new MUserRoleDto();
		selectedUserRole = dto;
		selectedUserRole.setIsActive(false);
		selectedUserRole.setModifiedBy(user.getFullname());
		selectedUserRole.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@Command
	@NotifyChange("selectedUserRole")
	public void ubahStatusNonAktif(@BindingParam("dto") MUserRoleDto dto){
		showConfirm("E003");
		selectedUserRole = new MUserRoleDto();
		selectedUserRole = dto;
		selectedUserRole.setIsActive(true);
		selectedUserRole.setModifiedBy(user.getFullname());
		selectedUserRole.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@Command
	@NotifyChange({"recordCount","userRoleList","maxPage"})
	public void yesConfirm(){
		try{
			uMUserRoleSvc.save(selectedUserRole);
			flagConfirmMsg = false;
			showSuccessMsgBox("E000");
			//reload list
			recordCount = uMUserRoleSvc.countAll(searchData);
			userRoleList= uMUserRoleSvc.findAll(searchData, pageNo, 
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);

			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
			BindUtils.postNotifyChange(null, null, this, "flagConfirmMsg");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Command("back")
	public void back() {
		if (activeCheck()) {
			Messagebox.show("Data tidak akan disimpan. Apakah Anda yakin?", "Perhatian",
				new Button[] { Button.YES, Button.NO },
				Messagebox.QUESTION, Button.NO,
				new EventListener<Messagebox.ClickEvent>() {
					@Override
					public void onEvent(ClickEvent event) throws Exception {
						if (Messagebox.ON_YES.equals(event.getName())) {
							Sessions.getCurrent().removeAttribute("editRole");
							navigate(urlIndex);
						}
					}
				}
			);
		}
	}

	@Command("add")
	@NotifyChange({"editMode","editRole","freezeId","pageTitle",
		"searchData","viewMode","detailWindow"})
	public void add() {
		editMode = true;
		viewMode = false;
		detailWindow = true;
		
		editRole = new MUserRoleDto();
		searchData = null;
		
		freezeId = false;
		pageTitle = "Create User Role";
		
		//default mudole app
		editRole.setModuleApps("Web");
	}

	@Command("edit")
	@NotifyChange({"editMode","editRole","freezeId",
		"pageTitle","searchData","viewMode","detailWindow"})
	public void edit(@BindingParam("item") MUserRoleDto edit) {
		if (edit==null || edit.getRoleId() == null)
			Messagebox.show("Pilih data yang akan dirubah!", "Message", 0, null);
		else {
			
			editMode = true;
			viewMode = false;
			detailWindow = true;
			
			editRole = edit;
			searchData = null;
										
			freezeId = true;
			pageTitle = "Update User Role";
						
		}
	}
	
	@Command
	@NotifyChange({"editRole","viewMode","editMode","detailWindow","viewMode"})
	public void view(@BindingParam("item") MUserRoleDto dto){
		editRole = dto;
		viewMode = true;
		editMode = false;
		detailWindow = true;
		
	}

	@Command("save")
	@NotifyChange({ "editRole", "rolePrivs", "maxPage", "recordCount", "userRoleList","editMode","searchData" })
	public void save(@BindingParam("btn")Component btn) {
			
		Timestamp time = Timestamp.from(OffsetDateTime.now(ZoneOffset.ofHours(7)).toInstant());
		
		String fullErrMsg = StringConstants.MSG_FORM_TEMPLATE;
		int error = 0;
		if (StringUtils.isEmpty(editRole.getRoleId())) {
			error++;
			fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Role Id");
		} 
		if (StringUtils.isEmpty(editRole.getEmail())) {
			error++;
			fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Email");
		}
		if (StringUtils.isEmpty(editRole.getModuleApps())) {
			error++;
			fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Module Apps");
		} 

		if (error > 0) {
			Messagebox.show(fullErrMsg, "Message", 0, null);
		}else{
			
			editRole.setIsActive(true);
			editRole.setCreatedBy(user.getFullname());
			editRole.setCreatedDate(time);
			editRole.setModifiedBy(user.getFullname());
			editRole.setModifiedDate(time);
			
			if(uMUserRoleSvc.save(editRole) == 1){
				showSuccessMsgBox("E000");
			}else{
				showErrorMsgBox("E001");
			}
			
			//reload Data
			recordCount = uMUserRoleSvc.countAll(searchData);
			userRoleList = uMUserRoleSvc.findAll(searchData, pageNo, 
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);

			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);

			
			editMode = false;
			searchData = null;
			((Window)btn.getParent().getParent()).setVisible(false);
		}	
	}
	
	@Command("reload")
	@NotifyChange({"recordCount","userRoleList","maxPage"})
	public void reload(){
		recordCount = uMUserRoleSvc.countAll(searchData);
		userRoleList = uMUserRoleSvc.findAll(searchData, pageNo, 
				StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);

		maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
	}

	@Command("src")
	@NotifyChange({"userRoleList", "pageNo", "maxPage", "recordCount" })
	public void search() {
		recordCount = uMUserRoleSvc.countAll(searchData);
		userRoleList = uMUserRoleSvc.findAll(searchData, pageNo, 
				StringUtils.isEmpty(sortBy) ? "email" : sortBy, sortAsc);

		maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
	}
	
	@Command
	@NotifyChange("listUser")
	public void searchUser(){
		listUser = uMUserRoleSvc.cbListUser(searchData);
	}

	@Command("switchPage")
	@NotifyChange({ "pageNo", "userRoleList" })
	public void switchPage(@BindingParam("action") String action) {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try{
					pageNo = Integer.parseInt(action);
				}catch(NumberFormatException e){
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}
	
			userRoleList = uMUserRoleSvc.findAll(searchData, pageNo, 
					StringUtils.isEmpty(sortBy) ? "email" : sortBy, sortAsc);
		}
	}

	@Command("sort")
	@NotifyChange("roleList")
	public void sort(@BindingParam("col") String column) {
		if (activeCheck()) {
			if (sortBy.equals(column)) {
				sortAsc = !sortAsc;
			} else {
				sortBy = column;
				sortAsc = true;
			}
			
			userRoleList = uMUserRoleSvc.findAll(searchData, pageNo, 
					StringUtils.isEmpty(sortBy) ? "email" : sortBy, sortAsc);
		}
	}
	
	@Command
	public void loadListRole(){
		listRole = uMUserRoleSvc.cbListRole();
		BindUtils.postNotifyChange(null, null, this, "listRole");
	}
	
	@Command
	public void loadListUser(){
		listUser = uMUserRoleSvc.cbListUser(searchData);
		BindUtils.postNotifyChange(null, null, this, "listUser");
	}
	
	@Command
	@NotifyChange("editRole")
	public void selectRole(){
		editRole.setRoleId(selectedRole.getId());
		editRole.setRoleName(selectedRole.getRoleName());
		BindUtils.postNotifyChange(null, null, editRole, "roleId");
		BindUtils.postNotifyChange(null, null, editRole, "roleName");
		System.out.println("role id ="+selectedRole.getId());
	}
	
	@Command
	@NotifyChange("editRole")
	public void selectModule(@BindingParam("item") String s){
		editRole.setModuleApps(s);
	}
	
	@Command
	@NotifyChange("editRole")
	public void selectEmail(@BindingParam("item") MUserDto dto){
		editRole.setEmail(dto.getEmail());
	}
	
	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnDel() {
		return btnDel;
	}

	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isBtnDl() {
		return btnDl;
	}

	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}

	public boolean isFreezeId() {
		return freezeId;
	}

	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}
		
	public List<String> getModuleList() {
		return moduleList;
	}

	public void setModuleList(List<String> moduleList) {
		this.moduleList = moduleList;
	}
	
	public MUserDto getUser() {
		return user;
	}

	public void setUser(MUserDto user) {
		this.user = user;
	}

	public List<MUserRoleDto> getUserRoleList() {
		return userRoleList;
	}

	public void setUserRoleList(List<MUserRoleDto> userRoleList) {
		this.userRoleList = userRoleList;
	}

	public MUserRoleDto getEditRole() {
		return editRole;
	}

	public void setEditRole(MUserRoleDto editRole) {
		this.editRole = editRole;
	}

	public MUserRoleDto getSelectedUserRole() {
		return selectedUserRole;
	}

	public void setSelectedUserRole(MUserRoleDto selectedUserRole) {
		this.selectedUserRole = selectedUserRole;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public boolean isSortAsc() {
		return sortAsc;
	}

	public void setSortAsc(boolean sortAsc) {
		this.sortAsc = sortAsc;
	}

	public List<MRoleDto> getListRole() {
		return listRole;
	}

	public void setListRole(List<MRoleDto> listRole) {
		this.listRole = listRole;
	}

	public List<MUserDto> getListUser() {
		return listUser;
	}

	public void setListUser(List<MUserDto> listUser) {
		this.listUser = listUser;
	}

	public MRoleDto getSelectedRole() {
		return selectedRole;
	}

	public void setSelectedRole(MRoleDto selectedRole) {
		this.selectedRole = selectedRole;
	}

	public Boolean getViewMode() {
		return viewMode;
	}

	public void setViewMode(Boolean viewMode) {
		this.viewMode = viewMode;
	}

	public Boolean getDetailWindow() {
		return detailWindow;
	}

	public void setDetailWindow(Boolean detailWindow) {
		this.detailWindow = detailWindow;
	}
	
				
}
