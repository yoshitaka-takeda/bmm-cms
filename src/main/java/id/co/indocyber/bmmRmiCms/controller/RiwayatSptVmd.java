package id.co.indocyber.bmmRmiCms.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MobSptDto;
import id.co.indocyber.bmmRmiCms.service.RiwayatSptSvc;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class RiwayatSptVmd extends BaseVmd {
	
	@WireVariable
	private RiwayatSptSvc riwayatSptSvc;
	
	private MUserDto user;
	
	private List<MobSptDto> listIndex = new ArrayList<>();
	private MobSptDto selectedDto;
	
	private boolean freezeId;

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private boolean superAdmin;
	private Integer recordCount = 1;
	
	private boolean viewMode = false;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		
	@SuppressWarnings("unchecked")
	@Init
	public void load() throws ParseException{
		try{
			user = (MUserDto) Sessions.getCurrent().getAttribute("user");
			
			if(user.isAdministrator()){
				listIndex = riwayatSptSvc.findAll(searchData,filterStartDate,filterEndtDate,pageNo, perPage, "modifiedDate desc, a.sptNo", "desc");
				recordCount = riwayatSptSvc.countAll(searchData, filterStartDate,filterEndtDate);
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);				
			}else{
				if(user.isCustomer()){
					Map<String, Object> map = riwayatSptSvc.findByCustomer(user.getReferenceCode(), searchData, filterStartDate,filterEndtDate ,pageNo, perPage, "modifiedDate desc, a.sptNo", "desc");
					listIndex = (List<MobSptDto>) map.get("content");
					recordCount = (Integer) map.get("totalSize");
				}else{
					Map<String, Object> map = riwayatSptSvc.findByTransportir(user.getReferenceCode(), searchData, filterStartDate,filterEndtDate ,pageNo, perPage, "modifiedDate desc, a.sptNo", "desc");
					listIndex = (List<MobSptDto>) map.get("content");
					recordCount = (Integer) map.get("totalSize");					
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Command("switchPage")
	@NotifyChange({ "pageNo", "listIndex"})
	public void switchPage(@BindingParam("action") String action) throws ParseException {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}

			if(user.isAdministrator()){
<<<<<<< HEAD
				listIndex = riwayatSptSvc.findAll(searchData,filterStartDate,filterEndtDate,pageNo, perPage, "modifiedDate desc,a.sptNo", "desc");
=======
				listIndex = riwayatSptSvc.findAll(searchData,filterStartDate,filterEndtDate,pageNo, perPage, "modifiedDate desc, a.sptNo", "desc");
>>>>>>> branch 'master' of https://gitlab.com/ari.haryanto/rmi-spt-sj.git
				recordCount = riwayatSptSvc.countAll(searchData, filterStartDate,filterEndtDate);
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);				
			}else{
				if(user.isCustomer()){
					Map<String, Object> map = riwayatSptSvc.findByCustomer(user.getReferenceCode(), searchData, filterStartDate,filterEndtDate ,pageNo, perPage, "modifiedDate desc, a.sptNo", "desc");
					listIndex = (List<MobSptDto>) map.get("content");
					recordCount = (Integer) map.get("totalSize");
				}else{
					Map<String, Object> map = riwayatSptSvc.findByTransportir(user.getReferenceCode(), searchData, filterStartDate,filterEndtDate ,pageNo, perPage, "modifiedDate desc, a.sptNo", "desc");
					listIndex = (List<MobSptDto>) map.get("content");
					recordCount = (Integer) map.get("totalSize");					
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Command("src")
	@NotifyChange({"listIndex", "pageNo", "maxPage", "recordCount","filterEndtDate","filterStartDate"})
	public void search() {
		search = true;
		pageNo = 1;
		
		if(filterStartDate == null && filterEndtDate != null){
			filterStartDate = new Date();
		}
		if(filterEndtDate == null && filterStartDate != null){
			filterEndtDate = new Date();
		}
		
		if(user.isAdministrator()){
			listIndex = riwayatSptSvc.findAll(searchData,filterStartDate,filterEndtDate,pageNo, perPage, "modifiedDate desc, a.sptNo", "desc");
			recordCount = riwayatSptSvc.countAll(searchData, filterStartDate,filterEndtDate);
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);				
		}else{
			if(user.isCustomer()){
<<<<<<< HEAD
				Map<String, Object> map = riwayatSptSvc.findByCustomer(user.getReferenceCode(), searchData, filterStartDate,filterEndtDate ,pageNo, perPage, "modifiedDate desc,a.sptNo", "desc");
=======
				Map<String, Object> map = riwayatSptSvc.findByCustomer(user.getReferenceCode(), searchData, filterStartDate,filterEndtDate ,pageNo, perPage, "modifiedDate desc, a.sptNo", "desc");
>>>>>>> branch 'master' of https://gitlab.com/ari.haryanto/rmi-spt-sj.git
				listIndex = (List<MobSptDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
			}else{
<<<<<<< HEAD
				Map<String, Object> map = riwayatSptSvc.findByTransportir(user.getReferenceCode(), searchData, filterStartDate,filterEndtDate ,pageNo, perPage, "modifiedDate desc,a.sptNo", "desc");
=======
				Map<String, Object> map = riwayatSptSvc.findByTransportir(user.getReferenceCode(), searchData, filterStartDate,filterEndtDate ,pageNo, perPage, "modifiedDate desc, a.sptNo", "desc");
>>>>>>> branch 'master' of https://gitlab.com/ari.haryanto/rmi-spt-sj.git
				listIndex = (List<MobSptDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");					
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Command("reload")
	@NotifyChange({"recordCount","listIndex","maxPage","searchData","filterEndtDate","filterStartDate"})
	public void reload() throws ParseException{
		searchData = null;
		filterStartDate = null;
		filterEndtDate = null;
		
		if(user.isAdministrator()){
			listIndex = riwayatSptSvc.findAll(searchData,filterStartDate,filterEndtDate,pageNo, perPage, "modifiedDate desc, a.sptNo", "desc");
			recordCount = riwayatSptSvc.countAll(searchData, filterStartDate,filterEndtDate);
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);				
		}else{
			if(user.isCustomer()){
				Map<String, Object> map = riwayatSptSvc.findByCustomer(user.getReferenceCode(), searchData, filterStartDate,filterEndtDate ,pageNo, perPage, "modifiedDate desc, a.sptNo", "desc");
				listIndex = (List<MobSptDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
			}else{
				Map<String, Object> map = riwayatSptSvc.findByTransportir(user.getReferenceCode(), searchData, filterStartDate,filterEndtDate ,pageNo, perPage, "modifiedDate desc, a.sptNo", "desc");
				listIndex = (List<MobSptDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");					
			}
		}
	}
	
	
	@Command
	public void view(@BindingParam("item") MobSptDto dto) {
		selectedDto = dto;
		viewMode = true;
		BindUtils.postNotifyChange(null, null, this, "viewMode");
		BindUtils.postNotifyChange(null, null, this, "selectedDto");
	}
	
	@Command
	public void close(){
		viewMode = true;
		BindUtils.postNotifyChange(null, null, this, "viewMode");
	}

	public MUserDto getUser() {
		return user;
	}

	public void setUser(MUserDto user) {
		this.user = user;
	}

	public List<MobSptDto> getListIndex() {
		return listIndex;
	}

	public void setListIndex(List<MobSptDto> listIndex) {
		this.listIndex = listIndex;
	}

	public MobSptDto getSelectedDto() {
		return selectedDto;
	}

	public void setSelectedDto(MobSptDto selectedDto) {
		this.selectedDto = selectedDto;
	}

	public boolean isFreezeId() {
		return freezeId;
	}

	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

	public boolean isSuperAdmin() {
		return superAdmin;
	}

	public void setSuperAdmin(boolean superAdmin) {
		this.superAdmin = superAdmin;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public boolean isViewMode() {
		return viewMode;
	}

	public void setViewMode(boolean viewMode) {
		this.viewMode = viewMode;
	}

	
	
}
