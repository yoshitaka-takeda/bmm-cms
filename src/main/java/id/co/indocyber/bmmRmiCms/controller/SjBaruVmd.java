package id.co.indocyber.bmmRmiCms.controller;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.springframework.util.StringUtils;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.image.AImage;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MVehicleTypeDto;
import id.co.indocyber.bmmRmiCms.dto.MobDriverDto;
import id.co.indocyber.bmmRmiCms.dto.MobGoodsReceiveAttachmentDto;
import id.co.indocyber.bmmRmiCms.dto.MobGoodsReceiveDto;
import id.co.indocyber.bmmRmiCms.dto.MobSjDto;
import id.co.indocyber.bmmRmiCms.service.MGlobalImageSvc;
import id.co.indocyber.bmmRmiCms.service.SjBaruSvc;
import id.co.indocyber.bmmRmiCms.tools.StringConstants;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class SjBaruVmd extends BaseVmd {
	
	@WireVariable
	private SjBaruSvc sjBaruSvc;
	
	@WireVariable
	private MGlobalImageSvc mGlobalImageSvc;
	
	private MUserDto user;
	
	private static final String urlGoogleMaps = "https://www.google.com/maps/place/";	
	
	private List<MobSjDto> listIndex = new ArrayList<>();
	private MobSjDto selectedDto;
	
	private MobGoodsReceiveDto receiveDto;
	
	private List<MobDriverDto> listDriver = new ArrayList<>();
	
	private List<MVehicleTypeDto> listVehicleType = new ArrayList<>();
	private MVehicleTypeDto selectedVehicle;
	
	private List<MobGoodsReceiveAttachmentDto> listAttachment = new ArrayList<>();
	private MobGoodsReceiveAttachmentDto attachmentDto;
	
	private boolean freezeId;
	
	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;	

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private boolean superAdmin;
	private Integer recordCount = 1;
	
	private boolean viewKonfirmasiMode = false;
	private boolean konfirmasiMode = false;
	private boolean penugasanMode = false;		
	private boolean viewMode = false;
	
	private boolean btnSignature = true;
	private boolean btnUpload1 = true;
	private boolean btnUpload2 = true;
	private boolean btnUpload3 = true;
	private boolean btnUpload4 = true;
	private boolean btnUpload5 = true;
	
	private boolean imgUploadSignature = false;
	private boolean imgUpload1 = false;
	private boolean imgUpload2 = false;
	private boolean imgUpload3 = false;
	private boolean imgUpload4 = false;
	private boolean imgUpload5 = false;
	
	private AImage imgSignature;
	private AImage img1;
	private AImage img2;
	private AImage img3;
	private AImage img4;
	private AImage img5;
	
	private byte[] defImage;
		
	private java.sql.Date startDate, endDate;
	private String sStart, sEnd;
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	
	public java.sql.Date getStartDate() {
		return startDate;
	}

	public void setStartDate(java.sql.Date startDate) {
		this.startDate = startDate;
	}

	public java.sql.Date getEndDate() {
		return endDate;
	}

	public void setEndDate(java.sql.Date endDate) {
		this.endDate = endDate;
	}
	
	@SuppressWarnings("unchecked")
	@Init
	@NotifyChange({"btnAdd","btnDel","btnDl","btnEdit","user","img1",
		"img2","img3","img4","img5","defImage","imgSignature"})
	public void load() throws ParseException{
		try{			
			user = (MUserDto) Sessions.getCurrent().getAttribute("user");
			startDate = new java.sql.Date(new Date().getTime());
			endDate = new java.sql.Date(startDate.getTime());
			
//			//create default image
			defImage = mGlobalImageSvc.findOne("IMG001").getParmImgValue();
			setImgSignature(new AImage("img signature", defImage));
			setImg1(new AImage("img1", defImage));
			setImg2(new AImage("img2", defImage));
			setImg3(new AImage("img3", defImage));
			setImg4(new AImage("img4", defImage));
			setImg5(new AImage("img5", defImage));
			
			if(user.isAdministrator()){
				Map<String, Object> map = sjBaruSvc.findAll(searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
				listIndex = (List<MobSjDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);					
			}else{
				if(user.isCustomer()){
					System.err.println("Masuk sebagai customer");
<<<<<<< HEAD
					Map<String, Object> map = sjBaruSvc.findDataNewByCustomer(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc,  sj_no", "desc");
=======
					Map<String, Object> map = sjBaruSvc.findDataNewByCustomer(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
>>>>>>> branch 'master' of https://gitlab.com/ari.haryanto/rmi-spt-sj.git
					List<MobSjDto> listTemp = new ArrayList<>();
					listTemp = (List<MobSjDto>) map.get("content");
					/*listIndex = (List<MobSjDto>) map.get("content");*/
					for(MobSjDto dto : listTemp){
						dto.setFlagVisiblePenugasan(false);
						dto.setFlagVisibleKonfirmasi(true);
						listIndex.add(dto);
					}
					recordCount = (Integer) map.get("totalSize");
					maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);					
				}else{
					System.out.println("transportirrr = "+user.getReferenceCode());
<<<<<<< HEAD
					Map<String, Object> map = sjBaruSvc.findDataNewByTransportir(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc,sj_no", "desc");
=======
					Map<String, Object> map = sjBaruSvc.findDataNewByTransportir(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
>>>>>>> branch 'master' of https://gitlab.com/ari.haryanto/rmi-spt-sj.git
					listIndex = (List<MobSjDto>) map.get("content");
					recordCount = (Integer) map.get("totalSize");
					maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);										
				}
			}
			
			
			for (MMenuRoleDto x : user.getRolePrivs()) {
				if ("SSJ-1".equals(x.getMenuCode())) {
					btnAdd = !x.getIsAdd();
					btnDel = !x.getIsDelete();
					btnDl = !x.getIsDownload();
					btnEdit = !x.getIsEdit();
					break;
				}
			}

		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Command("switchPage")
	@NotifyChange({ "pageNo", "listIndex"})
	public void switchPage(@BindingParam("action") String action) throws ParseException {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}

			if(user.isAdministrator()){
				Map<String, Object> map = sjBaruSvc.findAll(searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
				listIndex = (List<MobSjDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);					
			}else{
				if(user.isCustomer()){
					System.err.println("Masuk sebagai customer");
					Map<String, Object> map = sjBaruSvc.findDataNewByCustomer(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
					List<MobSjDto> listTemp = new ArrayList<>();
					listTemp = (List<MobSjDto>) map.get("content");
					/*listIndex = (List<MobSjDto>) map.get("content");*/
					for(MobSjDto dto : listTemp){
						dto.setFlagVisiblePenugasan(false);
						dto.setFlagVisibleKonfirmasi(true);
						listIndex.add(dto);
					}
					recordCount = (Integer) map.get("totalSize");
					maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);					
				}else{
					System.out.println("transportirrr = "+user.getReferenceCode());
					Map<String, Object> map = sjBaruSvc.findDataNewByTransportir(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
					listIndex = (List<MobSjDto>) map.get("content");
					recordCount = (Integer) map.get("totalSize");
					maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);										
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Command("src")
	@NotifyChange({"listIndex", "pageNo", "maxPage", "recordCount","filterStartDate","filterEndtDate"})
	public void search() {
		search = true;
		pageNo = 1;
		
		sStart = sdf.format(startDate);
		sEnd = sdf.format(endDate);
		
		if(filterStartDate == null && filterEndtDate != null){
			filterStartDate = new Date();
		}
		if(filterEndtDate == null && filterStartDate != null){
			filterEndtDate = new Date();
		}
		
		if(user.isAdministrator()){
			Map<String, Object> map = sjBaruSvc.findAll(searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
			listIndex = (List<MobSjDto>) map.get("content");
			recordCount = (Integer) map.get("totalSize");
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);					
		}else{
			if(user.isCustomer()){
				System.err.println("Masuk sebagai customer");
				Map<String, Object> map = sjBaruSvc.findDataNewByCustomer(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
				List<MobSjDto> listTemp = new ArrayList<>();
				listTemp = (List<MobSjDto>) map.get("content");
				/*listIndex = (List<MobSjDto>) map.get("content");*/
				for(MobSjDto dto : listTemp){
					dto.setFlagVisiblePenugasan(false);
					dto.setFlagVisibleKonfirmasi(true);
					listIndex.add(dto);
				}
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);					
			}else{
				System.out.println("transportirrr = "+user.getReferenceCode());
				Map<String, Object> map = sjBaruSvc.findDataNewByTransportir(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
				listIndex = (List<MobSjDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);										
			}
		}		
	}
	
	@SuppressWarnings("unchecked")
	@Command("reload")
	@NotifyChange({"recordCount","listIndex","maxPage","searchData","filterStartDate","filterEndtDate"})
	public void reload() throws ParseException{
		searchData = null;
		filterStartDate = null;
		filterEndtDate = null;
		
		if(user.isAdministrator()){
			Map<String, Object> map = sjBaruSvc.findAll(searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
			listIndex = (List<MobSjDto>) map.get("content");
			recordCount = (Integer) map.get("totalSize");
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);					
		}else{
			if(user.isCustomer()){
				System.err.println("Masuk sebagai customer");
				Map<String, Object> map = sjBaruSvc.findDataNewByCustomer(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
				List<MobSjDto> listTemp = new ArrayList<>();
				listTemp = (List<MobSjDto>) map.get("content");
				/*listIndex = (List<MobSjDto>) map.get("content");*/
				for(MobSjDto dto : listTemp){
					dto.setFlagVisiblePenugasan(false);
					dto.setFlagVisibleKonfirmasi(true);
					listIndex.add(dto);
				}
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);					
			}else{
				System.out.println("transportirrr = "+user.getReferenceCode());
				Map<String, Object> map = sjBaruSvc.findDataNewByTransportir(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
				listIndex = (List<MobSjDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);										
			}
		}
	}
	
	@Command
	public void view(@BindingParam("item") MobSjDto dto){
		selectedDto = dto;
		viewMode = true;
		BindUtils.postNotifyChange(null, null, this, "viewMode");
		BindUtils.postNotifyChange(null, null, this, "selectedDto");
	}
	
	@Command
	public void penugasanSupir(@BindingParam("item") MobSjDto dto) {
		if(!dto.getStatusText().contains("Selesai")){
			showWarningMsgBox("E013");
			return;
		}else{
			selectedDto = dto;
			konfirmasiMode = false;
			penugasanMode = true;
			
			//loadList driver
			searchData = null;
			listDriver = sjBaruSvc.getListDriverByTransporte(selectedDto.getTransportirCode(), searchData);
			
			//loadList vehicleType
			listVehicleType = sjBaruSvc.getListVehicle();
			
			BindUtils.postNotifyChange(null, null, this, "konfirmasiMode");
			BindUtils.postNotifyChange(null, null, this, "penugasanMode");
			BindUtils.postNotifyChange(null, null, this, "selectedDto");
			BindUtils.postNotifyChange(null, null, this, "searchData");
			BindUtils.postNotifyChange(null, null, this, "listDriver");
			BindUtils.postNotifyChange(null, null, this, "listVehicleType");
		}
	}
	
	@Command
	public void konfirmasiBarang(@BindingParam("item") MobSjDto dto){
		try{
			selectedDto = dto;		
			penugasanMode = false;
				
			//set default image & btnUpload
			btnSignature = true;
			btnUpload1 = true;
			btnUpload2 = true;
			btnUpload3 = true;
			btnUpload4 = true;
			btnUpload5 = true;
			
			imgUploadSignature = false;
			imgUpload1 = false;
			imgUpload2 = false;
			imgUpload3 = false;
			imgUpload4 = false;
			imgUpload5 = false;
			
			setImg1(new AImage("img 1", defImage));
			setImg2(new AImage("img 1", defImage));
			setImg3(new AImage("img 1", defImage));
			setImg4(new AImage("img 1", defImage));
			setImg5(new AImage("img 1", defImage));
			
//			if(user.isAdministrator()){
//				receiveDto = sjBaruSvc.getReceive(dto.getSjNo());
//				listAttachment = sjBaruSvc.getListReceiveAttachment(dto.getSjNo());
//				konfirmasiMode = false;
//				viewKonfirmasiMode = true;				
//			}else{
//				if(sjBaruSvc.countDataAttchmentBySj(dto.getSjNo()) > 0){
//					receiveDto = sjBaruSvc.getReceive(dto.getSjNo());
//					listAttachment = sjBaruSvc.getListReceiveAttachment(dto.getSjNo());
//					konfirmasiMode = false;
//					viewKonfirmasiMode = true;
//				}else{
//					receiveDto = new MobGoodsReceiveDto();
//					konfirmasiMode = true;
//					viewKonfirmasiMode = false;				
//				}
//			}

			
			if(sjBaruSvc.countDataAttchmentBySj(dto.getSjNo()) > 0){
				receiveDto = sjBaruSvc.getReceive(dto.getSjNo());
				listAttachment = sjBaruSvc.getListReceiveAttachment(dto.getSjNo());
				konfirmasiMode = false;
				viewKonfirmasiMode = true;
			}else{
				receiveDto = new MobGoodsReceiveDto();
				konfirmasiMode = true;
				viewKonfirmasiMode = false;				
			}			
			
			BindUtils.postNotifyChange(null, null, this, "viewKonfirmasiMode");
			BindUtils.postNotifyChange(null, null, this, "konfirmasiMode");
			BindUtils.postNotifyChange(null, null, this, "penugasanMode");
			BindUtils.postNotifyChange(null, null, this, "selectedDto");
			BindUtils.postNotifyChange(null, null, this, "receiveDto");
			BindUtils.postNotifyChange(null, null, this, "btnSignature");
			BindUtils.postNotifyChange(null, null, this, "btnUpload1");
			BindUtils.postNotifyChange(null, null, this, "btnUpload2");
			BindUtils.postNotifyChange(null, null, this, "btnUpload3");
			BindUtils.postNotifyChange(null, null, this, "btnUpload4");
			BindUtils.postNotifyChange(null, null, this, "btnUpload5");
			BindUtils.postNotifyChange(null, null, this, "imgUploadSignature");
			BindUtils.postNotifyChange(null, null, this, "imgUpload1");
			BindUtils.postNotifyChange(null, null, this, "imgUpload2");
			BindUtils.postNotifyChange(null, null, this, "imgUpload3");
			BindUtils.postNotifyChange(null, null, this, "imgUpload4");
			BindUtils.postNotifyChange(null, null, this, "imgUpload5");
			BindUtils.postNotifyChange(null, null, this, "img1");
			BindUtils.postNotifyChange(null, null, this, "img2");
			BindUtils.postNotifyChange(null, null, this, "img3");
			BindUtils.postNotifyChange(null, null, this, "img4");
			BindUtils.postNotifyChange(null, null, this, "img5");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Command
	public void saveKonfirmasi(){
		try{
			
			String port = ( Executions.getCurrent().getServerPort()== 80 ) ? "" : (":"+Executions.getCurrent().getServerPort());
			String url = Executions.getCurrent().getScheme()+"://"+Executions.getCurrent().getServerName()+port;
			System.err.println("port99 "+port);
			System.err.println("url99 "+url);

			String folderAttachment = "attachment/attachment";
			String folderSignature = "signature/signature";
<<<<<<< HEAD
			String host = "http://mobile.berkahmm.com:8080";
=======
			String host = "http://iglo-api.rejosomanisindo.com:8080";		
>>>>>>> branch 'master' of https://gitlab.com/ari.haryanto/rmi-spt-sj.git
			List<MobGoodsReceiveAttachmentDto> listTempAttachment = new ArrayList<>();
			Integer sequence = 1;
			//tes lagi
			for(MobGoodsReceiveAttachmentDto dto : listAttachment){
				dto.setDocumentPath(host+saveByteArrayToPath(dto.getByteAttach(), folderAttachment, sequence).substring(19).replace("\\", "/"));
				dto.setDocumentBlob(Base64.getEncoder().encodeToString(dto.getByteAttach()));
				listTempAttachment.add(dto);
				sequence++;
			}
			
			String fullErrMsg = StringConstants.MSG_FORM_TEMPLATE;
			int error = 0;
			if (btnUpload1) {
				error++;
				fullErrMsg += String.format(
						StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Foto Surat Jalan");
			}			
			if (btnUpload2) {
				error++;
				fullErrMsg += String.format(
						StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Foto Bukti Penerimaan Barang");
			}			
			if (btnUpload3) {
				error++;
				fullErrMsg += String.format(
						StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Foto Slip Timbang");
			}			

			
			if (error > 0) {
				Messagebox.show(fullErrMsg, "Message", 0, null);
			}else{
				sjBaruSvc.saveAttactment(listTempAttachment);
				receiveDto.setSignature(host+saveByteArrayToPath(receiveDto.getImgSignatureByte(), folderSignature,sequence).substring(19).replace("\\", "/"));
				sjBaruSvc.updateReceive(receiveDto.getQtyReceive(), receiveDto.getNameOfRecipient(), 
						receiveDto.getNotes(), receiveDto.getSignature(),  user.getUsername(), new Date(), selectedDto.getSjNo());
				sjBaruSvc.updateStatusSj(selectedDto.getSjNo(), "c", new Date());
				
				showSuccessMsgBox("E000");
				
				konfirmasiMode = false;
				
<<<<<<< HEAD
				//update ke SAP
				sjBaruSvc.setApiSj(selectedDto.getSjNo(), receiveDto.getQtyReceive().doubleValue(), receiveDto.getNotes(), receiveDto.getSignature(), receiveDto.getNameOfRecipient());
				
=======
				//update ke sap
				sjBaruSvc.setApiSj(selectedDto.getSjNo(), receiveDto.getQtyReceive().doubleValue(), receiveDto.getNotes(), receiveDto.getSignature(), receiveDto.getNameOfRecipient());
>>>>>>> branch 'master' of https://gitlab.com/ari.haryanto/rmi-spt-sj.git
				
				//reload list
				if(user.isAdministrator()){
					Map<String, Object> map = sjBaruSvc.findAll(searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
					listIndex = (List<MobSjDto>) map.get("content");
					recordCount = (Integer) map.get("totalSize");
					maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);					
				}else{
					if(user.isCustomer()){
						System.err.println("Masuk sebagai customer");
<<<<<<< HEAD
						Map<String, Object> map = sjBaruSvc.findDataNewByCustomer(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
=======
						Map<String, Object> map = sjBaruSvc.findDataNewByCustomer(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no ", "desc");
>>>>>>> branch 'master' of https://gitlab.com/ari.haryanto/rmi-spt-sj.git
						List<MobSjDto> listTemp = new ArrayList<>();
						listTemp = (List<MobSjDto>) map.get("content");
						/*listIndex = (List<MobSjDto>) map.get("content");*/
						for(MobSjDto dto : listTemp){
							dto.setFlagVisiblePenugasan(false);
							dto.setFlagVisibleKonfirmasi(true);
							listIndex.add(dto);
						}
						recordCount = (Integer) map.get("totalSize");
						maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);					
					}else{
						System.out.println("transportirrr = "+user.getReferenceCode());
<<<<<<< HEAD
						Map<String, Object> map = sjBaruSvc.findDataNewByTransportir(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc,sj_no", "desc");
=======
						Map<String, Object> map = sjBaruSvc.findDataNewByTransportir(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
>>>>>>> branch 'master' of https://gitlab.com/ari.haryanto/rmi-spt-sj.git
						listIndex = (List<MobSjDto>) map.get("content");
						recordCount = (Integer) map.get("totalSize");
						maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);										
					}
				}			
				BindUtils.postNotifyChange(null, null, this, "konfirmasiMode");
				BindUtils.postNotifyChange(null, null, this, "listIndex");
				BindUtils.postNotifyChange(null, null, this, "recordCount");
				BindUtils.postNotifyChange(null, null, this, "maxPage");				
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
			showErrorMsgBox("E001");
		}
	}
	
	@Command
	public void selectDriver(@BindingParam("item") MobDriverDto dto){
		selectedDto.setDriverId(dto.getId());
		selectedDto.setDriverName(dto.getFullname());
		selectedDto.setPhone(dto.getPhoneNum());
		BindUtils.postNotifyChange(null, null, this, "selectedDto");
	}
	
	@Command
	public void selectVehicleType(){
		selectedDto.setVehicleType(selectedVehicle.getId());
		selectedDto.setVehicleTypeText(selectedVehicle.getVehicleTypeName());
		BindUtils.postNotifyChange(null, null, this, "selectedDto");		
	}
	
	@SuppressWarnings("unchecked")
	@Command
	@NotifyChange({"penugasanMode","listIndex","recordCount","maxPage"})
	public void savePenugasan(){
		try{
			sjBaruSvc.updateDriver(selectedDto.getDriverId(), selectedDto.getPhone(), 
					selectedDto.getVehicleNo(), selectedDto.getVehicleType(), selectedDto.getSjNo(), user.getUsername());
			
			//reloadList
			List<MobSjDto> listTemp = new ArrayList<>();
			if(user.isAdministrator()){
<<<<<<< HEAD
				Map<String, Object> map = sjBaruSvc.findAll(searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc,sj_no", "desc");
=======
				Map<String, Object> map = sjBaruSvc.findAll(searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
>>>>>>> branch 'master' of https://gitlab.com/ari.haryanto/rmi-spt-sj.git
				listIndex = (List<MobSjDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);					
			}else{
				if(user.isCustomer()){
					System.err.println("Masuk sebagai customer");
					Map<String, Object> map = sjBaruSvc.findDataNewByCustomer(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
					//List<MobSjDto> listTemp = new ArrayList<>();
					listTemp = (List<MobSjDto>) map.get("content");
					/*listIndex = (List<MobSjDto>) map.get("content");*/
					for(MobSjDto dto : listTemp){
						dto.setFlagVisiblePenugasan(false);
						dto.setFlagVisibleKonfirmasi(true);
						listIndex.add(dto);
					}
					recordCount = (Integer) map.get("totalSize");
					maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);					
				}else{
					System.out.println("transportirrr = "+user.getReferenceCode());
					Map<String, Object> map = sjBaruSvc.findDataNewByTransportir(user.getReferenceCode(), searchData, filterStartDate, filterEndtDate, pageNo, perPage, "sj_date desc, sj_no", "desc");
					listIndex = (List<MobSjDto>) map.get("content");
					recordCount = (Integer) map.get("totalSize");
					maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);										
				}
			}
			
			//update status driver
			listIndex.clear();
			for(MobSjDto dto : listTemp){
				if(dto.getSjNo().equals(selectedDto.getSjNo())){
					dto.setFlagVisibleKonfirmasi(true);
					dto.setFlagVisiblePenugasan(false);
				}
				listIndex.add(dto);
			}
			
			penugasanMode = false;
			
			showSuccessMsgBox("E000");
		}catch(Exception e){
			e.printStackTrace();
			showErrorMsgBox("E001");
		}
	}
	
	@Command
	public void close(){
		viewKonfirmasiMode = false;
		konfirmasiMode = false;
		penugasanMode = false;
		viewMode = false;
		BindUtils.postNotifyChange(null, null, this, "viewMode");
		BindUtils.postNotifyChange(null, null, this, "penugasanMode");
		BindUtils.postNotifyChange(null, null, this, "konfirmasiMode");
		BindUtils.postNotifyChange(null, null, this, "viewKonfirmasiMode");
	}
	
	@Command
	public void uploadSignature(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx){
		try{
			UploadEvent upEvent = (UploadEvent) ctx.getTriggerEvent(); 
			Media media = upEvent.getMedia();
			
			System.out.println("Ukuran file def = "+media.getByteData().length);
			System.out.println("Ukuran setelah dibagi = "+media.getByteData().length/1024);
			
			if(media.getFormat().equalsIgnoreCase("jpg") || media.getFormat().equalsIgnoreCase("jpeg") || media.getFormat().equalsIgnoreCase("png")){
				if((media.getByteData().length/1024) > 5000){
					showErrorMsgBox("E011");
				}else{
					//set image
					try {
						imgSignature = new AImage("img signature", media.getByteData());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					receiveDto.setImgSignatureByte(media.getByteData()==null?null:media.getByteData());
					btnSignature = false;
					imgUploadSignature = true;
															
					BindUtils.postNotifyChange(null, null, this, "receiveDto");
					BindUtils.postNotifyChange(null, null, this, "btnSignature");
					BindUtils.postNotifyChange(null, null, this, "imgSignature");
					BindUtils.postNotifyChange(null, null, this, "imgUploadSignature");									
				}
			}else{
				showErrorMsgBox("E012");
			}			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Command
	public void cekLokasiBerangkat(){
		//String koordinat = selectedDto.getDepartingFromLatitude() + ", " +selectedDto.getDepartingFromLongitude();
		//Executions.getCurrent().sendRedirect("http://www.google.com", "_blank");
		//System.err.println("final koordinat = "+urlGoogleMaps+koordinat);
		Executions.getCurrent().sendRedirect(urlGoogleMaps+selectedDto.getDepartingFromLatitudeLongitude(), "_blank");
	}

	@Command
	public void cekLokasiSampai(){
		//String koordinat = selectedDto.getDepartingFromLatitude() + ", " +selectedDto.getDepartingFromLongitude();
		//Executions.getCurrent().sendRedirect("http://www.google.com", "_blank");
		//System.err.println("final koordinat = "+urlGoogleMaps+koordinat);
		Executions.getCurrent().sendRedirect(urlGoogleMaps+selectedDto.getDestinationLatitudeLongitude(), "_blank");
	}
	
	@Command
	public void cekLokasiTransit(){
		//String koordinat = selectedDto.getDepartingFromLatitude() + ", " +selectedDto.getDepartingFromLongitude();
		//Executions.getCurrent().sendRedirect("http://www.google.com", "_blank");
		//System.err.println("final koordinat = "+urlGoogleMaps+koordinat);
		Executions.getCurrent().sendRedirect(urlGoogleMaps+selectedDto.getTransitLatitudeLongitude(), "_blank");
	}
	
	@Command
	public void cekLokasiSampaiTransit(){
		//String koordinat = selectedDto.getDepartingFromLatitude() + ", " +selectedDto.getDepartingFromLongitude();
		//Executions.getCurrent().sendRedirect("http://www.google.com", "_blank");
		//System.err.println("final koordinat = "+urlGoogleMaps+koordinat);
		Executions.getCurrent().sendRedirect(urlGoogleMaps+selectedDto.getFirstDestinationLatitudeLongitude(), "_blank");
	}	
	
	@Command
	public void uploadImg1(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx){
		try{
			UploadEvent upEvent = (UploadEvent) ctx.getTriggerEvent(); 
			Media media = upEvent.getMedia();
			
			System.out.println("Ukuran file def = "+media.getByteData().length);
			System.out.println("Ukuran setelah dibagi = "+media.getByteData().length/1024);
			
			if(media.getFormat().equalsIgnoreCase("jpg") || media.getFormat().equalsIgnoreCase("jpeg") || media.getFormat().equalsIgnoreCase("png")){
				if((media.getByteData().length/1024) > 5000){
					showErrorMsgBox("E011");
				}else{
					//set image
					try {
						if(media != null){
							img1 = new AImage("img1", media.getByteData());
							
							btnUpload1 = false;
							imgUpload1 = true;
							
							//set value attachment
							MobGoodsReceiveAttachmentDto dto = new MobGoodsReceiveAttachmentDto();
							dto.setSjNo(selectedDto.getSjNo());
							dto.setByteAttach(media.getByteData()==null?null:media.getByteData());
//							dto.setMedia(media==null?null:media);
							dto.setCreated(user.getUsername());
							dto.setCreatedDate(new Date());
							listAttachment.add(dto);
												
							BindUtils.postNotifyChange(null, null, this, "img1");
//							BindUtils.postNotifyChange(null, null, this, "media1");
							BindUtils.postNotifyChange(null, null, this, "btnUpload1");
							BindUtils.postNotifyChange(null, null, this, "imgUpload1");
							BindUtils.postNotifyChange(null, null, this, "listAttachment");									
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}else{
				showErrorMsgBox("E012");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Command
	public void uploadImg2(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx){
		try{
			UploadEvent upEvent = (UploadEvent) ctx.getTriggerEvent(); 
			Media media = upEvent.getMedia();
			
			if(media.getFormat().equalsIgnoreCase("jpg") || media.getFormat().equalsIgnoreCase("jpeg") || media.getFormat().equalsIgnoreCase("png")){
				if((media.getByteData().length/1024) > 5000 ){
					showErrorMsgBox("E011");
				}else{
					//set image
					try {
						if(media != null){
							img2 = new AImage("img2", media.getByteData());

							btnUpload2 = false;
							imgUpload2 = true;
							
							//set value attachment
							MobGoodsReceiveAttachmentDto dto = new MobGoodsReceiveAttachmentDto();
							dto.setSjNo(selectedDto.getSjNo());
							dto.setByteAttach(media.getByteData()==null?null:media.getByteData());
//							dto.setMedia(media==null?null:media);
							dto.setCreated(user.getUsername());
							dto.setCreatedDate(new Date());
							listAttachment.add(dto);					
							
							BindUtils.postNotifyChange(null, null, this, "img2");
//							BindUtils.postNotifyChange(null, null, this, "media2");
							BindUtils.postNotifyChange(null, null, this, "btnUpload2");
							BindUtils.postNotifyChange(null, null, this, "imgUpload2");
							BindUtils.postNotifyChange(null, null, this, "listAttachment");

						}						
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}else{
				showErrorMsgBox("E012");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Command
	public void uploadImg3(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx){
		try{
			UploadEvent upEvent = (UploadEvent) ctx.getTriggerEvent(); 
			Media media = upEvent.getMedia();

			if(media.getFormat().equalsIgnoreCase("jpg") || media.getFormat().equalsIgnoreCase("jpeg") || media.getFormat().equalsIgnoreCase("png")){
				if((media.getByteData().length/1024) > 5000 ){
					showErrorMsgBox("E011");
				}else{
					//set image
					try {
						if(media != null){
							img3 = new AImage("img3", media.getByteData());

							btnUpload3 = false;
							imgUpload3 = true;

							//set value attachment
							MobGoodsReceiveAttachmentDto dto = new MobGoodsReceiveAttachmentDto();
							dto.setSjNo(selectedDto.getSjNo());
//							dto.setDocumentBlob(convertByteArrayToBase64(media.getByteData()));
							dto.setByteAttach(media.getByteData()==null?null:media.getByteData());
//							dto.setMedia(media==null?null:media);
							dto.setCreated(user.getUsername());
							dto.setCreatedDate(new Date());
							listAttachment.add(dto);										
							
							BindUtils.postNotifyChange(null, null, this, "img3");
//							BindUtils.postNotifyChange(null, null, this, "media3");
							BindUtils.postNotifyChange(null, null, this, "btnUpload3");
							BindUtils.postNotifyChange(null, null, this, "imgUpload3");				
							BindUtils.postNotifyChange(null, null, this, "listAttachment");				

						}						
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}else{
				showErrorMsgBox("E012");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Command
	public void uploadImg4(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx){
		try{
			UploadEvent upEvent = (UploadEvent) ctx.getTriggerEvent(); 
			Media media = upEvent.getMedia();

			if(media.getFormat().equalsIgnoreCase("jpg") || media.getFormat().equalsIgnoreCase("jpeg") || media.getFormat().equalsIgnoreCase("png")){
				if((media.getByteData().length/1024) > 5000 ){
					showErrorMsgBox("E011");
				}else{
					//set image
					try {
						if(media != null){
							img4 = new AImage("img4", media.getByteData());

							btnUpload4 = false;
							imgUpload4 = true;

							//set value attachment
							MobGoodsReceiveAttachmentDto dto = new MobGoodsReceiveAttachmentDto();
							dto.setSjNo(selectedDto.getSjNo());
//							dto.setDocumentBlob(convertByteArrayToBase64(media.getByteData()));
							dto.setByteAttach(media.getByteData()==null?null:media.getByteData());
//							dto.setMedia(media==null?null:media);
							dto.setCreated(user.getUsername());
							dto.setCreatedDate(new Date());
							listAttachment.add(dto);															
							
							BindUtils.postNotifyChange(null, null, this, "img4");
//							BindUtils.postNotifyChange(null, null, this, "media4");
							BindUtils.postNotifyChange(null, null, this, "btnUpload4");
							BindUtils.postNotifyChange(null, null, this, "imgUpload4");					
							BindUtils.postNotifyChange(null, null, this, "imgUpload4");					
							BindUtils.postNotifyChange(null, null, this, "listAttachment");					
						}						
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}else{
				showErrorMsgBox("E012");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Command
	public void uploadImg5(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx){
		try{
			UploadEvent upEvent = (UploadEvent) ctx.getTriggerEvent(); 
			Media media = upEvent.getMedia();
			
			if(media.getFormat().equalsIgnoreCase("jpg") || media.getFormat().equalsIgnoreCase("jpeg") || media.getFormat().equalsIgnoreCase("png")){
				if((media.getByteData().length/1024) > 5000 ){
					showErrorMsgBox("E011");
				}else{
					//set image
					try {
						if(media != null){
							img5 = new AImage("img5", media.getByteData());

							btnUpload5 = false;
							imgUpload5 = true;

							//set value attachment
							MobGoodsReceiveAttachmentDto dto = new MobGoodsReceiveAttachmentDto();
							dto.setSjNo(selectedDto.getSjNo());
//							dto.setDocumentBlob(convertByteArrayToBase64(media.getByteData()));
							dto.setByteAttach(media.getByteData()==null?null:media.getByteData());
//							dto.setMedia(media==null?null:media);
							dto.setCreated(user.getUsername());
							dto.setCreatedDate(new Date());
							listAttachment.add(dto);																				
							
							BindUtils.postNotifyChange(null, null, this, "img5");
							BindUtils.postNotifyChange(null, null, this, "btnUpload5");
							BindUtils.postNotifyChange(null, null, this, "imgUpload5");
							BindUtils.postNotifyChange(null, null, this, "listAttachment");										
}						
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}else{
				showErrorMsgBox("E012");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
		
	public String saveByteArrayToPath(byte[] img, String folderName, Integer seq){
		String pathFile = "";
		try {
		String extension ="jpg";
		//tes lagi
		byte[] decodedBytes = img;
<<<<<<< HEAD
		pathFile = "/opt/Tomcat/webapps/bmm-asset/"+folderName+seq+new Date().getDate()+new Date().getMonth()+new Date().getYear()+new Date().getTime()+"."+extension;
=======
		pathFile = "/opt/tomcat/webapps/rmi-sptj-api-asset/"+folderName+seq+new Date().getDate()+new Date().getMonth()+new Date().getYear()+new Date().getTime()+"."+extension;
>>>>>>> branch 'master' of https://gitlab.com/ari.haryanto/rmi-spt-sj.git
		FileOutputStream imageOutFile = null;
		
			imageOutFile = new FileOutputStream(pathFile);
			imageOutFile.write(decodedBytes);
			imageOutFile.close();
		} catch (FileNotFoundException e) {
			System.err.println(e.toString());
		} catch (IOException e) {
			System.err.println(e.toString());
		}
		
		return pathFile;
	}
	
	public String saveFileToPath(Media media){
		BufferedInputStream in = null;
		BufferedOutputStream out = null;
		try{
			InputStream fin = media.getStreamData();
			in = new BufferedInputStream(fin);
//			
			String port = ( Executions.getCurrent().getServerPort()== 80 ) ? "" : (":"+Executions.getCurrent().getServerPort());
//			String url = Executions.getCurrent().getScheme()+"://"+Executions.getCurrent().getServerName()+port+"bmm-asset";
			String url = Executions.getCurrent().getScheme()+"://"+Executions.getCurrent().getServerName()+port+"/bmm-asset/attachment/";
			
			String pathUrl = "http://35.240.160.9:8080/bmm-asset/attachment/";
			
			String pathFile = pathUrl+new Date().getDate()+new Date().getMonth()+new Date().getYear()+new Date().getTime()+"."+media.getFormat();
			
			String workingDir ="/opt/tomcat/webapps/bmm-asset/attachment/";
			
			JSch jsch = new JSch();
	        Session session = null;
	        try {
	            session = jsch.getSession("adhitya.pradypta", "35.240.160.9", 22);
	            session.setConfig("PreferredAuthentications", "publickey");
	            session.setConfig("StrictHostKeyChecking", "no");
	            session.setPassword("1nd0cyb3r");
	            session.connect();
	            
	            Channel channel = session.openChannel("sftp");
	            channel.connect();
	            ChannelSftp sftpChannel = (ChannelSftp) channel;
	            sftpChannel.cd(workingDir);
	            
	            sftpChannel.put(media.getStreamData(), media.getName());  
	            sftpChannel.exit();
	            session.disconnect();
	        } catch (JSchException e) {
	            e.printStackTrace();  
	        } catch (SftpException e) {
	            e.printStackTrace();
	        }
			
//			pathFile = "/opt/tomcat/webapps/bmm-asset/"+folderName+new Date().getDate()+new Date().getMonth()+new Date().getYear()+new Date().getTime()+"."+extension;
			
			System.out.println("Hasil PATH = "+pathFile);
			File baseDir = new File(pathFile);
			
			//Save To Local
			if(!baseDir.exists()){
				baseDir.mkdirs();
			}
									
			OutputStream fout = new FileOutputStream(pathFile);
			out = new BufferedOutputStream(fout);
						
			byte[] buffer = media.getByteData();
			
			int ch = in.read(buffer);
			while (ch != -1) {
				out.write(buffer, 0, ch);
				ch = in.read(buffer);
			}
			return pathFile;
		}catch(IOException e){
			throw new RuntimeException(e);
		}catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			try {
				if (out != null)
					out.close();

				if (in != null)
					in.close();

			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	
	public String convertByteArrayToBase64(byte[] img){
		try{
			String encode = Base64.getEncoder().encodeToString(img);
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	public MUserDto getUser() {
		return user;
	}

	public void setUser(MUserDto user) {
		this.user = user;
	}

	public List<MobSjDto> getListIndex() {
		return listIndex;
	}

	public void setListIndex(List<MobSjDto> listIndex) {
		this.listIndex = listIndex;
	}

	public MobSjDto getSelectedDto() {
		return selectedDto;
	}

	public void setSelectedDto(MobSjDto selectedDto) {
		this.selectedDto = selectedDto;
	}

	public boolean isFreezeId() {
		return freezeId;
	}

	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

	public boolean isSuperAdmin() {
		return superAdmin;
	}

	public void setSuperAdmin(boolean superAdmin) {
		this.superAdmin = superAdmin;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public boolean isPenugasanMode() {
		return penugasanMode;
	}

	public void setPenugasanMode(boolean penugasanMode) {
		this.penugasanMode = penugasanMode;
	}

	public boolean isKonfirmasiMode() {
		return konfirmasiMode;
	}

	public void setKonfirmasiMode(boolean konfirmasiMode) {
		this.konfirmasiMode = konfirmasiMode;
	}

	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnDel() {
		return btnDel;
	}

	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isBtnDl() {
		return btnDl;
	}

	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}

	public boolean isViewMode() {
		return viewMode;
	}

	public void setViewMode(boolean viewMode) {
		this.viewMode = viewMode;
	}

	public List<MobDriverDto> getListDriver() {
		return listDriver;
	}

	public void setListDriver(List<MobDriverDto> listDriver) {
		this.listDriver = listDriver;
	}

	public List<MVehicleTypeDto> getListVehicleType() {
		return listVehicleType;
	}

	public void setListVehicleType(List<MVehicleTypeDto> listVehicleType) {
		this.listVehicleType = listVehicleType;
	}

	public MVehicleTypeDto getSelectedVehicle() {
		return selectedVehicle;
	}

	public void setSelectedVehicle(MVehicleTypeDto selectedVehicle) {
		this.selectedVehicle = selectedVehicle;
	}

	public boolean isBtnUpload1() {
		return btnUpload1;
	}

	public void setBtnUpload1(boolean btnUpload1) {
		this.btnUpload1 = btnUpload1;
	}

	public boolean isBtnUpload2() {
		return btnUpload2;
	}

	public void setBtnUpload2(boolean btnUpload2) {
		this.btnUpload2 = btnUpload2;
	}

	public boolean isBtnUpload3() {
		return btnUpload3;
	}

	public void setBtnUpload3(boolean btnUpload3) {
		this.btnUpload3 = btnUpload3;
	}

	public boolean isBtnUpload4() {
		return btnUpload4;
	}

	public void setBtnUpload4(boolean btnUpload4) {
		this.btnUpload4 = btnUpload4;
	}

	public boolean isBtnUpload5() {
		return btnUpload5;
	}

	public void setBtnUpload5(boolean btnUpload5) {
		this.btnUpload5 = btnUpload5;
	}

	public boolean isImgUpload1() {
		return imgUpload1;
	}

	public void setImgUpload1(boolean imgUpload1) {
		this.imgUpload1 = imgUpload1;
	}

	public boolean isImgUpload2() {
		return imgUpload2;
	}

	public void setImgUpload2(boolean imgUpload2) {
		this.imgUpload2 = imgUpload2;
	}

	public boolean isImgUpload3() {
		return imgUpload3;
	}

	public void setImgUpload3(boolean imgUpload3) {
		this.imgUpload3 = imgUpload3;
	}

	public boolean isImgUpload4() {
		return imgUpload4;
	}

	public void setImgUpload4(boolean imgUpload4) {
		this.imgUpload4 = imgUpload4;
	}

	public boolean isImgUpload5() {
		return imgUpload5;
	}

	public void setImgUpload5(boolean imgUpload5) {
		this.imgUpload5 = imgUpload5;
	}

	public AImage getImg1() {
		return img1;
	}

	public void setImg1(AImage img1) {
		this.img1 = img1;
	}

	public AImage getImg2() {
		return img2;
	}

	public void setImg2(AImage img2) {
		this.img2 = img2;
	}

	public AImage getImg3() {
		
		return img3;
	}

	public void setImg3(AImage img3) {
		this.img3 = img3;
	}

	public AImage getImg4() {
		return img4;
	}

	public void setImg4(AImage img4) {
		this.img4 = img4;
	}

	public AImage getImg5() {
		return img5;
	}

	public void setImg5(AImage img5) {
		this.img5 = img5;
	}

	public List<MobGoodsReceiveAttachmentDto> getListAttachment() {
		return listAttachment;
	}

	public void setListAttachment(List<MobGoodsReceiveAttachmentDto> listAttachment) {
		this.listAttachment = listAttachment;
	}

	public MobGoodsReceiveAttachmentDto getAttachmentDto() {
		return attachmentDto;
	}

	public void setAttachmentDto(MobGoodsReceiveAttachmentDto attachmentDto) {
		this.attachmentDto = attachmentDto;
	}

	public byte[] getDefImage() {
		return defImage;
	}

	public void setDefImage(byte[] defImage) {
		this.defImage = defImage;
	}

	public MobGoodsReceiveDto getReceiveDto() {
		return receiveDto;
	}

	public void setReceiveDto(MobGoodsReceiveDto receiveDto) {
		this.receiveDto = receiveDto;
	}

	public boolean isViewKonfirmasiMode() {
		return viewKonfirmasiMode;
	}

	public void setViewKonfirmasiMode(boolean viewKonfirmasiMode) {
		this.viewKonfirmasiMode = viewKonfirmasiMode;
	}

	public boolean isBtnSignature() {
		return btnSignature;
	}

	public void setBtnSignature(boolean btnSignature) {
		this.btnSignature = btnSignature;
	}

	public boolean isImgUploadSignature() {
		return imgUploadSignature;
	}

	public void setImgUploadSignature(boolean imgUploadSignature) {
		this.imgUploadSignature = imgUploadSignature;
	}

	public AImage getImgSignature() {
		return imgSignature;
	}

	public void setImgSignature(AImage imgSignature) {
		this.imgSignature = imgSignature;
	}

}
