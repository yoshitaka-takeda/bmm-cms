package id.co.indocyber.bmmRmiCms.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.util.StringUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MobDriverDto;
import id.co.indocyber.bmmRmiCms.dto.MobTransportirDto;
import id.co.indocyber.bmmRmiCms.service.MDTransporterSvc;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TransporterVmd extends BaseVmd {
	
	@WireVariable
	private MDTransporterSvc mDTransporterSvc;
	
	private MUserDto user;
	
	private List<MobTransportirDto> listTransporter = new ArrayList<>();
	private MobTransportirDto selectedTransporter, mobTransportirDto;
	
	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;

	private boolean freezeId;
	private int editIndex = -1;
	private Boolean viewMode;
	private String pageTitle;

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private boolean superAdmin;
	private Integer recordCount = 1;
	
	private String sortBy = "";
	private boolean sortAsc = true;
	
	
	@Init
	@NotifyChange({"user","listTransporter","recordCount","maxPage"})	
	public void load(){
		try{
			user = (MUserDto) Sessions.getCurrent().getAttribute("user");

			Map<String, Object> map = mDTransporterSvc.findByArg(searchData, pageNo, perPage,"is_active desc, code", true);
			listTransporter = (List<MobTransportirDto>) map.get("content");
			recordCount = (Integer) map.get("totalSize");
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
			for (MMenuRoleDto x : user.getRolePrivs()) {
				if ("UM-2".equals(x.getMenuCode())) {
					btnAdd = !x.getIsAdd();
					btnDel = !x.getIsDelete();
					btnDl = !x.getIsDownload();
					btnEdit = !x.getIsEdit();
					break;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Command("switchPage")
	@NotifyChange({ "pageNo", "listTransporter" })
	public void switchPage(@BindingParam("action") String action) {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}
			if (search) {
				Map<String, Object> map = mDTransporterSvc.findByArg(searchData, pageNo, perPage,StringUtils.isEmpty(sortBy) ? "is_active desc, code"  : sortBy, sortAsc);
				listTransporter = (List<MobTransportirDto>) map.get("content");
			} else {
				Map<String, Object> map = mDTransporterSvc.findByArg(searchData, pageNo, perPage,StringUtils.isEmpty(sortBy) ? "is_active desc, code"  : sortBy, sortAsc);
				listTransporter = (List<MobTransportirDto>) map.get("content");
			}
		}
	}
	
	@Command("src")
	@NotifyChange({"listTransporter", "pageNo", "maxPage", "recordCount"})
	public void search() {
		search = true;
		pageNo = 1;
		Map<String, Object> map = mDTransporterSvc.findByArg(searchData, pageNo, perPage,StringUtils.isEmpty(sortBy) ? "is_active desc, code"  : sortBy, sortAsc);
		listTransporter = (List<MobTransportirDto>) map.get("content");
		recordCount = (Integer) map.get("totalSize");
		maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
	}
	
	@Command
	public void back(){
		viewMode = false;
		BindUtils.postNotifyChange(null, null, this, "viewMode");
	}	
	
	@Command("reload")
	@NotifyChange({"recordCount","listTransporter","maxPage","searchData"})
	public void reload(){
		searchData = "";
		Map<String, Object> map = mDTransporterSvc.findByArg(searchData, pageNo, perPage,"is_active desc, code", true);
		listTransporter = (List<MobTransportirDto>) map.get("content");
		recordCount = (Integer) map.get("totalSize");
		maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
	}

		
	@Command
	@NotifyChange({"selectedTransporter","flagIconWarning"})
	public void ubahStatusAktif(@BindingParam("dto") MobTransportirDto dto){
		showConfirm("E002");
		setFlagIconWarning(true);
		selectedTransporter = new MobTransportirDto();
		selectedTransporter = dto;
		selectedTransporter.setIsActive(false);
	}
	
	@Command
	@NotifyChange({"selectedTransporter","flagIconWarning"})
	public void ubahStatusNonAktif(@BindingParam("dto") MobTransportirDto dto){
		showConfirm("E003");
		setFlagIconWarning(true);
		selectedTransporter = new MobTransportirDto();
		selectedTransporter = dto;
		selectedTransporter.setIsActive(true);
	}
	
	@Command
	@NotifyChange({"recordCount","listTransporter","maxPage"})
	public void yesConfirm(){
		int result = mDTransporterSvc.save(selectedTransporter);
		if(result == 1){
			flagConfirmMsg = false;
			showSuccessMsgBox("E000");
		}else{
			showErrorMsgBox("E001");
		}
		
		//reload list
		Map<String, Object> map = mDTransporterSvc.findByArg(searchData, pageNo, perPage,"is_active desc, code", true);
		listTransporter = (List<MobTransportirDto>) map.get("content");
		recordCount = (Integer) map.get("totalSize");
		maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
		BindUtils.postNotifyChange(null, null, this, "flagConfirmMsg");
	}
	
	@Command
	public void view(@BindingParam("dto") MobTransportirDto dto){
		mobTransportirDto = dto;
		viewMode = true;
		BindUtils.postNotifyChange(null, null, this, "viewMode");
		BindUtils.postNotifyChange(null, null, this, "mobTransportirDto");		
	}
	
	@Command("sort")
	@NotifyChange({"listTransporter"})
	public void sort(@BindingParam("col") String column) {
		if (activeCheck()) {
			if (sortBy.equals(column)) {
				sortAsc = !sortAsc;
			} else {
				sortBy = column;
				sortAsc = true;
			}
			Map<String, Object> map = mDTransporterSvc.findByArg(searchData, pageNo, perPage, sortBy, sortAsc);
			listTransporter = (List<MobTransportirDto>) map.get("content");
		}
	}

	
	public List<MobTransportirDto> getListTransporter() {
		return listTransporter;
	}
	public void setListTransporter(List<MobTransportirDto> listTransporter) {
		this.listTransporter = listTransporter;
	}
	public MobTransportirDto getSelectedTransporter() {
		return selectedTransporter;
	}
	public void setSelectedTransporter(MobTransportirDto selectedTransporter) {
		this.selectedTransporter = selectedTransporter;
	}
	public boolean isBtnAdd() {
		return btnAdd;
	}
	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}
	public boolean isBtnDel() {
		return btnDel;
	}
	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}
	public boolean isBtnEdit() {
		return btnEdit;
	}
	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}
	public boolean isBtnDl() {
		return btnDl;
	}
	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}
	public boolean isFreezeId() {
		return freezeId;
	}
	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}
	public int getEditIndex() {
		return editIndex;
	}
	public void setEditIndex(int editIndex) {
		this.editIndex = editIndex;
	}
	public String getPageTitle() {
		return pageTitle;
	}
	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}
	public Integer getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public Integer getMaxPage() {
		return maxPage;
	}
	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}
	public boolean isSearch() {
		return search;
	}
	public void setSearch(boolean search) {
		this.search = search;
	}
	public boolean isSuperAdmin() {
		return superAdmin;
	}
	public void setSuperAdmin(boolean superAdmin) {
		this.superAdmin = superAdmin;
	}
	public Integer getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public boolean isSortAsc() {
		return sortAsc;
	}
	public void setSortAsc(boolean sortAsc) {
		this.sortAsc = sortAsc;
	}
	public MUserDto getUser() {
		return user;
	}
	public void setUser(MUserDto user) {
		this.user = user;
	}

	public MobTransportirDto getMobTransportirDto() {
		return mobTransportirDto;
	}

	public void setMobTransportirDto(MobTransportirDto mobTransportirDto) {
		this.mobTransportirDto = mobTransportirDto;
	}

	public Boolean getViewMode() {
		return viewMode;
	}

	public void setViewMode(Boolean viewMode) {
		this.viewMode = viewMode;
	}

}
