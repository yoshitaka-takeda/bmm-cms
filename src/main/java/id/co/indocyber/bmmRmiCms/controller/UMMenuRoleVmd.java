package id.co.indocyber.bmmRmiCms.controller;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MUserRoleDto;
import id.co.indocyber.bmmRmiCms.service.UMMenuRoleSvc;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class UMMenuRoleVmd extends BaseVmd {
	
	@WireVariable
	private UMMenuRoleSvc uMMenuRoleSvc;
	
	private MUserDto user;
	
	private List<MMenuRoleDto> listIndex = new ArrayList<>();
	
	private List<MMenuRoleDto> listMenuRole = new ArrayList<>();	
	private MMenuRoleDto menuRoleDto, editMenuRoleDto, selectedMenuRoleDto;
		
	private List<MRoleDto> listRoleDto = new ArrayList<>();
	private MRoleDto selectedRole;
	
	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;

	private boolean freezeId;
	private int editIndex = -1;
	
	private Boolean editMode = false;
	private Boolean addMode = false;
	private Boolean detaillWindow = false;
	
	private String pageTitle;

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private boolean superAdmin;
	private Integer recordCount = 1;
	
	private String sortBy = "";
	private boolean sortAsc = false;
	
	private boolean flagUser = true;

	@Init
	@NotifyChange({"editMode","user","listIndex","recordCount","maxPage","listRoleDto","btnAdd", "btnDel", "btnEdit", "btnDl"})
	public void load(){
		
		try{
			editMode = Boolean.valueOf((Boolean) Sessions.getCurrent().getAttribute("editMode"));
			user = (MUserDto) Sessions.getCurrent().getAttribute("user");
			
			System.out.println("searchData ="+searchData);
			System.out.println("pageNo ="+pageNo);
			listIndex = uMMenuRoleSvc.findAllUserRole(searchData, pageNo, perPage ,"roleId", false);
			recordCount = uMMenuRoleSvc.countAll(searchData);
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
			
			for (MMenuRoleDto x : user.getRolePrivs()) {
				if ("UM-4".equals(x.getMenuCode())) {
					btnAdd = !x.getIsAdd();
					btnDel = !x.getIsDelete();
					btnDl = !x.getIsDownload();
					btnEdit = !x.getIsEdit();
					break;
				}
			}					
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Command("switchPage")
	@NotifyChange({ "pageNo", "listIndex", "resetUser" })
	public void switchPage(@BindingParam("action") String action) {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}

			listIndex = uMMenuRoleSvc.findAllUserRole(searchData, pageNo, perPage ,"roleId", false);
		}
	}
	
	@Command("src")
	@NotifyChange({"listIndex", "pageNo", "maxPage", "recordCount"})
	public void search() {
		search = true;
		pageNo = 1;
		listIndex = uMMenuRoleSvc.findAllUserRole(searchData, pageNo, perPage ,"roleId", sortAsc);
		recordCount = uMMenuRoleSvc.countAll(searchData);
		maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
	}
	
	@Command("reload")
	@NotifyChange({"recordCount","listIndex","maxPage","searchData"})
	public void reload(){
		searchData = null;
		recordCount = uMMenuRoleSvc.countAll(searchData);
		listIndex = uMMenuRoleSvc.findAllUserRole(searchData, pageNo, perPage ,"roleId", sortAsc);
		maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
	}

	
	@Command
	public void back(){
		editMode = false;
		addMode = false;
		detaillWindow = false;
		BindUtils.postNotifyChange(null, null, this, "addMode");
		BindUtils.postNotifyChange(null, null, this, "editMode");
		BindUtils.postNotifyChange(null, null, this, "detaillWindow");
	}
	
	@Command
	@NotifyChange({"selectedMenuRoleDto","flagIconWarning"})
	public void ubahStatusAktif(@BindingParam("dto") MMenuRoleDto dto){
		showConfirm("E002");
		setFlagIconWarning(true);
		selectedMenuRoleDto = new MMenuRoleDto();
		selectedMenuRoleDto = dto;		
		selectedMenuRoleDto.setIsActive(false);
		selectedMenuRoleDto.setModified(user.getFullname());
		selectedMenuRoleDto.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@Command
	@NotifyChange({"selectedMenuRoleDto","flagIconWarning"})
	public void ubahStatusNonAktif(@BindingParam("dto") MMenuRoleDto dto){
		showConfirm("E003");
		setFlagIconWarning(true);
		selectedMenuRoleDto = new MMenuRoleDto();
		selectedMenuRoleDto = dto;
		selectedMenuRoleDto.setIsActive(true);
		selectedMenuRoleDto.setModified(user.getFullname());
		selectedMenuRoleDto.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@Command
	@NotifyChange({"recordCount","listIndex","maxPage"})
	public void yesConfirm(){
		
		List<MMenuRoleDto> listDto = uMMenuRoleSvc.findMenuRoleByRoleId(selectedMenuRoleDto.getRoleId(), selectedMenuRoleDto.getIsActive());
//		System.out.println("Cek data");
//		for(MMenuRoleDto dto : listDto){
//			System.out.println("id. "+dto.getId());
//		}
		
		int result = uMMenuRoleSvc.updateStatus(listDto);
		if(result == 1){
			flagConfirmMsg = false;
			showSuccessMsgBox("E000");
		}else{
			showErrorMsgBox("E001");
		}
		
		//reload list
		recordCount = uMMenuRoleSvc.countAll(searchData);
		listIndex = uMMenuRoleSvc.findAllUserRole(searchData, pageNo, perPage ,"roleId", sortAsc);
		maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
		BindUtils.postNotifyChange(null, null, this, "flagConfirmMsg");
	}

	@Command
	public void add(){
		editMenuRoleDto = new MMenuRoleDto();
		editMode = false;
		addMode = true;
		detaillWindow = true;
		
		//load list menu roole
		listMenuRole = uMMenuRoleSvc.findAllMenuRole();
				
		BindUtils.postNotifyChange(null, null, this, "editMenuRoleDto");
		BindUtils.postNotifyChange(null, null, this, "listMenuRole");
		BindUtils.postNotifyChange(null, null, this, "editMode");
		BindUtils.postNotifyChange(null, null, this, "addMode");
		BindUtils.postNotifyChange(null, null, this, "detaillWindow");
	}
	
	@Command
	public void edit(@BindingParam("item") MMenuRoleDto dto){
		try{
			editMenuRoleDto = new MMenuRoleDto();
			editMenuRoleDto.setRoleId(dto.getRoleId());
			editMenuRoleDto.setRoleName(dto.getRoleName());
			editMenuRoleDto.setIsActive(dto.getIsActive());
			
			editMode = true;
			addMode = false;
			detaillWindow = true;
			
			//loadList MenuRole
			listMenuRole = uMMenuRoleSvc.findMenuRoleByParam(dto.getRoleId());
			
			BindUtils.postNotifyChange(null, null, this, "editMenuRoleDto");
			BindUtils.postNotifyChange(null, null, this, "listMenuRole");
			BindUtils.postNotifyChange(null, null, this, "editMode");
			BindUtils.postNotifyChange(null, null, this, "addMode");
			BindUtils.postNotifyChange(null, null, this, "detaillWindow");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Command
	public void test(){
		if(listMenuRole.size()==0){
			showWarningMsgBox("E201");
		}else{
			Messagebox.show("ok");
		}
	}
	
	@Command
	@NotifyChange({"editMode","recordCount","listIndex","maxPage","addMode","detaillWindow"})
	public void save(@BindingParam("btn")Component btn) {
		try{
			if(listMenuRole.size()==0){
				showWarningMsgBox("E201");
			}
			if(editMode = true){
				uMMenuRoleSvc.deleteByRole(editMenuRoleDto.getRoleId());
			}
			
			for(MMenuRoleDto dto : listMenuRole){
				if(dto.getCreated() == null){
					dto.setCreated(user.getFullname());				
				}
				if(dto.getCreatedDate() == null){
					dto.setCreatedDate(Timestamp.from(
							OffsetDateTime.now(ZoneOffset.ofHours(7)).toInstant()));
				}
				dto.setRoleId(editMenuRoleDto.getRoleId());
				dto.setModified(user.getFullname());
				dto.setModifiedDate(Timestamp.from(
							OffsetDateTime.now(ZoneOffset.ofHours(7)).toInstant()));
				uMMenuRoleSvc.save(dto);
			}
						

			showSuccessMsgBox("E000");
			
			editMode = false;
			addMode = false;
			detaillWindow = false;
			
			//reload
			recordCount = uMMenuRoleSvc.countAll(searchData);
			listIndex = uMMenuRoleSvc.findAllUserRole(searchData, pageNo, perPage,"roleId", sortAsc);
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
			
			((Window)btn.getParent().getParent()).setVisible(false);			
		}catch(Exception e){
			e.printStackTrace();
			showErrorMsgBox("E001");
		}
		
		
	}
	
	@Command
	public void loadListRole(){
		//loadList for combobox or bandbox
		listRoleDto = uMMenuRoleSvc.cbListRole(searchData);
		BindUtils.postNotifyChange(null, null, this, "listRoleDto");
	}
	
	@Command
	public void searchRole(@BindingParam("src")String search){
		searchData = search==null?"":search;
		listRoleDto = uMMenuRoleSvc.cbListRole(searchData);
		BindUtils.postNotifyChange(null, null, this, "listRoleDto");		
	}
		
	@Command
	@NotifyChange("editMenuRoleDto")
	public void selectRole(){
		editMenuRoleDto.setRoleId(selectedRole.getId());
		editMenuRoleDto.setRoleName(selectedRole.getRoleName());
	}
	
	@Command("sort")
	@NotifyChange({"listIndex"})
	public void sort(@BindingParam("col") String column) {
		if (activeCheck()) {
			if (sortBy.equals(column)) {
				sortAsc = !sortAsc;
			} else {
				sortBy = column;
				sortAsc = true;
			}
			listIndex = uMMenuRoleSvc.findAllUserRole(searchData, pageNo, perPage,sortBy, sortAsc);
		}
	}

		
	public MUserDto getUser() {
		return user;
	}
	public void setUser(MUserDto user) {
		this.user = user;
	}
	public MMenuRoleDto getMenuRoleDto() {
		return menuRoleDto;
	}
	public void setMenuRoleDto(MMenuRoleDto menuRoleDto) {
		this.menuRoleDto = menuRoleDto;
	}
	public MMenuRoleDto getEditMenuRoleDto() {
		return editMenuRoleDto;
	}
	public void setEditMenuRoleDto(MMenuRoleDto editMenuRoleDto) {
		this.editMenuRoleDto = editMenuRoleDto;
	}
	public MMenuRoleDto getSelectedMenuRoleDto() {
		return selectedMenuRoleDto;
	}
	public void setSelectedMenuRoleDto(MMenuRoleDto selectedMenuRoleDto) {
		this.selectedMenuRoleDto = selectedMenuRoleDto;
	}
	public boolean isBtnAdd() {
		return btnAdd;
	}
	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}
	public boolean isBtnDel() {
		return btnDel;
	}
	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}
	public boolean isBtnEdit() {
		return btnEdit;
	}
	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}
	public boolean isBtnDl() {
		return btnDl;
	}
	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}
	public boolean isFreezeId() {
		return freezeId;
	}
	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}
	public int getEditIndex() {
		return editIndex;
	}
	public void setEditIndex(int editIndex) {
		this.editIndex = editIndex;
	}
	public Boolean getEditMode() {
		return editMode;
	}
	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}
	public String getPageTitle() {
		return pageTitle;
	}
	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}
	public Integer getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public Integer getMaxPage() {
		return maxPage;
	}
	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}
	public boolean isSearch() {
		return search;
	}
	public void setSearch(boolean search) {
		this.search = search;
	}
	public boolean isSuperAdmin() {
		return superAdmin;
	}
	public void setSuperAdmin(boolean superAdmin) {
		this.superAdmin = superAdmin;
	}
	public Integer getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public boolean isSortAsc() {
		return sortAsc;
	}
	public void setSortAsc(boolean sortAsc) {
		this.sortAsc = sortAsc;
	}

	public List<MRoleDto> getListRoleDto() {
		return listRoleDto;
	}

	public void setListRoleDto(List<MRoleDto> listRoleDto) {
		this.listRoleDto = listRoleDto;
	}

	public MRoleDto getSelectedRole() {
		return selectedRole;
	}

	public void setSelectedRole(MRoleDto selectedRole) {
		this.selectedRole = selectedRole;
	}


	public List<MMenuRoleDto> getListMenuRole() {
		return listMenuRole;
	}

	public void setListMenuRole(List<MMenuRoleDto> listMenuRole) {
		this.listMenuRole = listMenuRole;
	}

	public List<MMenuRoleDto> getListIndex() {
		return listIndex;
	}

	public void setListIndex(List<MMenuRoleDto> listIndex) {
		this.listIndex = listIndex;
	}

	public boolean isFlagUser() {
		return flagUser;
	}

	public void setFlagUser(boolean flagUser) {
		this.flagUser = flagUser;
	}

	public Boolean getAddMode() {
		return addMode;
	}

	public void setAddMode(Boolean addMode) {
		this.addMode = addMode;
	}

	public Boolean getDetaillWindow() {
		return detaillWindow;
	}

	public void setDetaillWindow(Boolean detaillWindow) {
		this.detaillWindow = detaillWindow;
	}




	
}
