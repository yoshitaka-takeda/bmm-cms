package id.co.indocyber.bmmRmiCms.controller;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MUserRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MobCustomerDto;
import id.co.indocyber.bmmRmiCms.dto.MobDriverDto;
import id.co.indocyber.bmmRmiCms.dto.MobEmployeeDto;
import id.co.indocyber.bmmRmiCms.dto.MobTransportirDto;
import id.co.indocyber.bmmRmiCms.entity.MRole;
import id.co.indocyber.bmmRmiCms.entity.MUserRole;
import id.co.indocyber.bmmRmiCms.service.MRoleSvc;
import id.co.indocyber.bmmRmiCms.service.MUserRoleSvc;
import id.co.indocyber.bmmRmiCms.service.MUserSvc;
import id.co.indocyber.bmmRmiCms.service.UMUserSvc;
import id.co.indocyber.bmmRmiCms.tools.StringConstants;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.util.StringUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class UMUserVmd extends BaseVmd {
	@WireVariable
	UMUserSvc uMUserSvc;
		
	String urlIndex = "/userManagement/user/index.zul";
	String urlEdit = "/userManagement/user/edit.zul";
	String refresh = "/userManagement/user/ref.zul";

	private String prevPass;

	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;
 
	private MUserDto user, editUser, selectedUser = new MUserDto();
	private MRole role;
	private String module;
	private List<String> moduleList;
	private List<MUserDto> userList;
	
	private List<MRoleDto> listRole = new ArrayList<>();
	
	private List<MobTransportirDto> listTransportir = new ArrayList<>();
	
	private List<MobDriverDto> listDriver = new ArrayList<>();
	private MobDriverDto selectedDriver;
	
	private List<String> listModule = new ArrayList<>();
	private String selectedModule;
	
	private List<MobCustomerDto> listCustomer = new ArrayList<>();
	
	private boolean flagIsActive = true;
	private int editIndex = -1;
	
	private Boolean editMode;
	private Boolean addMode;
	private Boolean detailWindow;
	
	private String pageTitle;	
	
	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private boolean superAdmin;
	private Integer recordCount = 1;
	private String arg, sArg;
	
	private String sortBy = "";
	private boolean sortAsc = false;
	
	private String passUser;
			
	private boolean flagImage = false;
	private boolean flagBtnUploadDef = true;
	private boolean flagProfil = false;
	
	private boolean adminMode = false;
	private boolean driverMode = false;
	private boolean transportirMode = false;
	private boolean customerMode = false;
	
	private boolean windowDriver = false;
	private boolean windowTransportir = false;
	private boolean windowCustomer = false;
	
	public String searchDataDriver;

	@SuppressWarnings("unchecked")
	@Init
	@NotifyChange({"editMode","user","userList","recordCount","maxPage","searchData"})
	public void load(){
		try{
			editMode = Boolean.valueOf((Boolean) Sessions.getCurrent().getAttribute("editMode"));
			user = (MUserDto) Sessions.getCurrent().getAttribute("user");
			searchData = null;
			System.out.println("reference code = "+user.getReferenceCode());
			if(user.isAdministrator() == true){
				//untuk login admin			
				Map<String, Object> map = uMUserSvc.findAll(searchData, pageNo, perPage ,"is_active desc,modified_date", false); 
				userList = (List<MUserDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
				System.out.println("Login Admin");
			}else{
				//untuk login transportir
				Map<String, Object> map = uMUserSvc.findByTransporterId(user.getReferenceCode(), searchData, pageNo, "is_active desc, modified_date", false);
				userList = (List<MUserDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
				System.out.println("Login Bukan Admin");
			}
			
			for (MMenuRoleDto x : user.getRolePrivs()) {
				if ("UM-1".equals(x.getMenuCode())) {
					btnAdd = !x.getIsAdd();
					btnDel = !x.getIsDelete();
					btnDl = !x.getIsDownload();
					btnEdit = !x.getIsEdit();
					break;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}

	}
	
	@SuppressWarnings("unchecked")
	@Command("switchPage")
	@NotifyChange({ "pageNo", "userList", "resetUser" })
	public void switchPage(@BindingParam("action") String action) {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}

			if(user.isAdministrator() == true){
				//untuk login administrator
				Map<String, Object> map = uMUserSvc.findAll(searchData, pageNo, perPage ,"is_active desc,modified_date", false); 
				userList = (List<MUserDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
			}else{
				//untuk login transportir			
				Map<String, Object> map = uMUserSvc.findByTransporterId(user.getReferenceCode(), searchData, pageNo, "is_active desc, modified_date", false);
				userList = (List<MUserDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
			}
			
		}
	}
	
	@SuppressWarnings("unchecked")
	@Command("src")
	@NotifyChange({"userList", "pageNo", "maxPage", "recordCount"})
	public void search() {
		search = true;
		pageNo = 1;
		if(user.isAdministrator() == true){
			//untuk login administrator
			Map<String, Object> map = uMUserSvc.findAll(searchData, pageNo, perPage ,"is_active desc,modified_date", false); 
			userList = (List<MUserDto>) map.get("content");
			recordCount = (Integer) map.get("totalSize");
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);			
		}else{
			//untuk login transportir			
			Map<String, Object> map = uMUserSvc.findByTransporterId(user.getReferenceCode(), searchData, pageNo,"is_active desc, modified_date", false);
			userList = (List<MUserDto>) map.get("content");
			recordCount = (Integer) map.get("totalSize");
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
		}
		
	}
	
	@Command
	public void searchTransportir(@BindingParam("item")String search){
		searchData = search;
		listTransportir = uMUserSvc.getListTransportir(searchData.trim());
		BindUtils.postNotifyChange(null, null, this, "listTransportir");	
	}
	
	@Command
	public void searchCustomer(){
		listCustomer = uMUserSvc.getListCustomer(searchData);
		BindUtils.postNotifyChange(null, null, this, "listCustomer");
	}
	
	@Command
	public void searchDriver(){
		if(editUser.getTransportirCode() != null){
			listDriver = uMUserSvc.getListDriverByTransportir(editUser.getTransportirCode(), searchDataDriver);
			BindUtils.postNotifyChange(null, null, this, "listDriver");		
		}else{
			Messagebox.show("Transportir belum dipilih");
		}		
	}
	
	@Command
	public void searchListRole(){
		listRole = uMUserSvc.getListRole(editUser.getRoleName());
		BindUtils.postNotifyChange(null, null, this, "listRole");		
	}
	
	
	@SuppressWarnings("unchecked")
	@Command("reload")
	@NotifyChange({"recordCount","userList","maxPage","searchData"})
	public void reload(){
		searchData = null;
		if(user.isAdministrator() == true){
			Map<String, Object> map = uMUserSvc.findAll(searchData, pageNo, perPage ,"is_active desc,modified_date", false); 
			userList = (List<MUserDto>) map.get("content");
			recordCount = (Integer) map.get("totalSize");
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);			
		}else{
			//untuk login transportir			
			Map<String, Object> map = uMUserSvc.findByTransporterId(user.getReferenceCode(), searchData, pageNo, "is_active desc, modified_date", false);
			userList = (List<MUserDto>) map.get("content");
			recordCount = (Integer) map.get("totalSize");
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
		}
	}

	
	@Command
	public void back(){		
		editMode = false;
		addMode = false;
		detailWindow = false;
		BindUtils.postNotifyChange(null, null, this, "editMode");
		BindUtils.postNotifyChange(null, null, this, "addMode");
		BindUtils.postNotifyChange(null, null, this, "detailWindow");		
	}
	
	@Command
	public void close(){
		detailWindow = false;
		windowCustomer = false;
		windowDriver = false;
		windowTransportir = false;
		BindUtils.postNotifyChange(null, null, this, "detailWindow");
		BindUtils.postNotifyChange(null, null, this, "windowCustomer");
		BindUtils.postNotifyChange(null, null, this, "windowDriver");
		BindUtils.postNotifyChange(null, null, this, "windowTransportir");
	}
	
	@Command
	public void closeDetail(){
		windowCustomer = false;
		windowDriver = false;
		windowTransportir = false;
		BindUtils.postNotifyChange(null, null, this, "windowCustomer");
		BindUtils.postNotifyChange(null, null, this, "windowDriver");
		BindUtils.postNotifyChange(null, null, this, "windowTransportir");
	}

	
	@Command
	@NotifyChange({"selectedUser","flagIconWarning"})
	public void ubahStatusAktif(@BindingParam("dto") MUserDto dto){
		showConfirm("E002");
		setFlagIconWarning(true);
		selectedUser = new MUserDto();
		selectedUser = dto;
		selectedUser.setRehash(false);
		selectedUser.setIsActive(false);
		selectedUser.setModifiedBy(user.getFullname());
		selectedUser.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@Command
	@NotifyChange({"selectedUser","flagIconWarning"})
	public void clearImei(@BindingParam("dto")MUserDto dto){
		showConfirm("E010");
		setFlagIconWarning(true);
		selectedUser = new MUserDto();
		selectedUser = dto;
		selectedUser.setPassword("bmm2019");
		selectedUser.setRehash(true);
		selectedUser.setImei(null);
		selectedUser.setModifiedBy(user.getFullname());
		selectedUser.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@Command
	@NotifyChange({"selectedUser","flagIconWarning"})
	public void ubahStatusNonAktif(@BindingParam("dto") MUserDto dto){
		showConfirm("E003");
		setFlagIconWarning(true);
		selectedUser = new MUserDto();
		selectedUser = dto;
		selectedUser.setRehash(false);
		selectedUser.setIsActive(true);
		selectedUser.setModifiedBy(user.getFullname());
		selectedUser.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@SuppressWarnings("unchecked")
	@Command
	@NotifyChange({"recordCount","userList","maxPage"})
	public void yesConfirm(){
		try{
			selectedUser.setAddMode(false);
			uMUserSvc.save(selectedUser);
			flagConfirmMsg = false;
			showSuccessMsgBox("E000");

			//reload list
			if(user.isAdministrator() == true){
				Map<String, Object> map = uMUserSvc.findAll(searchData, pageNo, perPage ,"is_active desc,modified_date", false); 
				userList = (List<MUserDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
			}else{
				//untuk login transportir			
				Map<String, Object> map = uMUserSvc.findByTransporterId(user.getReferenceCode(), searchData, pageNo, "is_active desc, modified_date", false);
				userList = (List<MUserDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
			}
			BindUtils.postNotifyChange(null, null, this, "flagConfirmMsg");			
		}catch(Exception e){
			e.printStackTrace();
			showErrorMsgBox("E001");
		}		
	}

	@Command
	public void add(){
		
		addMode = true;
		editMode = false;
		detailWindow = true;
		
		passUser = null;
		searchData = null;				
		editUser = new MUserDto();
		editUser.setAddMode(true);
		editUser.setIsActive(true);
		
		adminMode = false;
		driverMode = false;
		transportirMode = false;
		customerMode = false;
		
		//loadList Module
		listModule = uMUserSvc.getListModul();
		
		if(user.isAdministrator() == false){
			editUser.setRoleName("Supir");
			editUser.setRoleId(2);			
			editUser.setModuleApps("Mobile");
			driverMode = true;
			flagProfil = true;
		}
		
		listRole = uMUserSvc.getListRole(editUser.getRoleName());
		BindUtils.postNotifyChange(null, null, this, "listRole");		
		
		BindUtils.postNotifyChange(null, null, this, "addMode");
		BindUtils.postNotifyChange(null, null, this, "editMode");
		BindUtils.postNotifyChange(null, null, this, "detailWindow");
		BindUtils.postNotifyChange(null, null, this, "passUser");
		BindUtils.postNotifyChange(null, null, this, "flagProfil");
		BindUtils.postNotifyChange(null, null, this, "editUser");		
		BindUtils.postNotifyChange(null, null, this, "searchData");
		BindUtils.postNotifyChange(null, null, this, "listModule");
		
		BindUtils.postNotifyChange(null, null, this, "adminMode");
		BindUtils.postNotifyChange(null, null, this, "driverMode");
		BindUtils.postNotifyChange(null, null, this, "transportirMode");
		BindUtils.postNotifyChange(null, null, this, "customerMode");
	}
	
	@Command
	public void edit(@BindingParam("item") MUserDto dto){
		
		editMode = true;
		addMode = false;
		detailWindow = true;
		
		searchData = null;
		editUser = dto;		
		editUser.setAddMode(false);
		passUser = dto.getPassword();
		
		
		//loadList Module
		listModule = uMUserSvc.getListModul();

		if(dto.getRoleId() == 2){
			listTransportir = uMUserSvc.getListTransportir(searchData);
			driverMode = true;
		}else if(dto.getRoleId() == 8){
			listTransportir = uMUserSvc.getListTransportir(searchData);
			transportirMode = true;
		}else if(dto.getRoleId() == 12){
			listCustomer = uMUserSvc.getListCustomer(searchData);
			customerMode = true;
		}else{
			System.out.println("admin mode");
			adminMode = true;
		}

		
		BindUtils.postNotifyChange(null, null, this, "editUser");
		BindUtils.postNotifyChange(null, null, this, "addMode");
		BindUtils.postNotifyChange(null, null, this, "detailWindow");
		BindUtils.postNotifyChange(null, null, this, "passUser");		
		BindUtils.postNotifyChange(null, null, this, "editMode");
		BindUtils.postNotifyChange(null, null, this, "searchData");
		BindUtils.postNotifyChange(null, null, this, "listModule");
		
		BindUtils.postNotifyChange(null, null, this, "listTransportir");
		BindUtils.postNotifyChange(null, null, this, "driverMode");
		BindUtils.postNotifyChange(null, null, this, "transportirMode");
		BindUtils.postNotifyChange(null, null, this, "listCustomer");
		BindUtils.postNotifyChange(null, null, this, "customerMode");
		BindUtils.postNotifyChange(null, null, this, "adminMode");
	}
		
	@Command
	@NotifyChange({"editMode","recordCount","userList","maxPage","passUser","detailWindow"})
	public void save() {
		try{
			if(editUser.getIsActive() == null){
				editUser.setIsActive(true);
			}
			
			if(editUser.getPersonId() == null){
				editUser.setPersonId(0);
			}
			
			System.err.println("Pass user = "+passUser);
			System.err.println("Pass kolom = "+editUser.getPassword());
			
			if(editUser.isAddMode()){
				editUser.setRehash(true);
				System.err.println("Add : Password di rehash");
			}else{
				if(editUser.getPassword().equalsIgnoreCase(passUser)){
					editUser.setRehash(false);
					System.err.println("Password tidak di rehash");
				}else{
					editUser.setRehash(true);
					System.err.println("Password di rehash");
				}			
			}
			
			editUser.setPassword(passUser);
			editUser.setCreatedBy(user.getFullname());
			editUser.setCreatedDate(new Timestamp(System.currentTimeMillis()));
			editUser.setModifiedBy(user.getFullname());
			editUser.setModifiedDate(new Timestamp(System.currentTimeMillis()));
			
			//deklar userRole
			MUserRoleDto userRoleDto = new MUserRoleDto();
			if(editUser.getRoleId() != null){
				userRoleDto.setRoleId(editUser.getRoleId());
			}		
			userRoleDto.setEmail(editUser.getEmail().toLowerCase());
			userRoleDto.setModuleApps(editUser.getModuleApps());
			userRoleDto.setIsActive(editUser.getIsActive());
			userRoleDto.setCreatedBy(user.getUsername());
			userRoleDto.setCreatedDate(new Timestamp(System.currentTimeMillis()));
			userRoleDto.setModifiedBy(user.getUsername());
			userRoleDto.setModifiedDate(new Timestamp(System.currentTimeMillis()));
			
			String fullErrMsg = StringConstants.MSG_FORM_TEMPLATE;
			int error = 0;
			if (StringUtils.isEmpty(editUser.getRoleName() == null)) {
				error++;
				fullErrMsg += String.format(
						StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Role");
			}else{
				if(editUser.getRoleName() == null){
					error++;
					fullErrMsg += String.format(
							StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Role");			
				}
			}
			if (StringUtils.isEmpty(editUser.getEmail())) {
				error++;
				fullErrMsg += String.format(
						StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Email");
			}else{
				if(addMode){
					if(uMUserSvc.cekDataByMail(editUser.getEmail()) > 0){
						error++;
						fullErrMsg += String.format(
								StringConstants.MSG_WAS_AVAILABLE, "Email");								
					}
				}
			}
			if (StringUtils.isEmpty(editUser.getUsername())) {
				error++;
				fullErrMsg += String.format(
						StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Username");
			}else{
				if(addMode){
					if(uMUserSvc.cekDataByUsername(editUser.getUsername()) > 0){
						error++;
						fullErrMsg += String.format(
								StringConstants.MSG_WAS_AVAILABLE, "Username");								
					}									
				}
			}
			if (StringUtils.isEmpty(editUser.getFullname())) {
				error++;
				fullErrMsg += String.format(
						StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Nama Lengkap");
			}		
			if (StringUtils.isEmpty(editUser.getPassword())) {
				error++;
				fullErrMsg += String.format(
						StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Password");
			}else{
				if(editUser.getPassword().length() < 6){					
					error++;
					fullErrMsg += String.format(
							StringConstants.MSG_FORM_LENGHT_PASSWORD, "Password");
				}
			}
			
			
			if (error > 0) {
				Messagebox.show(fullErrMsg, "Message", 0, null);
			}else{
				
				
				
				//save user
<<<<<<< HEAD
				editUser.setPhotoText("http://mobile.berkahmm.com:8080/bmm-asset/user/download.gif");
=======
				editUser.setPhotoText("http://iglo-api.rejosomanisindo.com:8080/rmi-sptj-api-asset/user/download.gif");
>>>>>>> branch 'master' of https://gitlab.com/ari.haryanto/rmi-spt-sj.git
				uMUserSvc.save(editUser);
				
				//save userRole
				uMUserSvc.saveUserRole(userRoleDto);
				
				editMode = false;
				passUser = null;
				
				if(user.isAdministrator() == true){
					//untuk login admin			
					Map<String, Object> map = uMUserSvc.findAll(searchData, pageNo, perPage ,"is_active desc,modified_date", false); 
					userList = (List<MUserDto>) map.get("content");
					recordCount = (Integer) map.get("totalSize");
					maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
					System.out.println("Login Admin");
				}else{
					//untuk login transportir
					Map<String, Object> map = uMUserSvc.findByTransporterId(user.getReferenceCode(), searchData, pageNo, "is_active desc, modified_date", false);
					userList = (List<MUserDto>) map.get("content");
					recordCount = (Integer) map.get("totalSize");
					maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
					System.out.println("Login Bukan Admin");
				}
				detailWindow = false;
				showSuccessMsgBox("E000");
				
			}					
		}catch(Exception e){
			e.printStackTrace();
			showErrorMsgBox("E001");
		}
	}
			
	@Command
	@NotifyChange({"windowCustomer","windowDriver","windowTransportir",
		"editUser","searchData","listDriver"})
	public void showPopUp(){
		searchData = null;
		
		if(editUser.getRoleName().equalsIgnoreCase("supir")){
			if(user.isAdministrator()==false){
				editUser.setTransportirName(user.getFullname());
				listDriver=uMUserSvc.getListDriverByTransportir(user.getFullname(), null);
				BindUtils.postNotifyChange(null, null, this, "listDriver");
				BindUtils.postNotifyChange(null, null, this, "editUser");
			}else{
				listTransportir = uMUserSvc.getListTransportir(searchData);
			}
			
			driverMode = true;
			editUser.setModuleApps("Mobile");
		}else if(editUser.getRoleName().equalsIgnoreCase("transportir")){
			listTransportir = uMUserSvc.getListTransportir(searchData);
			transportirMode = true;
			editUser.setModuleApps("Web");
		}else if(editUser.getRoleName().equalsIgnoreCase("pelanggan")){
			listCustomer = uMUserSvc.getListCustomer(searchData);
			customerMode = true;
			editUser.setModuleApps("Web");
		}else{
			adminMode = true;
			editUser.setModuleApps("Web");
		}

		
		if(customerMode){
			windowCustomer = true;
		}else if(driverMode){
			windowDriver = true;
			listDriver = new ArrayList<>();
			if(user.isAdministrator() == false){
				//findOne transportir
				MobTransportirDto dto = uMUserSvc.findOneTransportir(user.getReferenceCode());
				editUser.setTransportirCode(dto.getCode());
				editUser.setTransportirName(dto.getTransportirName());
				
				//get listDriver by Transportir
				listDriver = uMUserSvc.getListDriverByTransportir(dto.getCode(), searchData);
			}
		}else if(transportirMode){
			windowTransportir = true;
		}else{
			windowCustomer = false;
			windowDriver = false;
			windowTransportir = false;
		}
	}
	
	@Command
	@NotifyChange({"editUser","listTransportir","driverMode","transportirMode",
		"customerMode","adminMode","searchData","listCustomer","flagProfil"})
	public void selectRole(@BindingParam("item") MRoleDto dto){
		
		searchData = null;
		
		editUser.setTransportirCode(null);
		editUser.setTransportirName(null);
		editUser.setRoleId(dto.getId());
		editUser.setRoleName(dto.getRoleName());
		
		if(dto.getId() == 2){
			listTransportir = uMUserSvc.getListTransportir(searchData);
			driverMode = true;
			flagProfil = true;
			editUser.setModuleApps("Mobile");
		}else if(dto.getId() == 8){
			listTransportir = uMUserSvc.getListTransportir(searchData);
			transportirMode = true;
			flagProfil = true;
			editUser.setModuleApps("Web");
		}else if(dto.getId() == 12){
			listCustomer = uMUserSvc.getListCustomer(searchData);
			customerMode = true;
			flagProfil = true;
			editUser.setModuleApps("Web");			
		}else{
			flagProfil = false;
			adminMode = true;
			editUser.setModuleApps("Web");
		}
		
		editUser.setPersonId(null);
		editUser.setFullname(null);
		editUser.setEmail(null);
		editUser.setReferenceCode(null);		
	}
	
	@Command
	@NotifyChange({"editUser","windowTransportir"})	
	public void selectTransportir(@BindingParam("item") MobTransportirDto dto){
		editUser.setPersonId(0);
		editUser.setFullname(dto.getTransportirName());
		editUser.setReferenceCode(dto.getCode());
		
		//close window
		transportirMode = false;
		windowTransportir = false;
	}
	
	@Command
	@NotifyChange({"editUser","driverMode","windowDriver"})
	public void selectDriver(@BindingParam("item") MobDriverDto dto){
		editUser.setPersonId(0);
		editUser.setEmail(dto.getEmail());
		editUser.setFullname(dto.getFullname());		
		editUser.setUsername(dto.getPhoneNum().replace(" ", ""));
		editUser.setPhoneNum(dto.getPhoneNum().replace(" ", ""));
		editUser.setReferenceCode(dto.getId().toString());
		
//		if(user.isAdministrator() == true){
//			//untuk login admin			
//			editUser.setReferenceCode(dto.getId().toString());
//		}else{
//			//untuk login transportir
//			editUser.setReferenceCode(user.getReferenceCode());
//		}
		
		//close window
		driverMode = false;
		windowDriver = false;
	}
		
	@Command
	@NotifyChange("editUser")	
	public void selectModule(){
		editUser.setModuleApps(selectedModule);
	}
	
	@Command
	@NotifyChange({"editUser","customerMode","windowCustomer"})
	public void selectCustomer(@BindingParam("item") MobCustomerDto dto){
		editUser.setPersonId(0);
		editUser.setFullname(dto.getCustName());
		editUser.setReferenceCode(dto.getCustCode());
		
		//close window
		customerMode = false;
		windowCustomer = false;
	}
	
	@Command
	@NotifyChange({"editUser","listDriver","searchData"})
	public void selectTransportirToDriver(@BindingParam("item") MobTransportirDto dto){
		searchData = null;
		editUser.setTransportirCode(dto.getCode());
		editUser.setTransportirName(dto.getTransportirName());
		
		if(dto != null){
			listDriver = uMUserSvc.getListDriverByTransportir(dto.getCode().trim(), searchData);
			BindUtils.postNotifyChange(null, null, this, "listDriver");
		}
	}
		
	@SuppressWarnings("unchecked")
	@Command("sort")
	@NotifyChange({"userList"})
	public void sort(@BindingParam("col") String column) {
		if (activeCheck()) {
			if (sortBy.equals(column)) {
				sortAsc = !sortAsc;
			} else {
				sortBy = column;
				sortAsc = true;
			}
			if(user.isAdministrator() == true){
				Map<String, Object> map = uMUserSvc.findAll(searchData, pageNo, perPage ,"is_active desc,modified_date", false); 
				userList = (List<MUserDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
			}else{
				Map<String, Object> map = uMUserSvc.findByTransporterId(user.getReferenceCode(), searchData, pageNo, "is_active desc, modified_date", false);
				userList = (List<MUserDto>) map.get("content");
			}
		}
	}
	
	public MUserDto getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(MUserDto selectedUser) {
		this.selectedUser = selectedUser;
	}

	public boolean isFlagIsActive() {
		return flagIsActive;
	}

	public void setFlagIsActive(boolean flagIsActive) {
		this.flagIsActive = flagIsActive;
	}

	public String getPassUser() {
		return passUser;
	}

	public void setPassUser(String passUser) {
		this.passUser = passUser;
	}

	public String getRefresh() {
		return refresh;
	}

	public void setRefresh(String refresh) {
		this.refresh = refresh;
	}

	public String getPrevPass() {
		return prevPass;
	}

	public void setPrevPass(String prevPass) {
		this.prevPass = prevPass;
	}

	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnDel() {
		return btnDel;
	}

	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isBtnDl() {
		return btnDl;
	}

	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}

	public MUserDto getUser() {
		return user;
	}

	public void setUser(MUserDto user) {
		this.user = user;
	}

	public MUserDto getEditUser() {
		return editUser;
	}

	public void setEditUser(MUserDto editUser) {
		this.editUser = editUser;
	}

	public MRole getRole() {
		return role;
	}

	public void setRole(MRole role) {
		this.role = role;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public List<String> getModuleList() {
		return moduleList;
	}

	public void setModuleList(List<String> moduleList) {
		this.moduleList = moduleList;
	}

	public List<MUserDto> getUserList() {
		return userList;
	}

	public void setUserList(List<MUserDto> userList) {
		this.userList = userList;
	}

	public int getEditIndex() {
		return editIndex;
	}

	public void setEditIndex(int editIndex) {
		this.editIndex = editIndex;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

	public boolean isSuperAdmin() {
		return superAdmin;
	}

	public void setSuperAdmin(boolean superAdmin) {
		this.superAdmin = superAdmin;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public String getArg() {
		return arg;
	}

	public void setArg(String arg) {
		this.arg = arg;
	}

	public String getsArg() {
		return sArg;
	}

	public void setsArg(String sArg) {
		this.sArg = sArg;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public boolean isSortAsc() {
		return sortAsc;
	}

	public void setSortAsc(boolean sortAsc) {
		this.sortAsc = sortAsc;
	}

	public boolean isFlagImage() {
		return flagImage;
	}

	public void setFlagImage(boolean flagImage) {
		this.flagImage = flagImage;
	}

	public boolean isFlagBtnUploadDef() {
		return flagBtnUploadDef;
	}

	public void setFlagBtnUploadDef(boolean flagBtnUploadDef) {
		this.flagBtnUploadDef = flagBtnUploadDef;
	}

	public List<MRoleDto> getListRole() {
		return listRole;
	}

	public void setListRole(List<MRoleDto> listRole) {
		this.listRole = listRole;
	}

	public List<String> getListModule() {
		return listModule;
	}

	public void setListModule(List<String> listModule) {
		this.listModule = listModule;
	}

	public String getSelectedModule() {
		return selectedModule;
	}

	public void setSelectedModule(String selectedModule) {
		this.selectedModule = selectedModule;
	}

	public List<MobDriverDto> getListDriver() {
		return listDriver;
	}

	public void setListDriver(List<MobDriverDto> listDriver) {
		this.listDriver = listDriver;
	}

	public MobDriverDto getSelectedDriver() {
		return selectedDriver;
	}

	public void setSelectedDriver(MobDriverDto selectedDriver) {
		this.selectedDriver = selectedDriver;
	}

	public List<MobTransportirDto> getListTransportir() {
		return listTransportir;
	}

	public void setListTransportir(List<MobTransportirDto> listTransportir) {
		this.listTransportir = listTransportir;
	}

	public boolean isAdminMode() {
		return adminMode;
	}

	public void setAdminMode(boolean adminMode) {
		this.adminMode = adminMode;
	}

	public boolean isDriverMode() {
		return driverMode;
	}

	public void setDriverMode(boolean driverMode) {
		this.driverMode = driverMode;
	}

	public boolean isTransportirMode() {
		return transportirMode;
	}

	public void setTransportirMode(boolean transportirMode) {
		this.transportirMode = transportirMode;
	}

	public boolean isCustomerMode() {
		return customerMode;
	}

	public void setCustomerMode(boolean customerMode) {
		this.customerMode = customerMode;
	}

	public List<MobCustomerDto> getListCustomer() {
		return listCustomer;
	}

	public void setListCustomer(List<MobCustomerDto> listCustomer) {
		this.listCustomer = listCustomer;
	}

	public boolean isWindowDriver() {
		return windowDriver;
	}

	public void setWindowDriver(boolean windowDriver) {
		this.windowDriver = windowDriver;
	}

	public boolean isWindowTransportir() {
		return windowTransportir;
	}

	public void setWindowTransportir(boolean windowTransportir) {
		this.windowTransportir = windowTransportir;
	}

	public boolean isWindowCustomer() {
		return windowCustomer;
	}

	public void setWindowCustomer(boolean windowCustomer) {
		this.windowCustomer = windowCustomer;
	}

	public Boolean getAddMode() {
		return addMode;
	}

	public void setAddMode(Boolean addMode) {
		this.addMode = addMode;
	}

	public Boolean getDetailWindow() {
		return detailWindow;
	}

	public void setDetailWindow(Boolean detailWindow) {
		this.detailWindow = detailWindow;
	}

	public String getSearchDataDriver() {
		return searchDataDriver;
	}

	public void setSearchDataDriver(String searchDataDriver) {
		this.searchDataDriver = searchDataDriver;
	}

	public boolean isFlagProfil() {
		return flagProfil;
	}

	public void setFlagProfil(boolean flagProfil) {
		this.flagProfil = flagProfil;
	}


}
