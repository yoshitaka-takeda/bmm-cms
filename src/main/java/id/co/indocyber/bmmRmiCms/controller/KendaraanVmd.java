package id.co.indocyber.bmmRmiCms.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MVehicleTypeDto;
import id.co.indocyber.bmmRmiCms.dto.MobVehicleDto;
import id.co.indocyber.bmmRmiCms.entity.MVehicleType;
import id.co.indocyber.bmmRmiCms.service.MDKendaraanSvc;
import id.co.indocyber.bmmRmiCms.tools.StringConstants;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class KendaraanVmd extends BaseVmd {
	
	@WireVariable
	private MDKendaraanSvc mDKendaraanSvc;
	
	private MUserDto user;
	
	private List<MobVehicleDto> listKendaraan = new ArrayList<>();
	private MobVehicleDto mobVehicleDto, selectedVehicleDto;
	
	private List<MVehicleTypeDto> listJenisKendaraan = new ArrayList<>();
	private MVehicleTypeDto selectedJenisKendaraan;
	
	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean superAdmin;
	private Integer recordCount = 1;
	
	private String sortBy = "";
	private boolean sortAsc = false;
	private boolean search = false;

	
	private boolean detailWindow;
	private boolean addMode;
	private boolean editMode;
	
	@Init
	@NotifyChange({"editMode","user","listKendaraan","recordCount","maxPage","listJenisKendaraan"})
	public void load(){
		try{
			editMode = Boolean.valueOf((Boolean) Sessions.getCurrent().getAttribute("editMode"));
			user = (MUserDto) Sessions.getCurrent().getAttribute("user");
			
			listKendaraan = mDKendaraanSvc.findAll(searchData, pageNo, perPage ,"isActive desc, modified_date", false);
			recordCount = mDKendaraanSvc.countAll(searchData);
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
			
			//LoadList Other
			listJenisKendaraan = mDKendaraanSvc.listVehicletype();
			
			for (MMenuRoleDto x : user.getRolePrivs()) {
				if ("MD-4".equals(x.getMenuCode())) {
					btnAdd = !x.getIsAdd();
					btnDel = !x.getIsDelete();
					btnDl = !x.getIsDownload();
					btnEdit = !x.getIsEdit();
					break;
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Command("switchPage")
	@NotifyChange({ "pageNo", "listKendaraan" })
	public void switchPage(@BindingParam("action") String action) {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}
			listKendaraan = mDKendaraanSvc.findAll(searchData, pageNo, perPage ,StringUtils.isEmpty(sortBy) ? "isActive desc, modified_date" : sortBy, sortAsc);
		}
	}
	
	@Command("src")
	@NotifyChange({"listKendaraan", "pageNo", "maxPage", "recordCount"})
	public void search() {
		pageNo = 1;
		listKendaraan = mDKendaraanSvc.findAll(searchData, pageNo, perPage  ,"isActive desc, modified_date", sortAsc);
		recordCount = mDKendaraanSvc.countAll(searchData);
		maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
	}
	
	@Command("reload")
	@NotifyChange({"recordCount","listKendaraan","maxPage","searchData"})
	public void reload(){
		searchData = "";
		recordCount = mDKendaraanSvc.countAll(searchData);
		listKendaraan = mDKendaraanSvc.findAll(searchData, pageNo, perPage ,"isActive desc, modified_date", sortAsc);			
		maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
	}
	
	@Command
	@NotifyChange({"selectedVehicleDto","flagIconWarning"})
	public void ubahStatusAktif(@BindingParam("dto") MobVehicleDto dto){
		showConfirm("E002");
		setFlagIconWarning(true);
		selectedVehicleDto = new MobVehicleDto();
		selectedVehicleDto = dto;
		selectedVehicleDto.setIsActive(false);
		selectedVehicleDto.setModified(user.getFullname());
		selectedVehicleDto.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@Command
	@NotifyChange({"selectedVehicleDto","flagIconWarning"})
	public void ubahStatusNonAktif(@BindingParam("dto") MobVehicleDto dto){
		showConfirm("E003");
		setFlagIconWarning(true);
		selectedVehicleDto = new MobVehicleDto();
		selectedVehicleDto = dto;
		selectedVehicleDto.setIsActive(true);
		selectedVehicleDto.setModified(user.getFullname());
		selectedVehicleDto.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@Command
	@NotifyChange({"recordCount","listKendaraan","maxPage","detailWindow","addMode","editMode"})
	public void yesConfirm(){
		int result = mDKendaraanSvc.save(selectedVehicleDto);
		if(result == 1){
			flagConfirmMsg = false;
			showSuccessMsgBox("E000");
		}else{
			showErrorMsgBox("E001");
		}
		
		//reload list
		recordCount = mDKendaraanSvc.countAll(searchData);
		listKendaraan = mDKendaraanSvc.findAll(searchData, pageNo, perPage ,"isActive desc, modified_date", sortAsc);
		maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
		BindUtils.postNotifyChange(null, null, this, "flagConfirmMsg");
	}
	
	
	@Command
	@NotifyChange({"detailWindow","addMode","editMode","mobVehicleDto"})
	public void add(){
		
		mobVehicleDto = new MobVehicleDto();
		
		setDetailWindow(true);
		setAddMode(true);
		setEditMode(false);
	}
	
	@Command
	@NotifyChange({"detailWindow","addMode","editMode","mobVehicleDto","selectedJenisKendaraan"})	
	public void edit(@BindingParam("dto") MobVehicleDto dto){
		mobVehicleDto = dto;
		
		for(MVehicleTypeDto o : listJenisKendaraan){
			if(mobVehicleDto.getVehicleType()==o.getId()){
				selectedJenisKendaraan = new MVehicleTypeDto();
				selectedJenisKendaraan = o;
			}
		}
		
		setDetailWindow(true);
		setAddMode(false);
		setEditMode(true);		
	}
	
	@Command
	@NotifyChange({"detailWindow","addMode","editMode"})
	public void close(){
		setDetailWindow(false);
		setAddMode(false);
		setEditMode(false);
	}
	
	@Command
	@NotifyChange({"detailWindow","addMode","editMode",
		"listKendaraan","recordCount","maxPage"})	
	public void save(){
		try{
			MobVehicleDto dto = new MobVehicleDto();
			if(isAddMode()){
				if(mDKendaraanSvc.cekId(mobVehicleDto.getVehicleNo())==true){
					showWarningMsgBox("E007");
				}else{
					dto.setCapacity(mobVehicleDto.getCapacity()==null?null:mobVehicleDto.getCapacity());
					dto.setCreated(user.getUsername());
					dto.setCreatedDate(new Timestamp(System.currentTimeMillis()));
					dto.setVehicleType(selectedJenisKendaraan.getId()==0?null:selectedJenisKendaraan.getId());
					dto.setIsActive(true);
					dto.setModified(user.getUsername());
					dto.setModifiedDate(new Timestamp(System.currentTimeMillis()));
					dto.setProductionYear(mobVehicleDto.getProductionYear()==null?null:mobVehicleDto.getProductionYear());
					dto.setVehicleNo(mobVehicleDto.getVehicleNo()==null?null:mobVehicleDto.getVehicleNo());

					if(mDKendaraanSvc.save(dto)==1){
						showSuccessMsgBox("E000");
						close();
						//reload list
						recordCount = mDKendaraanSvc.countAll(searchData);
						listKendaraan = mDKendaraanSvc.findAll(searchData, pageNo, perPage ,"isActive desc, modified_date", sortAsc);
						maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
					}else{
						showErrorMsgBox("E001");
					}
				}
			}else{
				dto.setCapacity(mobVehicleDto.getCapacity()==null?mobVehicleDto.getCapacity():mobVehicleDto.getCapacity());
				dto.setCreated(mobVehicleDto.getCreated()==null?mobVehicleDto.getCreated():mobVehicleDto.getCreated());
				dto.setCreatedDate(mobVehicleDto.getCreatedDate()==null?mobVehicleDto.getCreatedDate():mobVehicleDto.getCreatedDate());
				dto.setVehicleType(selectedJenisKendaraan.getId()==0?mobVehicleDto.getVehicleType():selectedJenisKendaraan.getId());
				dto.setIsActive(mobVehicleDto.getIsActive());
				dto.setModified(user.getUsername());
				dto.setModifiedDate(new Timestamp(System.currentTimeMillis()));
				dto.setProductionYear(mobVehicleDto.getProductionYear()==null?mobVehicleDto.getProductionYear():mobVehicleDto.getProductionYear());
				dto.setVehicleNo(mobVehicleDto.getVehicleNo()==null?mobVehicleDto.getVehicleNo():mobVehicleDto.getVehicleNo());

				if(mDKendaraanSvc.save(dto)==1){
					showSuccessMsgBox("E000");
					close();
					//reload list
					recordCount = mDKendaraanSvc.countAll(searchData);
					listKendaraan = mDKendaraanSvc.findAll(searchData, pageNo, perPage ,"isActive desc, modified_date", sortAsc);
					maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
				}else{
					showErrorMsgBox("E001");
				}
			}

//			if(mobVehicleDto.getCreated() == null){
//				mobVehicleDto.setCreated(user.getFullname());
//			}
//			if(mobVehicleDto.getCreatedDate() == null){
//				mobVehicleDto.setCreatedDate(new Timestamp(System.currentTimeMillis()));
//			}
//			
//			mobVehicleDto.setModified(user.getFullname());
//			mobVehicleDto.setModifiedDate(new Timestamp(System.currentTimeMillis()));
//			
//			String fullErrMsg = StringConstants.MSG_FORM_TEMPLATE;
//			int error = 0;
//			if (StringUtils.isEmpty(mobVehicleDto.getVehicleNo())) {
//				error++;
//				fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Nomor Polisi");
//			}
//			else if(addMode && mDKendaraanSvc.cekId(mobVehicleDto.getVehicleNo())){
//				error++;
//				fullErrMsg += String.format(StringConstants.MSG_FORM_ID_EXISTS, "Nomor Polisi");
//			}
//			if (StringUtils.isEmpty(mobVehicleDto.getVehicleType())) {
//				error++;
//				fullErrMsg += String.format(
//						StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Jenis Kendaraan");
//			}
//			if (StringUtils.isEmpty(mobVehicleDto.getCapacity())) {
//				error++;
//				fullErrMsg += String.format(
//						StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Kapasitas Kendaraan");
//			}
//			else if(mobVehicleDto.getCapacity().compareTo(BigDecimal.ZERO)<0){
//				error++;
//				fullErrMsg += String.format(
//						StringConstants.MSG_FORM_NUMERIC_NEGATIVE, "Kapasitas Kendaraan");
//			}
//			if (StringUtils.isEmpty(mobVehicleDto.getProductionYear())) {
//				error++;
//				fullErrMsg += String.format(
//						StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Tahun Produksi Kendaraan");
//			}
//
//			if (error > 0) {
//				Messagebox.show(fullErrMsg, "Message", 0, null);
//			} else{
//				if( mDKendaraanSvc.save(mobVehicleDto) == 1){
//					showSuccessMsgBox("E000");
//				}else{
//					showErrorMsgBox("E001");
//				}
//
//				setDetailWindow(false);
//				setAddMode(false);
//				setEditMode(false);
//			}
			
		}catch(Exception e){
			e.printStackTrace();
			showErrorMsgBox("E001");
		}
	}
	
	@Command("sort")
	@NotifyChange({"listKendaraan"})
	public void sort(@BindingParam("col") String column) {
		if (activeCheck()) {
			if (sortBy.equals(column)) {
				sortAsc = !sortAsc;
			} else {
				sortBy = column;
				sortAsc = true;
			}
			listKendaraan = mDKendaraanSvc.findAll(searchData, pageNo, perPage ,sortBy, sortAsc);
		}
	}

	@Command
	@NotifyChange("mobVehicleDto")
	public void selectJenisKendaraan(){
		mobVehicleDto.setVehicleType(selectedJenisKendaraan.getId());
		mobVehicleDto.setVehicleTypeText(selectedJenisKendaraan.getVehicleTypeName());
	}
	
	public boolean isDetailWindow() {
		return detailWindow;
	}

	public void setDetailWindow(boolean detailWindow) {
		this.detailWindow = detailWindow;
	}

	public boolean isAddMode() {
		return addMode;
	}

	public void setAddMode(boolean addMode) {
		this.addMode = addMode;
	}

	public boolean isEditMode() {
		return editMode;
	}

	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}

	public List<MobVehicleDto> getListKendaraan() {
		return listKendaraan;
	}

	public void setListKendaraan(List<MobVehicleDto> listKendaraan) {
		this.listKendaraan = listKendaraan;
	}

	public MobVehicleDto getMobVehicleDto() {
		return mobVehicleDto;
	}

	public void setMobVehicleDto(MobVehicleDto mobVehicleDto) {
		this.mobVehicleDto = mobVehicleDto;
	}

	public MobVehicleDto getSelectedVehicleDto() {
		return selectedVehicleDto;
	}

	public void setSelectedVehicleDto(MobVehicleDto selectedVehicleDto) {
		this.selectedVehicleDto = selectedVehicleDto;
	}

	public List<MVehicleTypeDto> getListJenisKendaraan() {
		return listJenisKendaraan;
	}

	public void setListJenisKendaraan(List<MVehicleTypeDto> listJenisKendaraan) {
		this.listJenisKendaraan = listJenisKendaraan;
	}

	public MVehicleTypeDto getSelectedJenisKendaraan() {
		return selectedJenisKendaraan;
	}

	public void setSelectedJenisKendaraan(MVehicleTypeDto selectedJenisKendaraan) {
		this.selectedJenisKendaraan = selectedJenisKendaraan;
	}

	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnDel() {
		return btnDel;
	}

	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isBtnDl() {
		return btnDl;
	}

	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public boolean isSuperAdmin() {
		return superAdmin;
	}

	public void setSuperAdmin(boolean superAdmin) {
		this.superAdmin = superAdmin;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public boolean isSortAsc() {
		return sortAsc;
	}

	public void setSortAsc(boolean sortAsc) {
		this.sortAsc = sortAsc;
	}

	public MUserDto getUser() {
		return user;
	}

	public void setUser(MUserDto user) {
		this.user = user;
	}

	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

}
