package id.co.indocyber.bmmRmiCms.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.util.StringUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MobDriverDto;
import id.co.indocyber.bmmRmiCms.dto.MobMappingTransportirDriverDto;
import id.co.indocyber.bmmRmiCms.dto.MobTransportirDto;
import id.co.indocyber.bmmRmiCms.service.MappingTransporterSupirSvc;
import id.co.indocyber.bmmRmiCms.tools.StringConstants;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class MappingTransporterSupirVmd extends BaseVmd {
	
	@WireVariable
	private MappingTransporterSupirSvc mappingTransporterSupirSvc;
	
	private MUserDto user;
	
	private List<MobMappingTransportirDriverDto> listIndex = new ArrayList<>();
	private MobMappingTransportirDriverDto editDto, selectedDto;
	
	private List<MobTransportirDto> listTransportir = new ArrayList<>();
	
	private List<MobDriverDto> listDriver = new ArrayList<>();
	private List<MobDriverDto> listTempDriver = new ArrayList<>();
	
	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean superAdmin;
	private Integer recordCount = 1;
	
	private boolean detailWindow;
	private boolean detailWindow2;
	private boolean addMode;
	private boolean editMode;
	private boolean flagDelete;
	
	private String sortBy = "";
	private boolean sortAsc = false;
	private String sortDirection = "";
	
	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public boolean isSortAsc() {
		return sortAsc;
	}

	public void setSortAsc(boolean sortAsc) {
		this.sortAsc = sortAsc;
	}

	public String getSortDirection() {
		return sortDirection;
	}

	public void setSortDirection(String sortDirection) {
		this.sortDirection = sortDirection;
	}

	@Init
	@NotifyChange({"editMode","user","listIndex","recordCount","maxPage"})
	public void load(){
		try{
			editMode = Boolean.valueOf((Boolean) Sessions.getCurrent().getAttribute("editMode"));
			user = (MUserDto) Sessions.getCurrent().getAttribute("user");
			
			if(user.isAdministrator() == true){
				Map<String, Object> map = mappingTransporterSupirSvc.findByArg(searchData, pageNo, perPage,"is_active desc, modified_date", false);
				listIndex = (List<MobMappingTransportirDriverDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);				
			}else{
//				listIndex = mappingTransporterSupirSvc.findByTransportir(user.getReferenceCode(), searchData, pageNo, perPage , "modifiedDate", "desc");
//				recordCount = mappingTransporterSupirSvc.countByTransportir(user.getReferenceCode(),searchData);
//				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
			}
						
			for (MMenuRoleDto x : user.getRolePrivs()) {
				if ("MD-3".equals(x.getMenuCode())) {
					btnAdd = !x.getIsAdd();
					btnDel = !x.getIsDelete();
					btnDl = !x.getIsDownload();
					btnEdit = !x.getIsEdit();
					break;
				}
			}

		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Command("switchPage")
	@NotifyChange({ "pageNo", "listIndex", "resetUser" })
	public void switchPage(@BindingParam("action") String action) {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}

			if(user.isAdministrator() == true){
				Map<String, Object> map = mappingTransporterSupirSvc.findByArg(searchData, pageNo, perPage,StringUtils.isEmpty(sortBy)?"is_active desc, modified_date":sortBy, sortAsc);
				listIndex = (List<MobMappingTransportirDriverDto>) map.get("content");
			}else{
				listIndex = mappingTransporterSupirSvc.findByTransportir(user.getReferenceCode(), searchData, pageNo, perPage , "modifiedDate", "desc");
			}
			
		}
	}
	
	@Command("src")
	@NotifyChange({"listIndex", "pageNo", "maxPage", "recordCount"})
	public void search() {
		pageNo = 1;
		if(user.isAdministrator() == true){
			Map<String, Object> map = mappingTransporterSupirSvc.findByArg(searchData, pageNo, perPage,StringUtils.isEmpty(sortBy)?"is_active desc, modified_date":sortBy, sortAsc);
			listIndex = (List<MobMappingTransportirDriverDto>) map.get("content");
			recordCount = (Integer) map.get("totalSize");
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);				
		}else{
			listIndex = mappingTransporterSupirSvc.findByTransportir(user.getReferenceCode(), searchData, pageNo, perPage , "modifiedDate", "desc");
			recordCount = mappingTransporterSupirSvc.countByTransportir(user.getReferenceCode(),searchData);
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
		}
	}
	
	@Command
	public void searchDriver(){
		listDriver = mappingTransporterSupirSvc.getListDriver(searchData);
		BindUtils.postNotifyChange(null, null, this, "listDriver");
	}
	
	@Command
	public void searchTransportir(){
		listTransportir = mappingTransporterSupirSvc.getListTransporter(editDto.getTransportirName());
		BindUtils.postNotifyChange(null, null, this, "listTransportir");
	}
	
	
	@Command("reload")
	@NotifyChange({"recordCount","listIndex","maxPage","searchData"})
	public void reload(){
		searchData = "";
		if(user.isAdministrator() == true){
			Map<String, Object> map = mappingTransporterSupirSvc.findByArg(searchData, pageNo, perPage,"is_active desc, modified_date", false);
			listIndex = (List<MobMappingTransportirDriverDto>) map.get("content");
			recordCount = (Integer) map.get("totalSize");
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);				
		}else{
			listIndex = mappingTransporterSupirSvc.findByTransportir(user.getReferenceCode(), searchData, pageNo, perPage , "modifiedDate", "desc");
			recordCount = mappingTransporterSupirSvc.countByTransportir(user.getReferenceCode(),searchData);
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
		}
	}
	
	@Command
	@NotifyChange({"selectedDto","flagIconWarning"})
	public void ubahStatusAktif(@BindingParam("dto") MobMappingTransportirDriverDto dto){
		showConfirm("E002");
		setFlagIconWarning(true);
		selectedDto = new MobMappingTransportirDriverDto();
		selectedDto = dto;
		selectedDto.setIsActive(false);
		selectedDto.setModified(user.getFullname());
		selectedDto.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@Command
	@NotifyChange({"selectedDto","flagIconWarning"})
	public void ubahStatusNonAktif(@BindingParam("dto") MobMappingTransportirDriverDto dto){
		showConfirm("E003");
		setFlagIconWarning(true);
		selectedDto = new MobMappingTransportirDriverDto();
		selectedDto = dto;
		selectedDto.setIsActive(true);
		selectedDto.setModified(user.getFullname());
		selectedDto.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@Command
	@NotifyChange({"recordCount","listIndex","maxPage","flagDelete"})
	public void yesConfirm(){
		if(isFlagDelete()){
			try{
				int result = mappingTransporterSupirSvc.delete(selectedDto);
				if(result==1){
					flagConfirmMsg = false;
					showSuccessMsgBox("E004");				
					setFlagDelete(false);
				}else{
					showErrorMsgBox("E005");
				}
			}catch(Exception e){
				e.printStackTrace();
				showErrorMsgBox("E005");
			}
		}else{
			int result = mappingTransporterSupirSvc.save(selectedDto);
			if(result == 1){
				flagConfirmMsg = false;
				showSuccessMsgBox("E000");
			}else{
				showErrorMsgBox("E001");
			}			
		}
		
		//reload list
		if(user.isAdministrator() == true){
			Map<String, Object> map = mappingTransporterSupirSvc.findByArg(searchData, pageNo, perPage,StringUtils.isEmpty(sortBy)?"is_active desc, modified_date":sortBy, sortAsc);
			listIndex = (List<MobMappingTransportirDriverDto>) map.get("content");
			recordCount = (Integer) map.get("totalSize");
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);				
		}else{
			listIndex = mappingTransporterSupirSvc.findByTransportir(user.getReferenceCode(), searchData, pageNo, perPage , "modifiedDate", "desc");
			recordCount = mappingTransporterSupirSvc.countByTransportir(user.getReferenceCode(),searchData);
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
		}
		BindUtils.postNotifyChange(null, null, this, "flagConfirmMsg");
	}
	
	
	@Command
	@NotifyChange({"detailWindow","addMode","editMode","editDto","searchData","listTempDriver",
		"listDriver","listTransportir"})
	public void add(){
		
		searchData = null;
		listTempDriver.clear();
		editDto = new MobMappingTransportirDriverDto();
		
		//loadList for Combobox/BandBox
		listDriver = mappingTransporterSupirSvc.getListDriver(searchData);
		listTransportir = mappingTransporterSupirSvc.getListTransporter(searchData);
		
		setDetailWindow(true);
		setAddMode(true);
		setEditMode(false);
	}
	
	@Command
	@NotifyChange({"detailWindow","addMode","editMode","editDto","listTempDriver"})	
	public void edit(@BindingParam("item") MobMappingTransportirDriverDto dto){
		editDto = dto;
		
		listTempDriver = mappingTransporterSupirSvc.getListDriverByTransporter(dto.getTransportirCode());
		
		setDetailWindow(true);
		setAddMode(false);
		setEditMode(true);
	
	}

	@Command
	@NotifyChange({"detailWindow2","addMode","editMode","editDto"})	
	public void view(@BindingParam("item") MobMappingTransportirDriverDto dto){
		editDto = dto;

		setDetailWindow2(true);
		setAddMode(false);
		setEditMode(true);
	
	}	
	
	@Command
	@NotifyChange({"detailWindow2","addMode","editMode","detailWindow"})
	public void close(){
		searchData = null;
		setDetailWindow2(false);
		setDetailWindow(false);
		setAddMode(false);
		setEditMode(false);
	}
	
	@Command
	@NotifyChange({"listIndex","recordCount","maxPage",
		"addMode","editMode","detailWindow","detailWindow2"})
	public void selectDriver(@BindingParam("item") MobDriverDto dto){
		try{
			editDto.setTransportirCode(user.getReferenceCode());
			editDto.setDriverId(dto.getId());
			editDto.setIsActive(true);
			editDto.setCreated(user.getUsername());
			editDto.setCreatedDate(new Timestamp(System.currentTimeMillis()));
			editDto.setModified(user.getUsername());
			editDto.setModifiedDate(new Timestamp(System.currentTimeMillis()));
			if(mappingTransporterSupirSvc.save(editDto) == 1){
				showSuccessMsgBox("E000");
			}else{
				showErrorMsgBox("E001");
			}
			
			//Reload List Index
			if(user.isAdministrator() == true){
				Map<String, Object> map = mappingTransporterSupirSvc.findByArg(searchData, pageNo, perPage,StringUtils.isEmpty(sortBy)?"is_active desc, modified_date":sortBy, sortAsc);
				listIndex = (List<MobMappingTransportirDriverDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);				
			}else{
				listIndex = mappingTransporterSupirSvc.findByTransportir(user.getReferenceCode(), searchData, pageNo, perPage , "modifiedDate", "desc");
				recordCount = mappingTransporterSupirSvc.countByTransportir(user.getReferenceCode(),searchData);
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
			}
			
			addMode = false;
			editMode = false;
			detailWindow = false;
			detailWindow2 = false;
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Command
	@NotifyChange("editDto")
	public void selectTransportir(@BindingParam("item") MobTransportirDto dto){
		editDto.setTransportirCode(dto.getCode());
		editDto.setTransportirName(dto.getTransportirName());
		BindUtils.postNotifyChange(null, null, editDto, "transportirCode");
		BindUtils.postNotifyChange(null, null, editDto, "transportirName");
	}
	
	@Command
	public void save(){
		try{
			
			String fullErrMsg = StringConstants.MSG_FORM_TEMPLATE;
			int error = 0;
			if (StringUtils.isEmpty(editDto.getTransportirCode())) {
				error++;
				fullErrMsg += String.format(
						StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Transporter");
			}
			if (listTempDriver.size() == 0) {
				error++;
				fullErrMsg += String.format(
						StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Supir");
			}

			if (error > 0) {
				Messagebox.show(fullErrMsg, "Message", 0, null);
			}else{
				List<MobMappingTransportirDriverDto> listDto = new ArrayList<>();
				for(MobDriverDto d : listTempDriver){
					MobMappingTransportirDriverDto dto = new MobMappingTransportirDriverDto();
					dto.setTransportirCode(editDto.getTransportirCode());
					dto.setDriverId(d.getId());
					listDto.add(dto);
				}
				mappingTransporterSupirSvc.saveAll(listDto, user.getUsername());
				
				showSuccessMsgBox("E000");
				addMode = false;
				editMode = false;
				detailWindow = false;
				detailWindow2 = false;
				
				//reload list
				searchData = null;
				if(user.isAdministrator() == true){
					Map<String, Object> map = mappingTransporterSupirSvc.findByArg(searchData, pageNo, perPage,StringUtils.isEmpty(sortBy)?"is_active desc, modified_date":sortBy, sortAsc);
					listIndex = (List<MobMappingTransportirDriverDto>) map.get("content");
					recordCount = (Integer) map.get("totalSize");
					maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);				
				}else{
					listIndex = mappingTransporterSupirSvc.findByTransportir(user.getReferenceCode(), searchData, pageNo, perPage , "modifiedDate", "desc");
					recordCount = mappingTransporterSupirSvc.countByTransportir(user.getReferenceCode(),searchData);
					maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
				}
				
				BindUtils.postNotifyChange(null, null, this, "addMode");
				BindUtils.postNotifyChange(null, null, this, "editMode");
				BindUtils.postNotifyChange(null, null, this, "detailWindow");
				BindUtils.postNotifyChange(null, null, this, "detailWindow2");
				BindUtils.postNotifyChange(null, null, this, "listIndex");
				BindUtils.postNotifyChange(null, null, this, "recordCount");
				BindUtils.postNotifyChange(null, null, this, "maxPage");
				BindUtils.postNotifyChange(null, null, this, "searchData");
			}
			
		}catch(Exception e){
			e.printStackTrace();
			showErrorMsgBox("E001");
		}
	}
	
	@Command
	@NotifyChange({"flagIconWarning","selectedDto","flagDelete"})
	public void delete(@BindingParam("item")MobMappingTransportirDriverDto dto){
		showConfirm("E006");
		setFlagIconWarning(true);
		selectedDto = new MobMappingTransportirDriverDto();
		selectedDto = dto;
		setFlagDelete(true);
	}
	
	@Command("sort")
	@NotifyChange({"listIndex"})
	public void sort(@BindingParam("col") String column) {
		if (activeCheck()) {
			if (sortBy.equals(column)) {
				sortAsc = !sortAsc;
			} else {
				sortBy = column;
				sortAsc = true;
			}
			if(sortAsc==true){
				sortDirection = "asc";
			}else if(sortAsc ==false){
				sortDirection = "desc";
			}
			if(user.isAdministrator() == true){
				Map<String, Object> map = mappingTransporterSupirSvc.findByArg(searchData, pageNo, perPage,sortBy, sortAsc);
				listIndex = (List<MobMappingTransportirDriverDto>) map.get("content");
			}else{
				listIndex = mappingTransporterSupirSvc.findByTransportir(user.getReferenceCode(), searchData,pageNo, perPage , sortBy, sortDirection);
			}
		}
	}

	
	public MUserDto getUser() {
		return user;
	}

	public void setUser(MUserDto user) {
		this.user = user;
	}

	public List<MobMappingTransportirDriverDto> getListIndex() {
		return listIndex;
	}

	public void setListIndex(List<MobMappingTransportirDriverDto> listIndex) {
		this.listIndex = listIndex;
	}

	public MobMappingTransportirDriverDto getEditDto() {
		return editDto;
	}

	public void setEditDto(MobMappingTransportirDriverDto editDto) {
		this.editDto = editDto;
	}

	public MobMappingTransportirDriverDto getSelectedDto() {
		return selectedDto;
	}

	public void setSelectedDto(MobMappingTransportirDriverDto selectedDto) {
		this.selectedDto = selectedDto;
	}

	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnDel() {
		return btnDel;
	}

	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isBtnDl() {
		return btnDl;
	}

	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public boolean isSuperAdmin() {
		return superAdmin;
	}

	public void setSuperAdmin(boolean superAdmin) {
		this.superAdmin = superAdmin;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public boolean isDetailWindow() {
		return detailWindow;
	}

	public void setDetailWindow(boolean detailWindow) {
		this.detailWindow = detailWindow;
	}

	public boolean isDetailWindow2() {
		return detailWindow2;
	}

	public void setDetailWindow2(boolean detailWindow2) {
		this.detailWindow2 = detailWindow2;
	}

	public boolean isAddMode() {
		return addMode;
	}

	public void setAddMode(boolean addMode) {
		this.addMode = addMode;
	}

	public boolean isEditMode() {
		return editMode;
	}

	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}

	public List<MobDriverDto> getListDriver() {
		return listDriver;
	}

	public void setListDriver(List<MobDriverDto> listDriver) {
		this.listDriver = listDriver;
	}

	public List<MobDriverDto> getListTempDriver() {
		return listTempDriver;
	}

	public void setListTempDriver(List<MobDriverDto> listTempDriver) {
		this.listTempDriver = listTempDriver;
	}

	public List<MobTransportirDto> getListTransportir() {
		return listTransportir;
	}

	public void setListTransportir(List<MobTransportirDto> listTransportir) {
		this.listTransportir = listTransportir;
	}

	public boolean isFlagDelete() {
		return flagDelete;
	}

	public void setFlagDelete(boolean flagDelete) {
		this.flagDelete = flagDelete;
	}


}
