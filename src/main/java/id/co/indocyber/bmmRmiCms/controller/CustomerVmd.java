package id.co.indocyber.bmmRmiCms.controller;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MobCustomerDto;
import id.co.indocyber.bmmRmiCms.service.MDCustomerSvc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class CustomerVmd extends BaseVmd {
	
	@WireVariable
	private MDCustomerSvc mDCustomerSvc;
	
	private MUserDto user;
	
	private List<MobCustomerDto> listCustomer = new ArrayList<>();
	private MobCustomerDto selectedCustomer, mobCustomerDto;
	
	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;

	private boolean freezeId;
	private int editIndex = -1;
	private Boolean viewMode;
	private String pageTitle;

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private boolean superAdmin;
	private Integer recordCount = 1;
	
	private String sortBy = "";
	private boolean sortAsc = true;
	
	
	@Init
	@NotifyChange({"user","listCustomer","recordCount","maxPage"})	
	public void load(){
		try{
			user = (MUserDto) Sessions.getCurrent().getAttribute("user");

			listCustomer = mDCustomerSvc.findAll(searchData, pageNo, "custCode", true);
			recordCount = mDCustomerSvc.countAll(searchData);
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
			for (MMenuRoleDto x : user.getRolePrivs()) {
				if ("UM-5".equals(x.getMenuCode())) {
					btnAdd = !x.getIsAdd();
					btnDel = !x.getIsDelete();
					btnDl = !x.getIsDownload();
					btnEdit = !x.getIsEdit();
					break;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Command("switchPage")
	@NotifyChange({ "pageNo", "listCustomer" })
	public void switchPage(@BindingParam("action") String action) {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}
				listCustomer = mDCustomerSvc.findAll(searchData, pageNo,
						StringUtils.isEmpty(sortBy) ? "custCode" : sortBy, sortAsc);
		}
	}
	
	@Command("src")
	@NotifyChange({"listCustomer", "pageNo", "maxPage", "recordCount"})
	public void search() {
		search = true;
		pageNo = 1;
		listCustomer = mDCustomerSvc.findAll(searchData, pageNo,
				StringUtils.isEmpty(sortBy) ? "custCode" : sortBy, sortAsc);
		recordCount = mDCustomerSvc.countAll(searchData);
		maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
	}
	
	@Command
	public void back(){
		viewMode = false;
		BindUtils.postNotifyChange(null, null, this, "viewMode");
	}	
	
	@Command("reload")
	@NotifyChange({"recordCount","listCustomer","maxPage","searchData"})
	public void reload(){
		searchData="";
		listCustomer = mDCustomerSvc.findAll(searchData, pageNo,
				StringUtils.isEmpty(sortBy) ? "custCode" : sortBy, sortAsc);
		recordCount = mDCustomerSvc.countAll(searchData);
		maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
	}

		
	@Command
	@NotifyChange({"selectedCustomer","flagIconWarning"})
	public void ubahStatusAktif(@BindingParam("dto") MobCustomerDto dto){
		showConfirm("E002");
		setFlagIconWarning(true);
		selectedCustomer = new MobCustomerDto();
		selectedCustomer = dto;
		selectedCustomer.setIsActive(false);
	}
	
	@Command
	@NotifyChange({"selectedCustomer","flagIconWarning"})
	public void ubahStatusNonAktif(@BindingParam("dto") MobCustomerDto dto){
		showConfirm("E003");
		setFlagIconWarning(true);
		selectedCustomer = new MobCustomerDto();
		selectedCustomer = dto;
		selectedCustomer.setIsActive(true);
	}
	
	@Command
	@NotifyChange({"recordCount","listCustomer","maxPage"})
	public void yesConfirm(){
		int result = mDCustomerSvc.save(selectedCustomer);
		if(result == 1){
			flagConfirmMsg = false;
			showSuccessMsgBox("E000");
		}else{
			showErrorMsgBox("E001");
		}
		
		//reload list
		recordCount = mDCustomerSvc.countAll(searchData);
		listCustomer = mDCustomerSvc.findAll(searchData,pageNo, 
				StringUtils.isEmpty(sortBy) ? "custCode" : sortBy, sortAsc);
		maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
		BindUtils.postNotifyChange(null, null, this, "flagConfirmMsg");
	}
	
	@Command
	public void view(@BindingParam("dto") MobCustomerDto dto){
		mobCustomerDto = dto;
		viewMode = true;
		BindUtils.postNotifyChange(null, null, this, "viewMode");
		BindUtils.postNotifyChange(null, null, this, "mobCustomerDto");		
	}

	@Command("sort")
	@NotifyChange({"listCustomer"})
	public void sort(@BindingParam("col") String column) {
		if (activeCheck()) {
			if (sortBy.equals(column)) {
				sortAsc = !sortAsc;
			} else {
				sortBy = column;
				sortAsc = true;
			}
			if(user.isAdministrator() == true){
				listCustomer = mDCustomerSvc.findAll(searchData, pageNo, sortBy, sortAsc);
			}else{
				listCustomer = mDCustomerSvc.findAll(searchData, pageNo, sortBy, sortAsc);
			}
		}
	}

	
	public List<MobCustomerDto> getListCustomer() {
		return listCustomer;
	}
	public void setListCustomer(List<MobCustomerDto> listCustomer) {
		this.listCustomer = listCustomer;
	}
	public MobCustomerDto getSelectedCustomer() {
		return selectedCustomer;
	}
	public void setSelectedCustomer(MobCustomerDto selectedCustomer) {
		this.selectedCustomer = selectedCustomer;
	}
	public boolean isBtnAdd() {
		return btnAdd;
	}
	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}
	public boolean isBtnDel() {
		return btnDel;
	}
	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}
	public boolean isBtnEdit() {
		return btnEdit;
	}
	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}
	public boolean isBtnDl() {
		return btnDl;
	}
	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}
	public boolean isFreezeId() {
		return freezeId;
	}
	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}
	public int getEditIndex() {
		return editIndex;
	}
	public void setEditIndex(int editIndex) {
		this.editIndex = editIndex;
	}
	public String getPageTitle() {
		return pageTitle;
	}
	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}
	public Integer getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public Integer getMaxPage() {
		return maxPage;
	}
	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}
	public boolean isSearch() {
		return search;
	}
	public void setSearch(boolean search) {
		this.search = search;
	}
	public boolean isSuperAdmin() {
		return superAdmin;
	}
	public void setSuperAdmin(boolean superAdmin) {
		this.superAdmin = superAdmin;
	}
	public Integer getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public boolean isSortAsc() {
		return sortAsc;
	}
	public void setSortAsc(boolean sortAsc) {
		this.sortAsc = sortAsc;
	}
	public MUserDto getUser() {
		return user;
	}
	public void setUser(MUserDto user) {
		this.user = user;
	}

	public MobCustomerDto getMobCustomerDto() {
		return mobCustomerDto;
	}

	public void setMobCustomerDto(MobCustomerDto mobCustomerDto) {
		this.mobCustomerDto = mobCustomerDto;
	}

	public Boolean getViewMode() {
		return viewMode;
	}

	public void setViewMode(Boolean viewMode) {
		this.viewMode = viewMode;
	}
	
	
	

}
