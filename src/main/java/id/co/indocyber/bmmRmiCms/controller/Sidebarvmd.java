package id.co.indocyber.bmmRmiCms.controller;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.UserMenu;
import id.co.indocyber.bmmRmiCms.entity.MUserRole;
import id.co.indocyber.bmmRmiCms.entity.UserMenuAccordion;
import id.co.indocyber.bmmRmiCms.service.MMenuRoleSvc;
import id.co.indocyber.bmmRmiCms.service.MUserRoleSvc;
import id.co.indocyber.bmmRmiCms.service.MUserSvc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Default;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.DefaultTreeModel;
import org.zkoss.zul.DefaultTreeNode;
import org.zkoss.zul.Include;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tree;
import org.zkoss.zul.TreeNode;
import org.zkoss.zul.Messagebox.Button;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class Sidebarvmd extends SelectorComposer<Component> implements
		Serializable {
	private static final long serialVersionUID = 7010841483291616467L;
	@WireVariable
	private MUserRoleSvc mUserRoleSvc;
	@WireVariable
	private MMenuRoleSvc mMenuRoleSvc;
	@WireVariable
	private MUserSvc mUserSvc;

	private static final Logger logger = LoggerFactory
			.getLogger(Sidebarvmd.class);

	private static final String HOME_URI = "homeUri";
	private static final String WS_URI = "/auth";
	
	private List<UserMenu> listUserMenu = new ArrayList<>();

	private List<UserMenu> listUserMenuFavorite = new ArrayList<>();

	private ListModelList<UserMenuAccordion> listMenuAccordion = new ListModelList<>();
	private ListModelList<DefaultTreeModel<UserMenu>> listMenuTreeModel = new ListModelList<>();

	private Map<String, UserMenu> userMenuMap = new HashMap<>(); // FOR DEBUG
																	// PURPOSES

	private Map<String, UserMenu> authorizedUserMenu = new HashMap<>();

	@Wire("#tb")
	private Tabbox tb;

	@AfterCompose
	public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
	}

	@Init
	public void init() {
		collectUserMenu(true);
	}

	@Command
	public void toogleTree(@BindingParam("tree") Tree node) {
		if (node.getSelectedItem().isOpen()) {
			node.getSelectedItem().setOpen(false);
		} else {
			node.getSelectedItem().setOpen(true);
		}
	}

	/**
	 * Collect user menu.
	 */
	@GlobalCommand("collectUserMenu")
	@NotifyChange({ "listMenuAccordion", "listMenuTreeModel" })
	public void collectUserMenu(@Default("false") boolean firstTime) {
		try {
			Clients.showBusy(Labels.getLabel("processing"));

			listMenuAccordion.clear();
			listMenuTreeModel.clear();

			MUserDto user = (MUserDto) Sessions.getCurrent().getAttribute("user");
			//ur = mUserRoleSvc.cmsAccess(user.getEmail());
			List<MMenuRoleDto> listMenuRoles = user.getRolePrivs();

			for (MMenuRoleDto priv : listMenuRoles) {
				if (priv.getIsView()) {
					UserMenu u = new UserMenu();
					u.setMenuCode(priv.getMenuCode());
					u.setMenuParent(priv.getParentMenuCode());
					u.setMenuTitle(priv.getMenuName());
					u.setMenuUrl(priv.getMenuLink());
					listUserMenu.add(u);
				}
			}

			// map menu

			// declare new empty root
			DefaultTreeNode<UserMenu> root = new DefaultTreeNode<UserMenu>(
					null, (List) null);

			String tempRootAccord = "";

			int menuSize = listUserMenu.size();

			// parse every line menu tree from DB and construct it to zk tree
			// format
			for (int n = 0; n < menuSize; n++) {
				UserMenu currentUm = listUserMenu.get(n);

				// menu parent is null mean this node is used for accordion
				if (currentUm.getMenuParent() == null
						|| currentUm.getMenuParent().equals(currentUm.getMenuCode())) {
					listMenuAccordion.add(new UserMenuAccordion(currentUm.getMenuTitle()));

					// put accordion parent code in temporary var
					tempRootAccord = currentUm.getMenuCode();

					// for every new accordion, then declare new empty root
					root = new DefaultTreeNode<UserMenu>(new UserMenu(),
							new ArrayList<DefaultTreeNode<UserMenu>>());

					// add the root of tree into our tree model collection
					listMenuTreeModel.add(new DefaultTreeModel<>(root));
				} else { // otherwise, create a tree node
					// check next user menu
					UserMenu nextUm = null;

					// check next user menu if only is exist
					if (n + 1 < menuSize) {
						nextUm = listUserMenu.get(n + 1);
					}

					DefaultTreeNode<UserMenu> newNode;

					if (nextUm != null && nextUm.getMenuParent() != null) {
						// if next menu parent is current menu, then declare
						// current menu has child
						if (nextUm.getMenuParent().equals(currentUm.getMenuCode())) {
							newNode = new DefaultTreeNode<UserMenu>(new UserMenu(currentUm), (List) null);
						} else { // otherwise there will be no child for menu
							newNode = new DefaultTreeNode<UserMenu>(new UserMenu(currentUm));
						}
					} else { // last node in list, there will be no child for
								// menu
						newNode = new DefaultTreeNode<UserMenu>(new UserMenu(currentUm));
					}

					// if parent of node is accordion code, then it must be root
					// of tree
					//cek cekkk
					//if (currentUm.getMenuParent().equals(tempRootAccord)) {
						root.add(newNode);
						System.err.println("masuk if treee");
					//} else { // otherwise, then search true parent of node and
//								// added it as a child
//						DefaultTreeNode<UserMenu> parent = recurseSearch(root,
//								currentUm.getMenuParent());
//						parent.add(newNode);
//						System.err.println("masuk else treee");
//					}
 
					authorizedUserMenu.put(newNode.getData().getMenuUrl(),newNode.getData());

					// TODO :PRODUCTION MATTER: (DEVELOPMENT ONLY)
					if (System.getProperty("debug") != null) {
						userMenuMap.put(newNode.getData().getMenuCode(), newNode.getData());
					}
				}

			} // end for

			// remove accordion with empty tree
			Iterator<UserMenuAccordion> iterAcc = listMenuAccordion.listIterator();
			for (Iterator<DefaultTreeModel<UserMenu>> iter = listMenuTreeModel
					.listIterator(); iter.hasNext();) {

				DefaultTreeModel<UserMenu> ut = iter.next();
				iterAcc.next();

				if (ut.getChildCount(ut.getRoot()) <= 0) {
					iter.remove();
					iterAcc.remove();
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		} finally {
			Clients.clearBusy();
		}
	}

	@Command("navi")
	public void openMenu(@BindingParam("uri") final String locationUri,
			@BindingParam("title") final String title,
			@BindingParam("menuCode") final String menuCode) {
		//TODO: auto logout if account / role disabled (for navigation via sidebar)
		if(Sessions.getCurrent().getAttribute("editMode")==null)
			Sessions.getCurrent().setAttribute("editMode", false);
		
		if((Boolean)Sessions.getCurrent().getAttribute("editMode")){
			Messagebox.show("Data tidak akan disimpan. Apakah Anda yakin?", "Perhatian",
					new Button[] { Button.YES, Button.NO },
					Messagebox.QUESTION, Button.NO,
					new EventListener<Messagebox.ClickEvent>() {
						@Override
						public void onEvent(ClickEvent event) throws Exception {
							if (Messagebox.ON_YES.equals(event.getName())) {
								Executions.getCurrent().getDesktop().setBookmark(menuCode);
								navi2(locationUri);
							}
						}
					}
				);
		}
		else{
			Executions.getCurrent().getDesktop().setBookmark(menuCode);
			navi2(locationUri);
		}
	}
	
	public void navi2(String locationUri){
		Sessions.getCurrent().setAttribute("editMode", false);
		if (Sessions.getCurrent().getAttribute("user") != null) {
			MUserDto user1 = (MUserDto) Sessions.getCurrent().getAttribute("user");
			MUserRole x = mUserRoleSvc.cmsAccess(user1.getEmail());
			if(!mUserSvc.findByUsernameOrEmail(user1.getUsername()).getIsActive() ||
					!(x==null ? false : x.getIsActive())){
				Messagebox.show("Akun anda telah dinonaktifkan. Anda akan kembali ke halaman login.", 
						"Perhatian", new Button[] { Button.OK},
						Messagebox.ERROR, Button.OK,
						new EventListener<Messagebox.ClickEvent>() {
							@Override
							public void onEvent(ClickEvent event)
									throws Exception {
								// TODO Auto-generated method stub
								if (Messagebox.ON_OK.equals(event.getName())) {
									Sessions.getCurrent().removeAttribute("user");
									Sessions.getCurrent().removeAttribute("lastPage");
									Executions.getCurrent().sendRedirect("/login.zul");
								}
							}}
				);
			}
			else{
				Include include = (Include) Executions.getCurrent().getDesktop()
						.getPage("index").getFellow("mainInclude");
				Sessions.getCurrent().setAttribute("lastPage",locationUri);
				include.setSrc("/WEB-INF/pages" + locationUri);
			}
		}
	}

	/**
	 * Method for open/load a menu with checking user menu authorization first
	 * 
	 * @param menuUri
	 */
	@GlobalCommand("openAuthorizedMenu")
	public void openAuthorizedMenu(@BindingParam("uri") String menuUri) {
		if (authorizedUserMenu.containsKey(menuUri)) {
			UserMenu um = authorizedUserMenu.get(menuUri);

			openMenu(um.getMenuUrl(), um.getMenuTitle(), um.getMenuCode()
					.toString());
		} else {
		}
	}

	@GlobalCommand("openDebugMenu")
	public void openDebugMenu() {
		// TODO :PRODUCTION MATTER: (DEVELOPMENT ONLY)
		if (System.getProperty("debug") != null) {
			String menuCode = System.getProperty("menu");

			UserMenu um = userMenuMap.get(menuCode);
			try {
				openMenu(um.getMenuUrl(), um.getMenuTitle(), menuCode);
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Search correct / true parent for specific node recursively.
	 * 
	 * @param root
	 * @param menuParent
	 * @return
	 */
	private DefaultTreeNode<UserMenu> recurseSearch(
			DefaultTreeNode<UserMenu> root, String menuParent) {
		if (root.getData().getMenuCode().equals(menuParent))
			return root;

		DefaultTreeNode<UserMenu> res = null;

		if (root.getChildren() != null) {
			for (TreeNode<UserMenu> fn : root.getChildren()) {
				res = recurseSearch((DefaultTreeNode<UserMenu>) fn, menuParent);
			}
		}

		return res;
	}

	public ListModelList<DefaultTreeModel<UserMenu>> getListMenuTreeModel() {
		return listMenuTreeModel;
	}

	public List<UserMenuAccordion> getListMenuAccordion() {
		return listMenuAccordion;
	}

	public List<UserMenu> getListUserMenuFavorite() {
		return listUserMenuFavorite;
	}

	public Map<String, UserMenu> getAuthorizedUserMenu() {
		return authorizedUserMenu;
	}

	public void setAuthorizedUserMenu(Map<String, UserMenu> authorizedUserMenu) {
		this.authorizedUserMenu = authorizedUserMenu;
	}
}
