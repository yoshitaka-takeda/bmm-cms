package id.co.indocyber.bmmRmiCms.controller;

import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.entity.MRole;
import id.co.indocyber.bmmRmiCms.entity.MUserRole;
import id.co.indocyber.bmmRmiCms.service.MRoleSvc;
import id.co.indocyber.bmmRmiCms.service.MUserRoleSvc;
import id.co.indocyber.bmmRmiCms.service.MUserSvc;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class BannerVmd extends BaseVmd {
	@WireVariable
	private MRoleSvc mRoleSvc;
	@WireVariable
	private MUserRoleSvc mUserRoleSvc;
	@WireVariable
	private MUserSvc mUserSvc;
	
	private Date hariIni;
	private String tglini;
	private String username;
	private String role;
	private String position;
	private String user;
	private String name;
	private String image;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getTglini() {
		return tglini;
	}

	public void setTglini(String tglini) {
		this.tglini = tglini;
	}

	public Date getHariIni() {
		return hariIni;
	}

	public void setHariIni(Date hariIni) {
		this.hariIni = hariIni;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Init
	public void load() {
		OffsetDateTime odt = OffsetDateTime.now(ZoneOffset.ofHours(7));
		
		String formattedDateTime = DateTimeFormatter
		                            .ofPattern("EEEE, dd MMM yyyy HH:mm:ss")
		                            .format(odt);
		
		tglini = " " + formattedDateTime + " ";
		
		if (Sessions.getCurrent().getAttribute("user") != null) {
			MUserDto user1 = (MUserDto) Sessions.getCurrent().getAttribute("user");
			MUserRole cms = mUserRoleSvc.cmsAccess(user1.getEmail());
			if(!mUserSvc.findByUsernameOrEmail(user1.getUsername()).getIsActive() ||
					!(cms==null ? false : cms.getIsActive())){
				Messagebox.show("Akun anda telah dinonaktifkan. Anda akan kembali ke halaman login.", 
						"Perhatian", new Button[] { Button.OK},
						Messagebox.ERROR, Button.OK,
						new EventListener<Messagebox.ClickEvent>() {
							@Override
							public void onEvent(ClickEvent event)
									throws Exception {
								// TODO Auto-generated method stub
								if (Messagebox.ON_OK.equals(event.getName())) {
									Sessions.getCurrent().removeAttribute("user");
									Sessions.getCurrent().removeAttribute("lastPage");
									Executions.getCurrent().sendRedirect("/login.zul");
								}
							}}
				);
			}
			else{
				MRole thisRole = mRoleSvc.findOne(cms.getRoleId());
				// DsfMstUser2Dto user1 = (DsfMstUser2Dto)
				// Sessions.getCurrent().getAttribute("user");
				username = user1.getFullname();
				position = thisRole.getRoleName();
	
				image = StringUtils.isEmpty(user1.getPhotoText()) ? 
						"/res/button_profile.png" : user1.getPhotoText();
				int superAdmin = thisRole.getIsAdmin()?1:0;
				Sessions.getCurrent().setAttribute("superAdmin", superAdmin);
			}
			// posisi = dsfMstPositionSvc.selectById(position);
		}
		/*
		 * try{ if((Boolean) Sessions.getCurrent().getAttribute("isChangePass")
		 * == true){ Messagebox.show("Mohon segera mengganti password!", new
		 * Button[] { Button.OK }, new EventListener<Messagebox.ClickEvent>() {
		 * 
		 * @Override public void onEvent(ClickEvent event)throws Exception {
		 * changePass(); } }); } } catch(Exception e){
		 * Executions.sendRedirect("/login.zul"); }
		 */
	}

	@Command("logout")
	public void logout() {
		Sessions.getCurrent().removeAttribute("user");
		Sessions.getCurrent().removeAttribute("lastPage");
		Executions.getCurrent().sendRedirect("/login.zul");
	}

	@Command("updateTime")
	@NotifyChange("tglini")
	public void updateTime() {
		OffsetDateTime odt = OffsetDateTime.now(ZoneOffset.ofHours(7));
		
		String formattedDateTime = DateTimeFormatter
		                            .ofPattern("EEEE, dd MMM yyyy HH:mm:ss")
		                            .format(odt);
		
		tglini = " " + formattedDateTime + " ";
	}

	@Command("changePass")
	public void changePass() {
		if(Sessions.getCurrent().getAttribute("editMode")==null)
			Sessions.getCurrent().setAttribute("editMode", false);
		
		if((Boolean)Sessions.getCurrent().getAttribute("editMode")){
			Messagebox.show("Data tidak akan disimpan. Apakah Anda yakin?", "Perhatian",
					new Button[] { Button.YES, Button.NO },
					Messagebox.QUESTION, Button.NO,
					new EventListener<Messagebox.ClickEvent>() {
						@Override
						public void onEvent(ClickEvent event) throws Exception {
							if (Messagebox.ON_YES.equals(event.getName())) {
								Sessions.getCurrent().setAttribute("editMode", false);
								navigate("/userManagement/user/changePass.zul");
							}
						}
					}
				);
		}
		else{
			Sessions.getCurrent().setAttribute("editMode", false);
			navigate("/userManagement/user/changePass.zul");
		}
	}
	
	/*
	@Command("toCompany")
	public void toCompany() {
		if(Sessions.getCurrent().getAttribute("editMode")==null)
			Sessions.getCurrent().setAttribute("editMode", false);
		
		if((Boolean)Sessions.getCurrent().getAttribute("editMode")){
			Messagebox.show("Data tidak akan disimpan. Apakah Anda yakin?", "Perhatian",
					new Button[] { Button.YES, Button.NO },
					Messagebox.QUESTION, Button.NO,
					new EventListener<Messagebox.ClickEvent>() {
						@Override
						public void onEvent(ClickEvent event) throws Exception {
							if (Messagebox.ON_YES.equals(event.getName())) {
								Sessions.getCurrent().setAttribute("editMode", false);
								navigate("/company/index.zul");
							}
						}
					}
				);
		}
		else{
			Sessions.getCurrent().setAttribute("editMode", false);
			navigate("/company/index.zul");
		}
	}
	*/
}
