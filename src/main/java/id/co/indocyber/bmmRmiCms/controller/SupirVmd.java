package id.co.indocyber.bmmRmiCms.controller;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.springframework.util.StringUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import id.co.indocyber.bmmRmiCms.dto.MAddressTypeDto;
import id.co.indocyber.bmmRmiCms.dto.MEducationTypeDto;
import id.co.indocyber.bmmRmiCms.dto.MGenderDto;
import id.co.indocyber.bmmRmiCms.dto.MMaritalStatusDto;
import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MReligionDto;
import id.co.indocyber.bmmRmiCms.dto.MRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MobDriverDto;
import id.co.indocyber.bmmRmiCms.dto.MobMappingTransportirDriverDto;
import id.co.indocyber.bmmRmiCms.dto.MobSptDto;
import id.co.indocyber.bmmRmiCms.dto.MobTransportirDto;
import id.co.indocyber.bmmRmiCms.entity.MAddressType;
import id.co.indocyber.bmmRmiCms.entity.MReligion;
import id.co.indocyber.bmmRmiCms.service.MDSupirSvc;
import id.co.indocyber.bmmRmiCms.service.MDTransporterSvc;
import id.co.indocyber.bmmRmiCms.tools.StringConstants;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class SupirVmd extends BaseVmd {
	
	@WireVariable
	private MDSupirSvc mDSupirSvc;
	
	@WireVariable
	private MDTransporterSvc mdTransporterSvc;
	
	private MUserDto user;
	
	private List<MobDriverDto> listDriver = new ArrayList<>();
	private MobDriverDto mobDriverDto, selectedDriverDto;

	private List<MobTransportirDto> listTransportir = new ArrayList<>();
	private MobTransportirDto transportirDto = new MobTransportirDto();
	
	private List<MAddressTypeDto> listAddressType = new ArrayList<>();
	private List<String> listProvince = new ArrayList<>();	
	private List<MMaritalStatusDto> listMarital = new ArrayList<>();
	private List<MGenderDto> listGender = new ArrayList<>();
	private List<MReligionDto> listReligion = new ArrayList<>();
	private List<MEducationTypeDto> listEducation = new ArrayList<>();
	
	private MAddressTypeDto selectedAddress;
	private MMaritalStatusDto selectedMarital;
	private MGenderDto selectedGender;
	private MEducationTypeDto selectedEdu;
	private MReligionDto selectedReligion;
	
	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;

	private boolean freezeId;
	private int editIndex = -1;
	private String pageTitle;

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private boolean superAdmin;
	private Integer recordCount = 1;
	
	private String sortBy = "";
	private boolean sortAsc = false;
	
	
	private boolean detailWindow;
	private boolean addMode;
	private boolean editMode;
	
	private String jenisKelamin = null;
	
	@Init
	@NotifyChange({"editMode","user","listDriver","recordCount","maxPage"})	
	public void load(){
		try{
			editMode = Boolean.valueOf((Boolean) Sessions.getCurrent().getAttribute("editMode"));
			user = (MUserDto) Sessions.getCurrent().getAttribute("user");
			
			if(user.isAdministrator() == true){
				Map<String, Object> map = mDSupirSvc.findByArg(searchData, pageNo, perPage,"is_active desc, modified_date", false);
				listDriver = (List<MobDriverDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
			}else{
				Map<String, Object> map = mDSupirSvc.findByTransportir(user.getReferenceCode(), searchData, pageNo, perPage ,"is_active desc, modified_date", false);
				listDriver = (List<MobDriverDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
			}
			
			for (MMenuRoleDto x : user.getRolePrivs()) {
				if ("MD-2".equals(x.getMenuCode())) {
					btnAdd = !x.getIsAdd();
					btnDel = !x.getIsDelete();
					btnDl = !x.getIsDownload();
					btnEdit = !x.getIsEdit();
					break;
				}
			}			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Command("src")
	@NotifyChange({"listDriver", "pageNo", "maxPage", "recordCount"})
	public void search() {
		search = true;
		pageNo = 1;
		if(user.isAdministrator() == true){
			Map<String, Object> map = mDSupirSvc.findByArg(searchData, pageNo, perPage,StringUtils.isEmpty(sortBy)?"is_active desc, modified_date":sortBy, sortAsc);
			listDriver = (List<MobDriverDto>) map.get("content");
			recordCount = (Integer) map.get("totalSize");
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
		}else{
			Map<String, Object> map = mDSupirSvc.findByTransportir(user.getReferenceCode(), searchData, pageNo, perPage ,StringUtils.isEmpty(sortBy)?"is_active desc, modified_date":sortBy, sortAsc);
			listDriver = (List<MobDriverDto>) map.get("content");
			recordCount = (Integer) map.get("totalSize");
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Command("reload")
	@NotifyChange({"recordCount","listDriver","maxPage","searchData"})
	public void reload(){
		searchData = "";
		if(user.isAdministrator() == true){
			Map<String, Object> map = mDSupirSvc.findByArg(searchData, pageNo, perPage,"is_active desc, modified_date", false);
			listDriver = (List<MobDriverDto>) map.get("content");
			recordCount = (Integer) map.get("totalSize");
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
		}else{
			Map<String, Object> map = mDSupirSvc.findByTransportir(user.getReferenceCode(), searchData, pageNo, perPage ,"is_active desc, modified_date", false);
			listDriver = (List<MobDriverDto>) map.get("content");
			recordCount = (Integer) map.get("totalSize");
			maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
		}
	}
	
	public boolean cekUsia(Date tglLahir){
		Calendar c = Calendar.getInstance();
		c.setTime(tglLahir);
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH) + 1;
		int date = c.get(Calendar.DATE);
		LocalDate usia = LocalDate.of(year, month, date);
		LocalDate now1 = LocalDate.now();
		Period diff1 = Period.between(usia, now1);
		int batasUsia = 17;
		if(diff1.getYears()<=batasUsia){
			return true;
		}else{
			return false;
		}
	}
	
	@Command
	@NotifyChange({"selectedDriverDto","flagIconWarning"})
	public void ubahStatusAktif(@BindingParam("dto") MobDriverDto dto){
		showConfirm("E002");
		setFlagIconWarning(true);
		selectedDriverDto = new MobDriverDto();
		selectedDriverDto = mDSupirSvc.findOne(dto.getId());
		selectedDriverDto.setIsActive(false);
		selectedDriverDto.setModifiedBy(user.getFullname());
		selectedDriverDto.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@Command
	@NotifyChange({"selectedDriverDto","flagIconWarning"})
	public void ubahStatusNonAktif(@BindingParam("dto") MobDriverDto dto){
		showConfirm("E003");
		setFlagIconWarning(true);
		selectedDriverDto = new MobDriverDto();
		selectedDriverDto = mDSupirSvc.findOne(dto.getId());
		selectedDriverDto.setIsActive(true);
		selectedDriverDto.setModifiedBy(user.getFullname());
		selectedDriverDto.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@SuppressWarnings("unchecked")
	@Command
	@NotifyChange({"recordCount","listDriver","maxPage","flagConfirmMsg"})
	public void yesConfirm(){
		try{
			//jika login sbg transporter, maka save mapping
			MobMappingTransportirDriverDto mappingDto = new MobMappingTransportirDriverDto();
			if(user.isAdministrator() == false){				
				mappingDto.setTransportirCode(user.getReferenceCode());
				mappingDto.setIsActive(true);
				mappingDto.setCreated(user.getUsername());
				mappingDto.setCreatedDate(new Timestamp(System.currentTimeMillis()));
				mappingDto.setModified(user.getUsername());
				mappingDto.setModifiedDate(new Timestamp(System.currentTimeMillis()));
				mDSupirSvc.saveMappingDriver(mappingDto);
			}else{
				mappingDto = null;
			}
			int result = mDSupirSvc.save(selectedDriverDto, mappingDto);
			if(result == 1){
				flagConfirmMsg = false;
				showSuccessMsgBox("E000");

				//reload list
				if(user.isAdministrator() == true){
					Map<String, Object> map = mDSupirSvc.findByArg(searchData, pageNo,perPage, StringUtils.isEmpty(sortBy)?"is_active desc, modified_date":sortBy, sortAsc);
					listDriver = (List<MobDriverDto>) map.get("content");
					recordCount = (Integer) map.get("totalSize");
					maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
				}else{
					Map<String, Object> map = mDSupirSvc.findByTransportir(user.getReferenceCode(), searchData, pageNo, perPage ,StringUtils.isEmpty(sortBy)?"is_active desc, modified_date":sortBy, sortAsc);
					listDriver = (List<MobDriverDto>) map.get("content");
					recordCount = (Integer) map.get("totalSize");
					maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
				}
			}else if(result == 0){
				showErrorMsgBox("E001");
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	@Command
	@NotifyChange({"detailWindow","addMode","editMode",
		"listAddressType","listEducation","listGender",
		"listMarital","listProvince","listReligion","mobDriverDto"})
	public void add(){
		loadListCombo();
		mobDriverDto = new MobDriverDto();
		mobDriverDto.setDriverTransportir(true);
		setDetailWindow(true);
		setAddMode(true);
		setEditMode(false);
	}
	
	@Command
	@NotifyChange({"detailWindow","addMode","editMode","mobDriverDto",
		"listAddressType","listEducation","listGender",
		"listMarital","listProvince","listReligion",
		"selectedAddress","selectedEdu","selectedMarital","selectedReligion",
		"mobDriverDto"})	
	public void edit(@BindingParam("item") MobDriverDto dto){
		loadListCombo();
		mobDriverDto = mDSupirSvc.findOne(dto.getId());
		mapMarital(mobDriverDto.getMaritalStatus()==null?"":mobDriverDto.getMaritalStatus());
		mapEduType(mobDriverDto.getLastEducation()==null?"":mobDriverDto.getLastEducation());
		mapAddressType(mobDriverDto.getAddressType()==null?"":mobDriverDto.getAddressType());
		mapReligion(mobDriverDto.getReligion()==null?"":mobDriverDto.getReligion());
		mobDriverDto.setDriverType(mobDriverDto.getDriverType()==null?"Transportir":mobDriverDto.getDriverType());
		if(mobDriverDto.getDriverType().equalsIgnoreCase("Transportir")){
			mobDriverDto.setDriverTransportir(true);
			mobDriverDto.setDriverPetani(false);
		}else if(mobDriverDto.getDriverType().equalsIgnoreCase("Petani")){
			mobDriverDto.setDriverTransportir(false);
			mobDriverDto.setDriverPetani(true);
		}
		System.err.println("luthfi12 "+mobDriverDto.getGender());
		if(mobDriverDto.getGender().equalsIgnoreCase("1")){
			mobDriverDto.setGenderMale(true);
			setJenisKelamin("1");
		}else if(mobDriverDto.getGender().equalsIgnoreCase("2")){
			mobDriverDto.setGenderFemale(true);
			setJenisKelamin("2");
		}
		
		setDetailWindow(true);
		setAddMode(false);
		setEditMode(true);
	}
	
	@NotifyChange("selectedEdu")
	public void mapEduType(String eduType){
		for(MEducationTypeDto o : listEducation){
			if(eduType.equals(o.getCode())){
				selectedEdu = new MEducationTypeDto();
				selectedEdu = o;
			}
		}		
	}
	
	@NotifyChange("selectedMarital")
	public void mapMarital(String maritalType){
		for(MMaritalStatusDto o : listMarital){
			if(maritalType.equalsIgnoreCase(o.getCode())){
				selectedMarital = new MMaritalStatusDto();
				selectedMarital = o;
			}
		}		
	}
	
	@NotifyChange("selectedAddress")
	public void mapAddressType(String addressType){
		for(MAddressTypeDto o : listAddressType){
			if(addressType.equalsIgnoreCase(o.getCode())){
				selectedAddress = new MAddressTypeDto();
				selectedAddress = o;
			}
		}					
	}
	
	@NotifyChange("selectedReligion")
	public void mapReligion(String mapReligion){
		for(MReligionDto o : listReligion){
			if(mapReligion.equalsIgnoreCase(o.getCode())){
				selectedReligion = new MReligionDto();
				selectedReligion = o;				
			}
		}
	}
	
	@Command
	@NotifyChange({"detailWindow","addMode","editMode"})
	public void close(){
		setDetailWindow(false);
		setAddMode(false);
		setEditMode(false);
	}
	
	@SuppressWarnings("unchecked")
	@Command
	@NotifyChange({"listDriver","editMode","recordCount","maxPage","detailWindow","addMode","jenisKelamin","mobDriverDto"})
	public void save(){
		try{
			boolean cek = false;
			if(mobDriverDto.getBirthday()!=null){
				cek = cekUsia(mobDriverDto.getBirthday());
				if(cek==true){
					showErrorMsgBox("E200");
					return;
				}
			}
			if(getJenisKelamin()==null){
				System.err.println("luthfi20 "+mobDriverDto.getPhoneNum().substring(0,2));				
				showWarningMsgBox("E100");
				return;
			}
			if(mobDriverDto.getPhoneNum()!=null){
				if(!mobDriverDto.getPhoneNum().substring(0, 2).equalsIgnoreCase("08")){
					showWarningMsgBox("E102");
					return;
				}
			}
			
//			String fullErrMsg = StringConstants.MSG_FORM_TEMPLATE;
//			int error = 0;
//			if (StringUtils.isEmpty(getJenisKelamin() == null)) {
//				error++;
//				fullErrMsg += String.format(
//						StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Jenis Kelamin");
//			}
//			if(cek==true){
//				error++;
//				fullErrMsg += String.format(
//						StringConstants.MSG_FORM_BIRTHDAY, "Tanggal Lahir");								
//			}
//			if(StringUtils.isEmpty(mobDriverDto.getPhoneNum() == null)){
//				error++;
//				fullErrMsg += String.format(
//						StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Nomor Telepon");				
//			}else{
//				if(!mobDriverDto.getPhoneNum().substring(0,2).equals("08")){
//					error++;
//					fullErrMsg += String.format(
//							StringConstants.MSG_FORM_PHONE_NUMERIC_INVALID, "Nomor Telepon");				
//				}
//			}
//			
//			if(error > 0){
//				Messagebox.show(fullErrMsg, "Message", 0, null);
//			}else{
				MobDriverDto dto = new MobDriverDto();
				if(isAddMode()){
					System.err.println("masuk add");
					List<MobDriverDto> listA = new ArrayList<>();
					listA = mDSupirSvc.findDriverByPhoneNum(mobDriverDto.getPhoneNum());
					if(listA.size()>0){
						showWarningMsgBox("E014");
						return;
					}

					if(mobDriverDto.getEmail()!=null){
						List<MobDriverDto> listB = new ArrayList<>();
						listB = mDSupirSvc.findDriverByEmail(mobDriverDto.getEmail());
						if(listB.size()>0){
							showWarningMsgBox("E015");
							return;
						}
					}

					if(mobDriverDto.getNoKtp()!=null){
						List<MobDriverDto> listC = new ArrayList<>();
						listC = mDSupirSvc.findDriverByNoKtp(mobDriverDto.getNoKtp());
						if(listC.size()>0){
							showWarningMsgBox("E016");
							return;
						}
					}
					
					dto.setFullname(mobDriverDto.getFullname()==null?"":mobDriverDto.getFullname());
					dto.setBirthday(mobDriverDto.getBirthday()==null?null:mobDriverDto.getBirthday());
					dto.setPlaceOfBirthday(mobDriverDto.getPlaceOfBirthday()==null?"":mobDriverDto.getPlaceOfBirthday());
					dto.setAddressType(selectedAddress==null?"":selectedAddress.getCode());
					dto.setAddress(mobDriverDto.getAddress()==null?"":mobDriverDto.getAddress());
					dto.setRtrw(mobDriverDto.getRtrw()==null?"":mobDriverDto.getRtrw());
					dto.setVillage(mobDriverDto.getVillage()==null?"":mobDriverDto.getVillage());
					dto.setSubDistrict(mobDriverDto.getSubDistrict()==null?"":mobDriverDto.getSubDistrict());
					dto.setDistrict(mobDriverDto.getDistrict()==null?"":mobDriverDto.getDistrict());
					dto.setCity(mobDriverDto.getCity()==null?"":mobDriverDto.getCity());
					dto.setProvince(mobDriverDto.getProvince()==null?null:mobDriverDto.getProvince());
					dto.setZipcode(mobDriverDto.getZipcode()==null?null:mobDriverDto.getZipcode());
					dto.setPhoneNum(mobDriverDto.getPhoneNum()==null?null:mobDriverDto.getPhoneNum());
					dto.setEmail(mobDriverDto.getEmail()==null?"":mobDriverDto.getEmail());
					dto.setMaritalStatus(selectedMarital==null?"":selectedMarital.getCode());
					dto.setGender(jenisKelamin==null?null:jenisKelamin);
					dto.setReligion(selectedReligion==null?"":selectedReligion.getCode());
					dto.setLastEducation(selectedEdu==null?"":selectedEdu.getCode());
					dto.setNoKtp(mobDriverDto.getNoKtp()==null?"":mobDriverDto.getNoKtp());
					dto.setReferenceCode(mobDriverDto.getReferenceCode()==null?"":mobDriverDto.getReferenceCode());
					dto.setDriverType(mobDriverDto.getDriverType()==null?"Transportir":mobDriverDto.getDriverType());
					dto.setIsActive(true);
					dto.setCreatedBy(user.getUsername());
					dto.setCreatedDate(new Timestamp(System.currentTimeMillis()));
					dto.setModifiedBy(user.getUsername());
					dto.setModifiedDate(new Timestamp(System.currentTimeMillis()));
				}else{
					System.err.println("masuk edit");
					dto.setId(mobDriverDto.getId());
					dto.setFullname(mobDriverDto.getFullname()==null?"":mobDriverDto.getFullname());
					dto.setBirthday(mobDriverDto.getBirthday()==null?null:mobDriverDto.getBirthday());
					dto.setPlaceOfBirthday(mobDriverDto.getPlaceOfBirthday()==null?null:mobDriverDto.getPlaceOfBirthday());
					dto.setAddressType(selectedAddress==null?null:selectedAddress.getCode());
					dto.setAddress(mobDriverDto.getAddress()==null?null:mobDriverDto.getAddress());
					dto.setRtrw(mobDriverDto.getRtrw()==null?null:mobDriverDto.getRtrw());
					dto.setVillage(mobDriverDto.getVillage()==null?null:mobDriverDto.getVillage());
					dto.setSubDistrict(mobDriverDto.getSubDistrict()==null?null:mobDriverDto.getSubDistrict());
					dto.setDistrict(mobDriverDto.getDistrict()==null?null:mobDriverDto.getDistrict());
					dto.setCity(mobDriverDto.getCity()==null?null:mobDriverDto.getCity());
					dto.setProvince(mobDriverDto.getProvince()==null?null:mobDriverDto.getProvince());
					dto.setZipcode(mobDriverDto.getZipcode()==null?null:mobDriverDto.getZipcode());
					dto.setPhoneNum(mobDriverDto.getPhoneNum()==null?null:mobDriverDto.getPhoneNum());
					dto.setEmail(mobDriverDto.getEmail()==null?null:mobDriverDto.getEmail());
					dto.setMaritalStatus(selectedMarital==null?"":selectedMarital.getCode());
					dto.setGender(getJenisKelamin()==null?mobDriverDto.getGender():getJenisKelamin());
					dto.setReligion(selectedReligion==null?"":selectedReligion.getCode());
					dto.setLastEducation(selectedEdu==null?null:selectedEdu.getCode());
					dto.setNoKtp(mobDriverDto.getNoKtp()==null?null:mobDriverDto.getNoKtp());
					dto.setReferenceCode(mobDriverDto.getReferenceCode()==null?null:mobDriverDto.getReferenceCode());
					dto.setDriverType(mobDriverDto.getDriverType()==null?"Transportir":mobDriverDto.getDriverType());
					dto.setIsActive(true);
					dto.setCreatedBy(mobDriverDto.getCreatedBy());
					dto.setCreatedDate(mobDriverDto.getCreatedDate());
					dto.setModifiedBy(user.getUsername());
					dto.setModifiedDate(new Timestamp(System.currentTimeMillis()));
				}
				
				//jika login sbg transporter, maka save mapping
				MobMappingTransportirDriverDto mappingDto = new MobMappingTransportirDriverDto();
				if(user.isAdministrator() == false){				
					mappingDto.setTransportirCode(user.getReferenceCode());
					mappingDto.setIsActive(true);
					mappingDto.setCreated(user.getUsername());
					mappingDto.setCreatedDate(new Timestamp(System.currentTimeMillis()));
					mappingDto.setModified(user.getUsername());
					mappingDto.setModifiedDate(new Timestamp(System.currentTimeMillis()));
					mDSupirSvc.saveMappingDriver(mappingDto);
				}else{
					mappingDto = null;
				}
				int result = mDSupirSvc.save(dto, mappingDto);
				System.err.println("luthfi20 "+result);			
				if(result==1){
					showSuccessMsgBox("E000");
					close();
					
					//reload list
					if(user.isAdministrator() == true){
						Map<String, Object> map = mDSupirSvc.findByArg(searchData, pageNo,perPage,  StringUtils.isEmpty(sortBy)?"is_active desc, modified_date":sortBy, sortAsc);
						listDriver = (List<MobDriverDto>) map.get("content");
						recordCount = (Integer) map.get("totalSize");
						maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
					}else{
						Map<String, Object> map = mDSupirSvc.findByTransportir(user.getReferenceCode(), searchData, pageNo, perPage , StringUtils.isEmpty(sortBy)?"is_active desc, modified_date":sortBy, sortAsc);
						listDriver = (List<MobDriverDto>) map.get("content");
						recordCount = (Integer) map.get("totalSize");
						maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
					}
				}else if(result == 0){
					showErrorMsgBox("E001");
				}				
//			}
		}catch(Exception e){
			e.printStackTrace();
			showErrorMsgBox("E001");
		}
	}
	
	@SuppressWarnings("unchecked")
	@Command("sort")
	@NotifyChange({"listDriver"})
	public void sort(@BindingParam("col") String column) {
		if (activeCheck()) {
			if (sortBy.equals(column)) {
				sortAsc = !sortAsc;
			} else {
				sortBy = column;
				sortAsc = true;
			}
			if(user.isAdministrator() == true){
				Map<String, Object> map = mDSupirSvc.findByArg(searchData, pageNo,perPage, sortBy, sortAsc);
				listDriver = (List<MobDriverDto>) map.get("content");
			}else{
				Map<String, Object> map = mDSupirSvc.findByTransportir(user.getReferenceCode(), searchData, pageNo, perPage , sortBy, sortAsc);
				listDriver = (List<MobDriverDto>) map.get("content");
			}
		}
	}

	
	@Command
	@NotifyChange("mobDriverDto")
	public void selectMarital(){
		mobDriverDto.setMaritalStatus(selectedMarital.getCode());
		mobDriverDto.setMaritalStatusText(selectedMarital.getLongText());
	}
	
	@Command
	@NotifyChange("jenisKelamin")
	public void selectGender(@BindingParam("item")String gender){
		if(gender.equalsIgnoreCase("L")){
			setJenisKelamin("1");
			mobDriverDto.setGenderMale(true);
		}else if(gender.equalsIgnoreCase("P")){
			setJenisKelamin("2");
			mobDriverDto.setGenderFemale(true);
		}
	}
	
	@Command
	@NotifyChange("mobDriverDto")
	public void selectMale(){
		mobDriverDto.setGender("1");
	}
	
	@Command
	@NotifyChange("mobDriverDto")
	public void selectFemale(){
		mobDriverDto.setGender("2");
	}
	
	@Command
	@NotifyChange("mobDriverDto")	
	public void selectReligion(){
		mobDriverDto.setReligion(selectedReligion.getCode());
		mobDriverDto.setReligionText(selectedReligion.getLongText());
	}

	@Command
	@NotifyChange("mobDriverDto")		
	public void selectEdu(){
		mobDriverDto.setLastEducation(selectedEdu.getCode());
		mobDriverDto.setLastEducationText(selectedEdu.getLongText());
	}
	
	public void loadListCombo(){
		listAddressType = mDSupirSvc.cbListAddress();
		listEducation = mDSupirSvc.cbListEducation();
		listGender = mDSupirSvc.cbListGender();
		listMarital = mDSupirSvc.cbListMarital();
//		listProvince = mDSupirSvc.cbListProvince();
		listReligion = mDSupirSvc.cbListReligion();
	}
	
	@SuppressWarnings("unchecked")
	@Command("switchPage")
	@NotifyChange({ "pageNo", "listDriver" })
	public void switchPage(@BindingParam("action") String action) {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}
			if(user.isAdministrator() == true){
				Map<String, Object> map = mDSupirSvc.findByArg(searchData, pageNo,perPage, StringUtils.isEmpty(sortBy)?"is_active desc, modified_date":sortBy, sortAsc);
				listDriver = (List<MobDriverDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
			}else{
				Map<String, Object> map = mDSupirSvc.findByTransportir(user.getReferenceCode(), searchData, pageNo, perPage ,StringUtils.isEmpty(sortBy)?"is_active desc, modified_date":sortBy, sortAsc);
				listDriver = (List<MobDriverDto>) map.get("content");
				recordCount = (Integer) map.get("totalSize");
//				recordCount = mDSupirSvc.countByTransportir(user.getReferenceCode(), searchData);
				maxPage = (recordCount / perPage) + (recordCount % perPage == 0 ? 0 : 1);
			}

		}
	}

	
	public boolean isDetailWindow() {
		return detailWindow;
	}

	public void setDetailWindow(boolean detailWindow) {
		this.detailWindow = detailWindow;
	}

	public boolean isAddMode() {
		return addMode;
	}

	public void setAddMode(boolean addMode) {
		this.addMode = addMode;
	}

	public boolean isEditMode() {
		return editMode;
	}

	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}

	public MUserDto getUser() {
		return user;
	}

	public void setUser(MUserDto user) {
		this.user = user;
	}

	public List<MobDriverDto> getListDriver() {
		return listDriver;
	}

	public void setListDriver(List<MobDriverDto> listDriver) {
		this.listDriver = listDriver;
	}

	public MobDriverDto getMobDriverDto() {
		return mobDriverDto;
	}

	public void setMobDriverDto(MobDriverDto mobDriverDto) {
		this.mobDriverDto = mobDriverDto;
	}

	public MobDriverDto getSelectedDriverDto() {
		return selectedDriverDto;
	}

	public void setSelectedDriverDto(MobDriverDto selectedDriverDto) {
		this.selectedDriverDto = selectedDriverDto;
	}

	public List<MAddressTypeDto> getListAddressType() {
		return listAddressType;
	}

	public void setListAddressType(List<MAddressTypeDto> listAddressType) {
		this.listAddressType = listAddressType;
	}

	public List<String> getListProvince() {
		return listProvince;
	}

	public void setListProvince(List<String> listProvince) {
		this.listProvince = listProvince;
	}

	public List<MMaritalStatusDto> getListMarital() {
		return listMarital;
	}

	public void setListMarital(List<MMaritalStatusDto> listMarital) {
		this.listMarital = listMarital;
	}

	public List<MGenderDto> getListGender() {
		return listGender;
	}

	public void setListGender(List<MGenderDto> listGender) {
		this.listGender = listGender;
	}

	public List<MReligionDto> getListReligion() {
		return listReligion;
	}

	public void setListReligion(List<MReligionDto> listReligion) {
		this.listReligion = listReligion;
	}

	public List<MEducationTypeDto> getListEducation() {
		return listEducation;
	}

	public void setListEducation(List<MEducationTypeDto> listEducation) {
		this.listEducation = listEducation;
	}

	public MAddressTypeDto getSelectedAddress() {
		return selectedAddress;
	}

	public void setSelectedAddress(MAddressTypeDto selectedAddress) {
		this.selectedAddress = selectedAddress;
	}

	public MMaritalStatusDto getSelectedMarital() {
		return selectedMarital;
	}

	public void setSelectedMarital(MMaritalStatusDto selectedMarital) {
		this.selectedMarital = selectedMarital;
	}

	public MGenderDto getSelectedGender() {
		return selectedGender;
	}

	public void setSelectedGender(MGenderDto selectedGender) {
		this.selectedGender = selectedGender;
	}

	public MEducationTypeDto getSelectedEdu() {
		return selectedEdu;
	}

	public void setSelectedEdu(MEducationTypeDto selectedEdu) {
		this.selectedEdu = selectedEdu;
	}

	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnDel() {
		return btnDel;
	}

	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isBtnDl() {
		return btnDl;
	}

	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}

	public boolean isFreezeId() {
		return freezeId;
	}

	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}

	public int getEditIndex() {
		return editIndex;
	}

	public void setEditIndex(int editIndex) {
		this.editIndex = editIndex;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

	public boolean isSuperAdmin() {
		return superAdmin;
	}

	public void setSuperAdmin(boolean superAdmin) {
		this.superAdmin = superAdmin;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public boolean isSortAsc() {
		return sortAsc;
	}

	public void setSortAsc(boolean sortAsc) {
		this.sortAsc = sortAsc;
	}


	public MReligionDto getSelectedReligion() {
		return selectedReligion;
	}

	public void setSelectedReligion(MReligionDto selectedReligion) {
		this.selectedReligion = selectedReligion;
	}

	public String getJenisKelamin() {
		return jenisKelamin;
	}

	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}


}
