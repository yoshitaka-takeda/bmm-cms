package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the m_religion database table.
 * 
 */
@Entity
@Table(name="m_religion")
@NamedQuery(name="MReligion.findAll", query="SELECT m FROM MReligion m")
public class MReligion implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private String code;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="long_text")
	private String longText;

	@Column(name="processed_by")
	private String processedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="processed_date")
	private Date processedDate;

	@Column(name="short_text")
	private String shortText;

	public MReligion() {
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLongText() {
		return this.longText;
	}

	public void setLongText(String longText) {
		this.longText = longText;
	}

	public String getProcessedBy() {
		return this.processedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}

	public Date getProcessedDate() {
		return this.processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	public String getShortText() {
		return this.shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}