package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the mob_sj database table.
 * 
 */
@Entity
@Table(name="mob_sj")
@NamedQuery(name="MobSj.findAll", query="SELECT m FROM MobSj m")
public class MobSj implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="sj_no")
	private Integer sjNo;

	@Column(name="address_ship_from")
	private String addressShipFrom;

	@Column(name="address_ship_to")
	private String addressShipTo;

	@Column(name="container_no")
	private String containerNo;

	private String created;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="cust_code")
	private String custCode;

	@Column(name="cust_name")
	private String custName;

	@Column(name="departing_from_by")
	private String departingFromBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="departing_from_date")
	private Date departingFromDate;

	@Column(name="departing_from_latitude")
	private BigDecimal departingFromLatitude;

	@Column(name="departing_from_longitude")
	private BigDecimal departingFromLongitude;

	@Column(name="destination_by")
	private String destinationBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="destination_date")
	private Date destinationDate;

	@Column(name="destination_from_by")
	private String destinationFromBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="destination_from_date")
	private Date destinationFromDate;

	@Column(name="destination_latitude")
	private BigDecimal destinationLatitude;

	@Column(name="destination_longitude")
	private BigDecimal destinationLongitude;

	@Column(name="driver_id")
	private Integer driverId;

	@Column(name="first_destination_by")
	private String firstDestinationBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="first_destination_date")
	private Date firstDestinationDate;

	@Column(name="first_destination_latitude")
	private BigDecimal firstDestinationLatitude;

	@Column(name="first_destination_longitude")
	private BigDecimal firstDestinationLongitude;

	@Column(name="is_transit")
	private Boolean isTransit;

	@Column(name="item_code")
	private String itemCode;

	@Column(name="item_name")
	private String itemName;

	private String modified;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="modified_date")
	private Date modifiedDate;

	private String phone;

	private String pin;

	private BigDecimal qty;

	@Column(name="qty_sppb")
	private BigDecimal qtySppb;

	private String segel1;

	private String segel2;

	private String segel3;

	private String segel4;

	private String segel5;

	private String segel6;

	private String segel7;

	private String segel8;

	@Temporal(TemporalType.DATE)
	@Column(name="sj_date")
	private Date sjDate;

	@Column(name="spm_no")
	private String spmNo;

	@Column(name="spt_no")
	private Integer sptNo;

	private String status;

	@Column(name="transit_by")
	private String transitBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="transit_date")
	private Date transitDate;

	@Column(name="transit_latitude")
	private BigDecimal transitLatitude;

	@Column(name="transit_longitude")
	private BigDecimal transitLongitude;

	@Column(name="transportir_code")
	private String transportirCode;

	@Column(name="transportir_id")
	private Integer transportirId;

	@Column(name="transportir_name")
	private String transportirName;

	@Column(name="vehicle_no")
	private String vehicleNo;

	@Column(name="vehicle_type")
	private Integer vehicleType;

	public MobSj() {
	}

	public Integer getSjNo() {
		return this.sjNo;
	}

	public void setSjNo(Integer sjNo) {
		this.sjNo = sjNo;
	}

	public String getAddressShipFrom() {
		return this.addressShipFrom;
	}

	public void setAddressShipFrom(String addressShipFrom) {
		this.addressShipFrom = addressShipFrom;
	}

	public String getAddressShipTo() {
		return this.addressShipTo;
	}

	public void setAddressShipTo(String addressShipTo) {
		this.addressShipTo = addressShipTo;
	}

	public String getContainerNo() {
		return this.containerNo;
	}

	public void setContainerNo(String containerNo) {
		this.containerNo = containerNo;
	}

	public String getCreated() {
		return this.created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCustCode() {
		return this.custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getCustName() {
		return this.custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getDepartingFromBy() {
		return this.departingFromBy;
	}

	public void setDepartingFromBy(String departingFromBy) {
		this.departingFromBy = departingFromBy;
	}

	public Date getDepartingFromDate() {
		return this.departingFromDate;
	}

	public void setDepartingFromDate(Date departingFromDate) {
		this.departingFromDate = departingFromDate;
	}

	public BigDecimal getDepartingFromLatitude() {
		return this.departingFromLatitude;
	}

	public void setDepartingFromLatitude(BigDecimal departingFromLatitude) {
		this.departingFromLatitude = departingFromLatitude;
	}

	public BigDecimal getDepartingFromLongitude() {
		return this.departingFromLongitude;
	}

	public void setDepartingFromLongitude(BigDecimal departingFromLongitude) {
		this.departingFromLongitude = departingFromLongitude;
	}

	public String getDestinationBy() {
		return this.destinationBy;
	}

	public void setDestinationBy(String destinationBy) {
		this.destinationBy = destinationBy;
	}

	public Date getDestinationDate() {
		return this.destinationDate;
	}

	public void setDestinationDate(Date destinationDate) {
		this.destinationDate = destinationDate;
	}

	public String getDestinationFromBy() {
		return this.destinationFromBy;
	}

	public void setDestinationFromBy(String destinationFromBy) {
		this.destinationFromBy = destinationFromBy;
	}

	public Date getDestinationFromDate() {
		return this.destinationFromDate;
	}

	public void setDestinationFromDate(Date destinationFromDate) {
		this.destinationFromDate = destinationFromDate;
	}

	public BigDecimal getDestinationLatitude() {
		return this.destinationLatitude;
	}

	public void setDestinationLatitude(BigDecimal destinationLatitude) {
		this.destinationLatitude = destinationLatitude;
	}

	public BigDecimal getDestinationLongitude() {
		return this.destinationLongitude;
	}

	public void setDestinationLongitude(BigDecimal destinationLongitude) {
		this.destinationLongitude = destinationLongitude;
	}

	public Integer getDriverId() {
		return this.driverId;
	}

	public void setDriverId(Integer driverId) {
		this.driverId = driverId;
	}

	public String getFirstDestinationBy() {
		return this.firstDestinationBy;
	}

	public void setFirstDestinationBy(String firstDestinationBy) {
		this.firstDestinationBy = firstDestinationBy;
	}

	public Date getFirstDestinationDate() {
		return this.firstDestinationDate;
	}

	public void setFirstDestinationDate(Date firstDestinationDate) {
		this.firstDestinationDate = firstDestinationDate;
	}

	public BigDecimal getFirstDestinationLatitude() {
		return this.firstDestinationLatitude;
	}

	public void setFirstDestinationLatitude(BigDecimal firstDestinationLatitude) {
		this.firstDestinationLatitude = firstDestinationLatitude;
	}

	public BigDecimal getFirstDestinationLongitude() {
		return this.firstDestinationLongitude;
	}

	public void setFirstDestinationLongitude(BigDecimal firstDestinationLongitude) {
		this.firstDestinationLongitude = firstDestinationLongitude;
	}

	public Boolean getIsTransit() {
		return this.isTransit;
	}

	public void setIsTransit(Boolean isTransit) {
		this.isTransit = isTransit;
	}

	public String getItemCode() {
		return this.itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return this.itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getModified() {
		return this.modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPin() {
		return this.pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public BigDecimal getQty() {
		return this.qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public BigDecimal getQtySppb() {
		return this.qtySppb;
	}

	public void setQtySppb(BigDecimal qtySppb) {
		this.qtySppb = qtySppb;
	}

	public String getSegel1() {
		return this.segel1;
	}

	public void setSegel1(String segel1) {
		this.segel1 = segel1;
	}

	public String getSegel2() {
		return this.segel2;
	}

	public void setSegel2(String segel2) {
		this.segel2 = segel2;
	}

	public String getSegel3() {
		return this.segel3;
	}

	public void setSegel3(String segel3) {
		this.segel3 = segel3;
	}

	public String getSegel4() {
		return this.segel4;
	}

	public void setSegel4(String segel4) {
		this.segel4 = segel4;
	}

	public String getSegel5() {
		return this.segel5;
	}

	public void setSegel5(String segel5) {
		this.segel5 = segel5;
	}

	public String getSegel6() {
		return this.segel6;
	}

	public void setSegel6(String segel6) {
		this.segel6 = segel6;
	}

	public String getSegel7() {
		return this.segel7;
	}

	public void setSegel7(String segel7) {
		this.segel7 = segel7;
	}

	public String getSegel8() {
		return this.segel8;
	}

	public void setSegel8(String segel8) {
		this.segel8 = segel8;
	}

	public Date getSjDate() {
		return this.sjDate;
	}

	public void setSjDate(Date sjDate) {
		this.sjDate = sjDate;
	}

	public String getSpmNo() {
		return this.spmNo;
	}

	public void setSpmNo(String spmNo) {
		this.spmNo = spmNo;
	}

	public Integer getSptNo() {
		return this.sptNo;
	}

	public void setSptNo(Integer sptNo) {
		this.sptNo = sptNo;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTransitBy() {
		return this.transitBy;
	}

	public void setTransitBy(String transitBy) {
		this.transitBy = transitBy;
	}

	public Date getTransitDate() {
		return this.transitDate;
	}

	public void setTransitDate(Date transitDate) {
		this.transitDate = transitDate;
	}

	public BigDecimal getTransitLatitude() {
		return this.transitLatitude;
	}

	public void setTransitLatitude(BigDecimal transitLatitude) {
		this.transitLatitude = transitLatitude;
	}

	public BigDecimal getTransitLongitude() {
		return this.transitLongitude;
	}

	public void setTransitLongitude(BigDecimal transitLongitude) {
		this.transitLongitude = transitLongitude;
	}

	public String getTransportirCode() {
		return this.transportirCode;
	}

	public void setTransportirCode(String transportirCode) {
		this.transportirCode = transportirCode;
	}

	public Integer getTransportirId() {
		return this.transportirId;
	}

	public void setTransportirId(Integer transportirId) {
		this.transportirId = transportirId;
	}

	public String getTransportirName() {
		return this.transportirName;
	}

	public void setTransportirName(String transportirName) {
		this.transportirName = transportirName;
	}

	public String getVehicleNo() {
		return this.vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public Integer getVehicleType() {
		return this.vehicleType;
	}

	public void setVehicleType(Integer vehicleType) {
		this.vehicleType = vehicleType;
	}

}