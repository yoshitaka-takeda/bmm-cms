package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the mob_goods_receive_attachment database table.
 * 
 */
@Entity
@Table(name="mob_goods_receive_attachment")
@NamedQuery(name="MobGoodsReceiveAttachment.findAll", query="SELECT m FROM MobGoodsReceiveAttachment m")
public class MobGoodsReceiveAttachment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String created;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Lob
	@Column(name="document_blob")
	private String documentBlob;

	@Column(name="document_path")
	private String documentPath;

	@Lob
	private String notes;

	@Column(name="sj_no")
	private int sjNo;

	public MobGoodsReceiveAttachment() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCreated() {
		return this.created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getDocumentBlob() {
		return this.documentBlob;
	}

	public void setDocumentBlob(String documentBlob) {
		this.documentBlob = documentBlob;
	}

	public String getDocumentPath() {
		return this.documentPath;
	}

	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public int getSjNo() {
		return this.sjNo;
	}

	public void setSjNo(int sjNo) {
		this.sjNo = sjNo;
	}

}