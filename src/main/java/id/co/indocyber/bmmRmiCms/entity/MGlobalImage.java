package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the m_global_image database table.
 * 
 */
@Entity
@Table(name="m_global_image")
@NamedQuery(name="MGlobalImage.findAll", query="SELECT m FROM MGlobalImage m")
public class MGlobalImage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="parm_img_code")
	private String parmImgCode;

	@Column(name="created_by")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Lob
	@Column(name="parm_img_value")
	private byte[] parmImgValue;

	public MGlobalImage() {
	}

	public String getParmImgCode() {
		return this.parmImgCode;
	}

	public void setParmImgCode(String parmImgCode) {
		this.parmImgCode = parmImgCode;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public byte[] getParmImgValue() {
		return this.parmImgValue;
	}

	public void setParmImgValue(byte[] parmImgValue) {
		this.parmImgValue = parmImgValue;
	}

}