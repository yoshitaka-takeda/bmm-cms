package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the mob_customer database table.
 * 
 */
@Entity
@Table(name="mob_customer")
@NamedQuery(name="MobCustomer.findAll", query="SELECT m FROM MobCustomer m")
public class MobCustomer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="cust_code")
	private String custCode;

	private String address;

	@Column(name="cust_name")
	private String custName;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="phone_num")
	private String phoneNum;

	@Column(name="processed_by")
	private String processedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="processed_date")
	private Date processedDate;

	public MobCustomer() {
	}

	public String getCustCode() {
		return this.custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCustName() {
		return this.custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getPhoneNum() {
		return this.phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getProcessedBy() {
		return this.processedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}

	public Date getProcessedDate() {
		return this.processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}