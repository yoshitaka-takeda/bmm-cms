package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the mob_spt database table.
 * 
 */
@Entity
@Table(name="mob_spt")
@NamedQuery(name="MobSpt.findAll", query="SELECT m FROM MobSpt m")
public class MobSpt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="spt_no")
	private int sptNo;

	@Column(name="address_ship_from")
	private String addressShipFrom;

	@Column(name="address_ship_to")
	private String addressShipTo;

	@Column(name="container_no")
	private String containerNo;

	private String created;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="cust_code")
	private String custCode;

	@Column(name="cust_name")
	private String custName;

	@Column(name="driver_id")
	private int driverId;

	@Column(name="is_transit")
	private Boolean isTransit;

	@Column(name="item_code")
	private String itemCode;

	@Column(name="item_name")
	private String itemName;

	@Lob
	@Column(name="ktp_picture")
	private String ktpPicture;

	private String modified;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="modified_date")
	private Date modifiedDate;

	private String phone;

	private BigDecimal qty;

	@Column(name="qty_sppb")
	private BigDecimal qtySppb;

	@Column(name="reference_code")
	private String referenceCode;

	@Column(name="ship_from")
	private String shipFrom;

	@Column(name="sj_no")
	private String sjNo;

	@Column(name="sppb_key")
	private int sppbKey;

	@Column(name="sppb_no")
	private int sppbNo;

	@Temporal(TemporalType.DATE)
	@Column(name="spt_date")
	private Date sptDate;

	@Temporal(TemporalType.DATE)
	@Column(name="spt_expired_date")
	private Date sptExpiredDate;

	private String status;

	@Column(name="take_assignment_by")
	private String takeAssignmentBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="take_assignment_date")
	private Date takeAssignmentDate;

	@Column(name="take_assignment_latitude")
	private BigDecimal takeAssignmentLatitude;

	@Column(name="take_assignment_longitude")
	private BigDecimal takeAssignmentLongitude;

	@Column(name="transportir_code")
	private String transportirCode;

//	@Column(name="transportir_id")
//	private int transportirId;

	@Column(name="transportir_name")
	private String transportirName;

	@Column(name="vehicle_no")
	private String vehicleNo;

	@Column(name="vehicle_type")
	private Integer vehicleType;

	@Column(name="weight_bruto")
	private BigDecimal weightBruto;

	@Column(name="weight_netto")
	private BigDecimal weightNetto;

	public MobSpt() {
	}

	public int getSptNo() {
		return this.sptNo;
	}

	public void setSptNo(int sptNo) {
		this.sptNo = sptNo;
	}

	public String getAddressShipFrom() {
		return this.addressShipFrom;
	}

	public void setAddressShipFrom(String addressShipFrom) {
		this.addressShipFrom = addressShipFrom;
	}

	public String getAddressShipTo() {
		return this.addressShipTo;
	}

	public void setAddressShipTo(String addressShipTo) {
		this.addressShipTo = addressShipTo;
	}

	public String getContainerNo() {
		return this.containerNo;
	}

	public void setContainerNo(String containerNo) {
		this.containerNo = containerNo;
	}

	public String getCreated() {
		return this.created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCustCode() {
		return this.custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getCustName() {
		return this.custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public int getDriverId() {
		return this.driverId;
	}

	public void setDriverId(int driverId) {
		this.driverId = driverId;
	}

	public Boolean getIsTransit() {
		return this.isTransit;
	}

	public void setIsTransit(Boolean isTransit) {
		this.isTransit = isTransit;
	}

	public String getItemCode() {
		return this.itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return this.itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getKtpPicture() {
		return this.ktpPicture;
	}

	public void setKtpPicture(String ktpPicture) {
		this.ktpPicture = ktpPicture;
	}

	public String getModified() {
		return this.modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public BigDecimal getQty() {
		return this.qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public BigDecimal getQtySppb() {
		return this.qtySppb;
	}

	public void setQtySppb(BigDecimal qtySppb) {
		this.qtySppb = qtySppb;
	}

	public String getReferenceCode() {
		return this.referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public String getShipFrom() {
		return this.shipFrom;
	}

	public void setShipFrom(String shipFrom) {
		this.shipFrom = shipFrom;
	}

	public String getSjNo() {
		return this.sjNo;
	}

	public void setSjNo(String sjNo) {
		this.sjNo = sjNo;
	}

	public int getSppbKey() {
		return this.sppbKey;
	}

	public void setSppbKey(int sppbKey) {
		this.sppbKey = sppbKey;
	}

	public int getSppbNo() {
		return this.sppbNo;
	}

	public void setSppbNo(int sppbNo) {
		this.sppbNo = sppbNo;
	}

	public Date getSptDate() {
		return this.sptDate;
	}

	public void setSptDate(Date sptDate) {
		this.sptDate = sptDate;
	}

	public Date getSptExpiredDate() {
		return this.sptExpiredDate;
	}

	public void setSptExpiredDate(Date sptExpiredDate) {
		this.sptExpiredDate = sptExpiredDate;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTakeAssignmentBy() {
		return this.takeAssignmentBy;
	}

	public void setTakeAssignmentBy(String takeAssignmentBy) {
		this.takeAssignmentBy = takeAssignmentBy;
	}

	public Date getTakeAssignmentDate() {
		return this.takeAssignmentDate;
	}

	public void setTakeAssignmentDate(Date takeAssignmentDate) {
		this.takeAssignmentDate = takeAssignmentDate;
	}

	public BigDecimal getTakeAssignmentLatitude() {
		return this.takeAssignmentLatitude;
	}

	public void setTakeAssignmentLatitude(BigDecimal takeAssignmentLatitude) {
		this.takeAssignmentLatitude = takeAssignmentLatitude;
	}

	public BigDecimal getTakeAssignmentLongitude() {
		return this.takeAssignmentLongitude;
	}

	public void setTakeAssignmentLongitude(BigDecimal takeAssignmentLongitude) {
		this.takeAssignmentLongitude = takeAssignmentLongitude;
	}

	public String getTransportirCode() {
		return this.transportirCode;
	}

	public void setTransportirCode(String transportirCode) {
		this.transportirCode = transportirCode;
	}

//	public int getTransportirId() {
//		return this.transportirId;
//	}
//
//	public void setTransportirId(int transportirId) {
//		this.transportirId = transportirId;
//	}

	public String getTransportirName() {
		return this.transportirName;
	}

	public void setTransportirName(String transportirName) {
		this.transportirName = transportirName;
	}

	public String getVehicleNo() {
		return this.vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public BigDecimal getWeightBruto() {
		return this.weightBruto;
	}

	public void setWeightBruto(BigDecimal weightBruto) {
		this.weightBruto = weightBruto;
	}

	public BigDecimal getWeightNetto() {
		return this.weightNetto;
	}

	public void setWeightNetto(BigDecimal weightNetto) {
		this.weightNetto = weightNetto;
	}

	public Integer getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(Integer vehicleType) {
		this.vehicleType = vehicleType;
	}

}