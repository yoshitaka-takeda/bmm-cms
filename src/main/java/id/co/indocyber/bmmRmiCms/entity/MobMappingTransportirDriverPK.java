package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the mob_mapping_transportir_driver database table.
 * 
 */
@Embeddable
public class MobMappingTransportirDriverPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="transportir_code")
	private String transportirCode;

	@Column(name="driver_id")
	private int driverId;

	public MobMappingTransportirDriverPK() {
	}
	public int getDriverId() {
		return this.driverId;
	}
	public void setDriverId(int driverId) {
		this.driverId = driverId;
	}
	public String getTransportirCode() {
		return transportirCode;
	}
	public void setTransportirCode(String transportirCode) {
		this.transportirCode = transportirCode;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + driverId;
		result = prime * result + ((transportirCode == null) ? 0 : transportirCode.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MobMappingTransportirDriverPK other = (MobMappingTransportirDriverPK) obj;
		if (driverId != other.driverId)
			return false;
		if (transportirCode == null) {
			if (other.transportirCode != null)
				return false;
		} else if (!transportirCode.equals(other.transportirCode))
			return false;
		return true;
	}

}