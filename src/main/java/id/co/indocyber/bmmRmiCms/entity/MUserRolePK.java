package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;

public class MUserRolePK implements Serializable {
	private static final long serialVersionUID = -5778365524484767090L;

	private String email;	
	private Integer roleId;
	private String moduleApps;
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public String getModuleApps() {
		return moduleApps;
	}
	public void setModuleApps(String moduleApps) {
		this.moduleApps = moduleApps;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result
				+ ((moduleApps == null) ? 0 : moduleApps.hashCode());
		result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MUserRolePK other = (MUserRolePK) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (moduleApps == null) {
			if (other.moduleApps != null)
				return false;
		} else if (!moduleApps.equals(other.moduleApps))
			return false;
		if (roleId == null) {
			if (other.roleId != null)
				return false;
		} else if (!roleId.equals(other.roleId))
			return false;
		return true;
	}
}
