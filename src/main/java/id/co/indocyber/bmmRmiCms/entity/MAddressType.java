package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

@Entity
@Table(name = "m_address_type")
@NamedQuery(name = "MAddressType.findAll", query = "SELECT at FROM MAddressType at")
public class MAddressType implements Serializable {
	private static final long serialVersionUID = -5778365524484767090L;

	@Id
	@Column(name = "code")
	private String code;

	@Column(name = "short_text")
	private String shortText;

	@Column(name = "long_text")
	private String longText;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "processed_by")
	private String processedBy;

	@Column(name = "processed_date")
	private Timestamp processedDate;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getLongText() {
		return longText;
	}

	public void setLongText(String longText) {
		this.longText = longText;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getProcessedBy() {
		return processedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}

	public Timestamp getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(Timestamp processedDate) {
		this.processedDate = processedDate;
	}
}
