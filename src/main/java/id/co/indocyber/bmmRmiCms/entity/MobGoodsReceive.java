package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the mob_goods_receive database table.
 * 
 */
@Entity
@Table(name="mob_goods_receive")
@NamedQuery(name="MobGoodsReceive.findAll", query="SELECT m FROM MobGoodsReceive m")
public class MobGoodsReceive implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	private int id;

	private String created;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="destination_latitude")
	private BigDecimal destinationLatitude;

	@Column(name="destination_longitude")
	private BigDecimal destinationLongitude;

	@Column(name="driver_id")
	private int driverId;

	private String modified;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="modified_date")
	private Date modifiedDate;

	@Column(name="name_of_recipient")
	private String nameOfRecipient;

	@Lob
	private String notes;

	@Column(name="qty_receive")
	private BigDecimal qtyReceive;

	@Lob
	private String signature;

	@Column(name="sj_no")
	private int sjNo;

	@Column(name="vehicle_no")
	private String vehicleNo;

	@Column(name="vehicle_type")
	private int vehicleType;

	public MobGoodsReceive() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCreated() {
		return this.created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getDestinationLatitude() {
		return this.destinationLatitude;
	}

	public void setDestinationLatitude(BigDecimal destinationLatitude) {
		this.destinationLatitude = destinationLatitude;
	}

	public BigDecimal getDestinationLongitude() {
		return this.destinationLongitude;
	}

	public void setDestinationLongitude(BigDecimal destinationLongitude) {
		this.destinationLongitude = destinationLongitude;
	}

	public int getDriverId() {
		return this.driverId;
	}

	public void setDriverId(int driverId) {
		this.driverId = driverId;
	}

	public String getModified() {
		return this.modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getNameOfRecipient() {
		return this.nameOfRecipient;
	}

	public void setNameOfRecipient(String nameOfRecipient) {
		this.nameOfRecipient = nameOfRecipient;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public BigDecimal getQtyReceive() {
		return this.qtyReceive;
	}

	public void setQtyReceive(BigDecimal qtyReceive) {
		this.qtyReceive = qtyReceive;
	}

	public String getSignature() {
		return this.signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public int getSjNo() {
		return this.sjNo;
	}

	public void setSjNo(int sjNo) {
		this.sjNo = sjNo;
	}

	public String getVehicleNo() {
		return this.vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public int getVehicleType() {
		return this.vehicleType;
	}

	public void setVehicleType(int vehicleType) {
		this.vehicleType = vehicleType;
	}

}