package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the mob_transportir database table.
 * 
 */
@Entity
@Table(name="mob_transportir")
@NamedQuery(name="MobTransportir.findAll", query="SELECT m FROM MobTransportir m")
public class MobTransportir implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String code;

	@Column(name="area_code")
	private String areaCode;

	@Column(name="is_active")
	private Boolean isActive;

	private BigDecimal price;

	@Column(name="processed_by")
	private String processedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="processed_date")
	private Date processedDate;

	@Column(name="reference_code")
	private String referenceCode;

	@Column(name="transportir_name")
	private String transportirName;

	@Column(name="transportir_type")
	private String transportirType;

	@Column(name="vehicle_type")
	private String vehicleType;

	@Column(name="vendor_name")
	private String vendorName;

	public MobTransportir() {
	}

	public String getAreaCode() {
		return this.areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getProcessedBy() {
		return this.processedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}

	public Date getProcessedDate() {
		return this.processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	public String getReferenceCode() {
		return this.referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public String getTransportirName() {
		return this.transportirName;
	}

	public void setTransportirName(String transportirName) {
		this.transportirName = transportirName;
	}

	public String getTransportirType() {
		return this.transportirType;
	}

	public void setTransportirType(String transportirType) {
		this.transportirType = transportirType;
	}

	public String getVehicleType() {
		return this.vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getVendorName() {
		return this.vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}


}