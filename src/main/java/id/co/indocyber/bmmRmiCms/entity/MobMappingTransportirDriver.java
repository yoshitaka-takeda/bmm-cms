package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the mob_mapping_transportir_driver database table.
 * 
 */
@Entity
@Table(name="mob_mapping_transportir_driver")
@NamedQuery(name="MobMappingTransportirDriver.findAll", query="SELECT m FROM MobMappingTransportirDriver m")
@IdClass(MobMappingTransportirDriverPK.class)
public class MobMappingTransportirDriver implements Serializable {
	private static final long serialVersionUID = 1L;

//	@EmbeddedId
//	private MobMappingTransportirDriverPK id;
	
	@Id
	@Column(name="transportir_code")
	private String transportirCode;
	
	@Id
	@Column(name="driver_id")
	private int driverId;
	
	private String created;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	private String modified;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="modified_date")
	private Date modifiedDate;

	public MobMappingTransportirDriver() {
	}

	public String getCreated() {
		return this.created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModified() {
		return this.modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public int getDriverId() {
		return driverId;
	}

	public void setDriverId(int driverId) {
		this.driverId = driverId;
	}

	public String getTransportirCode() {
		return transportirCode;
	}

	public void setTransportirCode(String transportirCode) {
		this.transportirCode = transportirCode;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}