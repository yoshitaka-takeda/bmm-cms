package id.co.indocyber.bmmRmiCms.dto;

import java.math.BigDecimal;
import java.util.Date;

import org.zkoss.image.AImage;

public class MobGoodsReceiveDto {
	
	private int id;
	private String created;
	private Date createdDate;
	private BigDecimal destinationLatitude;
	private BigDecimal destinationLongitude;
	private int driverId;
	private String modified;
	private Date modifiedDate;
	private String nameOfRecipient;
	private String notes;
	private BigDecimal qtyReceive;
	private String signature;
	private int sjNo;
	private String vehicleNo;
	private int vehicleType;
	
	private String driverName;
	private String vehicleTypeText;
	private String phone;
	
	private AImage imgSignature;
	private byte[] defSignature;
	private byte[] imgSignatureByte;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public BigDecimal getDestinationLatitude() {
		return destinationLatitude;
	}
	public void setDestinationLatitude(BigDecimal destinationLatitude) {
		this.destinationLatitude = destinationLatitude;
	}
	public BigDecimal getDestinationLongitude() {
		return destinationLongitude;
	}
	public void setDestinationLongitude(BigDecimal destinationLongitude) {
		this.destinationLongitude = destinationLongitude;
	}
	public int getDriverId() {
		return driverId;
	}
	public void setDriverId(int driverId) {
		this.driverId = driverId;
	}
	public String getModified() {
		return modified;
	}
	public void setModified(String modified) {
		this.modified = modified;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getNameOfRecipient() {
		return nameOfRecipient;
	}
	public void setNameOfRecipient(String nameOfRecipient) {
		this.nameOfRecipient = nameOfRecipient;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public BigDecimal getQtyReceive() {
		return qtyReceive;
	}
	public void setQtyReceive(BigDecimal qtyReceive) {
		this.qtyReceive = qtyReceive;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public int getSjNo() {
		return sjNo;
	}
	public void setSjNo(int sjNo) {
		this.sjNo = sjNo;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public int getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(int vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getVehicleTypeText() {
		return vehicleTypeText;
	}
	public void setVehicleTypeText(String vehicleTypeText) {
		this.vehicleTypeText = vehicleTypeText;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public AImage getImgSignature() {
		return imgSignature;
	}
	public void setImgSignature(AImage imgSignature) {
		this.imgSignature = imgSignature;
	}
	public byte[] getDefSignature() {
		return defSignature;
	}
	public void setDefSignature(byte[] defSignature) {
		this.defSignature = defSignature;
	}
	public byte[] getImgSignatureByte() {
		return imgSignatureByte;
	}
	public void setImgSignatureByte(byte[] imgSignatureByte) {
		this.imgSignatureByte = imgSignatureByte;
	}
	
	

}
