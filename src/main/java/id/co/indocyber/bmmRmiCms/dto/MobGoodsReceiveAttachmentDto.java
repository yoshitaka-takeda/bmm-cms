package id.co.indocyber.bmmRmiCms.dto;

import java.util.Date;

import org.zkoss.image.AImage;
import org.zkoss.util.media.Media;

public class MobGoodsReceiveAttachmentDto {
	
	private int id;
	private String created;
	private Date createdDate;
	private String documentBlob;
	private String documentPath;
	private String notes;
	private int sjNo;
	
	private AImage attachment;
	private byte[] byteAttach;
	private Media media;
	private Media mediaSignature;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getDocumentBlob() {
		return documentBlob;
	}
	public void setDocumentBlob(String documentBlob) {
		this.documentBlob = documentBlob;
	}
	public String getDocumentPath() {
		return documentPath;
	}
	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public int getSjNo() {
		return sjNo;
	}
	public void setSjNo(int sjNo) {
		this.sjNo = sjNo;
	}
	public AImage getAttachment() {
		return attachment;
	}
	public void setAttachment(AImage attachment) {
		this.attachment = attachment;
	}
	public Media getMedia() {
		return media;
	}
	public void setMedia(Media media) {
		this.media = media;
	}
	public Media getMediaSignature() {
		return mediaSignature;
	}
	public void setMediaSignature(Media mediaSignature) {
		this.mediaSignature = mediaSignature;
	}
	public byte[] getByteAttach() {
		return byteAttach;
	}
	public void setByteAttach(byte[] byteAttach) {
		this.byteAttach = byteAttach;
	}
	
	
	
}
