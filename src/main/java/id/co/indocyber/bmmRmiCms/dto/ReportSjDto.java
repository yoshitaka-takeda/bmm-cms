package id.co.indocyber.bmmRmiCms.dto;

import java.util.Date;

public class ReportSjDto {
	
	private String sjNo;
	private Date sjDate,tglSpt,takeAssigmentDate,departingFromDate,firstDestinationDate,transitDate,destinationDate;
	private String custName;
	private String addressShipTo;
	private String itemName;
	private Double qty;
	private Double qtyReceive;
	private String nameOfRecipient;
	private String notes;
	private Integer noSppb;
	private Integer noSpt;
	private String noSpm;
	private String driverName,noPhone,noVehicle,noContainer,takeAssigmentLatLong,departingFromLatLong,
<<<<<<< HEAD
	firstDestinationLatLong,transitLatLong,destinationLatLong,status, transportirName;
	
=======
	firstDestinationLatLong,transitLatLong,destinationLatLong,status,transportirName;
>>>>>>> branch 'master' of https://gitlab.com/ari.haryanto/rmi-spt-sj.git
	
	public String getTransportirName() {
		return transportirName;
	}
	public void setTransportirName(String transportirName) {
		this.transportirName = transportirName;
	}
	public Date getTakeAssigmentDate() {
		return takeAssigmentDate;
	}
	public void setTakeAssigmentDate(Date takeAssigmentDate) {
		this.takeAssigmentDate = takeAssigmentDate;
	}
	public String getTakeAssigmentLatLong() {
		return takeAssigmentLatLong;
	}
	public void setTakeAssigmentLatLong(String takeAssigmentLatLong) {
		this.takeAssigmentLatLong = takeAssigmentLatLong;
	}
	public String getSjNo() {
		return sjNo;
	}
	public void setSjNo(String sjNo) {
		this.sjNo = sjNo;
	}
	public Date getSjDate() {
		return sjDate;
	}
	public void setSjDate(Date sjDate) {
		this.sjDate = sjDate;
	}
	public Date getTglSpt() {
		return tglSpt;
	}
	public void setTglSpt(Date tglSpt) {
		this.tglSpt = tglSpt;
	}
	public Date getDepartingFromDate() {
		return departingFromDate;
	}
	public void setDepartingFromDate(Date departingFromDate) {
		this.departingFromDate = departingFromDate;
	}
	public Date getFirstDestinationDate() {
		return firstDestinationDate;
	}
	public void setFirstDestinationDate(Date firstDestinationDate) {
		this.firstDestinationDate = firstDestinationDate;
	}
	public Date getTransitDate() {
		return transitDate;
	}
	public void setTransitDate(Date transitDate) {
		this.transitDate = transitDate;
	}
	public Date getDestinationDate() {
		return destinationDate;
	}
	public void setDestinationDate(Date destinationDate) {
		this.destinationDate = destinationDate;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getAddressShipTo() {
		return addressShipTo;
	}
	public void setAddressShipTo(String addressShipTo) {
		this.addressShipTo = addressShipTo;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getQtyReceive() {
		return qtyReceive;
	}
	public void setQtyReceive(Double qtyReceive) {
		this.qtyReceive = qtyReceive;
	}
	public String getNameOfRecipient() {
		return nameOfRecipient;
	}
	public void setNameOfRecipient(String nameOfRecipient) {
		this.nameOfRecipient = nameOfRecipient;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Integer getNoSppb() {
		return noSppb;
	}
	public void setNoSppb(Integer noSppb) {
		this.noSppb = noSppb;
	}
	public Integer getNoSpt() {
		return noSpt;
	}
	public void setNoSpt(Integer noSpt) {
		this.noSpt = noSpt;
	}
	public String getNoSpm() {
		return noSpm;
	}
	public void setNoSpm(String noSpm) {
		this.noSpm = noSpm;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getNoPhone() {
		return noPhone;
	}
	public void setNoPhone(String noPhone) {
		this.noPhone = noPhone;
	}
	public String getNoVehicle() {
		return noVehicle;
	}
	public void setNoVehicle(String noVehicle) {
		this.noVehicle = noVehicle;
	}
	public String getNoContainer() {
		return noContainer;
	}
	public void setNoContainer(String noContainer) {
		this.noContainer = noContainer;
	}
	public String getDepartingFromLatLong() {
		return departingFromLatLong;
	}
	public void setDepartingFromLatLong(String departingFromLatLong) {
		this.departingFromLatLong = departingFromLatLong;
	}

	public String getFirstDestinationLatLong() {
		return firstDestinationLatLong;
	}
	public void setFirstDestinationLatLong(String firstDestinationLatLong) {
		this.firstDestinationLatLong = firstDestinationLatLong;
	}
	public String getTransitLatLong() {
		return transitLatLong;
	}
	public void setTransitLatLong(String transitLatLong) {
		this.transitLatLong = transitLatLong;
	}
	public String getDestinationLatLong() {
		return destinationLatLong;
	}
	public void setDestinationLatLong(String destinationLatLong) {
		this.destinationLatLong = destinationLatLong;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
