package id.co.indocyber.bmmRmiCms.dto;

import java.sql.Timestamp;

public class MMenuDto {
	
	private String menuCode;
	private String menuName;
	private String menuDisplayName;
	private String parentMenuCode;
	private Integer position;
	private String description;
	private String menuLink;
	private String menuAction;
	private String menuLogo;
	private String appType;
	private Boolean isActive;
	private String created;
	private Timestamp createdDate;
	private String modified;
	private Timestamp modifiedDate;
	
	private Boolean isView = false;
	private Boolean isAdd = false;
	private Boolean isEdit = false;
	private Boolean isDelet = false;
	private Boolean isImport = false;
	private Boolean isExport = false;
	private Boolean isDownload = false;
	private Boolean isPrint = false;
	
	public String getMenuCode() {
		return menuCode;
	}
	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getMenuDisplayName() {
		return menuDisplayName;
	}
	public void setMenuDisplayName(String menuDisplayName) {
		this.menuDisplayName = menuDisplayName;
	}
	public String getParentMenuCode() {
		return parentMenuCode;
	}
	public void setParentMenuCode(String parentMenuCode) {
		this.parentMenuCode = parentMenuCode;
	}
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMenuLink() {
		return menuLink;
	}
	public void setMenuLink(String menuLink) {
		this.menuLink = menuLink;
	}
	public String getMenuAction() {
		return menuAction;
	}
	public void setMenuAction(String menuAction) {
		this.menuAction = menuAction;
	}
	public String getMenuLogo() {
		return menuLogo;
	}
	public void setMenuLogo(String menuLogo) {
		this.menuLogo = menuLogo;
	}
	public String getAppType() {
		return appType;
	}
	public void setAppType(String appType) {
		this.appType = appType;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public String getModified() {
		return modified;
	}
	public void setModified(String modified) {
		this.modified = modified;
	}
	public Timestamp getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public Boolean getIsView() {
		return isView;
	}
	public void setIsView(Boolean isView) {
		this.isView = isView;
	}
	public Boolean getIsAdd() {
		return isAdd;
	}
	public void setIsAdd(Boolean isAdd) {
		this.isAdd = isAdd;
	}
	public Boolean getIsEdit() {
		return isEdit;
	}
	public void setIsEdit(Boolean isEdit) {
		this.isEdit = isEdit;
	}
	public Boolean getIsDelet() {
		return isDelet;
	}
	public void setIsDelet(Boolean isDelet) {
		this.isDelet = isDelet;
	}
	public Boolean getIsImport() {
		return isImport;
	}
	public void setIsImport(Boolean isImport) {
		this.isImport = isImport;
	}
	public Boolean getIsExport() {
		return isExport;
	}
	public void setIsExport(Boolean isExport) {
		this.isExport = isExport;
	}
	public Boolean getIsDownload() {
		return isDownload;
	}
	public void setIsDownload(Boolean isDownload) {
		this.isDownload = isDownload;
	}
	public Boolean getIsPrint() {
		return isPrint;
	}
	public void setIsPrint(Boolean isPrint) {
		this.isPrint = isPrint;
	}


}
