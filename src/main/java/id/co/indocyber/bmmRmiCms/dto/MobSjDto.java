package id.co.indocyber.bmmRmiCms.dto;

import java.math.BigDecimal;
import java.util.Date;

public class MobSjDto {
	
	private Integer sjNo;
	private String addressShipFrom;
	private String addressShipTo;
	private String containerNo;
	private String created;
	private Date createdDate;
	private String custCode;
	private String custName;
	private String departingFromBy;
	private Date departingFromDate;
	private BigDecimal departingFromLatitude;
	private BigDecimal departingFromLongitude;
	private String destinationBy;
	private Date destinationDate;
	private String destinationFromBy;
	private Date destinationFromDate;
	private BigDecimal destinationLatitude;
	private BigDecimal destinationLongitude;
	private Integer driverId;
	private String driverName;
	private String firstDestinationBy;
	private Date firstDestinationDate;
	private BigDecimal firstDestinationLatitude;
	private BigDecimal firstDestinationLongitude;
	private Boolean isTransit;
	private String itemCode;
	private String itemName;
	private String modified;
	private Date modifiedDate;
	private String phone;
	private String pin;
	private BigDecimal qty;
	private BigDecimal qtySppb;
	private String segel1;
	private String segel2;
	private String segel3;
	private String segel4;
	private String segel5;
	private String segel6;
	private String segel7;
	private String segel8;
	private Date sjDate;
	private String spmNo;
	private Integer sptNo;
	private String status;
	private String transitBy;
	private Date transitDate;
	private BigDecimal transitLatitude;
	private BigDecimal transitLongitude;
	private String transportirCode;
	private Integer transportirId;
	private String transportirName;
	private String vehicleNo;
	private Integer vehicleType;

	private String statusText;
	private String vehicleTypeText;
	private Boolean isTransitSpt = false;
	
	private Boolean flagVisiblePenugasan;
	private Boolean flagVisibleKonfirmasi;
	
	private String departingFromLatitudeLongitude;
	private String firstDestinationLatitudeLongitude;
	private String transitLatitudeLongitude;
	private String destinationLatitudeLongitude;
	
	public MobSjDto() {
		// TODO Auto-generated constructor stub
	}

	public Integer getSjNo() {
		return sjNo;
	}

	public void setSjNo(Integer sjNo) {
		this.sjNo = sjNo;
	}

	public String getAddressShipFrom() {
		return addressShipFrom;
	}

	public void setAddressShipFrom(String addressShipFrom) {
		this.addressShipFrom = addressShipFrom;
	}

	public String getAddressShipTo() {
		return addressShipTo;
	}

	public void setAddressShipTo(String addressShipTo) {
		this.addressShipTo = addressShipTo;
	}

	public String getContainerNo() {
		return containerNo;
	}

	public void setContainerNo(String containerNo) {
		this.containerNo = containerNo;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getDepartingFromBy() {
		return departingFromBy;
	}

	public void setDepartingFromBy(String departingFromBy) {
		this.departingFromBy = departingFromBy;
	}

	public Date getDepartingFromDate() {
		return departingFromDate;
	}

	public void setDepartingFromDate(Date departingFromDate) {
		this.departingFromDate = departingFromDate;
	}

	public BigDecimal getDepartingFromLatitude() {
		return departingFromLatitude;
	}

	public void setDepartingFromLatitude(BigDecimal departingFromLatitude) {
		this.departingFromLatitude = departingFromLatitude;
	}

	public BigDecimal getDepartingFromLongitude() {
		return departingFromLongitude;
	}

	public void setDepartingFromLongitude(BigDecimal departingFromLongitude) {
		this.departingFromLongitude = departingFromLongitude;
	}

	public String getDestinationBy() {
		return destinationBy;
	}

	public void setDestinationBy(String destinationBy) {
		this.destinationBy = destinationBy;
	}

	public Date getDestinationDate() {
		return destinationDate;
	}

	public void setDestinationDate(Date destinationDate) {
		this.destinationDate = destinationDate;
	}

	public String getDestinationFromBy() {
		return destinationFromBy;
	}

	public void setDestinationFromBy(String destinationFromBy) {
		this.destinationFromBy = destinationFromBy;
	}

	public Date getDestinationFromDate() {
		return destinationFromDate;
	}

	public void setDestinationFromDate(Date destinationFromDate) {
		this.destinationFromDate = destinationFromDate;
	}

	public BigDecimal getDestinationLatitude() {
		return destinationLatitude;
	}

	public void setDestinationLatitude(BigDecimal destinationLatitude) {
		this.destinationLatitude = destinationLatitude;
	}

	public BigDecimal getDestinationLongitude() {
		return destinationLongitude;
	}

	public void setDestinationLongitude(BigDecimal destinationLongitude) {
		this.destinationLongitude = destinationLongitude;
	}

	public Integer getDriverId() {
		return driverId;
	}

	public void setDriverId(Integer driverId) {
		this.driverId = driverId;
	}

	public String getFirstDestinationBy() {
		return firstDestinationBy;
	}

	public void setFirstDestinationBy(String firstDestinationBy) {
		this.firstDestinationBy = firstDestinationBy;
	}

	public Date getFirstDestinationDate() {
		return firstDestinationDate;
	}

	public void setFirstDestinationDate(Date firstDestinationDate) {
		this.firstDestinationDate = firstDestinationDate;
	}

	public BigDecimal getFirstDestinationLatitude() {
		return firstDestinationLatitude;
	}

	public void setFirstDestinationLatitude(BigDecimal firstDestinationLatitude) {
		this.firstDestinationLatitude = firstDestinationLatitude;
	}

	public BigDecimal getFirstDestinationLongitude() {
		return firstDestinationLongitude;
	}

	public void setFirstDestinationLongitude(BigDecimal firstDestinationLongitude) {
		this.firstDestinationLongitude = firstDestinationLongitude;
	}
	
	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getModified() {
		return modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public BigDecimal getQty() {
		return qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public BigDecimal getQtySppb() {
		return qtySppb;
	}

	public void setQtySppb(BigDecimal qtySppb) {
		this.qtySppb = qtySppb;
	}

	public String getSegel1() {
		return segel1;
	}

	public void setSegel1(String segel1) {
		this.segel1 = segel1;
	}

	public String getSegel2() {
		return segel2;
	}

	public void setSegel2(String segel2) {
		this.segel2 = segel2;
	}

	public String getSegel3() {
		return segel3;
	}

	public void setSegel3(String segel3) {
		this.segel3 = segel3;
	}

	public String getSegel4() {
		return segel4;
	}

	public void setSegel4(String segel4) {
		this.segel4 = segel4;
	}

	public String getSegel5() {
		return segel5;
	}

	public void setSegel5(String segel5) {
		this.segel5 = segel5;
	}

	public String getSegel6() {
		return segel6;
	}

	public void setSegel6(String segel6) {
		this.segel6 = segel6;
	}

	public String getSegel7() {
		return segel7;
	}

	public void setSegel7(String segel7) {
		this.segel7 = segel7;
	}

	public String getSegel8() {
		return segel8;
	}

	public void setSegel8(String segel8) {
		this.segel8 = segel8;
	}

	public String getSpmNo() {
		return spmNo;
	}

	public void setSpmNo(String spmNo) {
		this.spmNo = spmNo;
	}

	public Integer getSptNo() {
		return sptNo;
	}

	public void setSptNo(Integer sptNo) {
		this.sptNo = sptNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTransitBy() {
		return transitBy;
	}

	public void setTransitBy(String transitBy) {
		this.transitBy = transitBy;
	}

	public Date getTransitDate() {
		return transitDate;
	}

	public void setTransitDate(Date transitDate) {
		this.transitDate = transitDate;
	}

	public BigDecimal getTransitLatitude() {
		return transitLatitude;
	}

	public void setTransitLatitude(BigDecimal transitLatitude) {
		this.transitLatitude = transitLatitude;
	}

	public BigDecimal getTransitLongitude() {
		return transitLongitude;
	}

	public void setTransitLongitude(BigDecimal transitLongitude) {
		this.transitLongitude = transitLongitude;
	}

	public Integer getTransportirId() {
		return transportirId;
	}

	public void setTransportirId(Integer transportirId) {
		this.transportirId = transportirId;
	}

	public String getTransportirName() {
		return transportirName;
	}

	public void setTransportirName(String transportirName) {
		this.transportirName = transportirName;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public Integer getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(Integer vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getStatusText() {
		return statusText;
	}

	public void setStatusText(String statusText) {
		this.statusText = statusText;
	}

	public Boolean getIsTransit() {
		return isTransit;
	}

	public void setIsTransit(Boolean isTransit) {
		this.isTransit = isTransit;
	}

	public Date getSjDate() {
		return sjDate;
	}

	public void setSjDate(Date sjDate) {
		this.sjDate = sjDate;
	}

	public String getTransportirCode() {
		return transportirCode;
	}

	public void setTransportirCode(String transportirCode) {
		this.transportirCode = transportirCode;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getVehicleTypeText() {
		return vehicleTypeText;
	}

	public void setVehicleTypeText(String vehicleTypeText) {
		this.vehicleTypeText = vehicleTypeText;
	}

	public Boolean getIsTransitSpt() {
		return isTransitSpt;
	}

	public void setIsTransitSpt(Boolean isTransitSpt) {
		this.isTransitSpt = isTransitSpt;
	}

	public Boolean getFlagVisiblePenugasan() {
		return flagVisiblePenugasan;
	}

	public void setFlagVisiblePenugasan(Boolean flagVisiblePenugasan) {
		this.flagVisiblePenugasan = flagVisiblePenugasan;
	}

	public Boolean getFlagVisibleKonfirmasi() {
		return flagVisibleKonfirmasi;
	}

	public void setFlagVisibleKonfirmasi(Boolean flagVisibleKonfirmasi) {
		this.flagVisibleKonfirmasi = flagVisibleKonfirmasi;
	}

	public String getDepartingFromLatitudeLongitude() {
		return departingFromLatitudeLongitude;
	}

	public void setDepartingFromLatitudeLongitude(String departingFromLatitudeLongitude) {
		this.departingFromLatitudeLongitude = departingFromLatitudeLongitude;
	}

	public String getFirstDestinationLatitudeLongitude() {
		return firstDestinationLatitudeLongitude;
	}

	public void setFirstDestinationLatitudeLongitude(String firstDestinationLatitudeLongitude) {
		this.firstDestinationLatitudeLongitude = firstDestinationLatitudeLongitude;
	}

	public String getTransitLatitudeLongitude() {
		return transitLatitudeLongitude;
	}

	public void setTransitLatitudeLongitude(String transitLatitudeLongitude) {
		this.transitLatitudeLongitude = transitLatitudeLongitude;
	}

	public String getDestinationLatitudeLongitude() {
		return destinationLatitudeLongitude;
	}

	public void setDestinationLatitudeLongitude(String destinationLatitudeLongitude) {
		this.destinationLatitudeLongitude = destinationLatitudeLongitude;
	}
	
	
	
}
