package id.co.indocyber.bmmRmiCms.dto;

import java.sql.Timestamp;

public class MMaritalStatusDto {

	private String code;
	private String shortText;
	private String longText;
	private Boolean isActive;
	private String processedBy;
	private Timestamp processedDate;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getShortText() {
		return shortText;
	}
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}
	public String getLongText() {
		return longText;
	}
	public void setLongText(String longText) {
		this.longText = longText;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getProcessedBy() {
		return processedBy;
	}
	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}
	public Timestamp getProcessedDate() {
		return processedDate;
	}
	public void setProcessedDate(Timestamp processedDate) {
		this.processedDate = processedDate;
	}
	
	
	
}
