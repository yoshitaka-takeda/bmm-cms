package id.co.indocyber.bmmRmiCms.dto;

import id.co.indocyber.bmmRmiCms.entity.MMenu;
import id.co.indocyber.bmmRmiCms.entity.MMenuRole;

import java.io.Serializable;
import java.sql.Timestamp;

public class MMenuRoleDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -172377259929140792L;
	private Integer id;
	private Integer roleId;
	private String module;
	private String menuName;
	private String menuCode;
	private String parentMenuCode;
	private String menuLink;
	private Boolean isView = false;
	private Boolean isAdd = false; 
	private Boolean isEdit = false;
	private Boolean isDelete = false;
	private Boolean isImport = false;
	private Boolean isExport = false;
	private Boolean isDownload = false;
	private Boolean isPrint = false;
	private Boolean isActive = false;
	private String created;
	private Timestamp createdDate;
	private String modified;
	private Timestamp modifiedDate;
	
	private String roleName;
	private String email;
	private String fullname;
	private String roleDescription;
	
	public MMenuRoleDto(MMenuRole mMenuRole, MMenu mMenu) {
		this.created = mMenuRole.getCreated();
		this.createdDate = mMenuRole.getCreatedDate();
		this.id = mMenuRole.getId();
		this.isActive = mMenuRole.getIsActive();
		this.isAdd = mMenuRole.getIsAdd();
		this.isDelete = mMenuRole.getIsDelete();
		this.isDownload = mMenuRole.getIsDownload();
		this.isEdit = mMenuRole.getIsEdit();
		this.isExport = mMenuRole.getIsExport();
		this.isImport = mMenuRole.getIsImport();
		this.isPrint = mMenuRole.getIsPrint();
		this.isView = mMenuRole.getIsView();
		this.menuCode = mMenuRole.getMenuCode();
		this.menuLink = mMenu.getMenuLink();
		this.menuName = mMenu.getMenuDisplayName();
		this.modified = mMenu.getModified();
		this.modifiedDate = mMenu.getModifiedDate();
		this.parentMenuCode = mMenu.getParentMenuCode();
		this.roleId = mMenuRole.getRoleId();
		this.module = mMenu.getAppType();
	}
	
	public MMenuRoleDto(){
		
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getMenuCode() {
		return menuCode;
	}
	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}
	public Boolean getIsView() {
		return isView;
	}
	public void setIsView(Boolean isView) {
		this.isView = isView;
	}
	public Boolean getIsAdd() {
		return isAdd;
	}
	public void setIsAdd(Boolean isAdd) {
		this.isAdd = isAdd;
	}
	public Boolean getIsEdit() {
		return isEdit;
	}
	public void setIsEdit(Boolean isEdit) {
		this.isEdit = isEdit;
	}
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	public Boolean getIsImport() {
		return isImport;
	}
	public void setIsImport(Boolean isImport) {
		this.isImport = isImport;
	}
	public Boolean getIsExport() {
		return isExport;
	}
	public void setIsExport(Boolean isExport) {
		this.isExport = isExport;
	}
	public Boolean getIsDownload() {
		return isDownload;
	}
	public void setIsDownload(Boolean isDownload) {
		this.isDownload = isDownload;
	}
	public Boolean getIsPrint() {
		return isPrint;
	}
	public void setIsPrint(Boolean isPrint) {
		this.isPrint = isPrint;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public String getModified() {
		return modified;
	}
	public void setModified(String modified) {
		this.modified = modified;
	}
	public Timestamp getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getParentMenuCode() {
		return parentMenuCode;
	}
	public void setParentMenuCode(String parentMenuCode) {
		this.parentMenuCode = parentMenuCode;
	}
	public String getMenuLink() {
		return menuLink;
	}
	public void setMenuLink(String menuLink) {
		this.menuLink = menuLink;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
		
	public boolean calculateAccess() {
		return this.isAdd||this.isEdit||this.isDelete||this.isView||this.isDownload||this.isExport
				||this.isImport||this.isPrint;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getRoleDescription() {
		return roleDescription;
	}

	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}

	
	
}
