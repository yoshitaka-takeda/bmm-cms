package id.co.indocyber.bmmRmiCms.dto;

import java.util.Date;

public class MobMappingTransportirDriverDto {
	
	
	private String transportirCode;
	private int driverId;
	private String created;
	private Date createdDate;
	private Boolean isActive;
	private String modified;
	private Date modifiedDate;
			
	private String transportirName;
	private String driverName;
	private String driverPhone;
	private String driverGender;
	private Date driverBirthday;
	private Integer driverAge;
	
	public int getDriverId() {
		return driverId;
	}
	public void setDriverId(int driverId) {
		this.driverId = driverId;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getModified() {
		return modified;
	}
	public void setModified(String modified) {
		this.modified = modified;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	public String getTransportirName() {
		return transportirName;
	}

	public void setTransportirName(String transportirName) {
		this.transportirName = transportirName;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getDriverPhone() {
		return driverPhone;
	}

	public void setDriverPhone(String driverPhone) {
		this.driverPhone = driverPhone;
	}

	public String getDriverGender() {
		return driverGender;
	}

	public void setDriverGender(String driverGender) {
		this.driverGender = driverGender;
	}

	public Integer getDriverAge() {
		return driverAge;
	}

	public void setDriverAge(Integer driverAge) {
		this.driverAge = driverAge;
	}

	public Date getDriverBirthday() {
		return driverBirthday;
	}

	public void setDriverBirthday(Date driverBirthday) {
		this.driverBirthday = driverBirthday;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getTransportirCode() {
		return transportirCode;
	}
	public void setTransportirCode(String transportirCode) {
		this.transportirCode = transportirCode;
	}
	
	


}
