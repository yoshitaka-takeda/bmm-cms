package id.co.indocyber.bmmRmiCms.dto;

import id.co.indocyber.bmmRmiCms.entity.MUser;
import id.co.indocyber.bmmRmiCms.entity.MobEmployee;
import id.co.indocyber.bmmRmiCms.entity.UserMenuAccordion;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public class MUserDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7352023155851094248L;
	private List<UserMenuAccordion> menuList;
	private List<MMenuRoleDto> rolePrivs;
	private String email;
	private String username;
	private String fullname;
	private String photoText;
	private String password;
	private String pswdDefault;
	private String nipeg;
	private Integer personId;
	private MobEmployee emp;
	private String phoneNum;
	private String imei;
	private String referenceCode;
	private Boolean isActive;
	private String createdBy;
	private Timestamp createdDate;
	private String modifiedBy;
	private Timestamp modifiedDate;
	private String roleName;
	private boolean rehash;
	private byte[] photo;
	
	private boolean isAdministrator;
	private boolean isCustomer;
	private Integer roleId;
	
	private String isActiveText;
	private String moduleApps;
	
	private String transportirCode;
	private String transportirName;
	
	private boolean addMode = false;
	
	public List<UserMenuAccordion> getMenuList() {
		return menuList;
	}
	public void setMenuList(List<UserMenuAccordion> menuList) {
		this.menuList = menuList;
	}
	public List<MMenuRoleDto> getRolePrivs() {
		return rolePrivs;
	}
	public void setRolePrivs(List<MMenuRoleDto> rolePrivs) {
		this.rolePrivs = rolePrivs;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getPhotoText() {
		return photoText;
	}
	public void setPhotoText(String photoText) {
		this.photoText = photoText;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNipeg() {
		return nipeg;
	}
	public void setNipeg(String nipeg) {
		this.nipeg = nipeg;
	}
	public Integer getPersonId() {
		return personId;
	}
	public void setPersonId(Integer personId) {
		this.personId = personId;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getReferenceCode() {
		return referenceCode;
	}
	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Timestamp getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public boolean isRehash() {
		return rehash;
	}
	public void setRehash(boolean rehash) {
		this.rehash = rehash;
	}
	public MobEmployee getEmp() {
		return emp;
	}
	public void setEmp(MobEmployee emp) {
		this.emp = emp;
	}
	public String getPswdDefault() {
		return pswdDefault;
	}
	public void setPswdDefault(String pswdDefault) {
		this.pswdDefault = pswdDefault;
	}
	public byte[] getPhoto() {
		return photo;
	}
	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}
	public String getIsActiveText() {
		return isActiveText;
	}
	public void setIsActiveText(String isActiveText) {
		this.isActiveText = isActiveText;
	}
	public MUserDto (MUser user){
		this.createdBy = user.getCreatedBy();
		this.createdDate = user.getCreatedDate();
		this.email = user.getEmail();
		this.fullname = user.getFullname();
		this.imei = user.getImei();
		this.isActive = user.getIsActive();
		this.modifiedBy = user.getModifiedBy();
		this.modifiedDate = user.getModifiedDate();
		this.nipeg = user.getNipeg();
		this.password = user.getPassword();
		this.personId = user.getPersonId();
		this.phoneNum = user.getPhoneNum();
		this.photoText = user.getPhotoText();
		this.pswdDefault = user.getPswdDefault();
		this.referenceCode = user.getReferenceCode();
		this.username = user.getUsername();
		this.rehash = false;
	}
	
	public MUserDto() {
		this.rehash = true;
		this.isActive = true;
	}
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public boolean isAdministrator() {
		return isAdministrator;
	}
	public void setAdministrator(boolean isAdministrator) {
		this.isAdministrator = isAdministrator;
	}
	public String getModuleApps() {
		return moduleApps;
	}
	public void setModuleApps(String moduleApps) {
		this.moduleApps = moduleApps;
	}
	public String getTransportirName() {
		return transportirName;
	}
	public void setTransportirName(String transportirName) {
		this.transportirName = transportirName;
	}
	public String getTransportirCode() {
		return transportirCode;
	}
	public void setTransportirCode(String transportirCode) {
		this.transportirCode = transportirCode;
	}
	public boolean isAddMode() {
		return addMode;
	}
	public void setAddMode(boolean addMode) {
		this.addMode = addMode;
	}
	public boolean isCustomer() {
		return isCustomer;
	}
	public void setCustomer(boolean isCustomer) {
		this.isCustomer = isCustomer;
	}
	
	
}