package id.co.indocyber.bmmRmiCms.dto;

import id.co.indocyber.bmmRmiCms.entity.MAddressType;
import id.co.indocyber.bmmRmiCms.entity.MEducationType;
import id.co.indocyber.bmmRmiCms.entity.MGender;
import id.co.indocyber.bmmRmiCms.entity.MMaritalStatus;
import id.co.indocyber.bmmRmiCms.entity.MobDriver;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.Period;

public class MobDriverDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3414311527774827535L;
	private Integer id;
	private String fullname;
	private Date birthday;
	private Integer age;
	private String placeOfBirthday;
	private String addressType;
	private String address;
	private String rtrw;
	private String village;
	private String subDistrict;
	private String district;
	private String city;
	private String province;
	private String zipcode;
	private String phoneNum;
	private String email;
	private String maritalStatus;
	private String gender;
	
	private String maritalStatusText;
	
	private boolean genderMale;
	private boolean genderFemale; 
	
	private String noKtp;
	private String referenceCode;
	private Boolean isActive;
	private String createdBy;
	private Timestamp createdDate;
	private String modifiedBy;
	private Timestamp modifiedDate;
	private String processedBy;
	private Timestamp processedDate;
	
	private String religion;
	private String religionText;	
	private String lastEducationText;
	
	private String lastEducation;	
	private String driverType;

	private boolean driverPetani;
	private boolean driverTransportir;
	
	private String namaTransportir;
	
	public boolean isDriverPetani() {
		return driverPetani;
	}

	public void setDriverPetani(boolean driverPetani) {
		this.driverPetani = driverPetani;
	}

	public boolean isDriverTransportir() {
		return driverTransportir;
	}

	public void setDriverTransportir(boolean driverTransportir) {
		this.driverTransportir = driverTransportir;
	}

	public MobDriverDto(){
		
	}

	public void reCalculateAge(){
		if(this.birthday!=null){
			this.age = Period.between(this.birthday.toLocalDate(), LocalDate.now()).getYears();
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getPlaceOfBirthday() {
		return placeOfBirthday;
	}

	public void setPlaceOfBirthday(String placeOfBirthday) {
		this.placeOfBirthday = placeOfBirthday;
	}


	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getRtrw() {
		return rtrw;
	}

	public void setRtrw(String rtrw) {
		this.rtrw = rtrw;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public String getSubDistrict() {
		return subDistrict;
	}

	public void setSubDistrict(String subDistrict) {
		this.subDistrict = subDistrict;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getReferenceCode() {
		return referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getProcessedBy() {
		return processedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}

	public Timestamp getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(Timestamp processedDate) {
		this.processedDate = processedDate;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getLastEducation() {
		return lastEducation;
	}

	public void setLastEducation(String lastEducation) {
		this.lastEducation = lastEducation;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public boolean isGenderMale() {
		boolean cek;
		if(getGender() != null){
			if(getGender().equals("1")){
				cek = true;
			}else{
				cek = false;
			}		
		}else{
			cek = false;
		}
		return cek;
	}

	public void setGenderMale(boolean genderMale) {
		this.genderMale = genderMale;
	}

	public boolean isGenderFemale() {
		boolean cek;
		if(getGender() != null){
			if(getGender().equals("2")){
				cek = true;
			}else{
				cek = false;
			}
		}else{
			cek = false;
		}
		return cek;
	}

	public void setGenderFemale(boolean genderFemale) {
		this.genderFemale = genderFemale;
	}

	public String getMaritalStatusText() {
		return maritalStatusText;
	}

	public void setMaritalStatusText(String maritalStatusText) {
		this.maritalStatusText = maritalStatusText;
	}

	public String getReligionText() {
		return religionText;
	}

	public void setReligionText(String religionText) {
		this.religionText = religionText;
	}

	public String getLastEducationText() {
		return lastEducationText;
	}

	public void setLastEducationText(String lastEducationText) {
		this.lastEducationText = lastEducationText;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getNoKtp() {
		return noKtp;
	}

	public void setNoKtp(String noKtp) {
		this.noKtp = noKtp;
	}

	public String getDriverType() {
		return driverType;
	}

	public void setDriverType(String driverType) {
		this.driverType = driverType;
	}

	public String getNamaTransportir() {
		return namaTransportir;
	}

	public void setNamaTransportir(String namaTransportir) {
		this.namaTransportir = namaTransportir;
	}

}