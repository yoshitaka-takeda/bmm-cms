package id.co.indocyber.bmmRmiCms.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


public class MobVehicleDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1441542878562539618L;
	private String vehicleNo;
	private BigDecimal capacity;
	private String created;
	private Date createdDate;
	private Boolean isActive;
	private String modified;
	private Date modifiedDate;
	private String productionYear;
	private int vehicleType;
	
	private String capacityText;
	
	private String vehicleTypeText;
	
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public BigDecimal getCapacity() {
		return capacity;
	}
	public void setCapacity(BigDecimal capacity) {
		this.capacity = capacity;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getModified() {
		return modified;
	}
	public void setModified(String modified) {
		this.modified = modified;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getProductionYear() {
		return productionYear;
	}
	public void setProductionYear(String productionYear) {
		this.productionYear = productionYear;
	}
	public int getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(int vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getVehicleTypeText() {
		return vehicleTypeText;
	}
	public void setVehicleTypeText(String vehicleTypeText) {
		this.vehicleTypeText = vehicleTypeText;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getCapacityText() {
		return capacityText;
	}
	public void setCapacityText(String capacityText) {
		this.capacityText = capacityText;
	}
	
	
	
}
