package id.co.indocyber.bmmRmiCms.dto;

import java.math.BigDecimal;
import java.util.Date;

public class MobTransportirDto {
	
	private String code;
	private String areaCode;
	private Boolean isActive;
	private BigDecimal price;
	private String processedBy;
	private Date processedDate;
	private String referenceCode;
	private String transportirName;
	private String transportirType;
	private String vehicleType;
	private String vendorName;
		
	private String vehicleTypeText;
	
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public String getProcessedBy() {
		return processedBy;
	}
	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}
	public Date getProcessedDate() {
		return processedDate;
	}
	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}
	public String getReferenceCode() {
		return referenceCode;
	}
	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}
	public String getTransportirName() {
		return transportirName;
	}
	public void setTransportirName(String transportirName) {
		this.transportirName = transportirName;
	}
	public String getTransportirType() {
		return transportirType;
	}
	public void setTransportirType(String transportirType) {
		this.transportirType = transportirType;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getVehicleTypeText() {
		return vehicleTypeText;
	}
	public void setVehicleTypeText(String vehicleTypeText) {
		this.vehicleTypeText = vehicleTypeText;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}


}
