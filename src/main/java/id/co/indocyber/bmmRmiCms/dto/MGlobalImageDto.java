package id.co.indocyber.bmmRmiCms.dto;

import java.util.Date;

public class MGlobalImageDto {
	
	private String parmImgCode;
	private String createdBy;
	private Date createdDate;
	private byte[] parmImgValue;
	public String getParmImgCode() {
		return parmImgCode;
	}
	public void setParmImgCode(String parmImgCode) {
		this.parmImgCode = parmImgCode;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public byte[] getParmImgValue() {
		return parmImgValue;
	}
	public void setParmImgValue(byte[] parmImgValue) {
		this.parmImgValue = parmImgValue;
	}
	
	
	
}
